<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_30f7dfab361537ef1760b61fff2c0818'] = 'Selecciona la tienda';
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_dd7bf230fde8d4836917806aff6a6b27'] = 'Dirección';
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_bcc254b55c4a1babdf1dcb82c207506b'] = 'Teléfono';
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_711eec5b0df657aa2b427fd45447a196'] = 'El horario de recojo es entre las 10am y 6pm';
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_0903670c0ff5f429a46441ef234a68d0'] = '(Selecciona la tienda)';
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_ee41a96b97b5c445402e63b0362c658a'] = 'La tienda más cercana';
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_ebd5d26bf16fb9035368db05f1a7f736'] = 'Ingrese una dirección';
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_1ddf7e2d1f057257fb5d17a43d598e06'] = 'Encontrar tienda';
$_MODULE['<{fspickupatstorecarrier}prestashop>carrier_extra_content_17_e0aa021e21dddbd6d8cecec71e9cf564'] = 'Enviar';
$_MODULE['<{fspickupatstorecarrier}prestashop>email_date_time_17_2bac1677997b7ba145c5429a8b880d8d'] = 'Seleccione fecha';
$_MODULE['<{fspickupatstorecarrier}prestashop>email_date_time_17_816f89f199eea34bf805360640de9a06'] = 'Seleccione horario';
