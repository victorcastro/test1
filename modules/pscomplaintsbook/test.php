<script src="http://www.actualidadjquery.es/ejemplos/js/jquery-1.4.2.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function(){

	var name = $('#name').val();
	var direction = $('#direction').val();
	var dni = $('#DNI').val();
	var phone = $('#PHONE').val();
	var email = $('#EMAIL').val();
	var representante = $('#REPRESENTANTE').val();
	var contrato_tipo = $('#CONTRATO_TIPO').val();
	var contrato_tipo = $('#CONTRATO_TIPO').val();
	var contrato_titulo = $('#CONTRATO_TITULO').val();
	var reclamo_tipo = $('#RECLAMO_TIPO').val();
	var reclamo_tipo = $('#RECLAMO_TIPO').val();
	var reclamo_detalle = $('#RECLAMO_DETALLE').val();
	var proveedor_detalles = $('#PROVEEDOR_DETALLES').val();
	var submit_complaintsrecord = $('#SUBMIT_COMPLAINTSRECORD').val();



	$("#form_complaintsbook").submit(function () {
		if($('#name').val().length < 1){ alert("El campo name es obligatorio"); return false;}
		if($('#direction').val().length < 1){ alert("El campo direction es obligatorio"); return false;}
		if(dni.length < 1){ alert("El campo dni es obligatorio"); return false;}
		if(phone.length < 1){ alert("El campo phone es obligatorio"); return false;}
		if(email.length < 1){ alert("El campo email es obligatorio"); return false;}
		if(representante.length < 1){ alert("El campo representante es obligatorio"); return false;}
		if(contrato_titulo.length < 1){ alert("El campo contrato_titulo es obligatorio"); return false;}
		if(reclamo_detalle.length < 1){ alert("El campo reclamo_detalle es obligatorio"); return false;}
		if(proveedor_detalles.length < 1){ alert("El campo proveedor_detalles es obligatorio"); return false;}
		return false;
	});


});

</script>

<form form="{$link->getModuleLink('pscomplaintsbook', 'complaintrecord')}" method="post" id="form_complaintsbook" class="form_complaintsbook">

<div id="complaintsbook-wrapper" class="container_9 grid_5">
	<div id="wrapper-date" class="grid_3 omega">
		<div class="title grid_3 center">{l s='COMPLAINTS BOOK' mod='pscomplaintsbook'} </div>

			<div class="grid_1 alpha omega"><span>FECHA:</span></div>
			<div class="grid_2 alpha omega"> {$date} {$month} {$year}</div>

	</div>
	<div id="wrapper-number" class="grid__2 alpha omega center">
		{l s='NUMBER CLAIM' mod='pscomplaintsbook'} <br>
		[Nº 00000000-{$year}]
	</div>
	<div id="datos-comercio" class="grid_5 omega">
		<div class="grid_5 alpha omega"><span class="company-name">{$company_name} </span></div>
		<div class="grid_5 alpha omega"><span class="company-ruc"> {l s='RUC :' mod='pscomplaintsbook'} {$company_ruc} </span></div>
		<div class="grid_5 alpha omega"><span class="company-direction">{l s='ADDRESS :' mod='pscomplaintsbook'} {$company_address}</span> </div>
	</div>

	<div class="grid_5 alpha omega title"><span>{l s='1. CONSUMER CLAIMANT IDENTIFICATION' mod='pscomplaintsbook'} </span></div>
	<div class="grid_5 alpha omega "><span> {l s='Name and Surname:' mod='pscomplaintsbook'} <input type="text" name="NAME" id="name"  size="76" ></span></div>
	<div class="grid_5 alpha omega "><span> {l s='Address: ' mod='pscomplaintsbook'} <input type="text" name="DIRECTION" id="direction" size="87" ></span></div>
	<div class="grid_2 alpha omega "><span> {l s='DNI/RUC: ' mod='pscomplaintsbook'} <input type="text" name="DNI" id="DNI" size="15" > </span></div>
	<div class="grid__3 alpha omega ">
		<span>
			{l s='Phone: ' mod='pscomplaintsbook'} <input type="tel" name="PHONE" id="PHONE" size="12"  > &nbsp;&nbsp;&nbsp;&nbsp;
			{l s='Email: ' mod='pscomplaintsbook'}<input type="email" name="EMAIL" id="EMAIL" size="22" >
		</span>
	</div>
	<div class="grid_5 alpha omega "><span> {l s='Parent or Guardian (for minors): ' mod='pscomplaintsbook'} <input type="text" name="REPRESENTANTE" id="REPRESENTANTE" size="50" ></span></div>
	<div class="grid_5 alpha omega title"><span>{l s='2. IDENTIFICATION OF THE HIRED' mod='pscomplaintsbook'} </span></div>
	<div class="grid_2 alpha omega">
		<span style="padding-left:10px;display:table">
			<div class="grid_1 alpha omega"><input type="radio" name="CONTRATO_TIPO" id="CONTRATO_TIPO" value="producto"> {l s='Product' mod='pscomplaintsbook'} </div>
			<div class="grid_1 alpha omega"><input type="radio" name="CONTRATO_TIPO" id="CONTRATO_TIPO" value="servicio"> {l s='Service' mod='pscomplaintsbook'} </div>
		</span>
	</div>
	<div class="grid_3 alpha omega" style="width:333px">
		<div>
			<span >Titulo :
				<input type="text" name="CONTRATO_TITULO" id="CONTRATO_TITULO" size="50">
			</span>
		</div>
	</div>
	<div class="grid_5 alpha omega title"><span>{l s='3. IDENTIFICATION OF CLAIM' mod='pscomplaintsbook'}</span></div>
		<div class="grid_5 alpha omega">
			<span >
				<input type="radio" name="RECLAMO_TIPO" id="RECLAMO_TIPO" value="reclamo"> {l s='Claim (Disagreement related to products or services)' mod='pscomplaintsbook'}
			</span>
		</div>
		<div class="grid_5 alpha omega">
			<span style="display:table">
				<input type="radio" name="RECLAMO_TIPO" id="RECLAMO_TIPO" value="queja"> {l s='Complaint (Disagreement unrelated to the goods or services, or discomfort or dissatisfaction with the customer service)' mod='pscomplaintsbook'}
			</span>
		</div>
		<div class="grid_5 alpha omega">
			<span>
				<textarea name="RECLAMO_DETALLE" id="RECLAMO_DETALLE" cols="98" rows="10"></textarea>
			</span>
		</div>

	<div class="grid_5 alpha omega title"><span>{l s='4. ACTIONS TAKEN BY THE PROVIDER' mod='pscomplaintsbook'} </span></div>
		<div class="grid_5 alpha omega">
			<span>
				<textarea name="PROVEEDOR_DETALLES" id="PROVEEDOR_DETALLES" cols="98" rows="10"></textarea>
			</span>
		</div>
</div>

 <input  type="submit" class="enviar_reclamacion" name="SUBMIT_COMPLAINTSRECORD" id="SUBMIT_COMPLAINTSRECORD" value="Enviar">

</form>