<?php
if (!defined('_PS_VERSION_'))
  exit;

class PsComplaintsBook extends Module
{

  public function __construct()
  {
    $this->name = 'pscomplaintsbook'; // nombre único asignado como identificador del modulo.
    $this->tab = 'others';    // pestaña a la que pertenece el modulo.
    $this->version = '1.1';   // Version actual del modulo.
    $this->author = 'Victor Castro'; // Autor del modulo.
    $this->need_instance = 1; // si el valor es 1 mostrará una advertencia en la seccion de modules ($this->warning).
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->module_key = "645801d7cb76992568bffe505c85d4f7";

    parent::__construct();

    $this->displayName = $this->l('Complaints Book'); // Nombre visible en el listado de modulos.
    $this->description = $this->l('Document by which a consumer may state a complaint about a product or service you purchased.'); // Descripción visible en el listado de modulos.
    $this->error = false;

    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?'); // Texto al desinstalar modulos.

    if (!Configuration::get('COMPANY_NAME'))
      $this->warning = $this->l('Insert your data of company'); // Advertencia mostrado cuando se activa  $this->need_instance.
  }


  private function init()
  {  // Se llama la funcion al momento de instalar

    $company_name = strval(Configuration::get('PS_SHOP_NAME'));
    $company_address = strval(Configuration::get('PS_SHOP_ADDR1'));
    $company_email = strval(Configuration::get('PS_SHOP_EMAIL'));

    Configuration::updateValue('COMPANY_NAME', $company_name);
    Configuration::updateValue('COMPANY_ADDRESS', $company_address);
    Configuration::updateValue('COMPANY_EMAIL', $company_email);
  }


  public function install()
  {
    if (Shop::isFeatureActive())
      Shop::setContext(Shop::CONTEXT_ALL);

    $this->init();

    return parent::install() && $this->registerHook('leftColumn') && $this->registerHook('header') && $this->registerHook('footer');
  }


  public function uninstall()
  {

    if (!parent::uninstall() || !Configuration::deleteByName('COMPANY_NAME') || !Configuration::deleteByName('COMPANY_RUC') || !Configuration::deleteByName('COMPANY_ADDRESS') || !Configuration::deleteByName('COMPANY_EMAIL'))
      return false;
    return true;
  }


  public function hookLeftColumn($params)
  {
    $this->context->smarty->assign(
        array(
            'company_name' => Configuration::get('COMPANY_NAME'),
            'company_ruc' => Configuration::get('COMPANY_RUC'),
            'company_address' => Configuration::get('COMPANY_ADDRESS'),
            'company_email' => Configuration::get('COMPANY_EMAIL'),
            'link_complaintrecord' => Context::getContext()->link->getModuleLink('pscomplaintsbook', 'complaintrecord'),
            'url_image' => $this->_path.'views/img/complaintsbook.jpg'
        )
    );

    $this->capturaDatos();

    return $this->display(__FILE__, 'pscomplaintsbook.tpl');

  }


  public function hookRightColumn($params)
  {
    return $this->hookLeftColumn($params);

  }


  public function hookDisplayHeader()
  {
    //$this->context->controller->addCSS($this->_path.'views/css/pscomplaintsbook.css', 'all');
    $this->context->controller->addJS($this->_path.'views/js/validation_complaintrecord.js', 'all');

  }

  public function hookFooter($params)
    {
        $this->smarty->assign(array(
            'path_complaints' => $this->_path,
            'link_complaintrecord' => Context::getContext()->link->getModuleLink('pscomplaintsbook', 'complaintrecord'),
        ));

        return $this->display(__FILE__, 'footer.tpl');
    }


  public function displayForm()
  {
      // Get default Language
      $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

      // Init Fields form array
      $fields_form[0]['form'] = array(

          'legend' => array(
              'title' => $this->l('Configurations'),
              'image' => _MODULE_DIR_.$this->name.'/logo.gif'
          ),

          'input' => array(
              array(
                  'type' => 'text',
                  'label' => $this->l('Name of Company'),
                  'name' => 'COMPANY_NAME',
                  'size' => 20,
                  'required' => true
              ),
              array(
                  'type' => 'text',
                  'label' => $this->l('RUC'),
                  'name' => 'COMPANY_RUC',
                  'size' => 20,
                  'required' => true
              ),
              array(
                  'type' => 'text',
                  'label' => $this->l('Addrress of Company'),
                  'name' => 'COMPANY_ADDRESS',
                  'size' => 80,
                  'required' => true,
              ),
              array(
                  'type' => 'text',
                  'label' => $this->l('Email to send reclamation'),
                  'name' => 'COMPANY_EMAIL',
                  'size' => 80,
                  'required' => true,
              )
          ),
          'submit' => array(
              'title' => $this->l('Save'),
              'class' => 'button'
          )
      );


      $helper = new HelperForm();

      // Module, token and currentIndex
      $helper->module = $this;
      $helper->name_controller = $this->name;
      $helper->token = Tools::getAdminTokenLite('AdminModules');
      $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

      // Language
      $helper->default_form_language = $default_lang;
      $helper->allow_employee_form_lang = $default_lang;

      // Title and toolbar
      $helper->title = $this->displayName;
      $helper->show_toolbar = true;        // false -> remove toolbar
      $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
      $helper->submit_action = 'submit'.$this->name;
      $helper->toolbar_btn = array(
          'save' =>
          array(
              'desc' => $this->l('Save'),
              'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
              '&token='.Tools::getAdminTokenLite('AdminModules'),
          ),
          'back' => array(
              'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
              'desc' => $this->l('Back to list')
          )
      );

      // Load current value
      $helper->fields_value['COMPANY_NAME'] = Configuration::get('COMPANY_NAME');
      $helper->fields_value['COMPANY_RUC'] = Configuration::get('COMPANY_RUC');
      $helper->fields_value['COMPANY_ADDRESS'] = Configuration::get('COMPANY_ADDRESS');
      $helper->fields_value['COMPANY_EMAIL'] = Configuration::get('PS_SHOP_EMAIL');
        
      

      $helper->token_adminstores = Tools::getAdminTokenLite('AdminStores');

      //$token_adminstores= $helper->token_adminstores." _PS_ADMIN_DIR_ = "._PS_ADMIN_DIR_." _PS_ROOT_DIR_ = "._PS_ROOT_DIR_." context->shop->domain = ".$context->shop->domain;

      return $helper->generateForm($fields_form);
  }


  public function getContent()
  {
      $output = null;

      if (Tools::isSubmit('submit'.$this->name))
      {
          $company_name = strval(Tools::getValue('COMPANY_NAME'));
          $company_ruc = strval(Tools::getValue('COMPANY_RUC'));
          $company_address = strval(Tools::getValue('COMPANY_ADDRESS'));
          $company_email = strval(Tools::getValue('COMPANY_EMAIL'));

          if (!$company_name  || empty($company_name) || !Validate::isGenericName($company_name))
              $output .= $this->displayError( $this->l('Invalid Configuration value') );
          else
          {
              Configuration::updateValue('COMPANY_NAME', $company_name);
              Configuration::updateValue('COMPANY_RUC', $company_ruc);
              Configuration::updateValue('COMPANY_ADDRESS', $company_address);
              Configuration::updateValue('COMPANY_EMAIL', $company_email);
              $output .= $this->displayConfirmation($this->l('Settings updated'));
          }
      }
      return $output.$this->displayForm();
  }


  public function capturaDatos()
  {
    if (Tools::isSubmit('SUBMIT_COMPLAINTSRECORD'))
    {
      $form_name = $_POST['NAME'];
      $form_direction = $_POST['DIRECTION'];
      $form_dni = $_POST['DNI'];
      $form_phone = $_POST['PHONE'];
      $form_email = $_POST['EMAIL'];

      $form_representante = $_POST['REPRESENTANTE'];
      $form_contrato_tipo = $_POST['CONTRATO_TIPO'];
      $form_contrato_titulo = $_POST['CONTRATO_TITULO'];
      $form_reclamo_tipo = $_POST['RECLAMO_TIPO'];
      $form_reclamo_detalle = $_POST['RECLAMO_DETALLE'];
      $form_proveedor_detalles = $_POST['PROVEEDOR_DETALLES'];

      // Enviar por email los datos capturados
      $this->enviarEmail($form_name, $form_direction, $form_dni, $form_phone, $form_email, $form_representante, $form_contrato_tipo, $form_contrato_titulo, $form_reclamo_tipo, $form_reclamo_detalle, $form_proveedor_detalles);

      $complaintrecord_thanks = Context::getContext()->link->getModuleLink('pscomplaintsbook', 'complaintrecordthanks');
      header("Location: $complaintrecord_thanks");

    }
  }


  public function enviarEmail($form_name, $form_direction, $form_dni, $form_phone, $form_email, $form_representante, $form_contrato_tipo, $form_contrato_titulo, $form_reclamo_tipo, $form_reclamo_detalle, $form_proveedor_detalles)
  {

      $cliente_name = $form_name;
      $cliente_direction = $form_direction;
      $cliente_dni = $form_dni;
      $cliente_phone = $form_phone;
      $cliente_email = $form_email;
      $cliente_representante = $form_representante;
      $cliente_contrato_tipo = $form_contrato_tipo;
      $cliente_contrato_titulo = $form_contrato_titulo;
      $cliente_reclamo_tipo = $form_reclamo_tipo;
      $cliente_reclamo_detalle = $form_reclamo_detalle;
      $cliente_proveedor_detalles = $form_proveedor_detalles;

      $today = getdate();

      $id_lang = (int)Context::getContext()->language->id;
      $iso = Language::getIsoById($id_lang);
      $template = "complaintsbook_customer"; // Nombre del mail en html y txt, tiene que estar ubicado en /modules/mymodule/mails/es|en/complaintsbook_customer.html
    //$subject = Mail::l('Complaints book', $this->context->language->id);
      $subject = "LIBRO DE RECLAMACIONES";
      $template_vars = array(
                  '{cliente_name}' => $cliente_name,
                  '{cliente_direction}' => $cliente_direction,
                  '{cliente_dni}' => $cliente_dni,
                  '{cliente_phone}' => $cliente_phone,
                  '{cliente_email}' => $cliente_email,
                  '{cliente_representante}' => $cliente_representante,
                  '{cliente_contrato_tipo}' => $cliente_contrato_tipo,
                  '{cliente_contrato_titulo}' => $cliente_contrato_titulo,
                  '{cliente_reclamo_tipo}' => $cliente_reclamo_tipo,
                  '{cliente_reclamo_detalle}' => $cliente_reclamo_detalle,
                  '{cliente_proveedor_detalles}' => $cliente_proveedor_detalles,
                  '{fecha}' => $today['mon'].'/'.$today['mday'].'/'.$today['year'],
                  );

      $to  = $cliente_email;
      $to_name = $cliente_name;
      $template_path = dirname(__FILE__).'/mails/';
      $id_shop = $this->context->shop->id;

      $to2 = Configuration::get('COMPANY_EMAIL');
      $template2 = "pscomplaintsbook_administrator";
      $subject2 = "LIBRO DE RECLAMACIONES";

      // Si existe el ID de la Tienda (multi-tiendas) mostrará el ID caso contrario saldrá null


      Mail::send($id_lang, $template, $subject, $template_vars, $to, $to_name, null , null      , null            , null      , $template_path, null, $id_shop);
      Mail::send($id_lang, $template,$subject2, $template_vars, $to2, $to_name,null , null      , null            , null      , $template_path, null, $id_shop);
  //  Mail::send($id_lang, $template, $subject, $template_vars, $to, $to_name, $from, $from_name, $file_attachment, $mode_smtp, $template_path, $die, $id_shop);
  }


}
