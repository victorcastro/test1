<?php

/**
 * @since 1.5.0
 */

class pscomplaintsbookcomplaintrecordModuleFrontController extends ModuleFrontController
{

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
		$this->setTemplate('complaintrecord.tpl');
		$today = getdate();

		$this->context->smarty->assign(
			array(
				'hour' => $today['hours'],
	            'minutes' => $today['minutes'],
	            'seconds' => $today['seconds'],
	            'day'=>$today['weekday'],
	            'month' =>$today['mon'],
	            'date' => $today['mday'],
	            'year' => $today['year']
			)
        );

	}





}
