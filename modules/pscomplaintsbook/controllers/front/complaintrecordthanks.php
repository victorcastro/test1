<?php

/**
 * @since 1.5.0
 */

class pscomplaintsbookcomplaintrecordthanksModuleFrontController extends ModuleFrontController
{

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
		$this->setTemplate('complaintrecord_thanks.tpl');
	}



}
