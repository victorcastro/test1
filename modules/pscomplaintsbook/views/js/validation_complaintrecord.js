
$(document).ready(function(){

	$("#form_complaintsbook").submit(function () {
		if($('#name').val().length < 1){ alert("El campo NOMBRE es obligatorio"); return false;}
		if($('#direction').val().length < 1){ alert("El campo DIRECCION es obligatorio"); return false;}
		if($('#DNI').val().length < 1){ alert("El campo DNI es obligatorio"); return false;}
		if($('#PHONE').val().length < 1){ alert("El campo TELEFONO es obligatorio"); return false;}
		if($('#EMAIL').val().length < 1){ alert("El campo EMAIL es obligatorio"); return false;}
		if($('#CONTRATO_TITULO').val().length < 1){ alert("El campo TITULO DE CONTRATO es obligatorio"); return false;}
		if($('#RECLAMO_DETALLE').val().length < 1){ alert("El campo DETALLE DE RECLAMO es obligatorio"); return false;}
		if($('#PROVEEDOR_DETALLES').val().length < 1){ alert("El campo DETALLE DE MEDIDAS DEL PROVEEDOR es obligatorio"); return false;}
		return true;
	});


});