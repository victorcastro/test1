<!-- Block pscomplaintsbook -->
<div id="pscomplaintsbook_block_left" class="block">
  <h4>{l s='Complaints book' mod='pscomplaintsbook'}</h4>
  <div class="block_content">
      <a href="{$link_complaintrecord}" title="{l s='Record of Complaint' mod='pscomplaintsbook'}"><img src="{$url_image}" width="180"></a>
    <p style="display:none">
      {l s='As provided in the Code and Consumer Protection This establishment has a Complaints Book at your disposal. Request it to register complaints or claims you have.' mod='pscomplaintsbook'}
    </p>

    <ul>
      <li><a href="{$link_complaintrecord}"  alt="{l s='Record of Complaint' mod='pscomplaintsbook'}">{l s='» Record of Complaint' mod='pscomplaintsbook'}</a></li>
    </ul>
  </div>
</div>
<!-- /Block pscomplaintsbook -->