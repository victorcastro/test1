<form form="{$link->getModuleLink('pscomplaintsbook', 'complaintrecord')}" method="post" id="form_complaintsbook">

<div id="complaintsbook-wrapper" class="col-sm-12 box">
	<!--
	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-4">
				<span>FECHA:</span>
				<span> {$date}/{if ($month != "10") OR ($month != "11") OR ($month != "12")}0{$month}{else}{$month}{/if}/{$year}</span>
			</div>
			<div id="wrapper-number" class="col-sm-4">
				 {l s='NUMBER CLAIM' mod='pscomplaintsbook'} <br> 
				{l s='RUC DE LA EMPRESA' mod='pscomplaintbook'} <br>
				{$company_ruc}
			</div>
			<div id="datos-comercio" class="col-sm-4">
				<span class="company-name">{$company_name} </span>
				<div class="grid_5 alpha omega"><span class="company-ruc"> {l s='RUC :' mod='pscomplaintsbook'} {$company_ruc} </span></div> 
				<span class="company-direction">{l s='ADDRESS :' mod='pscomplaintsbook'} {$company_address}</span> 
			</div>
		</div>
	</div>
	-->

	<div class="row">
		<div class="col-sm-12">
			<h3 class="page-subheading">{l s='1. CONSUMER CLAIMANT IDENTIFICATION' mod='pscomplaintsbook'} </h3>
		
			<div class="col-sm-4"> {l s='Name and Surname:' mod='pscomplaintsbook'} </div><input type="text" name="NAME" id="name"  class="col-sm-8" >
			<div class="col-sm-4"> {l s='Address: ' mod='pscomplaintsbook'} </div><input type="text" name="DIRECTION" id="direction" class="col-sm-8" >
			<div class="col-sm-4"> {l s='DNI/RUC: ' mod='pscomplaintsbook'}  </div><input type="text" name="DNI" id="DNI" class="col-sm-8" >
			<div class="col-sm-4">{l s='Phone: ' mod='pscomplaintsbook'}</div> <input type="tel" name="PHONE" id="PHONE" class="col-sm-8" >
			<div class="col-sm-4"> {l s='Email: ' mod='pscomplaintsbook'}</div><input type="email" name="EMAIL" id="EMAIL" class="col-sm-8">
			<div class="col-sm-4"> {l s='Parent or Guardian (for minors): ' mod='pscomplaintsbook'}</div> <input type="text" name="REPRESENTANTE" id="REPRESENTANTE" class="col-sm-8" >
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<h3 class="page-subheading">{l s='2. IDENTIFICATION OF THE HIRED' mod='pscomplaintsbook'} </h3>
			<div class="col-sm-4">
					<div class="col-sm-12"><input type="radio" name="CONTRATO_TIPO" id="CONTRATO_TIPO" value="producto"> {l s='Product' mod='pscomplaintsbook'} </div>
					<div class="col-sm-12"><input type="radio" name="CONTRATO_TIPO" id="CONTRATO_TIPO" value="servicio"> {l s='Service' mod='pscomplaintsbook'} </div>
			</div>
			<div class="col-sm-8" >
				<div>
					<span >Titulo :
						<input type="text" name="CONTRATO_TITULO" id="CONTRATO_TITULO" size="50">
					</span>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-sm-12">
				<h3 class="page-subheading">{l s='3. IDENTIFICATION OF CLAIM' mod='pscomplaintsbook'}</h3>
			
				<div class="grid_5 alpha omega">
					<span >
						<input type="radio" name="RECLAMO_TIPO" id="RECLAMO_TIPO" value="reclamo"> {l s='Claim (Disagreement related to products or services)' mod='pscomplaintsbook'}
					</span>
				</div>
				<div class="grid_5 alpha omega">
					<span style="display:table">
						<input type="radio" name="RECLAMO_TIPO" id="RECLAMO_TIPO" value="queja"> {l s='Complaint (Disagreement unrelated to the goods or services, or discomfort or dissatisfaction with the customer service)' mod='pscomplaintsbook'}
					</span>
				</div>
				<div class="grid_5 alpha omega">
					<span>
						<textarea name="RECLAMO_DETALLE" id="RECLAMO_DETALLE" cols="98" rows="10"></textarea>
					</span>
				</div>
		</div>
	</div>


	<div class="row">
		<div class="col-sm-12">
				<h3 class="page-subheading">{l s='4. ACTIONS TAKEN BY THE PROVIDER' mod='pscomplaintsbook'} </h3>
				<textarea name="PROVEEDOR_DETALLES" id="PROVEEDOR_DETALLES" cols="98" rows="10"></textarea>
			
		</div>
	</div>
</div>

 <button  type="submit" class="btn btn-default button button-medium" name="SUBMIT_COMPLAINTSRECORD" id="SUBMIT_COMPLAINTSRECORD" value="Enviar"><span>Enviar<i class="icon-chevron-right right"></i></span></button>



</form>