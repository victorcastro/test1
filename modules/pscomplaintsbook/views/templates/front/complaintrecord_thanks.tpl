<div class="success"> <img src="{$img_ps_dir}admin/icon-valid.png"> Se ha registrado su Reclamo con éxito</div>
<br><br>
<h4 style="text-align:center"> Considerado en el artículo 150º de la Ley Nº 29571 del Código de Protección y Defensa del Consumidor hemos procedido a gestionar su queja o reclamo presentado. </h4>
<br><br>
<p style="text-align:justify">El reclamo registrado en el "Libro de Reclamaciones Virtual" determina la obligación del proveedor de cumplir con atenderlo y darle respuesta en un plazo no mayor a treinta (30) días calendario. Dicho plazo puede  ser extendido por otro igual cuando la naturaleza del reclamo lo justifique, situación que es puesta en  conocimiento ante la culminación del plazo inicial, de conformidad con lo establecido en el artículo 24 de  la Ley Nº 29571, Código de Protección y Defensa del Consumidor; o norma que la modifique o sustituya.
No puede condicionarse la atención de reclamos de consumidores o usuarios al pago previo del producto o servicio materia de dicho reclamo o del monto que hubiera motivado ello, o de cualquier otro pago.
</p>
