<!--
*
* 2007-2015 PrestaShop
* Módulos de Pagos Alignet para Prestashop
* @author Alignet S.A.C.
* @copyright  2015 Alignet
*
-->
<script type="text/javascript">
{literal}
$(document).ready(function() {
	$('.ajax_cart_block_remove_link').click(function() {
		updateCarrierSelectionAndGift(); 
	});
	$('.cart_quantity_button').click(function() {  
		updateCarrierSelectionAndGift();  
	});     
	$('.cart_quantity_delete').click(function() {    
		updateCarrierSelectionAndGift();     
	});     
	$('#cgv').click(function() {      
		updateCarrierSelectionAndGift();   
	});
	$('#submitAccount').click(function() {
		updateCarrierSelectionAndGift();
	});
	$('#SubmitLogin').click(function() {
		updateCarrierSelectionAndGift();
	});
	$('#submitAddDiscount').click(function() {
		updateCarrierSelectionAndGift();
	});
	$('#send').click(function(){
		$('#alignet_form').submit();
	});
});
{/literal}
</script>
<div class="row">
	<div class="col-xs-12 col-md-6">
        <p class="payment_module">
            <a title="{l s='Pagar con Pay-me' mod='payme'}" id='send' style='cursor: pointer;'>
                <img src="{$module_dir}img/logo_alignet.png" alt="{l s='Pagar con Pay-me' mod='payme'}" />
                {l s='Pagar con Pay-me' mod='payme'}
            </a>
        </p>
    </div>
</div>
<form action="{$urlvpos}" method="post" id="alignet_form" class="hidden"> <!--class="hidden"-->
	<input type="hidden" name="acquirerId" id="acquirerId" value="{$acquirerId}" />
	<input type="hidden" name="idCommerce" id="idCommerce" value="{$idCommerce}" />
	<input type="hidden" name="purchaseOperationNumber" id="purchaseOperationNumber" value="{$purchaseOperationNumber}" />
	<input type="hidden" name="purchaseAmount" id="purchaseAmount" value="{$purchaseAmount}" />
	<input type="hidden" name="purchaseCurrencyCode" id="purchaseCurrencyCode" value="{$purchaseCurrencyCode}" />
	<input type="hidden" name="language" id="language" value="{$language}" />
	<input type="hidden" name="billingFirstName" id="billingFirstName" value="{$billingFirstName}" />
	<input type="hidden" name="billingLastName" id="billingLastName" value="{$billingLastName}" />
	<input type="hidden" name="billingEmail" id="billingEmail" value="{$billingEmail}" />
	<input type="hidden" name="billingAddress" id="billingAddress" value="{$billingAddress}" />
	<input type="hidden" name="billingZip" id="billingZip" value="{$billingZip}" />
	<input type="hidden" name="billingCity" id="billingCity" value="{$billingCity}" />
	<input type="hidden" name="billingState" id="billingState" value="{$billingState}" />
	<input type="hidden" name="billingCountry" id="billingCountry" value="{$billingCountry}" />
	<input type="hidden" name="billingPhone" id="billingPhone" value="{$billingPhone}" />
	<input type="hidden" name="shippingFirstName" id="shippingFirstName" value="{$shippingFirstName}" />
	<input type="hidden" name="shippingLastName" id="shippingLastName" value="{$shippingLastName}" />
	<input type="hidden" name="shippingEmail" id="shippingEmail" value="{$shippingEmail}" />
	<input type="hidden" name="shippingAddress" id="shippingAddress" value="{$shippingAddress}" />
	<input type="hidden" name="shippingZIP" id="shippingZIP" value="{$shippingZIP}" />
	<input type="hidden" name="shippingCity" id="shippingCity" value="{$shippingCity}" />
	<input type="hidden" name="shippingState" id="shippingState" value="{$shippingState}" />
	<input type="hidden" name="shippingCountry" id="shippingCountry" value="{$shippingCountry}" />
	<input type="hidden" name="mcc" id="mcc" value="{$mcc}" />
	<input type="hidden" name="commerceAssociated" id="commerceAssociated" value="{$commerceAssociated}" />
	<input type="hidden" name="descriptionProducts" id="descriptionProducts" value="{$descriptionProducts}" />
	<input type="hidden" name="programmingLanguage" id="programmingLanguage" value="{$programmingLanguage}" />
	<input type="hidden" name="purchaseVerification" id="purchaseVerification" value="{$purchaseVerification}" />
</form>