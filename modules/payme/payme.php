<?php

/**
*
*  @author    ALIGNET
*  @copyright 2014 ALIGNET
*/

if (!defined('_CAN_LOAD_FILES_'))
	exit;

@ini_set('display_errors', 'off');

class Payme extends PaymentModule
{
	private	$_html = '';
	private $_postErrores = array();
  
	public function __construct(){
		$this->name = 'payme';
		$this->tab = 'payments_gateways';
		$this->version = '2.0.1';
		$this->module_key = '2242729b0888a1184def9c91a426d357';

		parent::__construct();
		$this->page = basename(__FILE__, '.php');
		$this->author = "ALIGNET";
		$this->displayName = $this->l('Pay-me');
		$this->description = $this->l('La forma segura de pagar en línea.');

		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.4', 'max' => _PS_VERSION_);
		// $this->ps_versions_compliancy = array('min' => '1.4', 'max' => '1.6');

		$this->confirmUninstall = $this->l('¿Seguro de desintalar el módulo?');

		$config = Configuration::getMultiple(array('ALIGNET_URLTPV', 'ALIGNET_IDACQUIRER', 'ALIGNET_IDCOMMERCE', 'ALIGNET_KEY', 'ALIGNET_MCC', 'ALIGNET_DEBUG', 'ALIGNET_IDENTCOMMERCE', 'ALIGNET_KEYWALLET'));

		$this->env = $config['ALIGNET_URLTPV'];
		switch($this->env){
			case 0:
				$this->urltpv = "https://integracion.alignetsac.com/VPOS2/faces/pages/startPayme.xhtml";
				break;
			case 1:
				$this->urltpv = "https://vpayment.verifika.com/VPOS2/faces/pages/startPayme.xhtml";
				break;
		}

		if (isset($config['ALIGNET_IDACQUIRER']))
			$this->idacquirer = $config['ALIGNET_IDACQUIRER'];
		if (isset($config['ALIGNET_IDCOMMERCE']))
			$this->idcommerce = $config['ALIGNET_IDCOMMERCE'];
		if (isset($config['ALIGNET_KEY']))
			$this->key = $config['ALIGNET_KEY'];
		if (isset($config['ALIGNET_DEBUG']))
			$this->debug = $config['ALIGNET_DEBUG'];
		if (isset($config['ALIGNET_MCC']))
			$this->mcc = $config['ALIGNET_MCC'];
		if (isset($config['ALIGNET_IDENTCOMMERCE']))
			$this->identcommerce = $config['ALIGNET_IDENTCOMMERCE'];
		if (isset($config['ALIGNET_KEYWALLET']))
			$this->keywallet = $config['ALIGNET_KEYWALLET'];

		if (!isset($this->urltpv)
			OR !isset($this->idacquirer)
			OR !isset($this->idcommerce)
			OR !isset($this->key)
			OR !isset($this->mcc)
			OR !isset($this->identcommerce)
			OR !isset($this->keywallet))
			$this->warning = $this->l('Hay datos que faltan para establecer la configuración del módulo Alignet.');
	}

	public function install() {
		$this->_createStates();
		if (!parent::install() 
			OR !Configuration::updateValue('ALIGNET_URLTPV', '0')
			OR !Configuration::updateValue('ALIGNET_IDACQUIRER', '')
			OR !Configuration::updateValue('ALIGNET_IDCOMMERCE', '')
			OR !Configuration::updateValue('ALIGNET_KEY', '')
			OR !Configuration::updateValue('ALIGNET_MCC', '')
			OR !Configuration::updateValue('ALIGNET_IDENTCOMMERCE', '')
			OR !Configuration::updateValue('ALIGNET_KEYWALLET', '')
			OR !Configuration::updateValue('ALIGNET_DEBUG', '0')
			OR !$this->registerHook('payment')
			OR !$this->registerHook('paymentReturn'))
			return false;
		return true;
	}

	public function uninstall()	{
		if (!Configuration::deleteByName('ALIGNET_URLTPV')
			OR !Configuration::deleteByName('ALIGNET_IDACQUIRER')
			OR !Configuration::deleteByName('ALIGNET_IDCOMMERCE')
			OR !Configuration::deleteByName('ALIGNET_KEY')
			OR !Configuration::deleteByName('ALIGNET_MCC')
			OR !Configuration::deleteByName('ALIGNET_IDENTCOMMERCE')
			OR !Configuration::deleteByName('ALIGNET_KEYWALLET')
			OR !Configuration::deleteByName('ALIGNET_DEBUG')
			OR !Configuration::deleteByName('PAYME_OS_PAID')
			OR !Configuration::deleteByName('PAYME_OS_FAILED')
			OR !parent::uninstall())
			return false;
		return true;
	}

	private function _postValidacion() {
		if (isset($_POST['btnSubmit'])) {
			if (empty($_POST['ALIGNET_IDENTCOMMERCE'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Wallet" no puede estar vacio!');
			}
			if (!empty($_POST['ALIGNET_IDENTCOMMERCE']) && !is_numeric($_POST['ALIGNET_IDENTCOMMERCE'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Wallet" sólo permite números!');
			}
			if (empty($_POST['ALIGNET_KEYWALLET'])) {
				$this->_postErrores[] = $this->l('El campo requerido "Clave Wallet" no puede estar vacio!');
			}
			if (empty($_POST['ALIGNET_IDACQUIRER'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Adquirente" no puede estar vacio!');
			}
			if (!empty($_POST['ALIGNET_IDACQUIRER']) && !is_numeric($_POST['ALIGNET_IDACQUIRER'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Adquirente" sólo permite números!');
			}
			if (empty($_POST['ALIGNET_IDCOMMERCE'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Comercio" no puede estar vacio!');
			}
			if (!empty($_POST['ALIGNET_IDCOMMERCE']) && !is_numeric($_POST['ALIGNET_IDCOMMERCE'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Comercio" sólo permite números!');
			}
			if (empty($_POST['ALIGNET_MCC'])) {
				$this->_postErrores[] = $this->l('El campo requerido "MCC" no puede estar vacio!');
			}
			if (!empty($_POST['ALIGNET_MCC']) && !is_numeric($_POST['ALIGNET_MCC'])) {
				$this->_postErrores[] = $this->l('El campo requerido "MCC" sólo permite números!');
			}
			if (empty($_POST['ALIGNET_KEY'])) {
				$this->_postErrores[] = $this->l('El campo requerido "Clave V-POS2" no puede estar vacio!');
			}
		}
	}

	private function _postProceso()	{
		if (isset($_POST['btnSubmit'])){
			Configuration::updateValue('ALIGNET_URLTPV', $_POST['ALIGNET_URLTPV']);
			Configuration::updateValue('ALIGNET_IDACQUIRER', $_POST['ALIGNET_IDACQUIRER']);
			Configuration::updateValue('ALIGNET_IDCOMMERCE', $_POST['ALIGNET_IDCOMMERCE']);
			Configuration::updateValue('ALIGNET_KEY', $_POST['ALIGNET_KEY']);
			Configuration::updateValue('ALIGNET_KEYWALLET', $_POST['ALIGNET_KEYWALLET']);
			Configuration::updateValue('ALIGNET_IDENTCOMMERCE', $_POST['ALIGNET_IDENTCOMMERCE']);
			Configuration::updateValue('ALIGNET_MCC', $_POST['ALIGNET_MCC']);
			Configuration::updateValue('ALIGNET_DEBUG', $_POST['ALIGNET_DEBUG']);

			$this->env = $_POST['ALIGNET_URLTPV'];
			$this->idacquirer = $_POST['ALIGNET_IDACQUIRER'];
			$this->idcommerce = $_POST['ALIGNET_IDCOMMERCE'];
			$this->key = $_POST['ALIGNET_KEY'];
			$this->keywallet = $_POST['ALIGNET_KEYWALLET'];
			$this->mcc = $_POST['ALIGNET_MCC'];
			$this->identcommerce = $_POST['ALIGNET_IDENTCOMMERCE'];
			$this->debug = $_POST['ALIGNET_DEBUG'];

			if (_PS_VERSION_ >= '1.6') {
				$this->_html .= '<div class="bootstrap"><div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>' . $this->l('Configuración guardada correctamente.') . '</div></div>';
			} else {
				$this->_html .= '<p>' . $this->l('Configuración guardada correctamente.') . '</p>';
			}
		}
	}

	private function _mostrarFormulario()	{
		if ((Tools::getValue('ALIGNET_IDACQUIRER') == "") and (isset($this->idacquirer)))
			$idacquirer = $this->idacquirer;
		else
			$idacquirer = Tools::getValue('ALIGNET_IDACQUIRER');

		if ((Tools::getValue('ALIGNET_IDCOMMERCE') == "") and (isset($this->idcommerce)))
			$idcommerce = $this->idcommerce;
		else
			$idcommerce = Tools::getValue('ALIGNET_IDCOMMERCE');

		if ((Tools::getValue('ALIGNET_KEY') == "") and (isset($this->key)))
			$key = $this->key;
		else
			$key = Tools::getValue('ALIGNET_KEY');

		if ((Tools::getValue('ALIGNET_DEBUG') == "") and (isset($this->debug)))
			$debug = $this->debug;
		else
			$debug = Tools::getValue('ALIGNET_DEBUG');		

		if ((Tools::getValue('ALIGNET_URLTPV') == "") and (isset($this->env)))
			$ambiente = $this->env;
		else
			$ambiente = Tools::getValue('ALIGNET_URLTPV');

		if ((Tools::getValue('ALIGNET_KEYWALLET') == "") and (isset($this->keywallet)))
			$keywallet = $this->keywallet;
		else
			$keywallet = Tools::getValue('ALIGNET_KEYWALLET');

		if ((Tools::getValue('ALIGNET_MCC') == "") and (isset($this->mcc)))
			$mcc = $this->mcc;
		else
			$mcc = Tools::getValue('ALIGNET_MCC');

		if ((Tools::getValue('ALIGNET_IDENTCOMMERCE') == "") and (isset($this->identcommerce)))
			$identcommerce = $this->identcommerce;
		else
			$identcommerce = Tools::getValue('ALIGNET_IDENTCOMMERCE');

		$ambiente_prueba = ($ambiente == 0) ? ' selected="selected" ' : '';
		$ambiente_produccion = ($ambiente == 1) ? ' selected="selected" ' : '';

		if (_PS_VERSION_ >= '1.6') {
			$this->_html .= '
			<div id="content" class="bootstrap" style="margin-left: 0px; padding: 0px;">
				<div class="bootstrap">
					<div class="row">
						<div class="col-lg-12">
							<form autocomplete="off" enctype="multipart/form-data" action="'.$_SERVER['REQUEST_URI'].'" method="post" class="defaultForm form-horizontal AdminCustomers">
								<div class="panel" id="fieldset_0">
									<div class="form-wrapper">
										<h3><i class="icon-cog"></i> ' . $this->l('Configuración General') . '</h3>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('Ambiente') . ':</label>
											<div class="col-lg-4">
												<select name="ALIGNET_URLTPV">
													<option value="0"' . $ambiente_prueba . '>' . $this->l('Testing (Desarrollo)') . '</option>
													<option value="1"' . $ambiente_produccion . '>' . $this->l('Producción') . '</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('Activar Depuración') . ':</label>
											<div class="col-lg-9 ">
												<div class="radio t radio-inline" style="padding-left: 0;">
													<label><input name="ALIGNET_DEBUG" value="1" type="radio" ' . (Configuration::get('ALIGNET_DEBUG') == 1 ? 'checked="checked" ' : '') . '>' . $this->l('Activar') . '</label>
												</div>
												<div class="radio t radio-inline" style="padding-left: 0;">
													<label><input name="ALIGNET_DEBUG" value="0" type="radio" ' . (Configuration::get('ALIGNET_DEBUG') == 0 ? 'checked="checked" ' : '') . '>' . $this->l('Desactivar') . '</label>
												</div>
												<p class="help-block">' . $this->l('Activar esta funcionalidad para realizar pruebas. Cuando se activa, se muestran los valores enviados a la pasarela de pagos.') . '</p>
											</div>
										</div>
										<br>
										<h3><i class="icon-cog"></i> ' . $this->l('CONFIGURACIÓN WALLET') . '</h3>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('ID Wallet') . ':</label>
											<div class="col-lg-1">
												<input name="ALIGNET_IDENTCOMMERCE" id="ALIGNET_IDENTCOMMERCE" value="' . $identcommerce . '" class="" type="text">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('Clave Wallet') . ':</label>
											<div class="col-lg-4">
												<input name="ALIGNET_KEYWALLET" id="ALIGNET_KEYWALLET" value="' . $keywallet . '" class="" type="text">
											</div>
										</div>
										<br>
										<br>
										<h3><i class="icon-cog"></i> ' . $this->l('CONFIGURACIÓN V-POS2') . '</h3>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('ID Adquirente') . ':</label>
											<div class="col-lg-1">
												<input name="ALIGNET_IDACQUIRER" id="ALIGNET_IDACQUIRER" value="' . $idacquirer . '" class="" type="text">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('ID Comercio') . ':</label>
											<div class="col-lg-2">
												<input name="ALIGNET_IDCOMMERCE" id="ALIGNET_IDCOMMERCE" value="' . $idcommerce . '" class="" type="text">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('MCC') . ':</label>
											<div class="col-lg-1">
												<input name="ALIGNET_MCC" id="ALIGNET_MCC" value="' . $mcc . '" class="" type="text">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('Clave V-POS2') . ':</label>
											<div class="col-lg-4">
												<input name="ALIGNET_KEY" id="ALIGNET_KEY" value="' . $key . '" class="" type="text">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-3">' . $this->l('URL de Respuesta') . ':</label>
											<div class="col-lg-9">
												<p class="form-control-static">' . Tools::getShopDomain(true).__PS_BASE_URI__.'modules/payme/response.php' . '</p>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<button type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-default">
											<i class="process-icon-save"></i> ' . $this->l('Guardar') . '
										</button>
										<a onclick="window.history.back();" class="btn btn-default pull-right" href="index.php?controller=AdminModules&token=10a166001434c1f9047b0315186d3b33">
											<i class="process-icon-cancel"></i> ' . $this->l('Cancelar') . '
										</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>';
		} else {
			$this->_html .= '
			<table border="0" width="100%" cellpadding="0" cellspacing="4">
				<tr>
					<td width="100%" valign="top">
						<form autocomplete="off" enctype="multipart/form-data" action="'.$_SERVER['REQUEST_URI'].'" method="post">
							<fieldset>
								<legend>'.$this->l('Configuración General').'</legend>
								<table border="0" width="100%" cellpadding="0" cellspacing="4">
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('Ambiente') . ':</td>
										<td>
											<select style="width:170px" name="ALIGNET_URLTPV">
												<option value="0"' . $ambiente_prueba . '>' . $this->l('Testing (Desarrollo)') . '</option>
												<option value="1"' . $ambiente_produccion . '>' . $this->l('Producción') . '</option>
											</select>
										</td>
									</tr>
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('Activar Depuración') . ':</td>
										<td>
											<input name="ALIGNET_DEBUG" value="1" type="radio" ' . (Configuration::get('ALIGNET_DEBUG') == 1 ? 'checked="checked" ' : '') . '> ' . $this->l('Activar') . '
											<input name="ALIGNET_DEBUG" value="0" type="radio" ' . (Configuration::get('ALIGNET_DEBUG') == 0 ? 'checked="checked" ' : '') . '> ' . $this->l('Desactivar') . '
										</td>
									</tr>
									<tr>
										<td style="height: 25px; width: 25%;">&nbsp;</td>
										<td>
											<i>' . $this->l('Activar esta funcionalidad para realizar pruebas. Cuando se activa, se muestran los valores enviados a la pasarela de pagos.') . '</i>
										</td>
									</tr>
								</table>
							</fieldset>
							<br>
							<fieldset>
								<legend>'.$this->l('CONFIGURACIÓN WALLET').'</legend>
								<table border="0" width="100%" cellpadding="0" cellspacing="4">
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('ID Wallet') . ':</td>
										<td>
											<input name="ALIGNET_IDENTCOMMERCE" id="ALIGNET_IDENTCOMMERCE" value="' . $identcommerce . '" style="width:30px" type="text">
										</td>
									</tr>
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('Clave Wallet') . ':</td>
										<td>
											<input name="ALIGNET_KEYWALLET" id="ALIGNET_KEYWALLET" value="' . $keywallet . '" style="width:230px" type="text">
										</td>
									</tr>
								</table>
							</fieldset>
							<br>
							<fieldset>
								<legend>'.$this->l('CONFIGURACIÓN V-POS2').'</legend>
								<table border="0" width="100%" cellpadding="0" cellspacing="4">
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('ID Adquirente') . ':</td>
										<td>
											<input name="ALIGNET_IDACQUIRER" id="ALIGNET_IDACQUIRER" value="' . $idacquirer . '" style="width:30px" type="text">
										</td>
									</tr>
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('ID Comercio') . ':</td>
										<td>
											<input name="ALIGNET_IDCOMMERCE" id="ALIGNET_IDCOMMERCE" value="' . $idcommerce . '" style="width:90px" type="text">
										</td>
									</tr>
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('MCC') . ':</td>
										<td>
											<input name="ALIGNET_MCC" id="ALIGNET_MCC" value="' . $mcc . '" style="width:30px" type="text">
										</td>
									</tr>
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('Clave V-POS2') . ':</td>
										<td>
											<input name="ALIGNET_KEY" id="ALIGNET_KEY" value="' . $key . '" style="width:230px" type="text">
										</td>
									</tr>
									<tr>
										<td style="height: 25px; width: 25%;">' . $this->l('URL de Respuesta') . ':</td>
										<td>
											' . Tools::getShopDomain(true) . __PS_BASE_URI__ . 'modules/payme/response.php' . '
										</td>
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td colspan="2">
											<button type="submit" id="btnSubmit" name="btnSubmit">
												' . $this->l('Guardar') . '
											</button>
											<a onclick="window.history.back();" href="index.php?controller=AdminModules&token=10a166001434c1f9047b0315186d3b33">
												' . $this->l('Cancelar') . '
											</a>
										</td>
									</tr>
								</table>
							</fieldset>
						</form>
					</td>
				</tr>
			</table>
			';
		}
		$this->_html .= '
		<script>
			$(document).ready(function(){
				$("#main #content").removeClass("nobootstrap");
				$("#main #content").addClass("bootstrap");
			});
		</script>
		';
	}

	private function _createStates() {
		if (!Configuration::get('PAYME_OS_PAID'))
		{
			$order_state = new OrderState();
			$order_state->name = array();
			foreach (Language::getLanguages() as $language)
				$order_state->name[$language['id_lang']] = 'Pago aceptado';

			$order_state->send_email = false;
			$order_state->color = '#32CD32';
			$order_state->hidden = false;
			$order_state->delivery = false;
			$order_state->logable = false;
			$order_state->invoice = false;

			if ($order_state->add())
			{
				$source = dirname(__FILE__).'/logo.gif';
				$destination = dirname(__FILE__).'/../../img/os/'.(int)$order_state->id.'.gif';
				copy($source, $destination);
			}
			Configuration::updateValue('PAYME_OS_PAID', (int)$order_state->id);
		}

		if (!Configuration::get('PAYME_OS_FAILED'))
		{
			$order_state = new OrderState();
			$order_state->name = array();
			foreach (Language::getLanguages() as $language)
				$order_state->name[$language['id_lang']] = 'Pago fallido';

			$order_state->send_email = false;
			$order_state->color = '#DC143C';
			$order_state->hidden = false;
			$order_state->delivery = false;
			$order_state->logable = false;
			$order_state->invoice = false;

			if ($order_state->add())
			{
				$source = dirname(__FILE__).'/logo.gif';
				$destination = dirname(__FILE__).'/../../img/os/'.(int)$order_state->id.'.gif';
				copy($source, $destination);
			}
			Configuration::updateValue('PAYME_OS_FAILED', (int)$order_state->id);
		}

	}

	public function getContent() {
		if (!empty($_POST)) {
			$this->_postValidacion();
			if (!sizeof($this->_postErrores)) {
				$this->_postProceso();
			} else {
				if (_PS_VERSION_ >= '1.6') {
					$this->_html .= '<div class="bootstrap">';
					$this->_html .= '<div class="alert alert-danger">';
					$this->_html .= '<button data-dismiss="alert" class="close" type="button">×</button>';
				}
				$this->_html .= '<ol>';
				foreach ($this->_postErrores AS $err)
					$this->_html .= "<li>" . $err.'</li>';
				$this->_html .= '</ol>';
				if (_PS_VERSION_ >= '1.6') {
					$this->_html .= '</div>';
					$this->_html .= '</div>';
				}
			}
		}
		else
			$this->_html .= '<br />';	
		$this->_mostrarFormulario();
		return $this->_html;
	}

	public function hookPayment($params) {
		if (!$this->active)
			return;

		$module_dir = _PS_MODULE_DIR_ . $this->name . '/';
		$ALIGNET_IDACQUIRER = Configuration::get('ALIGNET_IDACQUIRER');

		if ($ALIGNET_IDACQUIRER == 29 || $ALIGNET_IDACQUIRER == 144 || $ALIGNET_IDACQUIRER == 84 || $ALIGNET_IDACQUIRER == 10)
			$payme_img = 'Forma_Pago_Pay-me_PST_PSP.PNG';
		elseif ($ALIGNET_IDACQUIRER == 123 || $ALIGNET_IDACQUIRER == 23 || $ALIGNET_IDACQUIRER == 205 || $ALIGNET_IDACQUIRER == 35)
			$payme_img = 'Forma_Pago_Pay-me_BIZLINKS_ALTERNATIVO.PNG';

		$this->context->smarty->assign(array(
			'module_dir' => $module_dir,
			'payme_img' => $payme_img
		));

		return $this->display(__FILE__, 'views/templates/hook/payme_payment.tpl');
	}

	public function hookPaymentReturn($params) {
		if (!$this->active)
			return;
		return $this->display(__FILE__, 'payment_ok.tpl');
	}
}
?>