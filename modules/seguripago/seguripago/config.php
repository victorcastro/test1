<?php
/*
 * Archivo de configuración
 */


/**
 * Identificador del comercio, proveído por Seguripago.
 */
$idSocio = "57";


/**
 * Key de encriptación. Este es el key de 32 caracteres proporcionado por Seguripago
 * y es utilizado para encriptar la información enviada a Seguripago.
 */
$key = "ad26ab71c1afe06b12ef6ec2988ce807";


/**
 * Modo: 'test', para ambiente de pruebas, o 'prod' para ambiente de producción
 */
$modo = 'prod';


/**
 * Ruta del logo del comercio
 */
$ruta_logo = "/modules/seguripago/resources/img/logo-youroutlet.jpg";


/**
 * Nombre del comercio (codificado para html)
 */
$nombre_comercio = "Youroutlet";
?>
