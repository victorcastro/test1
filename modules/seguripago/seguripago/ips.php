<?php
/**
 * PROGRAMA DE EJEMPLO DE ENVÍO DE TRAMA A SEGURIPAGO
 * ==================================================
 *
 * Este programa es un ejemplo de envío de trama desde el comercio electrónico
 * hacia Seguripago.
 *
 * ADVERTENCIA: Seguripago restringe la URL desde la cuál se envía la trama.
 * Ud. deberá informar a Seguripago, de la URL desde la cuál enviará la
 * información.
 * Si se recibe un mensaje de error de rechazo de URL, póngase en contacto con
 * Seguripago.
 *
 * NOTA: Utilice tabulación de 2 espacios para ver el código correctamente alineado.
 *
 * Actualizado al 7 de octubre de 2013.
 */

/**
 * Activando mensajes de error
 */
ini_set("display_errors", 1);
error_reporting(E_ALL);

/**
 * Archivo de configuración
 */
include('config.php');

/**
 * Incluyendo librería de Seguripago
 */
include('seguripago/seguripago_api.php');


/**
 * INICIALIZACIÓN DE VARIABLES
 */

/**
 * Creando el objeto de Envío de Seguripago
 */
$sp_envio = new seguripagoEnvio($idSocio, $key, $modo);


/**
 * Número de pedido del socio. Éste número es generado por el comercio y le
 * sirve para poder relacionar el pago con Seguripago.
 */
$numero_pedido = "4000006";


/**
 * Array de información de cliente. Eliminar este código si no desea enviar
 * información del cliente.
 */
/*$dato_cliente_array = array(
		512,										//-- Id del cliente, en el sistema del comercio electrónico
		"Carlos",								//-- Nombre(s)
		"Mejía",								//-- Apellido(s)
		"razon social",					//-- Razón social
		"DNI",									//-- Tipo de documento
		"55556666",							//-- Número de documento
		"camaringo@gmail.com",	//-- Correo
		"Jr. Parinachocas 111",	//-- Dirección
		"pais",									//-- País del cliente
		"M"											//-- Sexo (M)asculino o (F)emenino
);*/


/**
 * Array de información de artículo. Eliminar este código si no desea enviar
 * información de artículo
 */
/*$dato_articulo_array = array(
		1,				//-- Número de orden
		'106701',	//-- Código de artículo
		1,				//-- Cantidad
		15.50			//-- Precio
);*/


/**
 * Array con la data a enviar
 */
$data = array(
	'num_pedido'	=> $numero_pedido,				//-- $numero_pedido
	'fecha_hora'	=> time(),								//-- Fecha/Hora de creación en Unixtime
	'moneda'			=> 'PEN',									//-- Moneda (ISO 4217)
	'importe'			=> 15.60,									//-- Importe
	'vencimiento'	=> (time() + 72 * 3600),	//-- Fecha/Hora de vencimiento en Unixtime
	//'cliente'			=> $dato_cliente_array,		//-- Datos de cliente, opcional
	//'articulo'		=> $dato_articulo_array,	//-- Datos de artículo, opcional
	'pantalla'		=> 'S',										//-- Tipo de pantalla a utilizar: (H)orizontal, (V)ertical, opcional
	//'obviar'			=> '1',										//-- Producto de Seguripago que no quiere que aparezca: (1) SeguriCrédito, (2) SeguriCash, opcional.
);

/**
 * Enviamos trama a través del méotodo de Seguripago
 */
$sp_envio->enviar($data);

?>