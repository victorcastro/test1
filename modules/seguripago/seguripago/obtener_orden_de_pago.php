<?php
/**
 * PROGRAMA DE EJEMPLO DE OBTENCIÓN DE ORDEN DE PAGO DIFERIDO (SeguriCash y otros)
 * ===============================================================================
 *
 * Este programa es un ejemplo de como obtener un html del contenido de la
 * orden de pago de SeguriCash y otros.
 *
 * Este html normalmente es mostrado al usuario en el momento de generar la orden
 * de compras, pero si el comercio -por algún motivo- desea enviar la orden de pago,
 * como por ejemplo a través de correo, este ejemplo le indicará como obtener el html.
 *
 * NOTA: Tener en cuenta que el html de la orden de pago se construye con los datos
 * que el comercio le proporcione, por ese motivo se pide que se establezcan los
 * parámetros verídicos obtenidos desde su base de datos.
 *
 * Actualizado al 22 de octubre de 2013.
 */

/**
 * Activando mensajes de error
 */
ini_set("display_errors", 1);
error_reporting(E_ALL);

/**
 * Archivo de configuración
 */
include('config.php');

/**
 * Incluyendo librería de Seguripago
 */
include('seguripago/seguripago_api.php');

/**
 * Ponga aquí el archivo de inclusión para conectarse a su base de datos.
 */


/**
 * INICIALIZACIÓN DE VARIABLES
 */



/**
 * Consultando aquí los datos desde su base de datos
 */
$num_transacción = "99999999999999";
$importe = "999.99";
$moneda = "PEN";

/**
 * Creando el objeto de Plantillas de Seguripago
 */
$sp_plantillas = new seguripagoPlantillas($idSocio, $key, $modo);


$html = $sp_plantillas->seguricashLocal($ruta_logo, $nombre_comercio, true, $num_transaccion, $importe, $moneda);


/**
 * Enviar aquí el html por correo o generar PDF, etc.
 */

?>