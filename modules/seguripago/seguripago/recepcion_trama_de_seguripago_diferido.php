<?php
/**
 * PROGRAMA DE EJEMPLO DE RECEPCIÓN DE TRAMA DE SEGURIPAGO - DIFERIDO
 * ==================================================================
 *
 * Este programa es un ejemplo de la recepción de trama que envía SeguriPago
 * al comercio electrónico.
 *
 * La trama es enviada cuando el usuario realiza el pago diferido (SeguriCash
 * y otros) y también es enviada cuando la notificación de pago de SeguriCrédito
 * falla en el envío inmediato.
 *
 * La trama se envía cada 15 minutos (el tiempo puede variar) hasta que el comercio
 * nos confirme su recepción.
 *
 * Ud. deberá informar de ésta URL para que Seguripago sepa dónde enviar la
 * información.
 *
 * NOTA: Utilice tabulación de 2 espacios para ver el código correctamente alineado.
 *
 * Actualizado al 7 de octubre de 2013.
 */

/**
 * Activando mensajes de error
 */
ini_set("display_errors", 1);
error_reporting(E_ALL);

/**
 * Archivo de configuración
 */
include('config.php');

/**
 * Incluyendo librería de Seguripago
 */
include('seguripago/seguripago_api.php');

/**
 * Ponga aquí el archivo de inclusión para conectarse a su base de datos.
 */


/**
 * INICIALIZACIÓN DE VARIABLES
 */


/**
 * Creando el objeto de Envío de Seguripago
 */
$sp_recepcion = new seguripagoRecepcionDiferido($idSocio, $key, $modo);

$data = $sp_recepcion->recibir();

/**
 * Información sobre data recibida:
 *
 * $data["idSocio"]					: Identificador de Socio
 * $data["num_pedido"]			: Número de pedido de Socio
 * $data["num_transaccion"]	: Número de transacción generado por Seguripago
 * $data["fecha_hora_trans"]: Fecha/hora de transacción en Unixtime
 * $data["moneda"]					: Moneda
 * $data["importe"]					: Importe aprobado
 * $data["resultado"]				: Resultado de la transaccion. Aprobado (1), No aprobado (2)
 * $data["cod_respuesta"]		: Código de respuesta, generado por el medio de pago
 * $data["txt_respuesta"]		: Texto descriptivo de respuestas, generado por el medio de pago
 * $data["medio_pago"]			: Código de Medio de pago utilizado para SeguriCrédito (si es Seguricash se envía cero (0)). (1) Visa, (2) Mastercard, (3) American Express
 * $data["tipo_respuesta"]	: Tipo de respuestas: Inmediato (1), Batch (2)
 * $data["cod_autoriza"]		: Código de autorización, enviado por algunos medios de pago
 * $data["num_referencia"]	: Número de referencia, enviado por algunos medios e pago
 * $data["hash"]						: Resultado de la transaccion. Aprobado (1), No aprobado (2)
 * $data["cod_producto"]		: Código del Producto de SeguriPago: (1) SeguriCrédito, (2) SeguriCash.
 *
 */


/**
 * Si no se recibió un array, significa que hubo un error en la recepción de la data
 */
if(!is_array($data)) {
	switch($data) {
		case '01': echo "Error al recepcionar datos."; break;
		case '02': echo "Error en n&uacute;mero de pedido."; break;
		case '03': echo "Error en validaci&oacute;n de hash."; break;
	}
	exit();
}



/**
 * Valide aquí si el número de pedido ($data['num_pedido']) existe en su sistema.
 */

/**
 * Valide aquí si el número de pedido ya fue cancelado.
 */

/**
 * Valide aquí si el importe informado por SeguriPago ($data['importe']) coincide
 * con el monto registrado en su sistema.
 */

/**
 * Validar aquí el vencimiento del pago.
 */

/**
 * Actualice su base de datos con la información recibida.
 */


/**
 * ---------------------- PROCESANDO PAGO APROBADO ---------------------------
 */
if($data["resultado"] == "1") {
	/**
	 * Informar al usuario, por correo, informando de la aprobación de su pago,
	 * indicar información adicional para que acceda al producto o servicio.
	 */
}


/**
 * Enviando confirmación de recibo de datos
 */
$sp_recepcion->confirmar();

?>