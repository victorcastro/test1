<?php
/**
 * PROGRAMA DE EJEMPLO DE RECEPCIÓN DE TRAMA DE SEGURIPAGO - INMEDIATO
 * ===================================================================
 *
 * Este programa es un ejemplo de la recepción de trama que envía SeguriPago
 * al comercio electrónico.
 *
 * La trama es enviada inmediatamente después de que el usuario terminó la
 * transacción.
 *
 * Ud. deberá informar de ésta URL para que Seguripago sepa dónde enviar la
 * información.
 *
 * NOTA: Utilice tabulación de 2 espacios para ver el código correctamente alineado.
 *
 * Actualizado al 7 de octubre de 2013.
 */

/**
 * Activando mensajes de error
 */
ini_set("display_errors", 1);
error_reporting(E_ALL);

/**
 * Archivo de configuración
 */
include('config.php');

/**
 * Incluyendo librería de Seguripago
 */
include('seguripago/seguripago_api.php');

/**
 * Ponga aquí el archivo de inclusión para conectarse a su base de datos.
 */


/**
 * INICIALIZACIÓN DE VARIABLES
 */

/**
 * Creando el objeto de Envío de Seguripago
 */
$sp_recepcion = new seguripagoRecepcionInmediato($idSocio, $key, $modo);

$data = $sp_recepcion->recibir();

/**
 * Información sobre data recibida:
 *
 * $data["idSocio"]					: Identificador de Socio
 * $data["num_pedido"]			: Número de pedido de Socio
 * $data["num_transaccion"]	: Número de transacción generado por Seguripago
 * $data["fecha_hora_trans"]: Fecha/hora de transacción en Unixtime
 * $data["moneda"]					: Moneda
 * $data["importe"]					: Importe aprobado
 * $data["resultado"]				: Resultado de la transaccion. Aprobado (1), No aprobado (2)
 * $data["cod_respuesta"]		: Código de respuesta, generado por el medio de pago
 * $data["txt_respuesta"]		: Texto descriptivo de respuestas, generado por el medio de pago
 * $data["medio_pago"]			: Código de Medio de pago utilizado para SeguriCrédito (si es Seguricash se envía cero (0)). (1) Visa, (2) Mastercard, (3) American Express
 * $data["tipo_respuesta"]	: Tipo de respuestas: Inmediato (1), Batch (2)
 * $data["cod_autoriza"]		: Código de autorización, enviado por algunos medios de pago
 * $data["num_referencia"]	: Número de referencia, enviado por algunos medios e pago
 * $data["hash"]						: Resultado de la transaccion. Aprobado (1), No aprobado (2)
 * $data["cod_producto"]		: Código del Producto de SeguriPago: (1) SeguriCrédito, (2) SeguriCash.
 *
 */

/**
 * Valide aquí si el número de pedido ($data['num_pedido']) existe en su sistema.
 */

/**
 * Valide aquí si el número de pedido ya fue cancelado.
 */

/**
 * Valide aquí si el importe informado por SeguriPago ($data['importe']) coincide
 * con el monto registrado en su sistema.
 */

/**
 * Validar aquí el vencimiento del pago.
 */

/**
 * Actualice su base de datos con la información recibida.
 */

/**
 * Enviando confirmación de recibo de datos
 */
$sp_recepcion->confirmar();


/**
 * ---------------------- MENSAJE PARA PAGO APROBADO ---------------------------
 */
if($data['resultado'] == "1") {

	/**
	 * MENSAJE PARA PAGO INMEDIATO (SeguriCrédito)
	 */
	if($data['tipo_respuesta'] == "1") {
		/**
		* Envíar aquí un mensaje al usuario indicando que la operación fue aceptada
		* y dar otra información que crea conveniente.
		*/
		include("seguripago_api_plantillas/respuesta_inmediato_aceptado.php");

	/**
	 * MENSAJE PARA PAGO DIFERIDO (SeguriCash y otros)
	 */
	} else {
		/**
		* Envíar aquí un mensaje al usuario indicando que se generó un número de cupón
		* a ser cancelado en las entidades financieras BCP, Scotia, según lo establecido
		* en SeguriPago.
		*
		* Se pasa como parámetro la ruta del logo del comercio y el segundo parámetro
		* indica si se mostrará la orden de pago, en modo completo (true) o resumido (false - default)
		* Ver MANUAL.txt para más detalle.
		*/
		$html = $sp_recepcion->pantallaSeguricash($ruta_logo, $nombre_comercio, true);

		/**
		 * Enviar $html por correo (opcional)
		 */
		//$html_correo = "Sr. XXX, XXXX:<br />$html";

		/**
		 * Mostrando
		 */
		echo "<center><div style='width:800px;'>$html</div></center>";


	}

/**
 * ---------------------- MENSAJE PARA PAGO DESAPROBADO ------------------------
 */
} else {
	/**
	 * Envíar aquí un mensaje al usuario indicando que la operación no fue aceptada.
	 */
	include("seguripago_api_plantillas/respuesta_inmediato_rechazado.php");

}
?>