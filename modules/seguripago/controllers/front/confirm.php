<?php
/**
 * @since 1.5.0
 */

class SeguripagoConfirmModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $display_column_left = false;

    /**
     * @see FrontController::initContent()
     */
    public function init() {
        parent::init();
        $display_column_left = false;
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        $display_column_left = false;
    }

    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        parent::initContent();
        /**
         * Información sobre data recibida:
         *
         * $data["idSocio"]         : Identificador de Socio
         * $data["num_pedido"]      : Número de pedido de Socio
         * $data["num_transaccion"] : Número de transacción generado por Seguripago
         * $data["fecha_hora_trans"]: Fecha/hora de transacción en Unixtime
         * $data["moneda"]          : Moneda
         * $data["importe"]         : Importe aprobado
         * $data["resultado"]       : Resultado de la transaccion. Aprobado (1), No aprobado (2)
         * $data["cod_respuesta"]   : Código de respuesta, generado por el medio de pago
         * $data["txt_respuesta"]   : Texto descriptivo de respuestas, generado por el medio de pago
         * $data["medio_pago"]      : Código de Medio de pago utilizado para SeguriCrédito (si es Seguricash se envía cero (0)). (1) Visa, (2) Mastercard, (3) American Express
         * $data["tipo_respuesta"]  : Tipo de respuestas: Inmediato (1), Batch (2)
         * $data["cod_autoriza"]    : Código de autorización, enviado por algunos medios de pago
         * $data["num_referencia"]  : Número de referencia, enviado por algunos medios e pago
         * $data["hash"]            : Resultado de la transaccion. Aprobado (1), No aprobado (2)
         * $data["cod_producto"]    : Código del Producto de SeguriPago: (1) SeguriCrédito, (2) SeguriCash.
         * $data["num_tarjeta"]     : Numero de tarjeta asteriscada.
         * $data["nom_tarjetahabiente"]  : nombre del titular de  la tarjeta, solo algunos medios de pago.
         *
         */

        $html='';
        $this->display_column_left = true;

        $seguripago = new SeguriPago();
        $sp_recepcion = new seguripagoRecepcionInmediato($seguripago->sp_idSocio, $seguripago->sp_key, $seguripago->sp_modo);

        $data = $sp_recepcion->recibir();

        if(!isset($data["num_pedido"]) || empty($data["num_pedido"]))
            Tools::redirect('index.php?controller=order&step=1');

        $id_order=$data["num_pedido"]*1;

        $order = new Order($id_order);
        $customer = new Customer($order->id_customer);

        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');

        $currency = $this->context->currency;
        $total = (float)$order->total_paid;
        $medio_de_pago = $data["medio_pago"]=="1"?"Visa":($data["medio_pago"]=="2"?"Mastercard":($data["medio_pago"]=="3"?"American Express":"Otro"));
        $fecha_transaccion = date("d/m/Y", $data["fecha_hora_trans"])." a las ". date("H:i", $data["fecha_hora_trans"]);
        $moneda=$data["moneda"]=="PEN"?"S/.":$data["moneda"];

        /**
        * ----- MENSAJE PARA PAGO APROBADO -----
        */

        if($data['resultado'] == "1")
        {

             /**
             * MENSAJE PARA PAGO INMEDIATO (SeguriCrédito)
             */
            if($data['tipo_respuesta'] == "1")
            {

                $mailVars = array(
                    '{num_transaccion}' => $data["num_transaccion"],
                    '{medio_de_pago}'   => $medio_de_pago,
                    '{date_mediopago}'  => $fecha_transaccion,
                    '{num_tarjeta}'     => $data["num_tarjeta"],
                    '{num_referencia}'  => $data["num_referencia"],
                    '{nombre_mediopago}'=> 'seguripago',
                    '{nom_tarjetahabiente}'  => $data["nom_tarjetahabiente"]
                );

                $id_order_state = Configuration::get('PS_OS_SEGURICREDITO_PAGADO');

                $this->sendEmailConfirmacion($order,$id_order_state,$mailVars);

            /**
             * MENSAJE PARA PAGO PENDIENTE (SeguriCash y otros)
             */
            }
            else
            {

                $mailVars = array(
                    '{num_transaccion}' => $data["num_transaccion"],
                    '{importe}'         => (float)$data["importe"],
                    '{f_vencimiento}' => date("Y-m-d",strtotime($data["fecha_vencimiento"])),
                    '{h_vencimiento}' => date("H:i",strtotime($data["fecha_vencimiento"])),
                    '{nombre_mediopago}' => 'seguripago',
                    '{this_path_module}' => $this->module->getPathUri()
                );

                $id_order_state = Configuration::get('PS_OS_SEGURICASH_PENDIENTEPAGO');

                $this->sendEmailConfirmacion($order,$id_order_state,$mailVars);

            }

        /**
         * ----- MENSAJE PARA PAGO DESAPROBADO -----
         */
        }
        else
        {
            // Poner el pedido en estado cancelado
            $mailVars = array(
                '{medio_de_pago}'   => $medio_de_pago,
                '{txt_respuesta}'  => $data["txt_respuesta"],
                '{date_mediopago}'  => $fecha_transaccion
            );
            $id_order_state = Configuration::get('PS_OS_SEGURIPAGO_NOPROCESADO');

            $this->sendEmailConfirmacion($order,$id_order_state,$mailVars);

        }

        $this->context->smarty->assign(array(
            "idSocio"         => $data["idSocio"],
            "num_pedido"      => $id_order,
            "num_transaccion" => $data["num_transaccion"],
            "fecha_hora_trans"=> $fecha_transaccion,
            "moneda"          => $moneda,
            "importe"         => (float)$data["importe"],
            "resultado"       => $data["resultado"],
            "cod_respuesta"   => $data["cod_respuesta"],
            "txt_respuesta"   => $data["txt_respuesta"],
            "medio_pago"      => $medio_de_pago,
            "tipo_respuesta"  => $data["tipo_respuesta"] ,
            "cod_autoriza"    => $data["cod_autoriza"],
            "num_referencia"  => $data["num_referencia"],
            "hash"            => $data["hash"],
            "cod_producto"    => $data["cod_producto"],
            "num_tarjeta"     => $data["num_tarjeta"],
            "nom_tarjetahabiente"=> $data["nom_tarjetahabiente"],
            "reference_order" => $order->reference,
            "total_paid"      => (float)$order->total_paid,
            "total_shipping"  => $order->total_shipping,
            "lastname"        => $customer->lastname,
            "firstname"       => $customer->firstname,
            "html"            => $html,
            "fecha_vencimiento"=> $data['fecha_vencimiento'],
            "this_path_module" => $this->module->getPathUri()

        ));

        // Retorna respuesta a seguripago
        $sp_recepcion->confirmar();

        // Imprime los datos de respuesta
        $this->setTemplate('confirm_template.tpl');

    }

    function sendEmailConfirmacion($order,$id_order_state,$mailVars){

        // Cambiar estado del pedido e incluir email.
        $extra_vars = array();
        $history = new OrderHistory();
        $history->id_order = (int)$order->id;
        $history->changeIdOrderState($id_order_state, $order,false);
        $history->addWithemail(true, $mailVars);

        $order_status = new OrderState((int)$id_order_state, (int)$this->context->language->id);

        if (!Validate::isLoadedObject($order_status))
        {
            PrestaShopLogger::addLog('PaymentModule::validateOrder - Order Status cannot be loaded', 3, null, 'Order', (int)$order->id, true);
            throw new PrestaShopException('Can\'t load Order status');
        }

        // Enviar un e-mail de confirmacion al cliente (una orden = un e-mail)
        if ($id_order_state != Configuration::get('PS_OS_ERROR') && $id_order_state != Configuration::get('PS_OS_CANCELED') && $id_order_state != Configuration::get('PS_OS_SEGURIPAGO_NOPROCESADO') && $this->context->customer->id)
        {
            // Contruir el detalle de la orden para ser enviada por email
            $virtual_product = true;

            // Lista de productos
            $product_var_tpl_list = array();
            $order->product_list = $order->getCartProducts();

            foreach ($order->getCartProducts() as $product)
            {
                $price = Product::getPriceStatic((int)$product['id_product'], false, ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null), 6, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
                $price_wt = Product::getPriceStatic((int)$product['id_product'], true, ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null), 2, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});

                $product_price = Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt;

                $product_var_tpl = array(
                    'reference' => $product['reference'],
                    'name' => $product['product_name'],
                    'unit_price' => Tools::displayPrice($product_price, $this->context->currency, false),
                    'price' => Tools::displayPrice($product_price * $product['product_quantity'], $this->context->currency, false),
                    'quantity' => $product['product_quantity'],
                    'customization' => array()
                );

                $customized_datas = Product::getAllCustomizedDatas((int)$order->id_cart);
                if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']]))
                {
                    $product_var_tpl['customization'] = array();
                    foreach ($customized_datas[$product['id_product']][$product['id_product_attribute']][$order->id_address_delivery] as $customization)
                    {
                        $customization_text = '';
                        if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD]))
                            foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text)
                                $customization_text .= $text['name'].': '.$text['value'].'<br />';

                        if (isset($customization['datas'][Product::CUSTOMIZE_FILE]))
                            $customization_text .= sprintf(Tools::displayError('%d image(s)'), count($customization['datas'][Product::CUSTOMIZE_FILE])).'<br />';

                        $customization_quantity = (int)$product['product_quantity'];

                        $product_var_tpl['customization'][] = array(
                            'customization_text' => $customization_text,
                            'customization_quantity' => $customization_quantity,
                            'quantity' => Tools::displayPrice($customization_quantity * $product_price, $this->context->currency, false)
                        );
                    }
                }

                $product_var_tpl_list[] = $product_var_tpl;
                // Check if is not a virutal product for the displaying of shipping
                if (!$product['is_virtual'])
                    $virtual_product &= false;

            } // end foreach ($products)

            $product_list_txt = '';
            $product_list_html = '';

            if (count($product_var_tpl_list) > 0)
            {
                $product_list_txt = $this->getEmailTemplateContent('order_conf_product_list.txt', Mail::TYPE_TEXT, $product_var_tpl_list);
                $product_list_html = $this->getEmailTemplateContent('order_conf_product_list.tpl', Mail::TYPE_HTML, $product_var_tpl_list);
            }

            // Lista de descuentos
            $cart_rules = $order->getCartRules();
            $cart_rules_list = array();

            foreach ($cart_rules as $cart_rule)
            {
                $cart_rules_list[] = array(
                    'voucher_name' => $cart_rule['name'],
                    'voucher_reduction' => ($cart_rule['value'] != 0.00 ? '-' : '').Tools::displayPrice($cart_rule['value'], $this->context->currency, false)
                );
            }

            $cart_rules_list_txt = '';
            $cart_rules_list_html = '';
            if (count($cart_rules_list) > 0)
            {
                $cart_rules_list_txt = $this->getEmailTemplateContent('order_conf_cart_rules.txt', Mail::TYPE_TEXT, $cart_rules_list);
                $cart_rules_list_html = $this->getEmailTemplateContent('order_conf_cart_rules.tpl', Mail::TYPE_HTML, $cart_rules_list);
            }

            $carrier = new Carrier($order->id_carrier, $this->context->cart->id_lang);

            $invoice = new Address($order->id_address_invoice);
            $delivery = new Address($order->id_address_delivery);
            $delivery_state = $delivery->id_state ? new State($delivery->id_state) : false;
            $invoice_state = $invoice->id_state ? new State($invoice->id_state) : false;

            $data = array(
            '{firstname}' => $this->context->customer->firstname,
            '{lastname}' => $this->context->customer->lastname,
            '{email}' => $this->context->customer->email,
            '{delivery_block_txt}' => $this->_getFormatedAddress($delivery, "\n"),
            '{invoice_block_txt}' => $this->_getFormatedAddress($invoice, "\n"),
            '{delivery_block_html}' => $this->_getFormatedAddress($delivery, '<br />', array(
                'firstname' => '<span style="font-weight:bold;">%s</span>',
                'lastname'  => '<span style="font-weight:bold;">%s</span>'
            )),
            '{invoice_block_html}' => $this->_getFormatedAddress($invoice, '<br />', array(
                    'firstname' => '<span style="font-weight:bold;">%s</span>',
                    'lastname'  => '<span style="font-weight:bold;">%s</span>'
            )),
            '{delivery_company}' => $delivery->company,
            '{delivery_firstname}' => $delivery->firstname,
            '{delivery_lastname}' => $delivery->lastname,
            '{delivery_address1}' => $delivery->address1,
            '{delivery_address2}' => $delivery->address2,
            '{delivery_city}' => $delivery->city,
            '{delivery_postal_code}' => $delivery->postcode,
            '{delivery_country}' => $delivery->country,
            '{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
            '{delivery_phone}' => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
            '{delivery_other}' => $delivery->other,
            '{invoice_company}' => $invoice->company,
            '{invoice_vat_number}' => $invoice->vat_number,
            '{invoice_firstname}' => $invoice->firstname,
            '{invoice_lastname}' => $invoice->lastname,
            '{invoice_address2}' => $invoice->address2,
            '{invoice_address1}' => $invoice->address1,
            '{invoice_city}' => $invoice->city,
            '{invoice_postal_code}' => $invoice->postcode,
            '{invoice_country}' => $invoice->country,
            '{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
            '{invoice_phone}' => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
            '{invoice_other}' => $invoice->other,
            '{order_name}' => $order->getUniqReference(),
            '{date}' => Tools::displayDate(date('Y-m-d H:i:s'), null, 1),
            '{carrier}' => ($virtual_product || !isset($carrier->name)) ? Tools::displayError('No carrier') : $carrier->name,
            '{payment}' => Tools::substr($order->payment, 0, 32),
            '{products}' => $product_list_html,
            '{products_txt}' => $product_list_txt,
            '{discounts}' => $cart_rules_list_html,
            '{discounts_txt}' => $cart_rules_list_txt,
            '{total_paid}' => Tools::displayPrice($order->total_paid, $this->context->currency, false),
            '{total_products}' => Tools::displayPrice($order->total_paid - $order->total_shipping - $order->total_wrapping + $order->total_discounts, $this->context->currency, false),
            '{total_discounts}' => Tools::displayPrice($order->total_discounts, $this->context->currency, false),
            '{total_shipping}' => Tools::displayPrice($order->total_shipping, $this->context->currency, false),
            '{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $this->context->currency, false),
            '{total_tax_paid}' => Tools::displayPrice(($order->total_products_wt - $order->total_products) + ($order->total_shipping_tax_incl - $order->total_shipping_tax_excl), $this->context->currency, false));

            if (is_array($extra_vars))
                $data = array_merge($data, $extra_vars);

            // Join PDF invoice
            if ((int)Configuration::get('PS_INVOICE') && $order_status->invoice && $order->invoice_number)
            {
                $pdf = new PDF($order->getInvoicesCollection(), PDF::TEMPLATE_INVOICE, $this->context->smarty);
                $file_attachement['content'] = $pdf->render(false);
                $file_attachement['name'] = Configuration::get('PS_INVOICE_PREFIX', (int)$order->id_lang, null, $order->id_shop).sprintf('%06d', $order->invoice_number).'.pdf';
                $file_attachement['mime'] = 'application/pdf';
            }
            else
                $file_attachement = null;

            if (Validate::isEmail($this->context->customer->email))
                Mail::Send(
                     (int)$order->id_lang,
                     'order_conf',
                     Mail::l('Order confirmation', (int)$order->id_lang)." [#".$order->id."]",
                     $data,
                     $this->context->customer->email,
                     $this->context->customer->firstname.' '.$this->context->customer->lastname,
                     null,
                     null,
                     $file_attachement,
                     null, _PS_MAIL_DIR_, false, (int)$order->id_shop
                );
        }

    }

    /**
     * @param Object Address $the_address that needs to be txt formated
     * @return String the txt formated address block
     */
    function _getFormatedAddress(Address $the_address, $line_sep, $fields_style = array())
    {
        return AddressFormat::generateAddress($the_address, array('avoid' => array()), $line_sep, ' ', $fields_style);
    }

    /**
     * Fetch the content of $template_name inside the folder current_theme/mails/current_iso_lang/ if found, otherwise in mails/current_iso_lang
     *
     * @param string  $template_name template name with extension
     * @param integer $mail_type     Mail::TYPE_HTML or Mail::TYPE_TXT
     * @param array   $var           list send to smarty
     *
     * @return string
     */
    function getEmailTemplateContent($template_name, $mail_type, $var)
    {
        $email_configuration = Configuration::get('PS_MAIL_TYPE');
        if ($email_configuration != $mail_type && $email_configuration != Mail::TYPE_BOTH)
            return '';

        $theme_template_path = _PS_THEME_DIR_.'mails'.DIRECTORY_SEPARATOR.$this->context->language->iso_code.DIRECTORY_SEPARATOR.$template_name;
        $default_mail_template_path = _PS_MAIL_DIR_.$this->context->language->iso_code.DIRECTORY_SEPARATOR.$template_name;

        if (Tools::file_exists_cache($theme_template_path))
            $default_mail_template_path = $theme_template_path;

        if (Tools::file_exists_cache($default_mail_template_path))
        {
            $this->context->smarty->assign('list', $var);
            return $this->context->smarty->fetch($default_mail_template_path);
        }
        return '';
    }

}
