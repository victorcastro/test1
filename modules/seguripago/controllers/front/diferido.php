<?php
/**
 * @since 1.5.0
 */

class SeguripagoDiferidoModuleFrontController extends ModuleFrontController
{
    public $ssl = false;
    /**
     * @see FrontController::init()
     */
    public function init() {

        parent::init();
    }

    /**
     * @see FrontController::postProcess()
     */

    public function initContent()
    {

        $seguripago = new SeguriPago();
        $sp_recepcion = new seguripagoRecepcionDiferido($seguripago->sp_idSocio, $seguripago->sp_key, $seguripago->sp_modo);
        $data = $sp_recepcion->recibir();
        /**
         * Información sobre data recibida:
         *
         * $data["idSocio"]         : Identificador de Socio
         * $data["num_pedido"]      : Número de pedido de Socio
         * $data["num_transaccion"] : Número de transacción generado por Seguripago
         * $data["fecha_hora_trans"]: Fecha/hora de transacción en Unixtime
         * $data["moneda"]          : Moneda
         * $data["importe"]         : Importe aprobado
         * $data["resultado"]       : Resultado de la transaccion. Aprobado (1), No aprobado (2)
         * $data["cod_respuesta"]   : Código de respuesta, generado por el medio de pago
         * $data["txt_respuesta"]   : Texto descriptivo de respuestas, generado por el medio de pago
         * $data["medio_pago"]      : Código de Medio de pago utilizado para SeguriCrédito (si es Seguricash se envía cero (0)). (1) Visa, (2) Mastercard, (3) American Express
         * $data["tipo_respuesta"]  : Tipo de respuestas: Inmediato (1), Batch (2)
         * $data["cod_autoriza"]    : Código de autorización, enviado por algunos medios de pago
         * $data["num_referencia"]  : Número de referencia, enviado por algunos medios e pago
         * $data["hash"]            : Resultado de la transaccion. Aprobado (1), No aprobado (2)
         * $data["cod_producto"]    : Código del Producto de SeguriPago: (1) SeguriCrédito, (2) SeguriCash.
         * $data["num_tarjeta"]     : Numero de tarjeta asteriscada.
         * $data["nom_tarjetahabiente"]  : nombre del titular de  la tarjeta, solo algunos medios de pago.
         *
         */

          /**
           * Si no se recibió un array, significa que hubo un error en la recepción de la data
           */
        if(!is_array($data)) {
            switch($data) {
              case '01': echo "Error al recepcionar datos."; break;
              case '02': echo "Error en n&uacute;mero de pedido."; break;
              case '03': echo "Error en validaci&oacute;n de hash."; break;
            }

            exit();
          }

        $id_order=$data["num_pedido"]*1;

        $order = new Order($id_order);

        if (Validate::isLoadedObject($order))
        {

            $medio_de_pago = $data["medio_pago"]=="1"?"Visa":($data["medio_pago"]=="2"?"Mastercard":($data["medio_pago"]=="3"?"American Express":"Otro"));
            $fecha_transaccion = date("d/m/Y", $data["fecha_hora_trans"])." a las ". date("H:i", $data["fecha_hora_trans"]);
            $moneda=$data["moneda"]=="PEN"?"S/.":$data["moneda"];

           if($data["resultado"] == "1") {


                /**
                 * PAGO INMEDIATO (SeguriCrédito)
                 */
                if($data['tipo_respuesta'] == "1")
                {

                    $id_order_state = Configuration::get('PS_OS_SEGURICREDITO_PAGADO');
                    $mailVars = array(
                        '{num_transaccion}' => $data["num_transaccion"],
                        '{medio_de_pago}'   => $medio_de_pago,
                        '{date_mediopago}'  => $fecha_transaccion,
                        '{num_tarjeta}'     => $data["num_tarjeta"],
                        '{num_referencia}'  => $data["num_referencia"],
                        '{nombre_mediopago}'=> 'seguripago',
                        '{nom_tarjetahabiente}'  => $data["nom_tarjetahabiente"]
                    );

                    $id_order_state = Configuration::get('PS_OS_SEGURICREDITO_PAGADO');

                    $this->sendEmailConfirmacion($order,$id_order_state,$mailVars);

                /**
                 * PAGO INMEDIATO (SeguriCash y otros)
                 */
                }
                else
                {

                    $id_order_state = Configuration::get('PS_OS_SEGURICASH_PAGADO');

                    $this->sendEmailConfirmacion($order,$id_order_state,$mailVars);

                }
            /**
             * ----- MENSAJE PARA PAGO DESAPROBADO -----
             */
            }
            else
            {
                // Poner el pedido en estado cancelado
                $mailVars = array(
                    '{medio_de_pago}'   => $medio_de_pago,
                    '{txt_respuesta}'  => $data["txt_respuesta"],
                    '{date_mediopago}'  => $fecha_transaccion
                );
                $id_order_state = Configuration::get('PS_OS_SEGURIPAGO_NOPROCESADO');

                $this->sendEmailConfirmacion($order,$id_order_state,$mailVars);

            }

            // Responder a seguripago que se realizo la recepcion de datos
            // Acuse a Seguripago respecto a la data
            $sp_recepcion->confirmar();
            exit();
        }
        else
        {
                    //throw new PrestaShopException('object Order can\'t be loaded');
            echo "Error al recepcionar datos.";
            exit();

        }

    }

    function sendEmailConfirmacion($order,$id_order_state,$mailVars){

        // Cambiar estado del pedido y enviar email de notificacion.
        $extra_vars = array();
        $history = new OrderHistory();
        $history->id_order = (int)$order->id;
        $history->changeIdOrderState($id_order_state, $order,false);
        $history->addWithemail(true, $mailVars);
    }
}