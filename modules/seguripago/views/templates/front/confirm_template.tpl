<style type="text/css">
    .bor-gray-30 {
    border-color: #b3b3b3;
    }
    .bor-l {
    border-left-width: 1px;
    border-left-style: solid;
    }
</style>
<script type="text/javascript">
    $(document).ready(function()
    {

        $( "#imprimir" ).click(function() {
            $('.bankdetail').show();
            //location.reload();
            var fichaMuestra = document.getElementById('container-seguricash');
            var ventimp=window.open('','popUp');
            ventimp.document.write(fichaMuestra.innerHTML);
            ventimp.document.close();
            ventimp.print();
            ventimp.close();

        });

    });
</script>

{capture name=path}{l s='Pasarela de pagos Seguripago' mod='seguripago'}{/capture}


{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

  {if $resultado == 1}
    {if $tipo_respuesta == 1}
        <div id="container-seguricash">
            <h1 style="font: 600 18px/22px 'Open Sans', sans-serif; color: #555454; text-transform: uppercase; padding: 0px 0px 17px 0px; margin-bottom: 30px; border-bottom: 1px solid #d6d4d4; overflow: hidden;">{l s='Confirmación de Pago' mod='seguripago'}</h1>
            <div style="background: #fbfbfb; border: 1px solid #d6d4d4; padding: 14px 18px 13px; margin: 0 0 10px 0; line-height: 23px;">
                <p style="margin-bottom: 9px;">
                    <strong style="color: #333333;">{l s='Gracias por comprar en %s.' sprintf=$shop_name mod='seguripago'}</strong>
                </p>
                Hola {l s=$firstname} {l s=$lastname}, la siguiente informaci&oacute;n es importante:

                <section style="line-height: 18px;margin: 5px 0px;">
                    <strong>Datos de Youroutlet:</strong><br/>
                    N&uacute;mero pedido: {l s=$num_pedido}<br/>
                    Referencia pedido: {l s=$reference_order}<br/>
                </section>

                <section style="line-height: 18px;margin: 5px 0px">
                    <strong>Datos de Seguripago:</strong><br/>
                    N&uacute;mero de transacci&oacute;n: {l s=$num_transaccion}<br/>
                </section>

                <section style="line-height: 18px;margin: 5px 0px">
                    <strong>Medio de pago: {l s=$medio_pago}</strong><br>
                    N&uacute;mero de tarjeta: {l s=$num_tarjeta}<br/>
                    {if !empty($nom_tarjetahabiente)}
                       Nombre del Titular: {l s=$nom_tarjetahabiente}<br/>
                    {/if}
                    N&uacute;mero de Referencia: {l s=$num_referencia}<br/>
                    Fecha y hora: {l s=$fecha_hora_trans}<br/>
                    Importe Total: {l s=$moneda} {l s=$importe} <br/>
                </section>

                Estar&aacute; llegando a tu email la confirmaci&oacute;n del pedido.<br/>
            </div>
        </div>

        <div class="">
            <div style="text-align:left;">
                <button class="btn btn-sm btn-warning" id="imprimir">Imprimir</button>
            </div>
        </div>

    {else}

    <center>

        <div id="container-seguricash" style="width:568px;">
            <div style="width: 100%; overflow-y: hidden;overflow-x: hidden;color: #666;line-height: 18px;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;">
                <div id="print" style="background: white;text-align: center;with:100%;">

                    <div style="width: 100%;margin-bottom: 10px;overflow: hidden;">
                        <div style="width:50%; float: left;">
                            <div style="width:58%;">
                                <img title="SeguriPago" alt="SeguriPago"  style="max-width:100%;vertical-align: middle;" src={l s=$this_path_module}resources/img/logo-seguripago.jpg>
                            </div>
                        </div>
                        <div style="width:50%; float: right;">
                            <div style="width:58%;float: right;">
                                <img title="SeguriPago" alt="SeguriPago"  style="max-width:100%;vertical-align: middle;" src={l s=$this_path_module}resources/img/logo-youroutlet.jpg>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>

                    <div style="width: 100%;margin-bottom: 10px;overflow: hidden;">
                        <div style="overflow: hidden;padding: 12px;border-width: 1px;border-style: dotted;border-color: #999999;border-top-right-radius: 4px; border-top-left-radius: 4px; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px;">
                            <div style="width:50%; float: left; text-align:left;">
                                <small>Código de pago</small>
                                <h3 style="margin:0px;font-size: 24px;font-weight: 500;line-height: 1.1;color: inherit;">SeguriPago<br>{l s=$num_transaccion}</h3>

                                <small class="text-muted" style="color:#999;">Comercio: Youroutlet</small>
                            </div>
                            <div style="width:50%; float: right;text-align:right;">
                                <small>Importe a pagar</small>
                                <h3 style="margin:0px;font-size: 24px;font-weight: 500;line-height: 1.1;color: inherit;">S/. {l s=$importe}</h3>
                                <small class="text-muted" style="color:#999;">Fecha de vencimiento:</small>
                                <br>
                                <small class="text-muted" style="color:#999;">{l s=$fecha_vencimiento} - Hora de Perú</small>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>

                    <div style="width: 100%;margin-bottom: 10px;overflow: hidden;">
                        <h5 class="text-center" style="margin: 0px;font-size: 14px;font-weight: 500;line-height: 1.1;color: inherit;">Puedes pagar en cualquiera de estos bancos, ya sea en ventanilla o en la banca por internet.</h5>
                    </div>
                    <div style="clear:both;"></div>

                    <div style="width: 100%;margin-bottom: 10px;overflow: hidden;">
                        <div style="width:50%; float: left;">
                            <div style="padding-left: 15px;padding-right: 15px;">
                                <div class="text-center" style="cursor:pointer;" onclick="$('.bankdetail').slideToggle();">
                                    <img style="height:35px;" src={l s=$this_path_module}resources/img/scotiabank.png>
                                    <p style="margin-top: 10px;font-size: 11px;">
                                        Su recibo estará <strong>disponible inmediatamente.</strong>
                                    </p>
                                </div>
                                <div class="bankdetail f-xs marg-t" style="display:none;font-size: 11px;line-height: 1.4;color: #333;text-align:left;">
                                    <p style="margin-bottom: 7px;margin: 0 0 10px;"><b>1. En las ventanillas de las Agencias de Scotiabank o Cajero Express:</b></p>
                                    <ul style="margin-left: 20px;margin-top: 0;margin-bottom: 10px;list-style: initial;">
                                        <li>Acérquese a una agencia Scotiabank o Cajero Express.</li>
                                        <li>Indique que desea realizar el pago de Seguripago. Seguripago-Soles. <strong>(BT 50/186)</strong>.</li>
                                        <li>Indique que el servicio a pagar es Seguripago-soles.</li>
                                        <li>Indicar su código de pago <strong></strong> y se le entregará su voucher de Pago.</li>
                                    </ul>
                                    <p style="margin-bottom: 7px;margin: 0 0 10px;"><b>2. Vía Scotia en Línea (Banca Por Internet Scotiabank):</b></p>
                                    <ul style="margin-left: 20px;margin-top: 0;margin-bottom: 10px;list-style: initial;">
                                        <li>Ingrese a Scotia en Línea</li>
                                        <li>Seleccione su Tarjeta de Acceso y Débito e ingrese los 8 últimos dígitos de su tarjeta y la "clave principal".</li>
                                        <li>En el menú principal, seleccione Pagos – Otras Instituciones.</li>
                                        <li>En el tipo de Institución seleccione Otros y consulte.</li>
                                        <li>Seleccionar la institución según servicio: Seguripago. Seguripago-Soles.</li>
                                        <li>Ingrese su código de pago <strong></strong> y el sistema le mostrará una pantalla que podrá imprimir o enviar a una dirección de correo electrónico como constancia de pago.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div style="width:50%; float: right;">
                            <div style="border-color: #b3b3b3;border-left-width: 1px;border-left-style: solid;padding-left: 15px;padding-right: 15px;">

                                <div class="text-center" style="cursor:pointer;" onclick="$('.bankdetail').slideToggle();">
                                    <img style="height:35px;" src={l s=$this_path_module}resources/img/bcp.jpg>
                                    <p style="margin-top: 10px;font-size: 11px;">
                                        Su recibo estará <strong>disponible inmediatamente.</strong>
                                    </p>
                                </div>
                                <div class="bankdetail f-xs marg-t" style="display:none;font-size: 11px;line-height: 1.4;color: #333;text-align:left;">
                                    <p style="margin-bottom: 7px;margin: 0 0 10px;"><b>1. En una Agencia BCP:</b></p>
                                    <ul style="margin-left: 20px;margin-top: 0;margin-bottom: 10px;list-style: initial;">
                                        <li>Imprima esta boleta.</li>
                                        <li>Diríjase a una agencia BCP</li>
                                        <li>Indique que va a realizar un pago a la  empresa: Seguripago</li>
                                        <li>Indique que el servicio a pagar es SEGURIPAGO - soles</li>
                                        <li>Indique el código de pago de SEGURIPAGO: <strong>{l s=$num_transaccion}</strong>   </li>
                                        <li>Se le remitirá su voucher de Pago.</li>
                                    </ul>

                                    <p style="margin-bottom: 7px;margin: 0 0 10px;"><b>2. En un Agente BCP:</b></p>
                                    <ul style="margin-left: 20px;margin-top: 0;margin-bottom: 10px;list-style: initial;">
                                        <li>Imprima esta boleta.</li>
                                        <li>Diríjase a un agente BCP</li>
                                        <li>Indique que va a realizar un pago a la  empresa: Seguripago, código de agente 08051.</li>
                                        <li>Indique que el servicio a pagar es SEGURIPAGO - soles</li>
                                        <li>Indique el código de Pago de SEGURIPAGO: <strong>{l s=$num_transaccion}</strong>  </li>
                                        <li>Se le remitirá; su voucher de Pago</li>
                                    </ul>

                                    <p style="margin-bottom: 7px;margin: 0 0 10px;"><b>3. Vía internet BCP, seguir los siguientes pagos:</b></p>
                                    <ul style="margin-left: 20px;margin-top: 0;margin-bottom: 10px;list-style: initial;">
                                        <li>Ingresa en tu cuenta.</li>
                                        <li>Selecciona la opción Pago de Servicios ubicada en la columna izquierda.</li>
                                        <li>Selecciona Empresas Diversas, buscar Seguripago</li>
                                        <li>Selecciona el servicio SEGURIPAGO - soles</li>
                                        <li>Ingresa el siguiente Código de Pago: <strong>{l s=$num_transaccion}</strong>  </li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-right">
                <button class="btn btn-sm btn-warning" id="imprimir">Imprimir</button>
            </div>
        </div>

    </center>

    {/if}

    {else}
        <div id="container-seguricash">
            <h1 style="font: 600 18px/22px 'Open Sans', sans-serif; color: #555454; text-transform: uppercase; padding: 0px 0px 17px 0px; margin-bottom: 30px; border-bottom: 1px solid #d6d4d4; overflow: hidden;">{l s='No se realizó el pago' mod='seguripago'}</h1>
            <div style="background: #fbfbfb; border: 1px solid #d6d4d4; padding: 14px 18px 13px; margin: 0 0 10px 0; line-height: 23px;">
                <p style="margin-bottom: 9px;">
                    <strong style="color: #333333;">Hola {l s=$firstname} {l s=$lastname}, {l s='¡Lamentamos que no puedas realizar la compra en %s!' sprintf=$shop_name mod='seguripago'}</strong>
                </p>

                La siguiente informaci&oacute;n es importante:

                <section style="line-height: 18px;margin: 5px 0px">
                    <strong>El pago del pedido NO fue aceptado por:</strong><br/>
                    {l s=$medio_pago}<br/>
                    Razón:  {l s=$txt_respuesta}<br/>
                    Dicha transacción fue realizado el: {l s=$fecha_hora_trans}<br/>
                </section>

                <span >Al <strong>NO</strong> poder efectuar la compra, no habrá ningún cobro.</span>
            </div>
        </div>

    {/if}


