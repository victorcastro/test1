

<style>
    p.payment_module a.seguripago {
        background: url({$this_path}resources/img/logo-seguripago.jpg) 15px 12px no-repeat #fbfbfb;
        padding: 25px 40px 20px 180px;
    }
</style>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <p class="payment_module">

            <a class="seguripago"  href="{$link->getModuleLink('seguripago', 'validation')}" title="{l s='Pasarela de pagos (Confiable, Seguro, Fácil)' mod='seguripago'}">
                <img width="65" height="30" alt="" src="{$this_path}resources/img/visa.gif">
                <img width="65" height="30" alt="" src="{$this_path}resources/img/mastercard.jpg">
                <img width="65" height="30" alt="" src="{$this_path}resources/img/scotiabank.png">
                <img width="65" height="30" alt="" src="{$this_path}resources/img/bcp.png">
                <br>
                <span style="text-transform:none">{l s='Pague seguro con Tarjetas de crédito, débito, banca electrónica o Cash en su Banco de preferencia' mod='seguripago'}</span>
            </a>
        </p>
    </div>
</div>

