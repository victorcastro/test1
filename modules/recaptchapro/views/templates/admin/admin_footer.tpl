{**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *}

<div class="re_clearfix"></div>
<div id="re_footer" class="text-center text-muted mt-3">
    <ol>
        <li>
            {$created_by_l|escape:'htmlall':'UTF-8'} <strong>PrestaBucket</strong>
        </li>
        <li>
            {$current_version_l|escape:'htmlall':'UTF-8'}: <strong>{$module_version_f|escape:'htmlall':'UTF-8'}</strong>
        </li>
		<li>
            <a href="https://addons.prestashop.com/en/ratings.php" target="_blank" class="re-rate">
                <i class="icon-star"></i>{$rate_us_l|escape:'htmlall':'UTF-8'}
            </a>
        </li>
        <li>
            <a href="{$documentation_url|escape:'htmlall':'UTF-8'}modules/recaptchapro/docs/readme_{$documentation_lg|escape:'htmlall':'UTF-8'}.pdf" target="_blank">
                <i class="icon-book"></i>{$documentation_l|escape:'htmlall':'UTF-8'}
            </a>
        </li>
        <li>
            <a href="https://addons.prestashop.com/en/contact-us?id_product=28831" target="_blank">
                <i class="icon-question-circle"></i>{$need_help_l|escape:'htmlall':'UTF-8'} ?
            </a>
        </li>
    </ol>
</div>