{**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *}

{if $site_key}
    <script src='https://www.google.com/recaptcha/api.js?hl={$re_language|escape:"htmlall":"UTF-8"}'></script>
    <script>
        // Vanilla document ready
        document.addEventListener('DOMContentLoaded', function() {
            // Create structure variables
            var submit_button = findCM([
                '#login-panel #login_form button[name="submitLogin"]'
            ]);
            var login_form = findCM([
                '#login-panel #login_form'
            ]);
            var captcha_position = findCM([
                '#login-panel #login_form .form-group:nth-child(3)'
            ]);
            var error_header = findCM([
                '#login #login-header div.text-center'
            ]);
            
            // Add our captcha depending to reCaptcha version
            if (re_version == 1) {
                // reCaptcha Version 2
                $(captcha_position).after('<div class="g-recaptcha" data-sitekey="{$site_key|escape:"htmlall":"UTF-8"}" data-theme="{$re_theme|escape:"htmlall":"UTF-8"}" data-size="{$re_size|escape:"htmlall":"UTF-8"}"></div>');
                
                var clicked_first = true;
                
                $(submit_button).on('click', function() {
                    if (clicked_first == true) {
                        if ($(submit_button).prop('type') == 'submit') {
                            $(submit_button).prop('type', 'button');
                        }
                    }
                    
                    if (clicked_first == true) {
                        if (grecaptcha.getResponse()) {
                            $(submit_button).prop('type', 'submit');
                            clicked_first = false;
                            $(submit_button).click();
                        } else {
                            $('.alert, .error').remove();
                            $('<div class="alert alert-danger error" id="error"><p>{$there_is1|escape:"htmlall":"UTF-8"}</p><ol><li>{$wrong_captcha|escape:"htmlall":"UTF-8"}</ol></div>').hide().insertAfter(error_header).fadeIn(600);
                        }
                    }
                });
            } else if (re_version == 2) {
                // reCaptcha Invisible
                regCM(submit_button, login_form, error_header);
                
                $(submit_button).click(function(event) {
                    if (! grecaptcha.getResponse()) {
                        event.preventDefault();
                        grecaptcha.execute();
                    }
                });
            
                $('#login-panel').append('<div id="recaptcha" class="g-recaptcha" data-sitekey="{$site_key|escape:"htmlall":"UTF-8"}" data-callback="onreCSubmit" data-size="invisible"></div>');
                $(submit_button).attr('type', 'button');
            }
        });
        
        // Utility functions
        function onreCSubmit(token) {
            if (token) {
                $($('#re_submit_button').attr('data-parameter')).attr('type', 'submit');
                $($('#re_login_form').attr('data-parameter')).submit();
            }
        }
        
        function findCM(str) {
            var temp = false;
            
            $.each(str, function(key, value) {
                if ($(value).length) {
                    temp = value;
                    return false;
                }
            });
            
            if (temp) {
                return temp;   
            } else {
                return false;
            }
        }
        
        function regCM(submit_button, login_form, error_header) {
            $('body').append("<div id='re_error_header' data-parameter='" + error_header + "'></div>");
            $('body').append("<div id='re_submit_button' data-parameter='" + submit_button + "'></div>");
            $('body').append("<div id='re_login_form' data-parameter='" + login_form + "'></div>");
        }
    </script>
{/if}