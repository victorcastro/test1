/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

// Vanilla document ready
document.addEventListener('DOMContentLoaded', function() {
    if (site_key) {
        // Create structure variables
        var submit_button = findCM([
            '#password .forgotten-password .btn-primary',
            '#password #form_forgotpassword .submit > button',
            '#password #form_forgotpassword .submit > input'
        ]);
        var captcha_form = findCM([
            '#password .forgotten-password',
            '#password #form_forgotpassword fieldset > div:first-child',
            '#password #form_forgotpassword fieldset > .text'
        ]);
        var error_header = findCM([
            '#password #content',
            '#center_column .page-subheading',
            '#center_column > h1',
            '#password .forgotten-password',
            '#password #form_forgotpassword'
        ]);
        var captcha_invisible_form = findCM([
            '#password .forgotten-password',
            '#password #form_forgotpassword'
        ]);

        // Check if is whitelisted and display relevant content
        if (!whitelisted) {
            // Add our captcha depending to reCaptcha version
            if (re_version == 1) {
                // reCaptcha Version 2
                $(captcha_form).append('<div style="margin-top: 10px;" class="g-recaptcha" data-sitekey="' + site_key + '" data-theme="' + re_theme + '" data-size="' + re_size + '"></div>');
                
                var clicked_first = true;

                $(submit_button).on('click', function() {
                    if (clicked_first == true) {
                        if ($(submit_button).prop('type') == 'submit') {
                            $(submit_button).prop('type', 'button');
                        }
                    }

                    if (clicked_first == true) {
                        if (grecaptcha.getResponse()) {
                            $(submit_button).prop('type', 'submit');
                            clicked_first = false;
                            $(submit_button).click();
                        } else {
                            $('.alert, .error').remove();
                            if (p_version == '1.7') {
                                $("<div class='alert alert-danger error'><p>" + there_is1 + "</p><ol><li>" + wrong_captcha + "</ol></div>").hide().insertBefore(error_header).fadeIn(600);
                            } else {
                                $("<div class='alert alert-danger error'><p>" + there_is1 + "</p><ol><li>" + wrong_captcha + "</ol></div>").hide().insertAfter(error_header).fadeIn(600);
                            }
                        }
                    }
                });
            } else if (re_version == 2) {
                // reCaptcha Invisible
                regFORP(captcha_invisible_form, error_header);

                $(captcha_invisible_form).submit(function(event) {
                    if (!grecaptcha.getResponse()) {
                        event.preventDefault();
                        grecaptcha.execute();
                    }
                });

                $(captcha_invisible_form).append('<div id="recaptcha" class="g-recaptcha" data-sitekey="' + site_key + '" data-callback="onreCSubmitFORP" data-size="invisible"></div>');
                $(captcha_invisible_form).append('<input type="hidden" value="" name="SubmitLogin" />');
            }
        } else {
            if (whitelist_m) {
                $(captcha_form).append('<input type="hidden" name="recaptcha_whitelisted" value="1" />');
                $(captcha_form).append('<p class="recaptcha_whitelist">' + whitelist_m + '</p>');
            } else {
                $(captcha_form).append('<input type="hidden" name="recaptcha_whitelisted" value="1" />');
            }
        }
    }
});

// Utility functions
function onreCSubmitFORP(token) {
    if (token) {
        $($('#re_forgotp_captcha_invisible_form').attr('data-parameter')).submit();
    }
}

function findCM(str) {
    var temp = false;

    $.each(str, function(key, value) {
        if ($(value).length) {
            temp = value;
            return false;
        }
    });

    if (temp) {
        return temp;
    } else {
        return false;
    }
}

function regFORP(captcha_invisible_form, error_header) {
    $('body').append("<div id='re_forgotp_error_header' data-parameter='" + error_header + "'></div>");
    $('body').append("<div id='re_forgotp_captcha_invisible_form' data-parameter='" + captcha_invisible_form + "'></div>");
}