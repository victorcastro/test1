/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*/

$( document ).ready(function() {

	/* 
	This solves a problem regarding adding links or editing the source code of the TINYMCE
	http://stackoverflow.com/questions/18111582/tinymce-4-links-plugin-modal-in-not-editable
	*/
	$(document).on('focusin', function(e) {
	    if ($(e.target).closest(".mce-window").length) {
	        e.stopImmediatePropagation();
	    }
	});

	tinymce.init({
	    selector: "textarea.campaign_html_content_textarea",
	    editor_selector : "mceHtml",
	    plugins : "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor",
		toolbar1 : "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image",
		toolbar2: "",
		external_filemanager_path: ad+"/filemanager/",
		filemanager_title: "File manager" ,
		external_plugins: { "filemanager" : ad+"/filemanager/plugin.min.js"},
		language: iso,
		minHeight: 300,
		skin: "prestashop",
		statusbar: false,
		relative_urls : false,
		convert_urls: false,
		extended_valid_elements : "em[class|name|id]",
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste | selectall'},
			insert: {title: 'Insert', items: 'media image link | pagebreak'},
			view: {title: 'View', items: 'visualaid'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
			tools: {title: 'Tools', items: 'code'}
		}

	 });

	tinymce.init({
	    selector: "textarea.edit_campaign_html_content_textarea",
	    editor_selector : "mceEditHtml",
	    plugins : "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor",
		toolbar1 : "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image",
		toolbar2: "",
		external_filemanager_path: ad+"/filemanager/",
		filemanager_title: "File manager" ,
		external_plugins: { "filemanager" : ad+"/filemanager/plugin.min.js"},
		language: iso,
		skin: "prestashop",
		statusbar: false,
		relative_urls : false,
		convert_urls: false,
		extended_valid_elements : "em[class|name|id]",
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste | selectall'},
			insert: {title: 'Insert', items: 'media image link | pagebreak'},
			view: {title: 'View', items: 'visualaid'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
			tools: {title: 'Tools', items: 'code'}
		}

	 });

	tinymce.init({
	    selector: "textarea.campaign_template_header_textarea",
	    editor_selector : "mceTemplateHeader",
	    plugins : "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor",
		toolbar1 : "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image",
		toolbar2: "",
		external_filemanager_path: ad+"/filemanager/",
		filemanager_title: "File manager" ,
		external_plugins: { "filemanager" : ad+"/filemanager/plugin.min.js"},
		language: iso,
		skin: "prestashop",
		statusbar: false,
		relative_urls : false,
		convert_urls: false,
		extended_valid_elements : "em[class|name|id]",
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste | selectall'},
			insert: {title: 'Insert', items: 'media image link | pagebreak'},
			view: {title: 'View', items: 'visualaid'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
			tools: {title: 'Tools', items: 'code'}
		}

	 });

	tinymce.init({
	    selector: "textarea.edit_campaign_template_header_textarea",
	    editor_selector : "mceEditTemplateHeader",
	    plugins : "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor",
		toolbar1 : "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image",
		toolbar2: "",
		external_filemanager_path: ad+"/filemanager/",
		filemanager_title: "File manager" ,
		external_plugins: { "filemanager" : ad+"/filemanager/plugin.min.js"},
		language: iso,
		skin: "prestashop",
		statusbar: false,
		relative_urls : false,
		convert_urls: false,
		extended_valid_elements : "em[class|name|id]",
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste | selectall'},
			insert: {title: 'Insert', items: 'media image link | pagebreak'},
			view: {title: 'View', items: 'visualaid'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
			tools: {title: 'Tools', items: 'code'}
		}

	 });

	tinymce.init({
	    selector: "textarea.campaign_template_content_textarea",
	    editor_selector : "mceTemplate",
	    plugins : "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor",
		toolbar1 : "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image",
		toolbar2: "",
		external_filemanager_path: ad+"/filemanager/",
		filemanager_title: "File manager" ,
		external_plugins: { "filemanager" : ad+"/filemanager/plugin.min.js"},
		language: iso,
		skin: "prestashop",
		statusbar: false,
		relative_urls : false,
		convert_urls: false,
		extended_valid_elements : "em[class|name|id]",
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste | selectall'},
			insert: {title: 'Insert', items: 'media image link | pagebreak'},
			view: {title: 'View', items: 'visualaid'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
			tools: {title: 'Tools', items: 'code'}
		}
	 });

	tinymce.init({
	    selector: "textarea.edit_campaign_template_content_textarea",
	    editor_selector : "mceEditTemplate",
	    plugins : "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor",
		toolbar1 : "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image",
		toolbar2: "",
		external_filemanager_path: ad+"/filemanager/",
		filemanager_title: "File manager" ,
		external_plugins: { "filemanager" : ad+"/filemanager/plugin.min.js"},
		language: iso,
		skin: "prestashop",
		statusbar: false,
		relative_urls : false,
		convert_urls: false,
		extended_valid_elements : "em[class|name|id]",
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste | selectall'},
			insert: {title: 'Insert', items: 'media image link | pagebreak'},
			view: {title: 'View', items: 'visualaid'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
			tools: {title: 'Tools', items: 'code'}
		}
	 });

	$.ajaxSetup({
		type: "POST",
		url: admin_module_ajax_url,
		data: {
			ajax : true,
			id_tab : current_id_tab,
			controller : admin_module_controller,
		}
	});

	$("#mailchimp_product_search_add").keyup(function (e) {
		search_val = $("#mailchimp_product_search_add").val();
		if(e.keyCode != 13) 
		{
			if (search_val.length >= 3)
				mailchimpProductSearch(lang, 'add');	// Calls the search function.

			if (search_val.length < 3)
				$("#mailchimp_product_search_result_add").html("");
		}
		else
			e.preventDefault();
	});

	$("#mailchimp_product_search_edit").keyup(function (e) {
		search_val = $("#mailchimp_product_search_edit").val();
		if(e.keyCode != 13) 
		{
			if (search_val.length >= 3)
				mailchimpProductSearch(lang, 'edit');	// Calls the search function.

			if (search_val.length < 3)
				$("#mailchimp_product_search_result_edit").html("");
		}
		else
			e.preventDefault();
	});

	
	$("#mc_template_info_content").hide();
	$("#mc_user_template_info").hide();
	$("#add_to_list_report_table").hide();
	$("#add_to_list_alert_success").hide();
	$("#add_to_list_spin_icon").hide();
	$("#new_seg_condition_1").show();

	$("#add_to_list_report_label").click(function(){
		$("#add_to_list_report_table").show();
	});

	$("#client_list").change(
		function()
		{
			var id = "info-"+$(this).val();
			$("#info_lists").children().hide();
			$("#info_lists").show();
			$("#confirmation_mail_switch").show();
			$("#"+id).show(300);
		}
	);
    // Load functions
    Main.init();
});

// Main Function
var Main = function () {
	// function to custom select
	var runCustomElement = function () {
            // check submit
            var is_submit = $("#modulecontent").attr('role');

            if (is_submit == 1) {
                $(".list-group-item").each(function() {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass("active");
                    }
                    else if ($(this).attr('href') == "#config") {
                        $(this).addClass("active");
                    }
                });
                $('#config').addClass("active");
                $('#documentation').removeClass("active");
            }
            if (is_submit == 2) {
                $(".list-group-item").each(function() {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass("active");
                    }
                    else if ($(this).attr('href') == "#lists") {
                        $(this).addClass("active");
                    }
                });
                $('#lists').addClass("active");
                $('#documentation').removeClass("active");
            }

			// toggle panel
			$(".list-group-item").on('click', function() {
				var $el = $(this).parent().closest(".list-group").children(".active");
				if ($el.hasClass("active")) {
					$el.removeClass("active");
					$(this).addClass("active");
				}
			});

			// Hide ugly toolbar
			$('table[class="table"]').each(function(){
				$(this).hide();
				$(this).next('div.clear').hide();
			});

			// Hide ugly multishop select
			if (typeof(_PS_VERSION_) !== 'undefined') {
				var version = _PS_VERSION_.substr(0,3);
				if(version === '1.5') {
					$('.multishop_toolbar').addClass("panel panel-default");
					$('.shopList').removeClass("chzn-done").removeAttr("id").css("display", "block").next().remove();
					cloneMulti = $(".multishop_toolbar").clone(true, true);
					$(".multishop_toolbar").first().remove();
					cloneMulti.find('.shopList').addClass('selectpicker show-menu-arrow').attr('data-live-search', 'true');
					cloneMulti.insertBefore("#modulecontent");
					// Copy checkbox for multishop
					cloneActiveShop = $.trim($('table[class="table"] tr:nth-child(2) th').first().html());
					$(cloneActiveShop).insertAfter("#tab_translation");
				}
			}

			// Custom Select
			$('.selectpicker').selectpicker();

			// Fix bug form builder + bootstrap select
			var z = 1;
			$('.selectpicker').each(function(){
				var select = $(this);
				select.on('click', function() {
					$(this).parents('.bootstrap-select').addClass('open');
					$(this).parents('.bootstrap-select').toggleClass('open');
				});
			});

			// Custom Textarea
			$('.textarea-animated').autosize({append: "\n"});
		};
		return {
			//main function to initiate template pages
			init: function () {
				runCustomElement();
			}
		};
	}();

	function mailchimpProductSearch(lang, type)
	{
		var product_count = $("#mailchimp_product_count_"+type).val();
		var product_per_row_count = $("#mailchimp_product_per_row_count_"+type).val();
		var extra_column = $("#mailchimp_product_extra_col_"+type).val();
		$.ajax({
				data: { 
					value: $("#mailchimp_product_search_"+type).val(),
					type : type,
					product_count : product_count,
					product_per_row_count : product_per_row_count,
					extra_column : extra_column,
					action: 'mailchimpProductSearch',
					lang: lang
				},
			dataType : 'json'
		  }).success(function(json) 
		  {
		  		$('#mailchimp_product_search_result_'+type).html(json['content']);
		  		$("#mailchimp_product_search_result_"+type).show();
		  		delete json;
		  });
	}

	function addProductsToNewsletter(type)
	{
		var id_products = [];
		$(".products_pre_newsletter").each(function(){
			id_products.push($(this).val());
		});

		var id_lang = $('#mailchimp_lang_for_prod_'+type).val();
		var id_currency = $('#mailchimp_currency_for_prod_'+type).val();
		var products_per_row = $("#mailchimp_product_per_row_count_"+type).val();
		var extra_column = $("#mailchimp_product_extra_col_"+type).val();
		var tax_included = $('input:radio[name=include_tax_in_price_'+type+']:checked').val();

		$.ajax({
				data: { 
					id_lang : id_lang,
					id_currency : id_currency,
					id_products : id_products,
					tax_included : tax_included,
					extra_column : extra_column,
					products_per_row : products_per_row,
					action: 'addProductsToNewsletter',
				},
			dataType : 'json'
		}).success(function(json) 
		{
			tinymce.activeEditor.insertContent(json.content);
			delete json;
		});
	}

	function addProductToPreNewsletterList(id_product, type)
	{
		var id_lang = $('#mailchimp_add_product_'+id_product).val();
		$.ajax({
				data: { 
					id_lang : id_lang,
					type : type,
					id_product : id_product,
					action: 'addProductToPreNewsletterList',
				},
			dataType : 'json'
		}).success(function(json) 
		{
			$("#mailchimp_product_search_selected_"+json.type).append(json.content);
			delete json;
		});
	}

	function removeThisProductFromPreList(id_product)
	{
		$("#product_pre_newsletter_list_"+id_product).remove();
	}

	function update_div_class(id) {

	    if(id == "include_tax_in_price_yes_label_add")
	    {
	        document.getElementById("include_tax_in_price_yes_label_add").className = "btn btn-info";
	        document.getElementById("include_tax_in_price_no_label_add").className = "btn btn-default";
	    }
	    if(id == "include_tax_in_price_no_label_add")
	    {
	        document.getElementById("include_tax_in_price_no_label_add").className = "btn btn-info";
	        document.getElementById("include_tax_in_price_yes_label_add").className = "btn btn-default";
	    }

	    if(id == "include_tax_in_price_yes_label_edit")
	    {
	        document.getElementById("include_tax_in_price_yes_label_edit").className = "btn btn-info";
	        document.getElementById("include_tax_in_price_no_label_edit").className = "btn btn-default";
	    }
	    if(id == "include_tax_in_price_no_label_edit")
	    {
	        document.getElementById("include_tax_in_price_no_label_edit").className = "btn btn-info";
	        document.getElementById("include_tax_in_price_yes_label_edit").className = "btn btn-default";
	    }

	    if(id == "new_campaign_type_html_label")
	    {
	        document.getElementById("new_campaign_type_html_label").className = "btn btn-info";
	        document.getElementById("new_campaign_type_text_label").className = "btn btn-default";
	        document.getElementById("new_campaign_type_template_label").className = "btn btn-default";
	        tinyMCE.activeEditor.setContent('');
			$("#campaign_html_content_div").show();
	        $("#new_campaign_submit_buttons").show();
			$("#campaign_text_content_div").hide();
			$("#campaign_template_content_div").hide();
			$("#campaign_template_choice_div").hide();
			$("#adding_product_to_newsletter_header_add").show();
	        $("#adding_product_to_newsletter_div_add").hide();
	    }
	    if(id == "new_campaign_type_text_label")
	    {
	        document.getElementById("new_campaign_type_text_label").className = "btn btn-info";
	        document.getElementById("new_campaign_type_html_label").className = "btn btn-default";
	        document.getElementById("new_campaign_type_template_label").className = "btn btn-default";
			$("#campaign_text_content_div").show();
	        $("#new_campaign_submit_buttons").show();
	        $("#campaign_html_content_div").hide();
	        $("#adding_product_to_newsletter_header_add").hide();
	        $("#adding_product_to_newsletter_div_add").hide();
	        $("#campaign_template_content_div").hide();
	        $("#campaign_template_choice_div").hide();
	    }
	    if(id == "new_campaign_type_template_label")
	    {
	        document.getElementById("new_campaign_type_template_label").className = "btn btn-info";
	        document.getElementById("new_campaign_type_text_label").className = "btn btn-default";
	        document.getElementById("new_campaign_type_html_label").className = "btn btn-default";
	        $("#campaign_template_choice_div").show();
	        $("#campaign_template_content_div").hide();
	        $("#secondary_choose_template_button").hide();
			$("#campaign_text_content_div").hide();
	        $("#campaign_html_content_div").hide();
	        $("#new_campaign_submit_buttons").hide();
	        $("#mc_user_template_info").hide();
	        $("#adding_product_to_newsletter_header_add").hide();
	        $("#adding_product_to_newsletter_div_add").hide();
	    }
	}

	function chooseMailChimpTemplate()
	{
		var id_template = $('input[type=radio][name=template]:checked').attr('value');

		if(typeof id_template !== 'undefined')
		{
			$('#mailchimp_play_it_cool_modal').modal('show');
			$.ajax({
					data: {
						id_template: id_template,
						action : 'getTemplateSource',
					},
					dataType : 'json'
			  }).done(function(info) {
			  	var header;
			  	var content;
			  	var str = info["output"];
			  	var sections = info["sections"];

				if(sections.length > 0)
				{
					$('<div/>').html(str).find('*[mc\\:edit]').each(function () {
						var attr_name = $(this).attr('mc:edit');

						if(attr_name == "header" || attr_name == "header_content")
						{
							header = $(this).html();
							$('#mcedit_header_name').val(attr_name);
						}
						else
							header = header+'';

						if(attr_name == "main" || attr_name == "body" || attr_name == "content" || attr_name == "body_content" || attr_name == "main_content")
						{
							content = $(this).html();
							$('#mcedit_content_name').val(attr_name);
						}
						else
							content = content+'';
					});	// End of ForEach Loop

			        $("#campaign_template_content_div").show();
			        $("#new_campaign_submit_buttons").show();

					if(header == null || header == "undefined")
						$("#campaign_template_header_textarea").hide();
					else
					{
						$("#campaign_template_header_textarea").show();
				  		tinyMCE.get('campaign_template_header_textarea').setContent(header);
				  	}

				  	if(content != '')
				  		tinyMCE.get('campaign_template_content_textarea').setContent(content);
				  	else
				  		tinyMCE.get('campaign_template_content_textarea').setContent("Please create a campaign with this template on Mailchimp.com");
			        $("#campaign_html_content_div").hide();
				  	$("#campaign_text_content_div").hide();
			        $("#campaign_template_choice_div").hide();
			        $("#adding_product_to_newsletter_header_add").show();
			        hideAddingProductToNewsletterDiv();
			    }
			    else
		        	$('#mailchimp_template_error_modal').modal('show');

		        $('#mailchimp_play_it_cool_modal').modal('hide');
			});
		}
		else
			$('#mailchimp_template_error_modal').modal('show');
	}

	function increaseImageSize(id_img)
	{
		$('gallery_template_div_'+id_img).removeClass('col-xs-12 col-sm-6 col-md-3 col-lg-3').addClass('col-xs-12 col-sm-12 col-md-6 col-lg-6');
		var yourImg = document.getElementById('template_img_'+id_img);
		if(yourImg && yourImg.style) {
			yourImg.style.maxWidth = '300px';
		}
	}

	function normalImg(id_img) {
		$('gallery_template_div_'+id_img).removeClass('col-xs-12 col-sm-12 col-md-6 col-lg-6').addClass('col-xs-12 col-sm-6 col-md-3 col-lg-3');
		var yourImg = document.getElementById('template_img_'+id_img);
	    if(yourImg && yourImg.style) {
			yourImg.style.maxWidth = '150px';
		}
	}

	function selectTemplateOnClick(id_img)
	{
		$('input:radio[name="template"]').filter('[value="'+id_img+'"]').attr('checked', true);
	}

	function showNewTemplateInfo()
	{
		var blahblah = $("#mc_user_template_info").css( "display" );
		if(blahblah == "none")
			$("#mc_user_template_info").show(400);
		else
			$("#mc_user_template_info").hide(400);
	}

	function showNewTemplateInfoDocsTab()
	{
		var blahblah = $("#mc_template_info_content").css( "display" );
		if(blahblah == "none")
			$("#mc_template_info_content").show(400);
		else
			$("#mc_template_info_content").hide(400);
	}

	function showFullGallery()
	{
		$('#secondary_choose_template_button').modal('show');
		$("#template_gallery_two_div").show();
		$("#show_more_gallery").hide();
	}

	function hideFullGallery()
	{
		$("#template_gallery_two_div").hide();
		$("#show_more_gallery").show();
	}

	function addToList(id_list) {
		$("#add_to_list_spin_icon").show();
		$.ajax({
				data: {
					id_list: id_list,
					action : 'addToList'
				},
				dataType : 'json'
		  }).done(function(result) {
		  	if(result.status !='error')
		  	{
			  	$("#add_to_list_spin_icon").hide();
			  	$("#add_to_list_alert_success").show();
			  	$("#add_to_list_report_table_body").html(result.html);
				$("#add_to_list_report").show(300);
				setTimeout('$("#add_to_list_alert_success").hide(500)',3000);
			}
			else
				alert('Error - addToList');
		  });
	}

	function addToDetailedList(id_list) {
		$("#add_to_list_spin_icon").show();
		$.ajax({
				data: {
					id_list: id_list,
					action : 'addToDetailedList'
				},
				dataType : 'json'
		  }).done(function(result) {
		  	if(result.status !='error')
		  	{
			  	$("#add_to_list_spin_icon").hide();
			  	$("#add_to_list_alert_success").show();
			  	$("#add_to_list_report_table_body").html(result.html);
				$("#add_to_list_report").show(300);
				setTimeout('$("#add_to_list_alert_success").hide(500)', 3000);
			}
			else
				alert('Error - addToDetailedList');
		  });
	}

	function showSendCampaignModal(id_campaign){
		$('#mailchimp_send_campaign_modal').modal('show');
		$("#send_campaign_modal_campaign_id").val(id_campaign);
	}

	function showEditCampaignModal(id_campaign, type, id_template){
		$('#mailchimp_product_search_selected_add').html('');
		$('#mailchimp_product_search_selected_edit').html('');
		$('#edit_campaign_text_content_div').hide();
		$('#edit_campaign_html_content_div').hide();
		$('#edit_campaign_template_content_div').hide();
		$('#edit_campaign_error_div').hide();

		$('#type_campaign_edit').val(type);
		$('#id_campaign_edit').val(id_campaign);

		$.ajax({
				data: {
					id_template : id_template,
					id_campaign: id_campaign,
					action : 'getCampaignContent'
				},
				dataType : 'json'
		}).done(function(campaign) {
			if(type == 'regular')
			{
				tinyMCE.get('edit_campaign_html_content_textarea').setContent(campaign.html);
				$("#edit_campaign_html_content_div").show();
			}
			else if(type=='text')
			{
				$('#edit_campaign_text_content').val(campaign.text);
				$("#edit_campaign_text_content_div").show();
			}
			else if(type=='template')
			{
				$.ajax({
						data: {
							id_template : id_template,
							action : 'getTemplateSource'
						},
						dataType : 'json'
				}).done(function(template) {
					if(template.status == "error")
					{
						$('#edit_campaign_error_div').html("Sections not found. This may be caused because you did not create the campaign in the Prestashop module.");
						$('#edit_campaign_error_div').show();
					}
					else {
						var header;
					  	var content;
					  	var str = template.output;
					  	var sections = template.sections;

						if(sections.length > 0)
						{
							$('<div/>').html(str).find('*[mc\\:edit]').each(function () {
								var attr_name = $(this).attr('mc:edit');

								if(attr_name == "header" || attr_name == "header_content")
								{
									var id_header = $(this).attr('id');
									header = $(this).html();
									$('#edit_mcedit_header_name').val(attr_name);
								}
								else
									header = header+'';

								if(attr_name == "main" || attr_name == "body" || attr_name == "content" || attr_name == "body_content" || attr_name == "main_content")
								{
									var id_content = $(this).attr('id');
									content = $(campaign.html).find("#"+id_content)[0].outerHTML;

									$('#edit_mcedit_content_name').val(attr_name);
								}
								else
									content = content+'';
							});	// End of ForEach Loop
					        $("#campaign_template_content_div").show();

							if(header == null || header == "undefined")
								$("#edit_campaign_template_header").hide();
							else
							{
								$("#edit_campaign_template_header").show();
						  		tinyMCE.get('edit_campaign_template_header_textarea').setContent(header);
						  	}

						  	if(content != '' && content != null && content != 'undefined')
						  		tinyMCE.get('edit_campaign_template_content_textarea').setContent(content);
						  	else
						  		tinyMCE.get('edit_campaign_template_content_textarea').setContent("Div not found.");
					    	
					    	$('#edit_campaign_template_content_div').show();
					    }
					}
				});
			}
			$('#mailchimp_edit_campaign_modal').modal('show');	
		});
	}

	function sendCampaign() {
		
		var id_campaign = $("#send_campaign_modal_campaign_id").val();
		$('#mailchimp_play_it_cool_modal').modal('show');
		$.ajax({
				data: {
					id_campaign: id_campaign,
					action : 'sendCampaign'
				}
		  }).done(function(table) {
		  	if(table == "error")
		  		alert('Error - sendCampaign');
		  	else
		  	{
			  	refreshCampaignTable();
			  	timeout = setTimeout('refreshCampaignTable()', 3000);
			  	$('#mailchimp_play_it_cool_modal').modal('hide');
			}
		  });
	}

	function setVal(config_name, val) {
		if(config_name == "mailchimp_auto_sync_list_select")
			val = $("#sync_client_list").val();

		$.ajax({
				data: {
					config_name: config_name,
					val: val,
					action : 'setVal'
				}
		  }).done(function(result) {

		  	if(result != 'error')
		  	{
			  	$("#add_clients_success_alert").show();
				setTimeout('$("#add_clients_success_alert").hide(500)', 3000);
			}
			else
			{
				$("#add_clients_error_alert").show();
				setTimeout('$("#add_clients_error_alert").hide(500)', 3000);
			}
		  });
	}

	function showScheduleModal(id_campaign, title_campaign){
		$('#id_campaign_schedule').val(id_campaign);
		$('#span_campaign_title_schedule').text(title_campaign);
		$('#mailchimp_schedule_modal').modal('show');
	}

	function showCampaignModal(){
		$('#mailchimp_product_search_selected_add').html('');
		$('#mailchimp_product_search_selected_edit').html('');
		hideAddingProductToNewsletterDiv();
		$("#new_campaign_from_email").val(mailchimp_email_address);
		$('#mailchimp_campaign_modal').modal('show');
		$("#new_campaign_step1").show();
		$("#new_campaign_next_step_buttons").show();
		$("#newcampaign_client_list_div").show();
		$("#new_campaign_step2").hide();
		$("#new_campaign_step3").hide();
		$("#new_campaign_previous_step_buttons").hide();
		$("#new_campaign_submit_buttons").hide();
		$("#new_campaign_current_step").val(1);
	}

	function previousStepNewCampaign(){
		var current_step = parseInt(new_campaign_current_step.value, 10);
		var previous_step = current_step-1;
		$("#new_campaign_current_step").val(previous_step);
		if(previous_step == 2)
	  	{
	  		$("#new_campaign_step3").hide();
	  		$("#new_campaign_step2").show();
	  		$("#new_campaign_submit_buttons").hide();
	  		$("#new_campaign_next_step_buttons").show();
	  	}
	  	if(previous_step == 1)
	  	{
	  		$("#new_campaign_step2").hide();
	  		$("#newcampaign_client_list_div").show();
	  		$("#new_campaign_step1").show();
	  		$("#new_campaign_previous_step_buttons").hide();
	  		$("#new_campaign_submit_buttons").hide();
	  		$("#campaign_template_choice_div").hide();
	  		$("#new_campaign_next_step_buttons").show();
	  	}
	}

	function nextStepNewCampaign(){
		var current_step = parseInt(new_campaign_current_step.value, 10);
		var next_step = current_step+1;
		if(next_step == 2)
	  	{
	  		var value = $('#new_campaign_client_list').val();
	  		if(value != 'empty')
	  		{
		  		$("#newcampaign_client_list_div").hide();

		  		$("#new_campaign_step1").hide();
		  		$("#new_campaign_step2").show();
		  		$("#new_campaign_previous_step_buttons").show();
		  		$("#new_campaign_next_step_buttons").show();
		  		$("#new_campaign_current_step").val(next_step);
		  		$('#newcampaign_client_list_div').removeClass("has-error");
		  	}
		  	else
		  		$('#newcampaign_client_list_div').addClass("form-group has-error");

	  	}
	  	if(next_step == 3)
	  	{
	  		var value1 = $('#new_campaign_subject').val();
	  		var value2 = $('#new_campaign_from_email').val();
	  		var value3 = $('#new_campaign_from_name').val();
	  		var value4 = $('#new_campaign_to_name').val();

	  		if(value1 != '' && value2 != '' && value3 != '' && value4 != '')
	  		{
		  		tinyMCE.activeEditor.setContent('');
		  		tinyMCE.get('campaign_template_header_textarea').setContent('');
			  	tinyMCE.get('campaign_template_content_textarea').setContent('');
		  		$("#new_campaign_step2").hide();
		  		$("#new_campaign_step3").show();
		  		$("#new_campaign_next_step_buttons").hide();
		  		$("#new_campaign_submit_buttons").show();
		  		$("#campaign_html_content_div").show();
				$("#campaign_text_content_div").hide();
				$("#campaign_template_choice_div").hide();
				$("#campaign_template_content_div").hide();
				document.getElementById("new_campaign_type_html_label").className = "btn btn-info";
		        document.getElementById("new_campaign_type_text_label").className = "btn btn-default";
		        document.getElementById("new_campaign_type_template_label").className = "btn btn-default";
		        $("#new_campaign_current_step").val(next_step);

	    		$('#new_campaign_subject_div').removeClass("has-error");
	    		$('#new_campaign_from_email_div').removeClass("has-error");
	    		$('#new_campaign_from_name_div').removeClass("has-error");
	    		$('#new_campaign_to_name_div').removeClass("has-error");
		    }
		    else
		    {
		    	if(value1 == '')
		    		$('#new_campaign_subject_div').addClass("form-group has-error");
		    	if(value2 == '')
		    		$('#new_campaign_from_email_div').addClass("form-group has-error");
		    	if(value3 == '')
		    		$('#new_campaign_from_name_div').addClass("form-group has-error");
		    	if(value4 == '')
		    		$('#new_campaign_to_name_div').addClass("form-group has-error");
		    }
	  	}
	}

	function submitSchedule(){
		$('#mailchimp_play_it_cool_modal').modal('show');
		$.ajax({
				data: {
					schedule_time_date: schedule_campaign_form.datetimepicker_schedule.value+':00',
					id_campaign: schedule_campaign_form.id_campaign_schedule.value,
					action : 'scheduleCampaign'
				}
		  }).done(function(result) {
		  	if(result.status!='error')
		  	{
			  	refreshCampaignTable();
			  	$('#mailchimp_play_it_cool_modal').modal('hide');
			  	timeout = setTimeout('refreshCampaignTable()', 3000);
		  	}
			else
				alert('Error - scheduleCampaign');
		  });
	}

	function submitEditCampaign(){
		$('#mailchimp_play_it_cool_modal').modal('show');
		var id_campaign = $('#id_campaign_edit').val();
		var type = $('#type_campaign_edit').val();
		var content = '';
		var header = '';
		if(type == 'regular')
			content = tinyMCE.get('edit_campaign_html_content_textarea').getContent();
		else if(type == 'template')
		{
			var header_name = $('#edit_mcedit_header_name').val();
			var content_name = $('#edit_mcedit_content_name').val();
			header = tinyMCE.get('edit_campaign_template_header_textarea').getContent();
			content = tinyMCE.get('edit_campaign_template_content_textarea').getContent();
		}
		else
			content = $('#edit_campaign_text_content').val();
		$.ajax({
				data: {
					id_campaign: id_campaign,
					content: content,
					header : header,
					header_name : header_name,
					content_name : content_name,
					type: type,
					action : 'editCampaign'
				}
		}).done(function(result) {
		  	if(result.status!='error')
		  	{
		  		$('#edit_campaign_html_content_div').hide();
		  		$('#edit_campaign_text_content_div').hide();
		  		$('#edit_campaign_template_content_div').hide();
			  	$('#mailchimp_play_it_cool_modal').modal('hide');
		  	}
			else
				alert('Error - editCampaign');
		});
	}

	function submitCampaign(){
		var type;
		var content;
		var header;
		var segment = $('#new_campaign_segments').val();

		$('input[name=new_campaign_type]').each(function () {
			if ($(this).is(':checked'))
				type = $(this).val();
		});
		if(type=='regular')
		{
			$('#mailchimp_play_it_cool_modal').modal('show');

			content =tinyMCE.get('campaign_html_content_textarea').getContent();
			$.ajax({
					data: {
						id_list : new_campaign_client_list.value,
						campaign_type : type,
						subject : new_campaign_subject.value,
						from_email : new_campaign_from_email.value,
						from_name : new_campaign_from_name.value,
						to_name : new_campaign_to_name.value,
						content: content,
						id_segment : segment,
						action : 'createCampaign'
					},
				dataType : 'json'
			  }).done(function(result) {
			  	if(result.status!='error')
			  	{
				  	$('#new_campaign_subject').val("");
				  	$('#new_campaign_from_email').val("");
				  	$('#new_campaign_from_name').val("");
				  	$('#new_campaign_to_name').val("");
				  	$('#new_campaign_client_list').val("");
				  	$('#template').val("");
				  	$('input:radio[id=new_campaign_type_html]').prop('checked', true);
				  	refreshCampaignTable();
				  	$('#mailchimp_play_it_cool_modal').modal('hide');
				}
				else
				{
					alert(result.error);
					$('#mailchimp_play_it_cool_modal').modal('hide');
				}
			  });
		}

		if(type=='template')
		{
			$('#mailchimp_play_it_cool_modal').modal('show');
			var id_template = $('input[type=radio][name=template]:checked').attr('value');
			var template_type = $('input[type=radio][name=template]:checked').attr('id')
			header =tinyMCE.get('campaign_template_header_textarea').getContent();
			content =tinyMCE.get('campaign_template_content_textarea').getContent();
			var section_name_header = $("#mcedit_header_name").val();
			var section_name = $("#mcedit_content_name").val();
			$.ajax({
					data: {
						id_list : new_campaign_client_list.value,
						campaign_type : type,
						id_template : id_template,
						template_type : template_type,
						subject : new_campaign_subject.value,
						from_email : new_campaign_from_email.value,
						from_name : new_campaign_from_name.value,
						to_name : new_campaign_to_name.value,
						content: content,
						mcedit_header_content: header,
						section_name : section_name,
						section_name_header : section_name_header,
						id_segment : segment,
						action : 'createCampaign'
					},
				dataType : 'json'
			  }).done(function(result) {
			  	if(result.status!='error')
			  	{
				  	$('#new_campaign_subject').val("");
				  	$('#new_campaign_from_email').val("");
				  	$('#new_campaign_from_name').val("");
				  	$('#new_campaign_to_name').val("");
				  	$('#new_campaign_client_list').val("");
				  	$('#template').val("");
				  	$('input:radio[id=new_campaign_type_html]').prop('checked', true);
				  	refreshCampaignTable();
				  	$('#mailchimp_play_it_cool_modal').modal('hide');
			  	}
				else
				{
					alert(result.error);
					$('#mailchimp_play_it_cool_modal').modal('hide');
				}
			  });
		}
			
		if(type=='plaintext')
		{
			$('#mailchimp_play_it_cool_modal').modal('show');
			content = campaign_text_content.value;
			$.ajax({
					data: {
						id_list : new_campaign_client_list.value,
						campaign_type : type,
						subject : new_campaign_subject.value,
						from_email : new_campaign_from_email.value,
						from_name : new_campaign_from_name.value,
						to_name : new_campaign_to_name.value,
						content: content,
						id_segment : segment,
						action : 'createCampaign'
					},
				dataType : 'json'
			  }).done(function(result) {
			  	if(result.status!='error')
			  	{
				  	$('#new_campaign_subject').val("");
				  	$('#new_campaign_from_email').val("");
				  	$('#new_campaign_from_name').val("");
				  	$('#new_campaign_to_name').val("");
				  	$('#new_campaign_client_list').val("");
				  	$('#template').val("");
				  	$('input:radio[id=new_campaign_type_html]').prop('checked', true);
				  	refreshCampaignTable();
				  	$('#mailchimp_play_it_cool_modal').modal('hide');
			  	}
				else
				{
					alert(result.error);
					$('#mailchimp_play_it_cool_modal').modal('hide');
				}
			});
		}
	}

	function showOp(i)
	{
		var option = document.getElementById("new_seg_field_"+i).value;

		if(option=='mc_language')
		{
			$("#new_seg_op_aim_"+i).hide();
			$("#new_seg_op_gender_"+i).hide();
			$("#new_seg_op_age_"+i).hide();
			$("#new_seg_op_social_"+i).hide();
			$("#new_seg_op_lang_"+i).show();
		}
		else if(option=='aim')
		{
			$("#new_seg_op_lang_"+i).hide();
			$("#new_seg_op_gender_"+i).hide();
			$("#new_seg_op_age_"+i).hide();
			$("#new_seg_op_social_"+i).hide();
			$("#new_seg_op_aim_"+i).show();
		}
		else if(option=='social_gender')
		{
			$("#new_seg_op_aim_"+i).hide();
			$("#new_seg_op_lang_"+i).hide();
			$("#new_seg_op_age_"+i).hide();
			$("#new_seg_op_social_"+i).hide();
			$("#new_seg_op_gender_"+i).show();
		}
		else if(option=='social_age')
		{
			$("#new_seg_op_gender_"+i).hide();
			$("#new_seg_op_aim_"+i).hide();
			$("#new_seg_op_lang_"+i).hide();
			$("#new_seg_op_social_"+i).hide();
			$("#new_seg_op_age_"+i).show();
		}
		else if(option=='social_network')
		{
			$("#new_seg_op_gender_"+i).hide();
			$("#new_seg_op_aim_"+i).hide();
			$("#new_seg_op_lang_"+i).hide();
			$("#new_seg_op_age_"+i).hide();
			$("#new_seg_op_social_"+i).show();
		}
		else
		{
			if(i==1)
				$('#submitMailChimpSegements').attr("disabled", "disabled");
			$("#add_seg_condition_button_"+i).hide();
			$("#new_seg_op_gender_"+i).hide();
			$("#new_seg_op_aim_"+i).hide();
			$("#new_seg_op_lang_"+i).hide();
			$("#new_seg_op_age_"+i).hide();
			$("#new_seg_op_social_"+i).hide();
		}
		$("#add_seg_condition_button_"+i).hide();
		$("#new_seg_values_div_"+i).hide();
		$("#new_seg_op_gender_"+i).val('empty');
		$("#new_seg_op_aim_"+i).val('empty');
		$("#new_seg_op_lang_"+i).val('empty');
		$("#new_seg_op_age_"+i).val('empty');
		$("#new_seg_op_social_"+i).val('empty');
	}

	function showValue(i)
	{
		var field = document.getElementById("new_seg_field_"+i).value;
		var option_lang = document.getElementById("new_seg_op_lang_"+i).value;
		var option_aim = document.getElementById("new_seg_op_aim_"+i).value;
		var option_gender = document.getElementById("new_seg_op_gender_"+i).value;
		var option_age = document.getElementById("new_seg_op_age_"+i).value;
		var option_social = document.getElementById("new_seg_op_social_"+i).value;

		if(i==1 && option_lang =='empty' && option_aim =='empty' && option_gender =='empty' && option_age =='empty' && option_social =='empty')
		{
			$('#submitMailChimpSegements').attr("disabled", "disabled");
			$("#add_seg_condition_button_"+i).hide(300);
		}

		$("#new_seg_values_div_"+i).show();
		if(field=='mc_language' && option_lang !='empty')
		{
			$("#new_seg_value_age_"+i).hide();
			$("#new_seg_value_campaign_"+i).hide();
			$("#new_seg_value_gender_"+i).hide();
			$("#new_seg_value_social_"+i).hide();
			$("#new_seg_value_lang_"+i).show();
		}
		else if(field=='aim' && option_aim !='empty')
		{
			$("#new_seg_value_age_"+i).hide();
			$("#new_seg_value_gender_"+i).hide();
			$("#new_seg_value_social_"+i).hide();
			$("#new_seg_value_lang_"+i).hide();
			$("#new_seg_value_campaign_"+i).show();
		}
		else if(field=='social_gender' && option_gender !='empty')
		{
			$("#new_seg_value_age_"+i).hide();
			$("#new_seg_value_social_"+i).hide();
			$("#new_seg_value_lang_"+i).hide();
			$("#new_seg_value_campaign_"+i).hide();
			$("#new_seg_value_gender_"+i).show();
		}
		else if(field=='social_age' && option_age !='empty')
		{
			$("#new_seg_value_social_"+i).hide();
			$("#new_seg_value_lang_"+i).hide();
			$("#new_seg_value_campaign_"+i).hide();
			$("#new_seg_value_gender_"+i).hide();
			$("#new_seg_value_age_"+i).show();
		}
		else if(field=='social_network' && option_social !='empty')
		{
			$("#new_seg_value_lang_"+i).hide();
			$("#new_seg_value_campaign_"+i).hide();
			$("#new_seg_value_gender_"+i).hide();
			$("#new_seg_value_age_"+i).hide();
			$("#new_seg_value_social_"+i).show();
		}
		else
		{
			$("#new_seg_value_lang_"+i).hide();
			$("#new_seg_value_campaign_"+i).hide();
			$("#new_seg_value_gender_"+i).hide();
			$("#new_seg_value_age_"+i).hide();
			$("#new_seg_value_social_"+i).hide();
		}
		$("#new_seg_value_gender_"+i).val('empty');
		$("#new_seg_value_aim_"+i).val('empty');
		$("#new_seg_value_lang_"+i).val('empty');
		$("#new_seg_value_age_"+i).val('empty');
		$("#new_seg_value_social_"+i).val('empty');
	}

	function showAddSegButton(i)
	{
		if(i < 5)
			$("#add_seg_condition_button_"+i).show();
		if(i==1)
		{
			var age = document.getElementById("new_seg_value_age_1").value;
			var campaign = document.getElementById("new_seg_value_campaign_1").value;
			var lang = document.getElementById("new_seg_value_lang_1").value;
			var gender = document.getElementById("new_seg_value_gender_1").value;
			var social = document.getElementById("new_seg_value_social_1").value;
			if (age == "empty" && campaign == "empty" && lang == "empty" && gender == "empty" && social == "empty")
				$('#submitMailChimpSegements').attr("disabled", "disabled");
			else
				$('#submitMailChimpSegements').removeAttr("disabled");
		}	
	}

	function showListsSegment(i, id_list)
	{
		$('#mailchimp_play_it_cool_modal_lists').modal('show');
		closeMembersTable(i);
		$.ajax({
				data: {
					id_list: id_list,
					index : i,
					action : 'createSegmentTable'
				},
				dataType : 'json'
		  }).done(function(result) {
		  	if(result.status!='error')
		  	{
			  	$("#list_row_"+i).show(200);
			  	$("#list_row_data_"+i).show(200);
			  	$("#list_row_data_"+i).html(result.html);
			  	$('#mailchimp_play_it_cool_modal_lists').modal('hide');
			}
			else
				alert('Error - showListsSegment');
		  });
	}

	function showListMembers(i, id_list)
	{
		$('#mailchimp_play_it_cool_modal_lists').modal('show');
		closeSegmentTable(i);
		$.ajax({
				data: {
					id_list: id_list,
					index : i,
					action : 'createMembersTable'
				},
				dataType : 'json'
		  }).done(function(result) {
		  	if(result.status!='error')
		  	{
			  	$("#list_members_row_"+i).show(200);
			  	$("#list_members_row_data_"+i).show(200);
			  	$("#list_members_row_data_"+i).html(result.table);
			  	$('#mailchimp_play_it_cool_modal_lists').modal('hide');
			}
			else
				alert('Error - showListMembers');
		  });
	}

	function closeSegmentTable(i)
	{
		$("#list_row_"+i).hide(200);
		$('#list_row_data_'+i).empty();
	}

	function closeMembersTable(i)
	{
		$('#list_members_row_data_'+i).empty();
		$("#list_members_row_"+i).hide(200);
	}

	function show_new_seg_condition(id)
	{
		var next_id = parseInt(id, 10)+1;
		$("#add_seg_condition_button_"+id).hide(300);
		$("#new_seg_condition_"+next_id).show();
		$("#new_seg_field_"+next_id).show(300);

		if(id==4)
			$("#add_seg_condition_button_"+next_id).hide();
	}

	function deleteCampaign(id_campaign) {
		$('#mailchimp_play_it_cool_modal').modal('show');
		$.ajax({
				data: {
					id_campaign: id_campaign,
					action : 'deleteCampaign'
				},
				dataType : 'json'
		  }).done(function(result) {
		  	if(result.delete.status!='error')
		  	{
			  	$("#add_to_list_spin_icon").hide(300);
			  	$("#campaign_id_"+id_campaign).empty();
			  	$('#mailchimp_play_it_cool_modal').modal('hide');
		  	}
		  	else
				alert('Error - deleteCampaign');
		  });
	}

	function previewCampaignInModule(id_campaign)
	{
		$('#mailchimp_play_it_cool_modal').modal('show');
		$.ajax({
				data: {
					id_campaign: id_campaign,
					action : 'previewCampaignInModule'
				},
				dataType : 'json'
		  }).done(function(result) {
		  	if(result.status!='error')
		  	{
			  	$("#preview_campaign_iframe").show(300);
			  	$('#mailchimp_play_it_cool_modal').modal('hide');

			  	$('#campaign_preview_div_in_iframe').html('');
			  	$('#campaign_preview_div_in_iframe').html(result.html);

				var iframe = window.frames["previewCampaignIFrame"].document;
				iframe.open();
				iframe.write('');
				iframe.write(result.html);
				iframe.close();
			  	$('html, body').animate({
			        scrollTop: $("#preview_campaign_iframe").offset().top
			    }, 2000);
		    }
		  	else
				alert('Error - previewCampaignInfo');
		});
	}

	function hidePreview()
	{
	  	$("#preview_campaign_iframe").hide(500);
	}

	function showAddingProductToNewsletterDiv(type)
	{
		$("#adding_product_to_newsletter_div_"+type).show(500);
		$("#adding_product_to_newsletter_header_"+type).hide(500);
	}

	function hideAddingProductToNewsletterDiv(type)
	{
		$("#adding_product_to_newsletter_header_"+type).show(500);
		$("#adding_product_to_newsletter_div_"+type).hide(500);
	}

	function previewCampaign(id_campaign, type)
	{
	  	$('#mailchimp_preview_modal').modal('show');
	  	$('#preview_modal_id_campaign').val(id_campaign);
	  	$('#preview_modal_campaign_type').val(type);
	}

	function sendPreviewCampaign()
	{
		var id_campaign = document.getElementById("preview_modal_id_campaign").value;
		var type = document.getElementById("preview_modal_campaign_type").value;
		var email = $('#preview_employee_email').val();

	  	$('#mailchimp_play_it_cool_modal').modal('show');
		$.ajax({
				data: {
					id_campaign: id_campaign,
					email : email,
					type : type,
					action : 'previewCampaign'
				},
				dataType : 'json'
		  }).done(function(result) {

		  	if(result.complete == true)
			  	$('#mailchimp_play_it_cool_modal').modal('hide');
		  	else
				alert('Error - previewCampaignInfo');
		});
	}

	function viewCampaignInfo(id_campaign) {
		$('#mailchimp_play_it_cool_modal_stats').modal('show');
		$.ajax({
				data: {
					id_campaign: id_campaign,
					action : 'viewCampaign'
				},
				dataType : 'json'
		  }).done(function(result) {
		  	if(result.status!='error')
		  	{
			  	$("#individual_statistics_table_body").empty();
			  	$("#individual_statistics_table_body").html(result.html);
			  	$("#individual_statistics_table_div").show();
			  	$('#mailchimp_play_it_cool_modal_stats').modal('hide');
		  	}
		  	else
				alert('Error - viewCampaignInfo');
		  });
	}

	function replicateCampaign(id_campaign) {
		$('#mailchimp_play_it_cool_modal').modal('show');
		$.ajax({
				data: {
					id_campaign: id_campaign,
					action : 'replicateCampaign'
				},
				dataType : 'json'
		  }).done(function(result){
		  	if(result.status!='error')
		  	{
			  	refreshCampaignTable();
			  	timeout = setTimeout('refreshCampaignTable()', 3000);
			  	$('#mailchimp_play_it_cool_modal').modal('hide');
		  	}
		  	else
				alert('Error - replicateCampaign');
		  });
	}

	function refreshCampaignTable() {
		$.ajax({
				data: {
					action : 'refreshCampaignTable'
				},
				dataType : 'json'
		  }).done(function(result) {
		  	$("#campaign_table_body").empty();
		  	$("#campaign_table_body").html(result['table']);
		  });
	}

	function getListSegments() {
		var id_list = document.getElementById("new_campaign_client_list").value;
		if(id_list != 'empty')
		{
			$.ajax({
					data: {
						dataType : 'json',
						id_list: id_list,
						action : 'getSegments'
					},
					dataType : 'json'
			  }).done(function(result) {

			  	if(result.status!=='error')
		  		{
		  			if(result.count > 0)
		  			{
					  	$("#mc_list_segments_select_div").html(result.html);
					  	$("#mc_list_segments_select_div").show(200);
					}
					else
						$("#mc_list_segments_select_div").hide(200);
			  	}
			  	else
					alert('Error - getListSegments');
			  });
		}
		else
			$("#mc_list_segments_select_div").html('');
	}

	function ajaxfilemanager(field_name, url, type, win) {
		var ajaxfilemanagerurl = ad+"/ajaxfilemanager/ajaxfilemanager.php";
		switch (type) {
			case "image":
				break;
			case "media":
				break;
			case "flash": 
				break;
			case "file":
				break;
			default:
				return false;
	}

    tinyMCE.activeEditor.windowManager.open({
        url: ajaxfilemanagerurl,
        width: 782,
        height: 640,
        inline : "yes",
        close_previous : "no"
    },{
        window : win,
        input : field_name
    });
}