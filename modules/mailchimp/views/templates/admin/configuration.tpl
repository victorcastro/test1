{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{literal}
<script>
	current_id_tab = '{/literal}{$current_id_tab|intval}{literal}';
	admin_module_ajax_url = '{/literal}{$controller_url}{literal}';
	admin_module_controller = "{/literal}{$controller_name|escape:'htmlall':'UTF-8'}{literal}";
</script>
{/literal}

{if $ps_version == 0}
    <div class="bootstrap">
	{include file="./header.tpl"}
{/if}
	<!-- Module content -->
	<div id="modulecontent" class="clearfix" role="{$is_submit|escape:'htmlall':'UTF-8'}">
		<!-- Nav tabs -->
		<div class="col-lg-2">

			<div class="list-group">
				<a class="list-group-item"><i class="icon-info"></i> {l s='Version' mod='modname'} {$module_version|escape:'htmlall':'UTF-8'}</a>
				<a href="#documentation" class="list-group-item active" data-toggle="tab"><i class="icon-book"></i> {l s='Documentation' mod='mailchimp'}</a>
				<a href="#config" class="list-group-item" data-toggle="tab"><i class="icon-briefcase"></i> {l s='Campaigns' mod='mailchimp'}</a>
				{if $ping_flag neq 0 || $list_count neq 0}<a href="#lists" class="list-group-item" data-toggle="tab"><i class="icon-file-text-o"></i> {l s='Lists & Segments' mod='mailchimp'}</a>{/if}
				{if $ping_flag neq 0 || $list_count neq 0}<a href="#statistics" class="list-group-item" data-toggle="tab"><i class="icon-bar-chart"></i> {l s='Statistics' mod='mailchimp'}</a>{/if}
				<a href="#contacts" class="list-group-item" data-toggle="tab"><i class="icon-envelope"></i> {l s='Contact' mod='mailchimp'}</a>
			</div>
		</div>
		<!-- Tab panes -->
		<div class="tab-content col-lg-10">

			<div class="tab-pane active panel" id="documentation">
				{include file="./tabs/documentation.tpl"}
			</div>
   
			<div class="tab-pane panel" id="config">
                {include file="./tabs/config.tpl"}
			</div>

			<div class="tab-pane panel" id="lists">
                {include file="./tabs/lists.tpl"}
			</div>

			<div class="tab-pane panel" id="statistics">
                {include file="./tabs/statistics.tpl"}
			</div>
		
			{include file="./addons.tpl"}
		</div>
	</div>

{if $ps_version == 0}
	<!-- Manage translations -->
	{include file="./translations.tpl"}
</div>
{/if}
