{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<!-- Edit Campaign Modal -->
<div class="modal fade" id="mailchimp_edit_campaign_modal" tabindex="-1" role="dialog" aria-labelledby="edit_campaign_modal" aria-hidden="true">
	<div style="float:inherit;margin-left: auto;margin-right: auto;" class="modal-dialog col-xs-9 col-sm-9 col-md-9 col-lg-9">
		<div class="modal-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="edit_campaign_modal_title">{l s='Edit Campaign' mod='mailchimp'}</h4>
			</div>
			<div class="modal-body">
				<!-- Adding product to newsletter -->
                	{include file="./addProductToNewsletter.tpl" type="edit"}
                <!-- End of add product to newsletter -->
                <div class="clear">&nbsp;</div>
				<div class="clear">&nbsp;</div>
				<div id="edit_campaign_text_content_div" style="display:none;">
					<textarea rows="10" id="edit_campaign_text_content" name="edit_campaign_text_content"></textarea>
				</div>
				
				<div id="edit_campaign_html_content_div" style="display:none;">
					<div id="edit_campaign_html_content" name="edit_campaign_html_content">
						<textarea rows="10" class="edit_campaign_html_content_textarea" id="edit_campaign_html_content_textarea"></textarea>
					</div>
				</div>

				<div id="edit_campaign_template_content_div" style="display:none;">
					<div id="edit_campaign_template_header" name="edit_campaign_template_header">
						<textarea rows="5" class="edit_campaign_template_header_textarea" id="edit_campaign_template_header_textarea"></textarea>
					</div>
					<div id="edit_campaign_template_content" name="edit_campaign_template_content">
						<textarea rows="10" class="edit_campaign_template_content_textarea" id="edit_campaign_template_content_textarea"></textarea>
					</div>
				</div>
				<div id="edit_campaign_error_div" style="display:none;"></div>
			</div>
			<div class="modal-footer">
				<div id="edit_campaign_footer_buttons">
					<input type="hidden" value="" name="id_campaign_edit" id="id_campaign_edit">
					<input type="hidden" value="" name="type_campaign_edit" id="type_campaign_edit">
					<input type="hidden" name="edit_mcedit_header_name" id="edit_mcedit_header_name" value="">
                	<input type="hidden" name="edit_mcedit_content_name" id="edit_mcedit_content_name" value="">
					<button type="button" class="btn btn-default" data-dismiss="modal" onclick="submitEditCampaign();"><i class="process-icon-save"></i>{l s='Save' mod='mailchimp'}</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="process-icon-cancel"></i>{l s='Close' mod='mailchimp'}</button>
				</div>
			</div>
		</div>
	</div>
</div>