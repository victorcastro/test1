{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
	<tr>
		<th>{l s='Email' mod='mailchimp'}</th>
		<th>{l s='Language' mod='mailchimp'}</th>
		<th>
			{l s='Rating' mod='mailchimp'}
			<i class="icon-times" style="cursor:pointer;float:right;" onclick="closeMembersTable('{$index|escape:'htmlall':'UTF-8'}');"></i>
		</th>

	</tr>
	{foreach from=$data item=member}
	    <tr id="member_id_{$member.id|escape:'htmlall':'UTF-8'}">
			<td>{$member.email|escape:'htmlall':'UTF-8'}</td>
			<td>{$member.language|escape:'htmlall':'UTF-8'}</td>
			<td>{$member.member_rating|escape:'htmlall':'UTF-8'}</td>
		</tr>
	{/foreach}
</table>