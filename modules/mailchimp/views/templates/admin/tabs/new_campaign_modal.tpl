{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<!-- New Campaign Modal -->
<div class="modal fade" id="mailchimp_campaign_modal" tabindex="-1" role="dialog" aria-labelledby="campaign_modal" aria-hidden="true">
	<div style="float:inherit;margin-left: auto;margin-right: auto;" class="modal-dialog col-xs-9 col-sm-9 col-md-9 col-lg-9">
		<div class="modal-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="campaign_modal_title">{l s='Create Campaign' mod='mailchimp'}</h4>
				</div>
				<div class="modal-body">
					<div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="new_campaign_step1">
							<div id="newcampaign_client_list_div" class="form-group">
								<label for="new_campaign_client_list">{l s='Select a client list' mod='mailchimp'}</label>
								<select id="new_campaign_client_list" name="new_campaign_client_list" onChange="getListSegments();">
										<option value="empty">{l s='Please choose a list' mod='mailchimp'}</option>
									{for $i=0 to $list_count-1}
										<option value="{$lists.$i.id|escape:'htmlall':'UTF-8'}">{$lists.$i.name|escape:'htmlall':'UTF-8'}&nbsp;({$lists.$i.default_language|escape:'htmlall':'UTF-8'})</option>
									{/for}
								<select>
							</div>
							<div class="clear">&nbsp;</div>
							<div id="mc_list_segments_select_div"></div>
							<input type="hidden" value="1" id="new_campaign_current_step" />
							<div class="clear">&nbsp;</div>
                        </div>
                        <div id="new_campaign_step2">
							<div class="clear">&nbsp;</div>
							<div id="new_campaign_subject_div" class="form-group">
								<label for="new_campaign_subject">{l s='Campaign Subject' mod='mailchimp'}</label>
								<input type="text" id="new_campaign_subject" placeholder="{l s='Campaign Subject' mod='mailchimp'}" class="form-control"/>
							</div>
							<div class="clear">&nbsp;</div>
							<div id="new_campaign_from_email_div" class="form-group">
								<label for="new_campaign_from_email">{l s='Email Address for your Campaign Message' mod='mailchimp'}</label>
								<input type="text" id="new_campaign_from_email" placeholder="{l s='Email Address for your Campaign Message' mod='mailchimp'}" class="form-control" value=""/>
								<span class="help-block">{l s='An email address must contain a single @ and must be verified to send from it' mod='mailchimp'}</span>
							</div>
							<div class="clear">&nbsp;</div>
							<div id="new_campaign_from_name_div" class="form-group">
								<label for="new_campaign_from_name">{l s='Name for your Campaign Message (not an email address)' mod='mailchimp'}</label>
								<input type="text" id="new_campaign_from_name" class="form-control" placeholder="{l s='Name for your Campaign Message (not an email address)' mod='mailchimp'}"/>
							</div>
							<div class="clear">&nbsp;</div>
							<div id="new_campaign_to_name_div" class="form-group">
								<label for="new_campaign_to_name">{l s='Name Recipients Will See (not an email address)' mod='mailchimp'}</label>
								<input type="text" id="new_campaign_to_name" class="form-control" placeholder="{l s='Name Recipients Will See (not an email address)' mod='mailchimp'}"/>
							</div>
							<div class="clear">&nbsp;</div>
						</div>
					</div>
					<div id="new_campaign_step3">
						<label for="new_campaign_type_div">{l s='Type of Newsletter (Please, choose just one type of newsletter)' mod='mailchimp'}&nbsp;:</label>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="buttons" id="new_campaign_type_div">
                            	<label style="width:33%;" class="btn btn-info" id="new_campaign_type_html_label" onclick="update_div_class('new_campaign_type_html_label');">
	                                <input type="radio" name="new_campaign_type" id="new_campaign_type_html" value="regular" checked='checked'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{l s='Regular (HTML Content)' mod='mailchimp'}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                            </label>

	                            <label style="width:33%;" class="btn btn-default" id="new_campaign_type_text_label" onclick="update_div_class('new_campaign_type_text_label');">
	                                <input type="radio" name="new_campaign_type" id="new_campaign_type_text" value="plaintext" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{l s='Plain Text (no pictures or formatting)' mod='mailchimp'}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                            </label>

	                            <label style="width:33%;" class="btn btn-default" id="new_campaign_type_template_label" onclick="update_div_class('new_campaign_type_template_label');">
	                                <input type="radio" name="new_campaign_type" id="new_campaign_type_template" value="template" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{l s='Template (Use a template)' mod='mailchimp'}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                            </label>
                            </div>
							<div class="clear">&nbsp;</div>
							<div class="clear">&nbsp;</div>
							<div class"add">
	                            <!-- Adding product to newsletter -->
	                            {include file="./addProductToNewsletter.tpl" type="add"}
	                            <!-- End of add product to newsletter -->
	                        </div>
                        </div>
                        <div id="campaign_template_choice_div">
                        	<button type="button" class="btn btn-default" onclick="chooseMailChimpTemplate();" style="float:right;"><i class="icon-arrow-right"></i>&nbsp;{l s='Choose Template' mod='mailchimp'}</button>
                    		<div class="clear">&nbsp;</div>
                        	<div style="height:600px;overflow:scroll;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        		<button type="button" class="btn btn-default" onclick="showNewTemplateInfo()"><i class="icon-info-circle"></i>&nbsp;{l s='How to use customised templates from Mailchimp' mod='mailchimp'}</button>
	                    		<div class="clear">&nbsp;</div>
	                        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mc_user_template_info">
									<p><a href="http://kb.mailchimp.com/templates/code/export-templates-html" target="_blank">{l s='Mailchimp article on How To Export a HTML Template.' mod='mailchimp'}</a></p>
									<p><a href="http://kb.mailchimp.com/templates/code/import-an-html-template" target="_blank">{l s='Mailchimp article on How To Import a HTML Template.' mod='mailchimp'}</a></p>
									<p>
										{l s='To create campaigns using templates that you have created on the Mailchimp site, you need to create the templates by the "Code Your Own" option.' mod='mailchimp'}
										{l s='The reason for this is that you need to notify the module of where the content goes. (Header is also optional)' mod='mailchimp'}
										{l s='If you cannot code your own template, please use the following steps :' mod='mailchimp'}
									</p>
									<ul>
										<li>{l s='Create a template using the "Basic" or "Themes" as your starting point.' mod='mailchimp'}</li>
										<li>{l s='When you have finished designing your template, save it under a name that is temporary.' mod='mailchimp'}</li>
										<li>{l s='In your Mailchimp interface, click "Templates" and you can see a list of your templates.' mod='mailchimp'}</li>
										<li>{l s='Beside the "Edit" button, click the dropdown arrow and select "Export".' mod='mailchimp'}</li>
										<li>{l s='Click "Got It, Export Template"' mod='mailchimp'}</li>
										<li>{l s='A HTML file will download. Remember where this file is located.' mod='mailchimp'}</li>
										<li>{l s='Now you will create the new template by clicking "New Template". Choose the "Import HTML" option.' mod='mailchimp'}</li>
										<li>{l s='Locate the Template File, give the template a name & click "Upload".' mod='mailchimp'}</li>
										<li>{l s='Now you need to identify the element with the MailChimp tag.' mod='mailchimp'}</li>
										<li>{l s='Locate where you want to have the main content of the campaign, it can be a <div>/<td> and in this tag, you need to copy the following :' mod='mailchimp'}</li>
										<p class="well">{literal}mc:edit="content"{/literal}</p>
										<li>{l s='So your code will look like this :' mod='mailchimp'}</li>
										<p class="well">&#60;div mc:edit="content"&#62;{l s='This will be your content.' mod='mailchimp'}&#60;&#47;div&#62;</p>
										<li>{l s='Save your new template.' mod='mailchimp'}</li>
										<li>{l s='You can also do the same for an area called "header".' mod='mailchimp'}</li>
									</ul>
									<div class="clear">&nbsp;</div>
								</div>

	                        	{if $user_templates}
	                        		<p><b>{l s='User Templates' mod='mailchimp'}</b></p>
	                        		{foreach from=$user_templates item=foo}
	                        			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="float:left;padding:3px;" onmouseover="increaseImageSize('{$foo.id|escape:'htmlall':'UTF-8'}')" onmouseout="normalImg('{$foo.id|escape:'htmlall':'UTF-8'}')" id="gallery_template_div_{$foo.id|escape:'htmlall':'UTF-8'}">
	                        				<p style="text-align:center;">{$foo.name|escape:'htmlall':'UTF-8'}</p>
	                        				<center><img src="{$foo.preview_image|escape:'htmlall':'UTF-8'}" style="max-width:150px;" id="template_img_{$foo.id|escape:'htmlall':'UTF-8'}" onclick="selectTemplateOnClick('{$foo.id|escape:'htmlall':'UTF-8'}')"></center>
	                        				<div class='clearfix'> </div>
	                        				<p style="text-align:center;"><input type="radio" name="template" value="{$foo.id|escape:'htmlall':'UTF-8'}" id="user_{$foo.id|escape:'htmlall':'UTF-8'}"></p>
	                        			</div>
	                        		{/foreach}
	        						<div class='clearfix'> </div>
	                        	{/if}
	                        	
	                        	{if $gallery_templates}<p><b>{l s='Gallery Templates' mod='mailchimp'}</b></p>
	                        		<div id="show_more_gallery">
	                        			<button type="button" class="btn btn-default" onclick="showFullGallery();"><i class="icon-plus-square-o"></i>&nbsp;&nbsp;{l s='Show all' mod='mailchimp'}</button>
	                        		</div>
	        						<div class='clearfix'></div>
	                        		{foreach from=$gallery_templates item=foo}
	                        			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="float:left;padding:3px;" onmouseover="increaseImageSize('{$foo.id|escape:'htmlall':'UTF-8'}')" onmouseout="normalImg('{$foo.id|escape:'htmlall':'UTF-8'}')" id="gallery_template_div_{$foo.id|escape:'htmlall':'UTF-8'}">
	                        				<p style="text-align:center;">{$foo.name|escape:'htmlall':'UTF-8'}</p>
	                        				<center><img src="{$foo.preview_image|escape:'htmlall':'UTF-8'}" style="max-width:150px;" id="template_img_{$foo.id|escape:'htmlall':'UTF-8'}" onclick="selectTemplateOnClick('{$foo.id|escape:'htmlall':'UTF-8'}')"></center>
	                        				<div class='clearfix'> </div>
	                        				<p style="text-align:center;"><input type="radio" name="template" value="{$foo.id|escape:'htmlall':'UTF-8'}" id="gall_{$foo.id|escape:'htmlall':'UTF-8'}"></p>
	                        			</div>
	                        		{/foreach}
	        						<div class='clearfix'></div>
	                        		<div id="template_gallery_two_div" style="display:none;">
	                        			<button type="button" class="btn btn-default" onclick="hideFullGallery();"><i class="icon-minus-square-o"></i>&nbsp;&nbsp;{l s='Hide' mod='mailchimp'}</button>
	        							<div class="clear">&nbsp;</div>
	                        			{foreach from=$gallery_templates_two item=foo}
		                        			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="float:left;padding:3px;" onmouseover="increaseImageSize('{$foo.id|escape:'htmlall':'UTF-8'}')" onmouseout="normalImg('{$foo.id|escape:'htmlall':'UTF-8'}')" id="gallery_template_div_{$foo.id|escape:'htmlall':'UTF-8'}">
			                        				<p style="text-align:center;">{$foo.name|escape:'htmlall':'UTF-8'}</p>
			                        				<center><img src="{$foo.preview_image|escape:'htmlall':'UTF-8'}" style="max-width:150px;" id="template_img_{$foo.id|escape:'htmlall':'UTF-8'}" onclick="selectTemplateOnClick('{$foo.id|escape:'htmlall':'UTF-8'}')"></center>
			                        				<div class="clear">&nbsp;</div>
			                        				<p style="text-align:center;"><input type="radio" name="template" value="{$foo.id|escape:'htmlall':'UTF-8'}"></p>
	        										<div class="clear">&nbsp;</div>
		                        			</div>
		                        		{/foreach}
	                        		</div>
	                        	{/if}
	                        	<input type="hidden" name="mcedit_header_name" id="mcedit_header_name" value="">
	                        	<input type="hidden" name="mcedit_content_name" id="mcedit_content_name" value="">
                        	</div>
                        </div>
                    	<div id="campaign_template_content_div">
                    		<label>{l s='Enter the content that will appear in the header of your newsletter :' mod='mailchimp'}</label>
                    		<small>{l s='We advise that you do not put products in this section' mod='mailchimp'}</small>
							<div id="campaign_template_header_textarea">
								<textarea rows="3" class="campaign_template_header_textarea" id="campaign_template_header_textarea"></textarea>
							</div>
							<div class="clear">&nbsp;</div>
							<label>{l s='Enter the content that will be the main content in your newsletter :' mod='mailchimp'}</label>
                    		<textarea rows="20" class="campaign_template_content_textarea" id="campaign_template_content_textarea"></textarea>
                    	</div>

        				<div class="clear">&nbsp;</div>

						<div id="campaign_text_content_div">
							<label for="campaign_text_content">{l s='Enter the text that will be in your newsletter :' mod='mailchimp'}</label>
							<textarea rows="10" id="campaign_text_content" name="campaign_text_content"></textarea>
						</div>

						<div id="campaign_html_content_div">
							<label>{l s='Enter the content that will be the main content in your newsletter :' mod='mailchimp'}</label>
							<div id="campaign_html_content" name="campaign_html_content">
								<textarea rows="25" class="campaign_html_content_textarea" id="campaign_html_content_textarea"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div id="new_campaign_footer_buttons">
						<div id="new_campaign_previous_step_buttons">
							<button type="button" class="btn btn-default" onclick="previousStepNewCampaign();" style="float:left;"><i class="icon-arrow-left"></i>&nbsp;{l s='Previous' mod='mailchimp'}</button>
						</div>
						<div id="new_campaign_next_step_buttons">
							<button type="button" class="btn btn-default" onclick="nextStepNewCampaign();">{l s='Next' mod='mailchimp'}&nbsp;<i class="icon-arrow-right"></i></button>
						</div>
						<div id="new_campaign_submit_buttons">
							<button type="button" class="btn btn-default" data-dismiss="modal" onclick="submitCampaign();"><i class="process-icon-save"></i>{l s='Save' mod='mailchimp'}</button>
							<button type="button" class="btn btn-default" data-dismiss="modal"><i class="process-icon-cancel"></i>{l s='Close' mod='mailchimp'}</button>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>