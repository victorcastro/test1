{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*} 
<table>
	<tr>
		{foreach from=$products key=k item=foo}
			{if $k % $products_per_row eq 0}
				<tr>
			{/if}
					<td>
						<img src="{$foo.image|escape:'htmlall':'UTF-8'}" style="max-width:200px;max-height:200px;" alt="{$foo.name|escape:'htmlall':'UTF-8'}">
					</td>
					<td>
						<div id="product_{$foo.id_product|escape:'htmlall':'UTF-8'}" style="max-width:400px;">
						    <a href="{$foo.link|escape:'htmlall':'UTF-8'}" target="_blank">
						    	<div>
							    	<center>
										<p><b>{$foo.name|escape:'htmlall':'UTF-8'}</b></p>
										{if $foo.id_specific_price neq 0}<span style="background-color:red;color:#FFFFFF;"><b>{l s='On Sale' mod='mailchimp'}</b></span>{/if}
										<p>{$foo.display_price|escape:'htmlall':'UTF-8'}</p>
										<button> <a href="{$foo.link|escape:'htmlall':'UTF-8'}" target="_blank">{l s='View Product' mod='mailchimp'}</a></button>
									</center>
								</div>
							</a>
						</div>
					</td>
			{if $extra_column eq 'yes'}<td>{l s='Extra text here.' mod='mailchimp'}</td>{/if}
			{if ($k+1) % $products_per_row eq 0}
				</tr>
			{/if}
		{/foreach}
	</tr>
</table>