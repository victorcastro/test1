{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<h3>
	<i class="icon-link"></i> {l s='Mailchimp Campaigns' mod='mailchimp'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small>
</h3>
 {$alert_config}<!-- Escaped in the method that is called -->
 {if $ping_flag neq 0 || $list_count neq 0}
<!-- Campaigns -->
	<div class="well" id="campaigns_div">
		<h3>
			<i class="icon-link"></i> {l s='Campaigns' mod='mailchimp'}
		</h3>

		<p>{l s='Here is a list of your campaigns. If a campaign has not been sent, you can choose to modify, delete or send it. If the campaign is sent, you can delete it or duplicate it.' mod='mailchimp'}</p>
		<span style="float:right;cursor:pointer;" onclick="refreshCampaignTable();"><p><i class="icon-refresh"></i>&nbsp;&nbsp;{l s='Refresh Table' mod='mailchimp'}</p></span>
		<div id="campaign_table_div">
			<table id='campaign_table' cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>{l s='View' mod='mailchimp'}</th>
						<th>{l s='Test' mod='mailchimp'}</th>
						<th>{l s='Title' mod='mailchimp'}</th>
						<th>{l s='Subject' mod='mailchimp'}</th>
						<th>{l s='Type' mod='mailchimp'}</th>
						<th>{l s='Status' mod='mailchimp'}</th>
						<th>{l s='Created' mod='mailchimp'}</th>
						<th>{l s='Sent/Scheduled' mod='mailchimp'}</th>
						<th>{l s='Actions' mod='mailchimp'}</th>
					</tr>
				</thead>
				<tbody id="campaign_table_body" name="campaign_table_body">
					{include file="./campaignTable.tpl"}
				</tbody>
			</table>
		</div>
		<center>
			<button type="button" class="btn btn-default btn-lg" onclick="showCampaignModal();">
				{l s='Create a New Campaign' mod='mailchimp'}
			</button>
		</center>
	</div>
	<div class="clear">&nbsp;</div>
	<div id="preview_campaign_iframe" style="display:none;margin-top: 30px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<button id="mailchimp_close_preview" onclick="hidePreview();" class="btn btn-default">{l s='Hide' mod='mailchimp'}</button>
		<iframe src="../modules/mailchimp/previewCampaign.php" name="previewCampaignIFrame" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<p>{l s='Your browser does not support iframes.' mod='mailchimp'}</p>
		</iframe>
	</div>
	
	{include file="./preview_campaign_modal.tpl"}
	{include file="./new_campaign_modal.tpl"}
	{include file="./edit_campaign_modal.tpl"}
	{include file="./send_campaign_modal.tpl"}
	{include file="./schedule_campaign_modal.tpl"}
	<div class="clear">&nbsp;</div>
{/if}
<form role="form" action="{$requestUri|escape:'htmlall':'UTF-8'}" method="post">
<!-- API Key -->
	<div id="api-key-div" class="well">
		<h3>
			<i class="icon-link"></i> {l s='API Key' mod='mailchimp'}
		</h3>
		{if $api_key_error eq 1}
			<div class="alert alert-danger">{l s='There is a problem with your API key. Please verify that it is correct.' mod='mailchimp'}</div>
		{/if}
		<label for="api_key">{l s='API Key :' mod='mailchimp'}</label>
		<input type="text" id="api_key" name="api_key" value="{$api_key|escape:'htmlall':'UTF-8'}">
		<div class="clear">&nbsp;</div>
		<p>{l s='The MailChimp API key is provided to you by MailChimp.' mod='mailchimp'}&nbsp;<a href="https://login.mailchimp.com/signup?" target="_blank">{l s='Register with MailChimp' mod='mailchimp'}</a></p>
		<p>{l s='Your Mailchimp API key is located in your Mailchimp Account Settings under "Extras -> API Keys".' mod='mailchimp'}</p>
		<p><a href="http://kb.mailchimp.com/accounts/management/about-api-keys" target="_blank">{l s='More information on API' mod='mailchimp'}</a></p>
		<div class="clear">&nbsp;</div>
		<label for="mailchimp_email_address">{l s='Email address associated with your Mailchimp account :' mod='mailchimp'}</label>
		<input type="text" id="mailchimp_email_address" name="mailchimp_email_address" value="{$mailchimp_email_address|escape:'htmlall':'UTF-8'}">
		<div class="clear">&nbsp;</div>
		<center>
			<input type="submit" name="submitMailChimp" value="{l s='Update settings' mod='mailchimp'}" class="btn btn-default" />
		</center>
	</div>
</form>

<!-- Executing Code Modal -->
<div class="modal fade" id="mailchimp_play_it_cool_modal" tabindex="-1" role="dialog" aria-labelledby="play_it_cool_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
				<div class="modal-body">
					<p style="text-align:center;">
						<img src="{$module_dir|escape:'htmlall':'UTF-8'}/logo.png"><br /><br />
						<i class="icon-spinner icon-spin"></i><br />
						{l s='Please Wait...' mod='mailchimp'}
					</p>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Template Error Modal -->
<div class="modal fade type-error" id="mailchimp_template_error_modal" tabindex="-1" role="dialog" aria-labelledby="template_error_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="template_error_modal_title">{l s='Template Error' mod='mailchimp'}<span id="span_campaign_title_template_error"></span></h4>
			</div>
			<div class="modal-body">
				<p style="text-align:center;">
					<img src="{$module_dir|escape:'htmlall':'UTF-8'}/logo.png"><br /><br />
					<i class="icon-frown-o"></i><br />
					{l s='There is a problem finding the content div in your template.' mod='mailchimp'}
					{l s='Please consult the documentation to create a compatible template on Mailchimp or choose another template.' mod='mailchimp'}
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="process-icon-cancel"></i>{l s='Close' mod='mailchimp'}</button>
			</div>
		</div>
	</div>
</div>

{literal}
<script>

var lang = "{/literal}{$lang|escape:'htmlall':'UTF-8'}{literal}";
var ps_url = "{/literal}{$ps_url|escape:'htmlall':'UTF-8'}{literal}";
var iso = 'en';
var ad = "{/literal}{$ad|escape:'htmlall':'UTF-8'}{literal}";
var pathCSS = "{/literal}{$pathCSS|escape:'htmlall':'UTF-8'}{literal}";
var mailchimp_email_address = "{/literal}{$mailchimp_email_address|escape:'htmlall':'UTF-8'}{literal}";
</script>
{/literal}