{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

{for $foo=0 to $stats_info.count}
    <tr id="stats_id_{$stats_info.campaign.$foo.id|escape:'htmlall':'UTF-8'}">
		<td>
			<i class="icon-eye" title="{l s='View' mod='mailchimp'}" onclick="viewCampaignInfo('{$stats_info.campaign.$foo.id|escape:'htmlall':'UTF-8'}');" style="cursor:pointer;"></i>
		</td>
		<td>{$stats_info.campaign.$foo.title|escape:'htmlall':'UTF-8'}</td>
		<td>{$stats_info.campaign.$foo.subject|escape:'htmlall':'UTF-8'}</td>
		<td>{$stats_info.summary.$foo.emails_sent|escape:'htmlall':'UTF-8'}</td>
		<td>{$stats_info.opened.$foo.total|escape:'htmlall':'UTF-8'}</td>
		<td>{$stats_info.summary.$foo.hard_bounces|escape:'htmlall':'UTF-8'}</td>
		<td>{$stats_info.summary.$foo.soft_bounces|escape:'htmlall':'UTF-8'}</td>
		<td>{$stats_info.summary.$foo.timeseries.0.timestamp|escape:'htmlall':'UTF-8'}</td>
		<td>{$stats_info.summary.$foo.unique_clicks|escape:'htmlall':'UTF-8'}</td>
		<td>{$stats_info.summary.$foo.facebook_likes|escape:'htmlall':'UTF-8'}</td>
	</tr>
{/for}