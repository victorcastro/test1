{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
	<h3>
		<i class="icon-link"></i> {l s='Mailchimp Statistics' mod='mailchimp'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small>
	</h3>
	{if $ping_flag neq 0 || $list_count neq 0}
		<div id="statistics_table_div" class="col-sm-12 col-md-12 col-lg-12">
			<table id='statistics_table' cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>{l s='View' mod='mailchimp'}</th>
						<th>{l s='Title' mod='mailchimp'}</th>
						<th>{l s='Subject' mod='mailchimp'}</th>
						<th>{l s='Emails Sent' mod='mailchimp'}</th>
						<th>{l s='Emails Opened' mod='mailchimp'}</th>
						<th>{l s='Hard Bounces' mod='mailchimp'}</th>
						<th>{l s='Soft Bounces' mod='mailchimp'}</th>
						<th>{l s='Time Sent' mod='mailchimp'}</th>
						<th>{l s='Clicks' mod='mailchimp'}</th>
						<th>{l s='Facebook Likes' mod='mailchimp'}</th>
					</tr>
				</thead>
				<tbody id="statistics_table_body" name="statistics_table_body">
					{include file="./statisticsTable.tpl"}
				</tbody>
			</table>
		</div>
		<div class="clear">&nbsp;</div>

		<div id="individual_statistics_table_div" style="display:none;">
			<table id='individual_statistics_table' cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>{l s='Advise Type' mod='mailchimp'}</th>
						<th>{l s='Message' mod='mailchimp'}</th>
					</tr>
				</thead>
				<tbody id="individual_statistics_table_body" name="individual_statistics_table_body">

				</tbody>
			</table>
		</div>

		<div class="clear">&nbsp;</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<h3>{l s='Hard Bounces' mod='mailchimp'}</h3>
			<ul>
				<li>{l s='Recipient email address does not exist.' mod='mailchimp'}</li>
				<li>{l s='Domain name does not exist.' mod='mailchimp'}</li>
				<li>{l s='Recipient email server has completely blocked delivery.' mod='mailchimp'}</li>
			</ul>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<h3>{l s='Soft Bounces' mod='mailchimp'}</h3>
			<ul>
				<li>{l s='Mailbox is full (over quota).' mod='mailchimp'}</li>
				<li>{l s='Recipient email server is down or offline.' mod='mailchimp'}</li>
				<li>{l s='Email message is too large.' mod='mailchimp'}</li>
			</ul>
		</div>
		<div class="clear">&nbsp;</div>
		<div class="clear">&nbsp;</div>
	{/if}

	<div class="modal fade" id="mailchimp_play_it_cool_modal_stats" tabindex="-1" role="dialog" aria-labelledby="play_it_cool_modal_stats" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
				<div class="modal-body">
					<p style="text-align:center;">
						<img src="{$module_dir|escape:'htmlall':'UTF-8'}/logo.png"><br /><br />
						<i class="icon-spinner icon-spin"></i><br />
						{l s='Please Wait...' mod='mailchimp'}
					</p>
				</div>
			</form>
		</div>
	</div>
</div>