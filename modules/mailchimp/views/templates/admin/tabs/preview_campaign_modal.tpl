{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<div class="modal fade" id="mailchimp_preview_modal" tabindex="-1" role="dialog" aria-labelledby="preview_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div id="mailchimp_campaign_preview_body">
				<div class="clear">&nbsp;</div>
				<div class='row col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2'>
					<label for="preview_employee_email">{l s='Choose an email address to receive a preview of the campaign' mod='mailchimp'}</label>
				</div>
				<div class='row col-xs-12 col-sm-12 col-md-12 col-lg-12'></div>
				<div class='row col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2'>
					<select name="preview_employee_email" id="preview_employee_email">
						{foreach from=$employee_emails item=foo}
							<option value="{$foo.email|escape:'htmlall':'UTF-8'}">{$foo.email|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
				</div>
				<div class='row col-xs-12 col-sm-12 col-md-12 col-lg-12'></div>
				<input type="hidden" id="preview_modal_id_campaign" value="">
				<input type="hidden" id="preview_modal_campaign_type" value="">
				<div class="clear">&nbsp;</div>
				<div class="clear">&nbsp;</div>
				<div class="clear">&nbsp;</div>
				<center>
					<button type="button" class="btn btn-default" data-dismiss="modal" onclick="sendPreviewCampaign();">{l s='Send Preview' mod='mailchimp'}</button>
				</center>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
	</div>
</div>