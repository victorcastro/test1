{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<!-- Schedule Campaign Modal -->
<div class="modal fade" id="mailchimp_schedule_modal" tabindex="-1" role="dialog" aria-labelledby="schedule_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form role="form" class="form-inline" enctype="multipart/form-data" action="{$requestUri|escape:'htmlall':'UTF-8'}" method="post" id="schedule_campaign_form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="schedule_modal_title">{l s='Schedule Campaign' mod='mailchimp'}&nbsp;-&nbsp;<span id="span_campaign_title_schedule"></span></h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="id_campaign_schedule" value=""/>

					<div class="container">
						<div class="row">
							<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>
								<label for="datetimepicker_schedule">{l s='Choose a date to schedule the newsletter.' mod='mailchimp'}</label>
							</div>
							<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>
								<input type='text' class="" id='datetimepicker_schedule'/>
							</div>
							<script type="text/javascript">
								$(function () {
									$('#datetimepicker_schedule').datetimepicker({ dateFormat: 'yy-mm-dd'});
								});
							</script>
						</div>
					</div>
				</div>
			</form>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="submitSchedule();"><i class="process-icon-save"></i>{l s='Save' mod='mailchimp'}</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="process-icon-cancel"></i>{l s='Close' mod='mailchimp'}</button>
			</div> 
		</div>
	</div>
</div>