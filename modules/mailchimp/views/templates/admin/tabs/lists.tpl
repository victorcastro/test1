{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<h3>
	<i class="icon-link"></i> {l s='Mailchimp Lists and Segments Configuration' mod='mailchimp'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small>
</h3>
{$alert_config}<!-- Escaped in the method that is called -->
{if $ping_flag neq 0 || $list_count neq 0}
	<div class="alert alert-success" role="alert" style="display:none;" id="add_clients_success_alert">{l s="Updated" mod='mailchimp'}</div>
    <div class="alert alert-danger" role="alert" style="display:none;" id="add_clients_error_alert">{l s="There was a problem" mod='mailchimp'}</div>
    
	<form role="form" action="{$requestUri|escape:'htmlall':'UTF-8'}" method="post">
	<!-- Automatic Sync Lists -->
	<div class="well" id="auto_list_sync_div" {if $ping_flag eq 0 || $list_count eq 0}style="display:none;"{/if}>
		<h3>
			<i class="icon-refresh"></i> {l s='Automatic Sync' mod='mailchimp'}
		</h3>
		<div class="col-sm-12 col-md-12 col-lg-12">

			<div class="col-sm-12 col-md-5 col-lg-4">
                <center>
                <span id="auto_sync_lists" class="switch prestashop-switch input-group col-sm-12 col-md-12 col-lg-12">
                    <input type="radio" name="auto_sync_lists" id="auto_sync_lists_yes" value="1" {if $auto_sync_lists eq 1}checked="checked"{/if} />
                    <label for="auto_sync_lists_yes" class="radioCheck" onclick="setVal('mailchimp_auto_sync_lists', 1);">
                            <i class="color_success"></i> {l s='Yes' mod='mailchimp'}
                    </label>
                    <input type="radio" name="auto_sync_lists" id="auto_sync_lists_no" value="0" {if $auto_sync_lists eq 0}checked="checked"{/if} />
                    <label for="auto_sync_lists_no" class="radioCheck" onclick="setVal('mailchimp_auto_sync_lists', 0);">
                        <i class="color_success"></i> {l s='No' mod='mailchimp'}
                    </label>
                    <a class="slide-button btn"></a>
                </span>
            	</center>
            </div>
			<div class="clear">&nbsp;</div>
            <span class="col-sm-12 col-md-12 col-lg-12">
            	{l s='The removal of clients will not occur automatically but it will occur just before a campaign is sent' mod='mailchimp'}
            </span>
        </div>
		<div class="clear">&nbsp;</div>
		<label for="sync_client_list">{l s='Select a client list from your Mailchimp account to sync clients into' mod='mailchimp'}</label>
		<p class="help-block">{l s='You can only choose one list for the automatic sync.' mod='mailchimp'}</p>
		<p class="help-block">{l s='Only new clients will be added to the list. If you want to add existing clients, please see below.' mod='mailchimp'}</p>
		<select id="sync_client_list" name="sync_client_list" onchange="setVal('mailchimp_auto_sync_list_select', 0);">
				<option value="">{l s='Please choose a list' mod='mailchimp'}</option>
			{for $i=0 to $list_count-1}
				<option value="{$lists.$i.id|escape:'htmlall':'UTF-8'}" {if $auto_sync_list_select eq $lists.$i.id} selected{/if}>{$lists.$i.name|escape:'htmlall':'UTF-8'}&nbsp;({$lists.$i.default_language|escape:'htmlall':'UTF-8'})</option>
			{/for}
		<select>
		<div class="clear">&nbsp;</div>
	</div>
	<!-- Lists -->	
		<div class="well" id="list_parameter_div" {if $ping_flag eq 0 || $list_count eq 0}style="display:none;"{/if}>
			<h3>
				<i class="icon-link"></i> {l s='List Parameters' mod='mailchimp'}
			</h3>

			<div class="col-sm-12 col-md-12 col-lg-12">
				<label for="include_blocknewsletter_list">{l s='Include email addresses from the Block Newsletter module' mod='mailchimp'}</label>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12">
				<div class="col-sm-12 col-md-5 col-lg-4">
	                <center>
	                <span id="include_blocknewsletter_list" class="switch prestashop-switch input-group col-sm-12 col-md-12 col-lg-12">
	                    <input type="radio" name="include_blocknewsletter_list" id="include_blocknewsletter_list_yes" value="1" {if $include_blocknewsletter_list eq 1}checked="checked"{/if} />
	                    <label for="include_blocknewsletter_list_yes" class="radioCheck" onclick="setVal('mailchimp_include_blocknewsletter_list', 1);">
	                            <i class="color_success"></i> {l s='Yes' mod='mailchimp'}
	                    </label>
	                    <input type="radio" name="include_blocknewsletter_list" id="include_blocknewsletter_list_no" value="0" {if $include_blocknewsletter_list eq 0}checked="checked"{/if} />
	                    <label for="include_blocknewsletter_list_no" class="radioCheck" onclick="setVal('mailchimp_include_blocknewsletter_list', 0);">
	                        <i class="color_success"></i> {l s='No' mod='mailchimp'}
	                    </label>
	                    <a class="slide-button btn"></a>
	                </span>
	            	</center>
	            </div>
	        </div>
			<div class="clear">&nbsp;</div>

			<label for="client_list">{l s='Add all existing clients to a list from your Mailchimp account' mod='mailchimp'}</label>
			<select id="client_list" name="client_list">
					<option value="">{l s='Please choose a list' mod='mailchimp'}</option>
				{for $i=0 to $list_count-1}
					<option value="{$lists.$i.id|escape:'htmlall':'UTF-8'}">{$lists.$i.name|escape:'htmlall':'UTF-8'}&nbsp;({$lists.$i.default_language|escape:'htmlall':'UTF-8'})</option>
				{/for}
			<select>
			<div id="info_lists" style="display:none;">
				{for $i=0 to $list_count-1}
					<div id="info-{$lists.$i.id|escape:'htmlall':'UTF-8'}" style="display:none;">
						<div class="clear">&nbsp;</div>
						<p><b>{l s='Selected List :' mod='mailchimp'}</b>&nbsp;{$lists.$i.name|escape:'htmlall':'UTF-8'}</p>
						<p><b>{l s='Number of Clients on list :' mod='mailchimp'}</b>&nbsp;{$lists.$i.stats.member_count|escape:'htmlall':'UTF-8'}</p>
						<p><b>{l s='Number of your Clients who are not on list :' mod='mailchimp'}</b>&nbsp;{math equation="x - y" x=$client_count y=$lists.$i.stats.member_count}</p>
						<button class="btn btn-success" type="button" onClick="addToList('{$lists.$i.id|escape:'htmlall':'UTF-8'}');">Add to List</button><small><span class="help-block">{l s='Click here to add your clients emails to a list' mod='mailchimp'}</span></small>
					</div>
				{/for}
				<div class="clear">&nbsp;</div>
				<!-- Confirmation Mail -->
				<div class="col-sm-12 col-md-12 col-lg-12" id="confirmation_mail_switch">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<label>{l s='When adding clients to a list, send a confirmation mail to the client :' mod='mailchimp'}</label>
					</div>
					<div class="col-sm-12 col-md-5 col-lg-4">
		                <center>
		                <span id="add_client_confirmation_mail" class="switch prestashop-switch input-group col-sm-12 col-md-12 col-lg-12">
		                    <input type="radio" name="add_client_confirmation_mail" id="add_client_confirmation_mail_yes" value="1" {if $add_client_confirmation_mail eq 1}checked="checked"{/if} />
		                    <label for="add_client_confirmation_mail_yes" class="radioCheck" onclick="setVal('add_client_confirmation_mail', 1);">
		                            <i class="color_success"></i> {l s='Yes' mod='mailchimp'}
		                    </label>
		                    <input type="radio" name="add_client_confirmation_mail" id="add_client_confirmation_mail_no" value="0" {if $add_client_confirmation_mail eq 0}checked="checked"{/if} />
		                    <label for="add_client_confirmation_mail_no" class="radioCheck" onclick="setVal('add_client_confirmation_mail', 0);">
		                        <i class="color_success"></i> {l s='No' mod='mailchimp'}
		                    </label>
		                    <a class="slide-button btn"></a>
		                </span>
	                	</center>
		            </div>
	            </div>
	            <div class="clear">&nbsp;</div>

				<div  id="add_to_list_spin_icon"><i class="icon-refresh icon-spin"></i>&nbsp;{l s='...please wait.' mod='mailchimp'}</div>
				<div id="add_to_list_report">
					<div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    <div  style="width:300px;" class="alert alert-success" role="alert" id="add_to_list_alert_success">
	                    	{l s='Success !' mod='mailchimp'}
	                    </div>
	                </div>
					<button type='button' class="btn btn-default" id="add_to_list_report_label">{l s='Show Details' mod='mailchimp'}</button>
					<div class="clear">&nbsp;</div>
					<div class="col-sm-12 col-md-12 col-lg-12">
						<table id="add_to_list_report_table" class="col-sm-12 col-md-12 col-lg-12">
							<theader>
								<tr>
									<th>{l s='Amount Added' mod='mailchimp'}</th>
									<th>{l s='Amount Updated' mod='mailchimp'}</th>
									<th>{l s='Error Count' mod='mailchimp'}</th>
								</tr>
							</theader>
							<tbody id="add_to_list_report_table_body">
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div id="mc_list_table_div" {if $list_count eq 0}style="display:none;"{/if}>
				<div class="clear">&nbsp;</div>
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
					<tr>
						<th>{l s='View Segments' mod='mailchimp'}</th>
						<th>{l s='ID' mod='mailchimp'}</th>
						<th>{l s='Name' mod='mailchimp'}</th>
						<th>{l s='Language' mod='mailchimp'}</th>
						<th>{l s='Member Count' mod='mailchimp'}</th>
						<th>{l s='Date Created' mod='mailchimp'}</th>
					</tr>
					{for $i=0 to $list_count-1}
						<tr>
							<td>
								<i class="icon-eye icon-2x" style="cursor:pointer;" onclick="showListsSegment('{$i|escape:'htmlall':'UTF-8'}', '{$lists.$i.id|escape:'htmlall':'UTF-8'}');"></i>
							</td>
							<td>
								{$lists.$i.id|escape:'htmlall':'UTF-8'}
							</td>
							<td>
								{$lists.$i.name|escape:'htmlall':'UTF-8'}
							</td>
							<td>
								{$lists.$i.default_language|escape:'htmlall':'UTF-8'}
							</td>
							<td>
								{$lists.$i.stats.member_count|escape:'htmlall':'UTF-8'}
								{if $lists.$i.stats.member_count > 0}
									<i class="icon-eye icon-2x" style="cursor:pointer;float:right;" onclick="showListMembers('{$i|escape:'htmlall':'UTF-8'}', '{$lists.$i.id|escape:'htmlall':'UTF-8'}');"></i>
								{/if}
							</td>
							<td>
								{$lists.$i.date_created|escape:'htmlall':'UTF-8'}
							</td>
						</tr>
						<tr id="list_row_{$i|escape:'htmlall':'UTF-8'}" style="display:none;">
							<td id="list_row_data_{$i|escape:'htmlall':'UTF-8'}" colspan="6"></td>
						</tr>
						<tr id="list_members_row_{$i|escape:'htmlall':'UTF-8'}" style="display:none;">
							<td id="list_members_row_data_{$i|escape:'htmlall':'UTF-8'}" colspan="6"></td>
						</tr>
					{/for}
				</table>
			</div>
		</div>

	<!-- Segements -->
		<div class="well" id="segements_div" {if $ping_flag eq 0 || $list_count eq 0}style="display:none;"{/if}>
			<h3>
				<i class="icon-link"></i> {l s='Segements' mod='mailchimp'}
			</h3>
			<p><b>{l s='To work with certain conditions on segments, you need to subscribe to the MailChimp Feature Social Pro :' mod='mailchimp'}</b></p>
			<a href="http://mailchimp.com/features/social-pro/" target="_blank">{l s='More information on MailChimp Social Pro' mod='mailchimp'}</a>
			<div class="clear">&nbsp;</div>
			{if $segment_name_conflict eq 1}
				<div class="alert alert-danger">{l s='Segment name already exists for this client list. Please choose another name.' mod='mailchimp'}</div>
				<div class="clear">&nbsp;</div>
			{/if}
			<label for="segment_name">{l s='Choose a name for your segment :' mod='mailchimp'}</label>
			<input type="text" id="segment_name" name="segment_name" required>
			<div class="clear">&nbsp;</div>
			<label for="segment_client_list">{l s='Select a client list' mod='mailchimp'}</label>
			<select id="segment_client_list" name="segment_client_list" required>
					<option value="">{l s='Please choose a list' mod='mailchimp'}</option>
				{for $i=0 to $list_count-1}
					<option value="{$lists.$i.id|escape:'htmlall':'UTF-8'}">{$lists.$i.name|escape:'htmlall':'UTF-8'}&nbsp;({$lists.$i.default_language|escape:'htmlall':'UTF-8'})</option>
				{/for}
			<select>
			<div class="clear">&nbsp;</div>
			<label>{l s='Add a condition (max 5)' mod='mailchimp'}</label>
			<div id="mc_new_segment" class="well col-xs-12 col-sm-12 col-md-12 col-lg-12">
				{for $i=1 to $to}
					<div class="clear">&nbsp;</div>
					<div id="new_seg_condition_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<!-- FIELD -->
						<select id="new_seg_field_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_field_{$i|escape:'htmlall':'UTF-8'}" {if $i neq 1}style="display:none;" {/if}class="col-xs-12 col-sm-12 col-md-4 col-lg-4" onChange="showOp('{$i|escape:'htmlall':'UTF-8'}');">
							<option value="empty">{l s='Please choose a field' mod='mailchimp'}</option>
							<option value="mc_language">{l s='Language' mod='mailchimp'}</option>
							<option value="aim">{l s='Subscribers' mod='mailchimp'}</option>
							<option value="social_gender">{l s='Gender' mod='mailchimp'}<span style="color:red;">&nbsp;&nbsp;{l s='* Social Pro required' mod='mailchimp'}</span></option>
							<option value="social_age">{l s='Age' mod='mailchimp'}<span style="color:red;">&nbsp;&nbsp;{l s='* Social Pro required' mod='mailchimp'}</span></option>
							<option value="social_network">{l s='Social Network' mod='mailchimp'}&nbsp;&nbsp;<span style="color:red;">{l s='* Social Pro required' mod='mailchimp'}</span></option>
						</select>

						<!-- OP -->
						<select id="new_seg_op_lang_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_op_lang_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showValue('{$i|escape:'htmlall':'UTF-8'}');">
							<option value="empty">{l s='Please choose an option' mod='mailchimp'}</option>
							<option value="eq">{l s='equal to' mod='mailchimp'}</option>
							<option value="ne">{l s='not equal to' mod='mailchimp'}</option>
						</select>
						<select id="new_seg_op_aim_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_op_aim_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showValue('{$i|escape:'htmlall':'UTF-8'}');">
							<option value="empty">{l s='Please choose an option' mod='mailchimp'}</option>
							<option value="open">{l s='who have opened' mod='mailchimp'}</option>
							<option value="noopen">{l s='who have not opened' mod='mailchimp'}</option>
							<option value="click">{l s='who have clicked' mod='mailchimp'}</option>
							<option value="sent">{l s='who have been sent' mod='mailchimp'}</option>
							<option value="nosent">{l s='who have not been sent' mod='mailchimp'}</option>
						</select>
						<select id="new_seg_op_gender_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_op_gender_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showValue('{$i|escape:'htmlall':'UTF-8'}');">
							<option value="empty">{l s='Please choose an option' mod='mailchimp'}</option>
							<option value="eq">{l s='equal to' mod='mailchimp'}</option>
							<option value="ne">{l s='not equal to' mod='mailchimp'}</option>
						</select>
						<select id="new_seg_op_age_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_op_age_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showValue('{$i|escape:'htmlall':'UTF-8'}');">
							<option value="empty">{l s='Please choose an option' mod='mailchimp'}</option>
							<option value="eq">{l s='equal to' mod='mailchimp'}</option>
							<option value="ne">{l s='not equal to' mod='mailchimp'}</option>
							<option value="gt">{l s='greater than' mod='mailchimp'}</option>
							<option value="lt">{l s='less than' mod='mailchimp'}</option>
						</select>
						<select id="new_seg_op_social_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_op_social_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showValue('{$i|escape:'htmlall':'UTF-8'}');">
							<option value="empty">{l s='Please choose an option' mod='mailchimp'}</option>
							<option value="member">{l s='member of' mod='mailchimp'}</option>
							<option value="notmember">{l s='not a member of' mod='mailchimp'}</option>
						</select>

						<!-- VALUES -->
						<div id="new_seg_values_div_{$i|escape:'htmlall':'UTF-8'}">
							<select id="new_seg_value_age_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_value_age_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showAddSegButton('{$i|escape:'htmlall':'UTF-8'}');">
								<option value="empty">{l s='Please choose a value' mod='mailchimp'}</option>
								{for $j=16 to 85}
									<option value="{$j|escape:'htmlall':'UTF-8'}">{$j|escape:'htmlall':'UTF-8'}</option>
								{/for}
							</select>
							<select id="new_seg_value_campaign_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_value_campaign_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showAddSegButton('{$i|escape:'htmlall':'UTF-8'}');">
								<option value="empty">{l s='Please choose a value' mod='mailchimp'}</option>
								<option value="any">{l s='Any Campaign' mod='mailchimp'}</option>
								{for $k=0 to $campaign_count-1}
									<option value="{$campaign_data.$k.id|escape:'htmlall':'UTF-8'}">{$campaign_data.$k.title|escape:'htmlall':'UTF-8'}</option>
								{/for}
							</select>
							<select id="new_seg_value_lang_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_value_lang_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showAddSegButton('{$i|escape:'htmlall':'UTF-8'}');">
								<option value="empty">{l s='Please choose a value' mod='mailchimp'}</option>
								{foreach $mail_chimp_langs key=k item=v}
									<option value="{$v|escape:'htmlall':'UTF-8'}">{$k|escape:'htmlall':'UTF-8'}</option>
								{/foreach}
							</select>
							<select id="new_seg_value_gender_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_value_gender_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showAddSegButton('{$i|escape:'htmlall':'UTF-8'}');">
								<option value="empty">{l s='Please choose a value' mod='mailchimp'}</option>
								<option value="male">{l s='Male' mod='mailchimp'}</option>
								<option value="female">{l s='Female' mod='mailchimp'}</option>
							</select>
							<select id="new_seg_value_social_{$i|escape:'htmlall':'UTF-8'}" name="new_seg_value_social_{$i|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;" onChange="showAddSegButton('{$i|escape:'htmlall':'UTF-8'}');">
								<option value="empty">{l s='Please choose a value' mod='mailchimp'}</option>
								<option value="twitter">Twitter</option>
								<option value="facebook">Facebook</option>
								<option value="myspace">MySpace</option>
								<option value="linkedin">LinkedIn</option>
								<option value="flickr">Flickr</option>
							</select>
						</div>
						<div id="add_seg_condition_button_{$i|escape:'htmlall':'UTF-8'}" style="display:none;">
							<div class="clear">&nbsp;</div>
							<i class="process-icon-new" style="color:green; float:left;" onclick="show_new_seg_condition('{$i|escape:'htmlall':'UTF-8'}');" id="add_new_seg_condition_{$i|escape:'htmlall':'UTF-8'}"></i>&nbsp;{l s='Add a new condition' mod='mailchimp'}
						</div>
					</div>
				{/for}
				<center><input type="submit" name="submitMailChimpSegements" id="submitMailChimpSegements" value="{l s='Create Segment' mod='mailchimp'}" class="btn btn-default" disabled="true"/></center>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="clear">&nbsp;</div>
	</form>
{/if}
<!-- Executing Code Modal -->
<div class="modal fade" id="mailchimp_play_it_cool_modal_lists" tabindex="-1" role="dialog" aria-labelledby="play_it_cool_modal_lists" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
				<div class="modal-body">
					<p style="text-align:center;">
						<img src="{$module_dir|escape:'htmlall':'UTF-8'}/logo.png"><br /><br />
						<i class="icon-spinner icon-spin"></i><br />
						{l s='Please Wait...' mod='mailchimp'}
					</p>
				</div>
			</form>
		</div>
	</div>
</div>