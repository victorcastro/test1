{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<div id="product_pre_newsletter_list_{$id_product|escape:'htmlall':'UTF-8'}">
	    	<p>
	    		{$id_product|escape:'htmlall':'UTF-8'}.&nbsp;{$name|escape:'htmlall':'UTF-8'}&nbsp;&nbsp;
	    			<i class="icon-times" onclick="removeThisProductFromPreList({$id_product|escape:'htmlall':'UTF-8'});" style="cursor:pointer;" title="{l s='Remove product from list' mod='mailchimp'}"></i>
	    	</p>
    <input type="hidden" class="products_pre_newsletter" value="{$id_product|escape:'htmlall':'UTF-8'}" name="products_pre_newsletter_{$id_product|escape:'htmlall':'UTF-8'}" id="products_pre_newsletter_{$id_product|escape:'htmlall':'UTF-8'}">
</div>