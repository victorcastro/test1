{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<h3><i class="icon-book"></i> {l s='Documentation' mod='mailchimp'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small></h3>
<div class="media">
        <a href="{$ps_url|escape:'htmlall':'UTF-8'}{$module_dir|escape:'htmlall':'UTF-8'}/doc/FAQ_{$doc_lang|escape:'htmlall':'UTF-8'}.pdf" target="_blank">
        	<img src="{$ps_url|escape:'htmlall':'UTF-8'}{$module_dir|escape:'htmlall':'UTF-8'}/views/img/pdf.png">{l s='Mailchimp Documentation' mod='mailchimp'}
        </a>
        <p>{l s='Access to Prestashops free documentation:' mod='mailchimp'}</p>
        <ul style='list-style-type:circle;'>
            <li><a href="http://doc.prestashop.com/dashboard.action" target="_blank"> http://doc.prestashop.com/dashboard.action</a></li>
        </ul>
        <p>{l s='Need help? Click the "contact" tab' mod='mailchimp'}</p>
        <div class="clear">&nbsp;</div>
        <div class="well col-xs-12 col-sm-12 col-md-12 col-lg-12" id="campaign_video">
        	<iframe width="560" height="315" src="https://www.youtube.com/embed/8UDJVRzL6-Y" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="well col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mc_template_info">
			<div id="mc_template_info_header" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 style="margin-left: 5px;">
					<i class="icon-plus-square" onclick="showNewTemplateInfoDocsTab()" style="cursor:pointer"></i>&nbsp;&nbsp;{l s='How to create customised templates that are compatible with this module.' mod='mailchimp'}
				</h3>
			</div>
			<div id="mc_template_info_content">
				<p><a href="http://kb.mailchimp.com/templates/code/export-templates-html" target="_blank">{l s='Mailchimp article on How To Export a HTML Template.' mod='mailchimp'}</a></p>
				<p><a href="http://kb.mailchimp.com/templates/code/import-an-html-template" target="_blank">{l s='Mailchimp article on How To Import a HTML Template.' mod='mailchimp'}</a></p>
				<p>
					{l s='To create campaigns using templates that you have created on the Mailchimp site, you need to create the templates by the "Code Your Own" option.' mod='mailchimp'}
					{l s='The reason for this is that you need to notify the module of where in the template the content goes. (Header is also optional)' mod='mailchimp'}
					{l s='If you cannot code your own template, please use the following steps :' mod='mailchimp'}
				</p>
				<ul>
					<li>{l s='Create a template using the "Basic" or "Themes" as your starting point.' mod='mailchimp'}</li>
					<li>{l s='When you have finished designing your template, save it under a name that is temporary.' mod='mailchimp'}</li>
					<li>{l s='We advise that you enter a phrase in the section that you will want as the campaign\'s main content, eg. "This is the main content".' mod='mailchimp'}</li>
					<li>{l s='In your Mailchimp interface, click "Templates" and you can see a list of your templates.' mod='mailchimp'}</li>
					<li>{l s='Beside the "Edit" button, click the dropdown arrow and select "Export".' mod='mailchimp'}</li>
					<li>{l s='Click "Got It, Export Template"' mod='mailchimp'}</li>
					<li>{l s='A HTML file will download. Remember where this file is located on your computer.' mod='mailchimp'}</li>
					<li>{l s='Now you will create the new template by clicking "New Template". Choose the "Import HTML" option.' mod='mailchimp'}</li>
					<li>{l s='Locate the Template File, give the template a name & click "Upload".' mod='mailchimp'}</li>
					<li>{l s='Locate where you want to have the main content of the campaign, it can be a' mod='mailchimp'}&nbsp;&#60;div&#62;&nbsp;{l s='or a' mod='mailchimp'}&#60;td&#62;&nbsp;{l s='and in this tag, you need to copy the following :' mod='mailchimp'}</li>
					<p class="well">{literal}mc:edit="content" id="content"{/literal}</p>
					<li>{l s='So your code will look like this :' mod='mailchimp'}</li>
					<p class="well">&#60;div mc:edit="content" id="content"&#62;{l s='This is the main content' mod='mailchimp'}&#60;&#47;div&#62;</p>
					<li><b>{l s='Be sure not to put the mc:edit in an element that includes surrounding sections from the main content, ie. the footer, the main logo.' mod='mailchimp'}</b></li>
					<li>{l s='If your footer is included in the Main Content section, it may be duplicated when you edit a campaign in the module\s back-office.' mod='mailchimp'}</li>
					<li>{l s='Save your new template.' mod='mailchimp'}</li>
					<li>{l s='You can also do the same for an area called "header".' mod='mailchimp'}</li>
				</ul>
			</div>
		</div>
</div>