{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<!-- Adding product to newsletter -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div id="adding_product_to_newsletter_header_{$type|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<span onclick="showAddingProductToNewsletterDiv('{$type|escape:'htmlall':'UTF-8'}');" style="cursor:pointer;">
			<h3><i class="icon-plus-square"></i>&nbsp;{l s='Add product to newsletter' mod='mailchimp'}</h3>
		</span>
		<a href="https://www.youtube.com/watch?v=8UDJVRzL6-Y" target="_blank"><p>{l s='Watch our video on how to add products to the newsletter' mod='mailchimp'}</p></a>
	</div>
	<div id="adding_product_to_newsletter_div_{$type|escape:'htmlall':'UTF-8'}" style="display:none;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    <span onclick="hideAddingProductToNewsletterDiv('{$type|escape:'htmlall':'UTF-8'}');" style="cursor:pointer;">
	    	<h3><i class="icon-eye-close icon-2x"></i>&nbsp;{l s='Hide this section' mod='mailchimp'}</h3>
	    </span>
    	<a href="https://www.youtube.com/watch?v=8UDJVRzL6-Y" target="_blank"><p>{l s='Watch our video on how to add products to the newsletter' mod='mailchimp'}</p></a>
	    <div id="mailchimp_add_product_{$type|escape:'htmlall':'UTF-8'}" class="well">
	    	<label for="mailchimp_product_per_row_count_{$type|escape:'htmlall':'UTF-8'}">{l s='How many products per row' mod='mailchimp'}&nbsp;:</label>
	    	<select id="mailchimp_product_per_row_count_{$type|escape:'htmlall':'UTF-8'}" name="mailchimp_product_per_row_count_{$type|escape:'htmlall':'UTF-8'}" style="width:50px;">
	    		<option value=1>1</option>
	    		<option value=2>2</option>
	    		<option value=3>3</option>
	    		<option value=4>4</option>
	    	</select>

			<div class="clear">&nbsp;</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
				<div class="btn-group col-xs-6 col-sm-6 col-md-4 col-lg-4" data-toggle="buttons" id="include_tax_in_price_div_{$type|escape:'htmlall':'UTF-8'}" style="padding:0px;margin:0px;">
	            	<label style="width:50%;" class="btn btn-info" id="include_tax_in_price_yes_label_{$type|escape:'htmlall':'UTF-8'}" onclick="update_div_class('include_tax_in_price_yes_label_{$type|escape:'htmlall':'UTF-8'}');">
	                    <input type="radio" name="include_tax_in_price_{$type|escape:'htmlall':'UTF-8'}" id="include_tax_in_price_yes_{$type|escape:'htmlall':'UTF-8'}" value=1 checked='checked'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{l s='Tax Included in Price' mod='mailchimp'}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                </label>

	                <label style="width:50%;" class="btn btn-default" id="include_tax_in_price_no_label_{$type|escape:'htmlall':'UTF-8'}" onclick="update_div_class('include_tax_in_price_no_label_{$type|escape:'htmlall':'UTF-8'}');">
	                    <input type="radio" name="include_tax_in_price_{$type|escape:'htmlall':'UTF-8'}" id="include_tax_in_price_no_{$type|escape:'htmlall':'UTF-8'}" value=0 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{l s='Tax Excluded in Price' mod='mailchimp'}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                </label>
	            </div>
	        </div>
			<div class="clear">&nbsp;</div>

	    	<label for="mailchimp_product_extra_col_{$type|escape:'htmlall':'UTF-8'}">{l s='Would you like an extra column next to the product to add extra content yourself' mod='mailchimp'}&nbsp;:</label>
	    	<select id="mailchimp_product_extra_col_{$type|escape:'htmlall':'UTF-8'}" name="mailchimp_product_extra_col_{$type|escape:'htmlall':'UTF-8'}" style="width:100px;">
	    		<option value="yes">{l s='Yes' mod='mailchimp'}</option>
	    		<option value="no">{l s='No' mod='mailchimp'}</option>
	    	</select>
	    	<!-- Newsletter language -->
			<div class="clear">&nbsp;</div>
			<label for="mailchimp_lang_for_prod_{$type|escape:'htmlall':'UTF-8'}">{l s='Newsletter language' mod='mailchimp'}&nbsp;:</label>
			<select id="mailchimp_lang_for_prod_{$type|escape:'htmlall':'UTF-8'}" name="mailchimp_lang_for_prod_{$type|escape:'htmlall':'UTF-8'}" style="width:200px;">
				{foreach from=$languages item=foo}
					<option value="{$foo.id_lang|escape:'htmlall':'UTF-8'}">{$foo.name|escape:'htmlall':'UTF-8'}</option>
				{/foreach}
			</select>
			<div class="clear">&nbsp;</div>
			<!-- product currency -->
			<label for="mailchimp_currency_for_prod_{$type|escape:'htmlall':'UTF-8'}">{l s='Currency' mod='mailchimp'}&nbsp;:</label>
			<select id="mailchimp_currency_for_prod_{$type|escape:'htmlall':'UTF-8'}" name="mailchimp_currency_for_prod_{$type|escape:'htmlall':'UTF-8'}" style="width:200px;">
				{foreach from=$currencies item=foo}
					<option value="{$foo.id_currency|escape:'htmlall':'UTF-8'}" {if $foo.id_currency eq $id_default_currency}selected{/if}>{$foo.name|escape:'htmlall':'UTF-8'}</option>
				{/foreach}
			</select>
			<div class="clear">&nbsp;</div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
		    <label for="mailchimp_product_search_{$type|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">{l s='Add products to newsletter' mod='mailchimp'}&nbsp;:</label>
		    <div class="help-block">{l s='The products you add from the search bar will appear in the column on the right before you add them to the newsletter all at once.' mod='mailchimp'}</div>
		    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px;margin:0px;">
		        <div id="mailchimp_product_search_div_{$type|escape:'htmlall':'UTF-8'}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
		        	<input type="text" id="mailchimp_product_search_{$type|escape:'htmlall':'UTF-8'}" placeholder="{l s='Search Products by Name' mod='mailchimp'}"/>
		    	</div>
		    </div>
		    <div id="mailchimp_product_search_selected_button_{$type|escape:'htmlall':'UTF-8'}" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		        	<button type="button" class="btn btn-default col-xs-12 col-sm-12 col-md-12 col-lg-12" onclick="addProductsToNewsletter('{$type|escape:'htmlall':'UTF-8'}');"><i class="icon-arrow-right"></i>&nbsp;&nbsp;{l s='Click here to add chosen products to newsletter' mod='mailchimp'}</button>

		        	<div class="help-block">{l s='Please click on the newsletter editor once before adding the products to the newsletter.' mod='mailchimp'}</div>
		    	</div>
		    </div>
		    <div id="mailchimp_product_search_result_{$type|escape:'htmlall':'UTF-8'}" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    </div>
		    <div id="mailchimp_product_search_selected_{$type|escape:'htmlall':'UTF-8'}" class="well col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    </div>
	    </div>
	    <div class="help-block">{l s='You can modify the appearance or text of the products in the editor. Please watch the video located above for guidance.' mod='mailchimp'}</div>
	</div>
</div>
<!-- End of add product to newsletter -->