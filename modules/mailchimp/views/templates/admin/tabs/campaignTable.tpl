{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
	{foreach $campaign_data item=campaign}
		<tr id="campaign_id_{$campaign.id|escape:'htmlall':'UTF-8'}">
			<td><i style="cursor:pointer;" class="icon-eye icon-2x" onclick="previewCampaignInModule('{$campaign.id|escape:'htmlall':'UTF-8'}');"></i></td> 
			<td><i style="cursor:pointer;" class="icon-paper-plane-o icon-2x" onclick="previewCampaign('{$campaign.id|escape:'htmlall':'UTF-8'}', '{$campaign.type|escape:'htmlall':'UTF-8'}');"></i></td> 
			<td>{$campaign.title|escape:'htmlall':'UTF-8'}</td>
			<td>{$campaign.subject|escape:'htmlall':'UTF-8'}</td>
			<td>{if $campaign.type eq 'plaintext'}{$campaign.type|escape:'htmlall':'UTF-8'}{else}{$campaign.content_type|escape:'htmlall':'UTF-8'}{/if}</td>
			<td>{$campaign.status|escape:'htmlall':'UTF-8'}</td>
			<td>{$campaign.create_time|escape:'htmlall':'UTF-8'}</td>
			<td>{$campaign.send_time|escape:'htmlall':'UTF-8'}</td>
			<td>
				{if $campaign.status eq 'save'}
					<i class="icon-envelope-o" title="{l s='Send' mod='mailchimp'}" onclick="showSendCampaignModal('{$campaign.id|escape:'htmlall':'UTF-8'}');" style="cursor:pointer;"></i>		
					&nbsp;&nbsp;
					<i class="icon-calendar-o" title="{l s='Schedule' mod='mailchimp'}"
					style="cursor:pointer;" onclick="showScheduleModal('{$campaign.id|escape:'htmlall':'UTF-8'}', '{$campaign.title|escape:'htmlall':'UTF-8'}', 0);"></i>
					&nbsp;&nbsp;
					{if $campaign.type eq 'regular' && $campaign.content_type eq 'html'}
						<i class="icon-edit" title="{l s='Edit' mod='mailchimp'}" onclick="showEditCampaignModal('{$campaign.id|escape:'htmlall':'UTF-8'}', 'regular', 0);" style="cursor:pointer;"></i>
						&nbsp;&nbsp;
					{/if}
					{if $campaign.type eq 'regular' && $campaign.content_type eq 'template'}
						<i class="icon-edit" title="{l s='Edit' mod='mailchimp'}" onclick="showEditCampaignModal('{$campaign.id|escape:'htmlall':'UTF-8'}', 'template', {$campaign.template_id|escape:'htmlall':'UTF-8'});" style="cursor:pointer;"></i>
						&nbsp;&nbsp;
					{/if}
					{if $campaign.type eq 'plaintext'}
						<i class="icon-edit" title="{l s='Edit' mod='mailchimp'}" onclick="showEditCampaignModal('{$campaign.id|escape:'htmlall':'UTF-8'}', 'text');" style="cursor:pointer;"></i>
						&nbsp;&nbsp;
					{/if}
					<i class="icon-times" title="{l s='Delete' mod='mailchimp'}" onclick="if(confirm('{l s='Attention ! Are you sure you want to delete this campaign?' mod='mailchimp'}'))deleteCampaign('{$campaign.id|escape:'htmlall':'UTF-8'}');" style="cursor:pointer;"></i>
				{/if}
				{if $campaign.status eq 'sent'}
					<i class="icon-files-o" title="{l s='Duplicate' mod='mailchimp'}" onclick="replicateCampaign('{$campaign.id|escape:'htmlall':'UTF-8'}')" style="cursor:pointer;"></i>
					&nbsp;&nbsp;
					<i class="icon-times" title="{l s='Delete' mod='mailchimp'}" onclick="if(confirm('{l s='Attention ! Are you sure you want to delete this campaign?' mod='mailchimp'}'))deleteCampaign('{$campaign.id|escape:'htmlall':'UTF-8'}');" style="cursor:pointer;"></i>
				{/if}
				{if $campaign.status eq 'schedule'}
					<i class="icon-times" title="{l s='Delete' mod='mailchimp'}" onclick="if(confirm('{l s='Attention ! Are you sure you want to delete this campaign?' mod='mailchimp'}'))deleteCampaign('{$campaign.id|escape:'htmlall':'UTF-8'}');" style="cursor:pointer;"></i>
				{/if}
			</td>
		</tr>

	{/foreach}