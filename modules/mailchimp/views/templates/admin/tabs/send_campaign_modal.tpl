{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<!-- Sending Campaign Modal -->
<div class="modal fade" id="mailchimp_send_campaign_modal" tabindex="-1" role="dialog" aria-labelledby="send_campaign_modal" aria-hidden="true">
	<div style="float:inherit;margin-left: auto;margin-right: auto;" class="modal-dialog col-xs-9 col-sm-9 col-md-6 col-lg-6">
		<div class="modal-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="modal-body">
					<div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<input type="hidden" value="" id="send_campaign_modal_campaign_id">
						<center>
							<img src="{$module_dir|escape:'htmlall':'UTF-8'}/logo.png"><br /><br />
							<p>{l s='Are you sure that you want to send this campaign?' mod='mailchimp'}</p>
							<button type="button" class="btn btn-default" data-dismiss="modal" onclick="sendCampaign();"><i class="icon-paper-plane-o"></i>&nbsp;&nbsp;{l s='Yes' mod='mailchimp'}</button>
							<button type="button" class="btn btn-default" data-dismiss="modal"><i class="icon-times"></i>&nbsp;&nbsp;{l s='No' mod='mailchimp'}</button>
						</center>
					</div>
				</div>
		</div>
	</div>
</div>