<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

include (_PS_MODULE_DIR_.'mailchimp/libraries/MailChimpAPI.php');

class MailChimp extends Module
{
	protected $js_path = null;
	protected $css_path = null;
	protected static $lang_cache;
	protected $api;
	protected $api_key;

	public function __construct()
	{
		$this->name = 'mailchimp';
		$this->tab = 'advertising_marketing';
		$this->version = '3.1.8';
		$this->author = 'Prestashop';
		$this->module_key = 'efa3eab3addf31c4f4e3692525661a74';
		$this->need_instance = 1;
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
		parent::__construct();

		$this->bootstrap = true;

		$this->displayName = $this->l('Official Mailchimp for PrestaShop');
		$this->description = $this->l('MailChimp helps you design email newsletters, update and segment your list of subscribers and track your results. It\'s like your own personal publishing platform.');
		$this->js_path = $this->_path.'views/js/';
		$this->css_path = $this->_path.'views/css/';
		if (version_compare(_PS_VERSION_, '1.6', '<'))
			$this->getLang();
		if (!extension_loaded('curl'))
			echo '<script language="javascript">alert("You must enable cURL on you server")</script>';
	}

	public function install()
	{
		if (Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);

		$token_mail_chimp = $this->generateToken();
		Configuration::updateValue('MAILCHIMP_TOKEN', pSQL($token_mail_chimp));
		Configuration::updateValue('add_client_confirmation_mail', 1);
		Configuration::updateValue('mailchimp_auto_sync_lists', 0);
		Configuration::updateValue('mailchimp_include_blocknewsletter_list', 0);
		Configuration::updateValue('mailchimp_auto_sync_list_select', 'none');

		$this->installTab();
		return parent::install() &&
			$this->registerHook('actionCustomerAccountAdd') && $this->registerHook('header');
	}

	/**
	* Install Tab
	* @return boolean
	*/
	private function installTab()
	{
		$tab = new Tab();	/* Requires at least one client to function. John Doe on clean install of Prestashop */
		$tab->id_parent = -1;
		$tab->class_name = 'Admin'.get_class($this);
		$tab->module = $this->name;
		$tab->active = 1;
		$tab->name = array();
		foreach (Language::getLanguages(true) as $lang)
			$tab->name[$lang['id_lang']] = $this->displayName;
		unset($lang);
		return $tab->add();
	}

	public function uninstall()
	{
		$this->uninstallTab();
		return parent::uninstall();
	}

	/**
	* Uninstall Tab
	* @return boolean
	*/
	private function uninstallTab()
	{
		$id_tab = (int)Tab::getIdFromClassName('Admin'.get_class($this));
		if ($id_tab)
		{
			$tab = new Tab($id_tab);
			if (Validate::isLoadedObject($tab))
				return $tab->delete();
			else
				return false;
		}
		else
			return true;
	}

	public function connect()
	{
		$this->api_key = pSQL(Configuration::get('MAILCHIMP_API_KEY'));
		if (!$this->api_key)
			return false;
		$this->api = new MailChimpAPI($this->api_key);
		return true;
	}

	public function getEmployeeEmails()
	{
		$employees = Db::getInstance()->executeS('
			SELECT `email`
			FROM `'._DB_PREFIX_.'employee`
			WHERE `active` = 1
			ORDER BY `id_employee` ASC
		');
		$emails = array();
		foreach ($employees as $employee)
			array_push($emails, pSQL($employee['email']));
		return $employees;
	}

	public function getContent()
	{
		$this->loadAsset();
		//$controller_name = 'Admin'.get_class($this); // SAV ticket #85490
		$controller_name = 'AdminMailChimp';
		$current_id_tab = (int)$this->context->controller->id;
		$controller_url = $this->context->link->getAdminLink($controller_name);
		$ps_url = Tools::usingSecureMode() ? Tools::getShopDomainSsl(true) : Tools::getShopDomain(true);
		$request_uri = $_SERVER['REQUEST_URI'];

		$submit = Tools::isSubmit('submitMailChimp');
		$submit_segments = Tools::isSubmit('submitMailChimpSegements');
		$alert_config = '';
		$segment_name_conflict = 0;
		if ($submit)	/* Configuration */
		{
			$this->connect();
			$this->api_key = pSQL(Tools::getValue('api_key'));
			$api_key_length = Tools::strlen($this->api_key);
			$api_key_dc = Tools::subStr($this->api_key, $api_key_length - 3);

			Configuration::updateValue('MAILCHIMP_API_KEY', pSQL($this->api_key));
			Configuration::updateValue('MAILCHIMP_EMAIL_ADDRESS', pSQL(Tools::getValue('mailchimp_email_address')));
			Configuration::updateValue('MAILCHIMP_API_KEY_DC', pSQL($api_key_dc));

			$alert_config = $this->displayConfirmation($this->l('Configuration updated.'));
		}
		else if ($submit_segments)
		{
			$segment_name_conflict = $this->createSegment();
			$submit = 2;
		}

		$ping_flag = 0;
		$api_key_error = 0;

		$result = array();
		if (Configuration::get('MAILCHIMP_API_KEY') != null)
		{
			$this->connect();
			$result = $this->api->call('helper/ping', array('apikey' => $this->api_key));	// empty or 'Everything's Chimpy!'

			if ($result['msg'] == 'Everything\'s Chimpy!')
				$ping_flag = 1;
			else
				$api_key_error = 1;
		}

		$client_count = Db::getInstance()->getValue('
			SELECT COUNT(email)
			FROM '._DB_PREFIX_.'customer
			WHERE newsletter = 1
			AND active = 1');

		$campaigns = array();
		$list_count = 0;
		$campaign_count = 0;
		$ad = dirname($_SERVER['PHP_SELF']);

		$iso_code = Context::getContext()->language->iso_code;
		$doc_lang = 'en';
		if ($iso_code == 'es' || $iso_code == 'fr')
			$doc_lang = $iso_code;
		$context = Context::getContext();
		$lang = $context->employee->id_lang;

		$this->context->smarty->assign(array(
			'alert_config' => $alert_config,
			'is_submit' => $submit,
			'ad' => $ad,
			'iso_code' => $iso_code,
			'doc_lang' => $doc_lang,
			'pathCSS' => _THEME_CSS_DIR_,
			'module_dir' => $this->_path,
			'requestUri' => $request_uri,
			'module_name' => $this->name,
			'module_path' => $this->_path,
			'module_display' => $this->displayName,
			'ps_version' => (bool)version_compare(_PS_VERSION_, '1.6', '>'),
			'ps_url' => $ps_url,
			'current_id_tab' => $current_id_tab,
			'controller_url' => $controller_url,
			'controller_name' => $controller_name,
			'api_key' => $this->api_key,
			'api_key_error' => $api_key_error,
			'mailchimp_email_address' => pSQL(Configuration::get('MAILCHIMP_EMAIL_ADDRESS')),
			'ping_flag' => $ping_flag,
			'client_count' => $client_count,
			'list_count' => $list_count,
			'lang' => $lang,
			'module_version' => $this->version,
			));

		if ($ping_flag === 1)
		{
			if (is_string($this->api_key))
				$result = $this->api->call('lists/list', array('apikey' => $this->api_key));

			$list_count = (int)$result['total'];
			$list_data = $result['data'];

			$campaigns = $this->getCampaigns();
			$campaign_count = $campaigns['total'];
			$campaign_data = $campaigns['data'];

			$stats_info = $this->reportSummaryInfo($campaign_data);
			$templates = $this->getTemplates();

			$user_templates = $templates['user'];
			$gallery_templates = $templates['gallery'];
			$gallery_templates_one = array_slice($gallery_templates, 0, 10);
			$gallery_templates_two = array_slice($gallery_templates, 11);

			$languages = Language::getLanguages();
			$currencies = Currency::getCurrencies();
			$currency = Currency::getDefaultCurrency();

			$base_templates = $templates['base'];
			$this->context->smarty->assign(array(
				'mail_chimp_langs' => $this->getLangArray(),
				'employee_emails' => $this->getEmployeeEmails(),
				'add_client_confirmation_mail' => (int)Configuration::get('add_client_confirmation_mail'),
				'auto_sync_lists' => (int)Configuration::get('mailchimp_auto_sync_lists'),
				'include_blocknewsletter_list' => (int)Configuration::get('mailchimp_include_blocknewsletter_list'),
				'auto_sync_list_select' => pSQL(Configuration::get('mailchimp_auto_sync_list_select')),
				'to' => 5,
				'lists' => $list_data,
				'list_count' => $list_count,
				'campaign_data' => $campaign_data,
				'campaign_count' => $campaign_count,
				'templates' => $templates,
				'user_templates' => $user_templates,
				'gallery_templates' => $gallery_templates_one,
				'gallery_templates_two' => $gallery_templates_two,
				'base_templates' => $base_templates,
				'segment_name_conflict' => $segment_name_conflict,
				'stats_info' => $stats_info,
				'languages' => $languages,
				'currencies' => $currencies,
				'default_currency' => $currency,
				'id_default_currency' =>$currency->id
			));
		}
		$display = $this->display(__FILE__, 'views/templates/admin/configuration.tpl');

		return $display;
	}

	public function hookDisplayHeader($params)
	{
		if (Tools::isSubmit('submitNewsletter'))
		{
			$this->connect();
			$email = pSQL(Tools::getValue('email'));
			$this->registerClient($email);
		}
	}

	public function hookActionCustomerAccountAdd($params)
    {
		$newsletter = (int)Tools::getValue('newsletter');
		if ($newsletter === 1)
		{
    		$this->connect();
    		$email = pSQL(Tools::getValue('email'));
			$this->registerClient($email);
		}
    }

	public function registerClient($email)
	{
		$id_list = pSQL(Configuration::get('mailchimp_auto_sync_list_select'));
		$new_email = new StdClass();
		$new_email->email = $email;
		
		$merge_vars = new StdClass();
		$merge_vars->optin_ip = Tools::getRemoteAddr();
		$merge_vars->mc_language = $this->context->language->iso_code;
		$subscribe_info = array('apikey' => $this->api_key,
				'id' => $id_list,
				'email' => $new_email,
				'merge_vars' => $merge_vars,
				'email_type' => 'html',
				'double_optin' => 'false'
			);
		try {
			$result = $this->api->call('lists/subscribe', $subscribe_info);
		}
		catch(Exception $e) {
		}
	}

	public function getUnsubscribed($id_list)
	{
		$unsubscribe = $this->getListMembers($id_list, 'unsubscribed');

		foreach ($unsubscribe['data'] as $client)
			$this->unsubscribeFromPrestashop($client['email']);
		unset($unsubscribe);
	}
	
	protected function unsubscribeFromPrestashop($email)
	{
		$update_query = 'UPDATE '._DB_PREFIX_.'customer 
									SET `newsletter` = 0 WHERE `email` = "'.pSQL($email).'"';
		return Db::getInstance()->execute($update_query);
	}

	public function getListMembers($id_list, $status = 'subscribed')
	{
		if (is_string($id_list))
			return $this->api->call('lists/members', array('apikey' => $this->api_key, 'id' => $id_list, 'status' => $status));

		return null;
	}

	public function getListSegments($id_list)
	{
		if (is_string($id_list))
		{
			$result = $this->api->call('lists/segments', array('apikey' => $this->api_key, 'id' => $id_list));
			return $result;
		}
		return null;
	}

	public function getSegementOpts($id_list, $id_segment)
	{
		$segments = $this->getListSegments($id_list);
		$static = $segments['static'];
		$saved = $segments['saved'];
		foreach ($static as $seg)
			if ($seg['id'] == $id_segment)
				return $seg['segment_opts'];

		foreach ($saved as $seg)
			if ($seg['id'] == $id_segment)
				return $seg['segment_opts'];
		return 0;
	}

	public function addBatch($id_list, $batch)
	{
		$mail_confirmation = (int)Configuration::get('add_client_confirmation_mail');
		if ($mail_confirmation === 0)
			$result = $this->api->call('lists/batch-subscribe', array('apikey' => $this->api_key, 'id' => $id_list, 'batch' => $batch, 'double_optin' => 'false'));
		else
			$result = $this->api->call('lists/batch-subscribe', array('apikey' => $this->api_key, 'id' => $id_list, 'batch' => $batch));

		return $result;
	}

	public function addToList($id_list)
	{
		$block_newsletter = (int)Configuration::get('mailchimp_include_blocknewsletter_list');
		$extra_emails = array();
		if ($block_newsletter === 1)
			$extra_emails = Db::getInstance()->ExecuteS('SELECT email
														FROM '._DB_PREFIX_.'newsletter
														WHERE active = 1');

		$this->connect();
		$array_newsletter = Db::getInstance()->ExecuteS('
		SELECT email
		FROM '._DB_PREFIX_.'customer
		WHERE newsletter = 1
		AND active = 1');
		$array_newsletter = array_merge($array_newsletter, $extra_emails);

		$batch = array();
		$i = 0;

		foreach ($array_newsletter as $email)
		{
			$new_email = new StdClass();
			$new_email->email = $email['email'];
			$batch[$i]['email'] = $new_email;
			$batch[$i]['type'] = 'html';
			$i++;
		}
		return $this->addBatch($id_list, $batch);
	}

	public function addToDetailedList($id_list)
	{
		$this->connect();
		$array_newsletter = Db::getInstance()->ExecuteS('
		SELECT email, id_gender, id_lang, firstname, lastname, birthday
		FROM '._DB_PREFIX_.'customer
		WHERE newsletter = 1
		AND active = 1');
		$batch = array();
		$i = 0;

		foreach ($array_newsletter as $email)
		{
			$new_email = new StdClass();
			$new_email->email = $email['email'];
			$batch[$i]['email'] = $new_email;
			$batch[$i]['type'] = 'html';
			$i++;
		}
		return $this->addBatch($id_list, $batch);
	}

	public function createCampaign($id_list, $content_inner, $type, $sections, $options, $id_segment)
	{
		$this->connect();
		$content = new StdClass();
		if ($type == 'plaintext')
			$content->text = $content_inner;
		if ($type == 'template')
		{
			$content->sections = $sections;
			$type = 'regular';
		}
		else
			$content->html = $content_inner;
		$create_campaign_array = array('apikey' => $this->api_key,
										'type' => $type,
										'options' => $options,
										'content' => $content
									);
		if ($id_segment != null || $id_segment != 'none')
		{
			$segment_opts = $this->getSegementOpts($id_list, $id_segment);
			if ($segment_opts != 0)
				$create_campaign_array['segment_opts'] = $segment_opts;
		}

			try
			{
				$result = $this->api->call('campaigns/create', $create_campaign_array);
			}
			catch (Mailchimp_Error $e)
			{
				$a = array(
					'status' => 'error',
					'error' =>$e->getMessage()
				);
				return Tools::jsonEncode($a);
			}
		return $result;
	}

	public function editCampaign($id_campaign, $value)
	{
		$this->connect();
		$edit_campaign_array = array('apikey' => $this->api_key,
										'cid' => $id_campaign,
										'name' => 'content',
										'value' => $value
									);
		$result = $this->api->call('campaigns/update', $edit_campaign_array);
		return $result;
	}

	public function getCampaigns()
	{
		$this->connect();
		$result = $this->api->call('campaigns/list', array('apikey' => $this->api_key));
		return $result;
	}

	public function getCampaign($id_campaign = null)
	{
		$this->connect();
		$filters = new StdClass();
		$filters->campaign_id = $id_campaign;
		$result = $this->api->call('campaigns/list', array('apikey' => $this->api_key, 'filters' => $filters));
		return $result;
	}

	public function previewCampaign($id_campaign, $email, $type)
	{
		if (is_string($id_campaign))
		{
			$this->connect();
			if ($type == 'plaintext')
				$send_type = 'text';
			else
				$send_type = 'html';

			$emails = array();
			array_push($emails, $email);
			$result = $this->api->call('campaigns/send-test', array('apikey' => $this->api_key, 'cid' => $id_campaign, 'test_emails' => $emails, 'send_type' => $send_type));
			return $result;
		}
		return null;
	}

	public function previewCampaignInModule($id_campaign)
	{
		$this->connect();
		$result = $this->api->call('campaigns/content', array('apikey' => $this->api_key, 'cid' => $id_campaign));
		return $result;
	}

	public function createCampaignTable($data)
	{
		$context = Context::getContext();
		$context->smarty->assign(array(
			'campaign_data' => $data,
			));
		$table = $context->smarty->fetch(dirname(__FILE__).'/views/templates/admin/tabs/campaignTable.tpl');
		return $table;
	}

	public function sendCampaign($cid)
	{
		if (is_string($cid))
		{
			$this->connect();
			$campaign = $this->getCampaign($cid);
			$this->getUnsubscribed($campaign['data'][0]['list_id']); /* Checks all unsubscribed emails against Prestashop block newsletter and changes their preference */
			$result = $this->api->call('campaigns/send', array('apikey' => $this->api_key, 'cid' => $cid));
			return $result;
		}
		return null;
	}

	public function replicateCampaign($cid)
	{
		if (is_string($cid))
		{
			$this->connect();
			$result = $this->api->call('campaigns/replicate', array('apikey' => $this->api_key, 'cid' => $cid));
			return $result;
		}
		return null;
	}

	public function scheduleCampaign($cid, $date_time)
	{
		if (is_string($cid))
		{
			$this->connect();
			$result = $this->api->call('campaigns/schedule', array('apikey' => $this->api_key, 'cid' => $cid, 'schedule_time' => $date_time));
			return $result;
		}
		return null;
	}

	public function deleteCampaign($cid)
	{
		if (is_string($cid))
		{
			$this->connect();
			$result = $this->api->call('campaigns/delete', array('apikey' => $this->api_key, 'cid' => $cid));
			return $result;
		}
		return null;
	}

	public function getCampaignContent($cid)
	{
		if (is_string($cid))
		{
			$options = new StdClass();
			$options->view = 'raw';
			$this->connect();
			$result = $this->api->call('campaigns/content', array('apikey' => $this->api_key, 'cid' => $cid, 'options' => $options));
			return $result;
		}
		return null;
	}

	public function reportSummaryInfo($data)
	{
		$this->connect();

		$summary_array = array();
		$opened_array = array();
		$campaign_array = array();
		$campaign_count = 0;
		foreach ($data as $campaign)
		{
			if ($campaign['status'] == 'sent')
			{
				$summary = $this->api->call('reports/summary', array('apikey' => $this->api_key, 'cid' => $campaign['id']));
				$opened = $this->api->call('reports/opened', array('apikey' => $this->api_key, 'cid' => $campaign['id']));
				array_push($opened_array, $opened);
				array_push($summary_array, $summary);
				array_push($campaign_array, $campaign);
				$campaign_count++;
			}
		}
		$statistics = array();
		$statistics['count'] = $campaign_count - 1;
		$statistics['campaign'] = $campaign_array;
		$statistics['summary'] = $summary_array;
		$statistics['opened'] = $opened_array;
		return $statistics;
	}

	public function viewCampaign($id_campaign)
	{
		if (is_string($id_campaign))
		{
			$this->connect();

			$advice = $this->api->call('reports/advice', array('apikey' => $this->api_key, 'cid' => $id_campaign));
			$result = array();
			$result['status'] = $advice['status'];
			$result['html'] .= '<tr>
						<td>'.$advice['0']['type'].'</td>
						<td>'.$advice['0']['msg'].'</td>
					</tr>';
			return $result;
		}
		return null;
	}

	public function getTemplates()
	{
		$this->connect();
		$types = new StdClass();
		$types->user = true;
		$types->gallery = true;
		$types->base = true;

		$filters = new StdClass();
		$filters->include_drag_and_drop = true;
		$result = $this->api->call('templates/list', array('apikey' => $this->api_key, 'types' => $types, 'filters' => $filters));
		return $result;
	}

	public function getTemplate($id_template)
	{
		if (is_string($id_template))
		{
			$this->connect();
			try {
				$result = $this->api->call('templates/info', array('apikey' => $this->api_key, 'template_id' => $id_template));
			}
			catch (Mailchimp_Error $e)
			{
				$a = array(
					'status' => 'error',
					'error' =>$e->getMessage()
				);
				return Tools::jsonEncode($a);
			}
			return $result;
		}
		return null;
	}

	public function checkSegmentNameExists($id_list, $name)
	{
		$segments = $this->getListSegments($id_list);
		foreach ($segments['static'] as $static)
			if ($name === $static['name'])
				return false;
		foreach ($segments['saved'] as $saved)
			if ($name === $saved['name'])
				return false;
		return true;
	}

	public function createSegment()
	{
		$this->connect();
		$id_client_list = pSQL(Tools::getValue('segment_client_list'));
		$segment_name = pSQL(Tools::getValue('segment_name'));
		if ($this->checkSegmentNameExists($id_client_list, $segment_name))
		{
			$segment_opts = new StdClass();
			$segment_opts->match = 'all';
			$segment_opts->conditions = $this->getSegConditions();

			$opts = new StdClass();
			$opts->type = 'saved';
			$opts->name = $segment_name;
			$opts->segment_opts = $segment_opts;

			$new_segment_data = array('apikey' => $this->api_key,
										'id' => $id_client_list,
										'opts' =>$opts);

			$this->api->call('lists/segment-add', $new_segment_data);
			return 0;
		}
		else
			return 1;
	}

	public function getSegConditions()
	{
		$conditions = array();
		$index = 1;

		while (Tools::getValue('new_seg_field_'.$index) !== 'empty')
		{
			$field = pSQL(Tools::getValue('new_seg_field_'.$index));
			switch ($field)
			{
				case 'mc_language':
					$op = pSQL(Tools::getValue('new_seg_op_lang_'.$index));
					$value = pSQL(Tools::getValue('new_seg_value_lang_'.$index));
					break;
				case 'aim':
					$op = pSQL(Tools::getValue('new_seg_op_aim_'.$index));
					$value = pSQL(Tools::getValue('new_seg_value_campaign_'.$index));
					break;
				case 'social_gender':
					$op = pSQL(Tools::getValue('new_seg_op_gender_'.$index));
					$value = pSQL(Tools::getValue('new_seg_value_gender_'.$index));
					break;
				case 'social_age':
					$op = pSQL(Tools::getValue('new_seg_op_age_'.$index));
					$value = pSQL(Tools::getValue('new_seg_value_age_'.$index));
					break;
				case 'social_network':
					$op = pSQL(Tools::getValue('new_seg_op_social_'.$index));
					$value = pSQL(Tools::getValue('new_seg_value_social_'.$index));
					break;
			}

			$cond = new StdClass();
			$cond->field = $field;
			$cond->op = $op;
			$cond->value = $value;
			array_push($conditions, $cond);
			$index++;
		}
		return $conditions;
	}

	public function getProductInfo($lang, $value)
	{
		$currency = Currency::getDefaultCurrency();
		$symbol = $currency->sign;

		$query = 'SELECT DISTINCT pl.id_product, pl.name FROM '._DB_PREFIX_.'product_lang pl WHERE pl.id_lang = '.(int)$lang.' AND pl.name LIKE "%'.pSQL($value).'%"';
		$result = Db::getInstance()->ExecuteS($query);

		$products = array();
		foreach ($result as $row)
		{
			$prod = new Product((int)$row['id_product']);

			$id_specific_price = $this->getSpecificId((int)$row['id_product']);

			$image_link = $this->getImage((int)$row['id_product']);
			if (!Validate::isLoadedObject($prod))
				Tools::displayError('Error loading $prod (function (getProductInfo)).');

			$price = number_format($prod->price, 2);
			$row['id_specific_price'] = $id_specific_price;
			$row['price'] = $price;
			$row['currency_symbol'] = $symbol;
			$row['image'] = $image_link;
			array_push($products, $row);
		}
		return $products;
	}

	public function getSpecificId($id_product)
	{
		$query_existing_specific_price_products = 'SELECT `id_specific_price` FROM `'._DB_PREFIX_.'specific_price`
					WHERE (`to` > NOW() OR `to` = "0000-00-00 00:00:00") AND `id_customer` = 0 AND id_product = '.(int)$id_product;
		$id_specific_price = Db::getInstance()->getValue($query_existing_specific_price_products);
		if ($id_specific_price != null)
			return $id_specific_price;
		return 0;
	}

	public function getImage($p_id, $id_image = null)
	{
		$context = Context::getcontext();
		$query_image_id = '
							SELECT id_image
							FROM '._DB_PREFIX_.'image
							WHERE id_product = '.(int)$p_id;

		if ($id_image != null)
			$query_image_id .= ' AND id_image = '.(int)$id_image;

		$query_image_id .= ' ORDER BY cover DESC';

		$images = Db::getInstance()->ExecuteS($query_image_id);

		$query = 'SELECT link_rewrite FROM '._DB_PREFIX_.'product_lang
			  WHERE id_product = '.(int)$p_id.' AND id_lang = '.(int)$context->language->id;
		$link_rewrite = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);

		$link = $context->link;

		if (Configuration::get('PS_LEGACY_IMAGES'))
			$image_link = Tools::getShopDomain(true).'/img/p/'.(int)$p_id.'-'.$images[0]['id_image'].'-large.jpg';
		else
			$image_link = $link->getImageLink($link_rewrite[0]['link_rewrite'], $images[0]['id_image']);

		return $image_link;
	}

	public function getLangArray()
	{
		$langs = array(
			'English' => 'en',
			'Arabic' => 'ar',
			'Afrikaans' => 'af',
			'Belarusian' => 'be',
			'Bulgarian' => 'bg',
			'Catalan' => 'ca',
			'Chinese' => 'zh',
			'Croatian' => 'hr',
			'Czech' => 'cs',
			'Danish' => 'da',
			'Dutch' => 'nl',
			'Estonian' => 'et',
			'Farsi' => 'fa',
			'Finnish' => 'fi',
			'French (France)' => 'fr',
			'French (Canada)' => 'fr_CA',
			'German' => 'de',
			'Greek' => 'el',
			'Hebrew' => 'he',
			'Hindi' => 'hi',
			'Hungarian' => 'hu',
			'Icelandic' => 'is',
			'Indonesian' => 'id',
			'Irish' => 'ga',
			'Italian' => 'it',
			'Japanese' => 'ja',
			'Khmer' => 'km',
			'Korean' => 'ko',
			'Latvian' => 'lv',
			'Lithuanian' => 'lt',
			'Maltese' => 'mt',
			'Malay' => 'ms',
			'Macedonian' => 'mk',
			'Norwegian' => 'no',
			'Polish' => 'pl',
			'Portuguese (Brazil)' => 'pt',
			'Portuguese (Portugal)' => 'pt_PT',
			'Romanian' => 'ro',
			'Russian' => 'ru',
			'Serbian' => 'sr',
			'Slovak' => 'sk',
			'Slovenian' => 'sl',
			'Spanish (Mexico)' => 'es',
			'Spanish (Spain)' => 'es_ES',
			'Swahili' => 'sw',
			'Swedish' => 'sv',
			'Tamil' => 'ta',
			'Thai' => 'th',
			'Turkish' => 'tr',
			'Ukrainian' => 'uk',
			'Vietnamese' => 'vi',
			);
		return $langs;
	}

	public function generateToken()
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randstring = '';
		for ($i = 0; $i < 20; $i++)
			$randstring .= $characters[rand(0, Tools::strlen($characters))];
		return $randstring;
	}

	private function getLang()
	{
		if (self::$lang_cache == null && !is_array(self::$lang_cache))
		{
			self::$lang_cache = array();
			if ($languages = Language::getLanguages())
			{
				foreach ($languages as $row)
				{
						$exprow = explode(' (', $row['name']);
						$subtitle = (isset($exprow[1]) ? trim(Tools::substr($exprow[1], 0, -1)) : '');
						self::$lang_cache[$row['iso_code']] = array (
								'title' => trim($exprow[0]),
								'subtitle' => $subtitle
						);
				}
				/* Clean memory */
				unset($row, $exprow, $subtitle, $languages);
			}
		}
	}

	/**
	* Loads asset resources
	*/
	public function loadAsset()
	{
		$css_compatibility = $js_compatibility = array();

		/* Load CSS */
		$css = array(
			$this->css_path.'bootstrap-select.min.css',
			$this->css_path.'bootstrap-dialog.min.css',
			$this->css_path.'bootstrap.vertical-tabs.min.css',
			$this->css_path.'DT_bootstrap.css',
			$this->css_path.$this->name.'.css',
		);
		if (version_compare(_PS_VERSION_, '1.6', '<'))
		{
			$css_compatibility = array(
				$this->css_path.'bootstrap.min.css',
				$this->css_path.'bootstrap.extend.css',
				$this->css_path.'bootstrap-responsive.min.css',
				$this->css_path.'font-awesome.min.css',
			);
			$css = array_merge($css_compatibility, $css);
		}
		$this->context->controller->addCSS($css, 'all');

		/* Load JS */
		$js = array(
			$this->js_path.'bootstrap-select.min.js',
			$this->js_path.'bootstrap-dialog.js',
			$this->js_path.'jquery.autosize.min.js',
			$this->js_path.'jquery.dataTables.js',
			$this->js_path.'DT_bootstrap.js',
			$this->js_path.'dynamic_table_init.js',
			$this->js_path.$this->name.'.js',
			__PS_BASE_URI__.'js/tiny_mce/tinymce.min.js',
			__PS_BASE_URI__.'js/tinymce.inc.js'
		);
		if (version_compare(_PS_VERSION_, '1.6', '<'))
		{
			$js_compatibility = array(
				$this->js_path.'bootstrap.min.js'
			);
			$js = array_merge($js_compatibility, $js);
		}
		$this->context->controller->addJS($js);

		/* Clean memory */
		unset($js, $css, $js_compatibility, $css_compatibility);
	}
}