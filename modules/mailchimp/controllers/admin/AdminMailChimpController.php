<?php
/**
* 2007-2015 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2015 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*/

class AdminMailChimpController extends ModuleAdminController
{
	public function ajaxProcessSetVal()
	{
		$id_config = pSQL(Tools::getValue('config_name'));
		$val = pSQL(Tools::getValue('val'));
		$result = Configuration::updateValue($id_config, $val);
		echo $result;
	}
	
	public function ajaxProcessMailchimpProductSearch()
	{
		$context = Context::getContext();
		$value = pSQL(trim(Tools::getValue('value')));
		$lang = (int)Tools::getValue('lang');
		$type = pSQL(Tools::getValue('type'));
		$mailchimp = new MailChimp();
		$product_info = $mailchimp->getProductInfo($lang, $value);

		$json = array();
		$html = '';
		$context->smarty->assign(array(
				'currency_symbol' => $context->currency->getSign(),
				'type' => $type,
				));
		foreach ($product_info as $product)
		{
			$context->smarty->assign(array(
				'product' => $product,
				));
			$html .= $context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/tabs/productSearchResult.tpl');
			
		}
		$json['content'] = $html;
		exit(Tools::jsonEncode($json));
	}

	public function ajaxProcessAddProductsToNewsletter()
	{
		$mailchimp = new MailChimp();
		$id_products = Tools::getValue('id_products');
		$id_lang = (int)Tools::getValue('id_lang');
		$id_currency = (int)Tools::getValue('id_currency');
		$currency = new Currency($id_currency);
		//$conversion_rate = $currency->getConversationRate(); // This function is only in PS v1.6.0.11
		$conversion_rate = (int)Configuration::get('PS_CURRENCY_DEFAULT') ? $currency->conversion_rate : 1;
		$products_per_row = (int)Tools::getValue('products_per_row');
		$extra_column = pSQL(Tools::getValue('extra_column'));
		$tax_included = (int)Tools::getValue('tax_included');
		$products = array();
		foreach ($id_products as $id_product)
		{
			$info = array();
			$product = new Product($id_product);
			$info['name'] = Product::getProductName($id_product, null, $id_lang);
			if ($tax_included === 1)
				$info['price'] = number_format($product->getPrice(true), 2);
			else
				$info['price'] = number_format($product->getPrice(false), 2);
			$converted_price = $info['price'];
			if ($conversion_rate != 1)
				$converted_price = $info['price'] * $conversion_rate;
			$info['display_price'] = Tools::displayPrice($converted_price, $id_currency);
			$info['image'] = $mailchimp->getImage($id_product);
			$link_obj = new Link();
			$info['link'] = $link_obj->getProductLink($product);
			$info['id_specific_price'] = $mailchimp->getSpecificId($id_product);
			array_push($products, $info);
		}

		$context = Context::getContext();
		$context->smarty->assign(array(
			'products' => $products,
			'products_per_row' => $products_per_row,
			'extra_column' => $extra_column,
			'currency_symbol' => $context->currency->getSign()
			));
		$html = $context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/tabs/productHtmlForNewsletter.tpl');
		$json['content'] = $html;
		exit(Tools::jsonEncode($json));
	}

	public function ajaxProcessAddProductToPreNewsletterList()
	{
		$id_product = (int)Tools::getValue('id_product');
		$type = pSQL(Tools::getValue('type'));
		$id_lang = (int)Tools::getValue('id_lang');
		$product = new Product($id_product);
		$name = Product::getProductName($id_product, null, $id_lang);
		$context = Context::getContext();
		$context->smarty->assign(array(
			'type' => $type,
			'id_product' => $id_product,
			'name' => $name,
			));
		$html = $context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/tabs/productHtmlForPreNewsletterList.tpl');
		$json['type'] = $type;
		$json['content'] = $html;
		exit(Tools::jsonEncode($json));
	}

	public function ajaxProcessAddToList()
	{
		$id_list = pSQL(Tools::getValue('id_list'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$value = $ps_mailchimp->addToList($id_list);
		$result = array();

		if (!array_key_exists('status', $value))
			$result['status'] = 'ok';

		$result['html'] = '<tr>
								<td>'.$value['add_count'].'</td>
								<td>'.$value['update_count'].'</td>
								<td>'.$value['error_count'].'</td>
							</tr>';
		if ($value['error_count'] > 0)
		{
			foreach ($value['errors'] as $error)
				$result['html'] .= '<tr>
									<td colspan="3">'.$error['error'].'</td>
								</tr>';
		}
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessAddToDetailedList()
	{
		$id_list = pSQL(Tools::getValue('id_list'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$value = $ps_mailchimp->addToDetailedList($id_list);
		$result = array();
		if (!array_key_exists('status', $value))
			$result['status'] = 'ok';
		else
			$result['status'] = $value['status'];
		$result['html'] = '<tr>
					<td>'.$value['add_count'].'</td>
					<td>'.$value['update_count'].'</td>
					<td>'.$value['error_count'].'</td>
				</tr>';
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessCreateCampaign()
	{
		$id_list = pSQL(Tools::getValue('id_list'));
		$subject = pSQL(Tools::getValue('subject'));
		$from_email = pSQL(Tools::getValue('from_email'));
		$from_name = pSQL(Tools::getValue('from_name'));
		$to_name = pSQL(Tools::getValue('to_name'));
		$content = Tools::getValue('content');
		$type = pSQL(Tools::getValue('campaign_type'));
		$id_segment = Tools::getValue('id_segment');
		$id_template = null;

		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();

		$options = new StdClass();
		$options->list_id = $id_list;
		$options->subject = $subject;
		$options->from_email = $from_email;
		$options->from_name = $from_name;
		$options->to_name = $to_name;

		$sections = new StdClass();

		if ($type == 'template')
		{
			$id_template = pSQL(Tools::getValue('id_template'));
			$template_type = Tools::substr(pSQL(Tools::getValue('template_type')), 0, 4);

			if ($template_type == 'gall')
				$options->gallery_template_id = $id_template;
			else
				$options->template_id = $id_template;
			$header = pSQL(Tools::getValue('mcedit_header_content'));
			$mcedit_header_name = pSQL(Tools::getValue('section_name_header'));
			$mcedit_content_name = pSQL(Tools::getValue('section_name'));
			if ($header != null || $header != '')
				$sections->$mcedit_header_name = $header;
			$sections->$mcedit_content_name = $content;
		}
		
		$result = $ps_mailchimp->createCampaign($id_list, $content, $type, $sections, $options, $id_segment);

		if ($result['status'] == 'error')
		{
			exit(Tools::jsonEncode($s));
		}
		else
			exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessEditCampaign()
	{
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$content = (string)Tools::getValue('content');
		$content_name = (string)Tools::getValue('content_name');
		$header = (string)Tools::getValue('header');
		$header_name = (string)Tools::getValue('header_name');
		$type = pSQL(Tools::getValue('type'));

		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();

		if ($type == 'regular')
			$value = array('html' => $content);
		if ($type == 'text')
			$value = array('text' => $content);
		if ($type == 'template')
		{
			$sections = new StdClass();
			if ($header_name != '' || $header_name != null || $header != '' || $header != null)
			{
				$sections->$header_name = $header;
				$sections->$content_name = $content;
			}
			else
				$sections->$content_name = $content;
			$value = array('sections' => $sections);
		}

		$result = $ps_mailchimp->editCampaign($id_campaign, $value);
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessSendCampaign()
	{
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$result = $ps_mailchimp->sendCampaign($id_campaign);
		if ($result['complete'] != 'true')
			echo 'error';
	}

	public function ajaxProcessViewCampaign()
	{
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$ps_mailchimp = new MailChimp();

		$result = $ps_mailchimp->viewCampaign($id_campaign);
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessPreviewCampaign()
	{
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$email = pSQL(Tools::getValue('email'));
		$type = pSQL(Tools::getValue('type'));
		$ps_mailchimp = new MailChimp();

		$result = $ps_mailchimp->previewCampaign($id_campaign, $email, $type);
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessPreviewCampaignInModule()
	{
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$ps_mailchimp = new MailChimp();

		$result = $ps_mailchimp->previewCampaignInModule($id_campaign);
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessCreateSegmentTable()
	{
		$id_list = pSQL(Tools::getValue('id_list'));
		$index = pSQL(Tools::getValue('index'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$segments = $ps_mailchimp->getListSegments($id_list);
		$static = $segments['static'];
		$saved = $segments['saved'];
		$result = array();
		$result['html'] = '';
		if (!array_key_exists('status', $result))
			$result['status'] = 'ok';
		else
			$result['status'] = $segments['status'];
		if (count($static) > 0 || count($saved) > 0)
		{
			$result['html'] = '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
						<tr><th>'.$ps_mailchimp->l('Segment ID').'</th><th>'.$ps_mailchimp->l('Segment Name').'<span style="float:right;"><i class="icon-times" style="cursor:pointer;" onclick="closeSegmentTable('.$index.');"></i></span></th></tr>';
						$count_static = count($static);
						$count_saved = count($saved);
						for ($i = 0; $i < $count_static; $i++)
							$result['html'] .= '<tr><td>'.$static[$i]['id'].'</td><td>'.$static[$i]['name'].'</td></tr>';
						for ($i = 0; $i < $count_saved; $i++)
							$result['html'] .= '<tr><td>'.$saved[$i]['id'].'</td><td>'.$saved[$i]['name'].'</td></tr>';
			$result['html'] .= '</table>';
		}
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessCreateMembersTable()
	{
		$id_list = pSQL(Tools::getValue('id_list'));
		$index = pSQL(Tools::getValue('index'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$members = $ps_mailchimp->getListMembers($id_list);
		$result = array();

		if (!array_key_exists('status', $members))
		{
			$data = $members['data'];
			$count = $members['total'];

			$context = Context::getContext();
			$context->smarty->assign(array(
				'data' => $data,
				'count' => $count,
				'index' => $index,
				));
			$table = $context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/tabs/membersTable.tpl');
			$result['table'] = $table;
		}
		else
		{
			$result['status'] = $members['status'];
			$result['error'] = $members['error'];
		}
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessGetSegments()
	{
		$id_list = pSQL(Tools::getValue('id_list'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$html = '';
		$result = array();
		$count = 0;
		$segments = $ps_mailchimp->getListSegments($id_list);

		$html = '<label>'.$ps_mailchimp->l('Select a segment').'</label><small style="color:red">&nbsp;&nbsp;'.$ps_mailchimp->l('* Optional').'</small><select id="new_campaign_segments" name="new_campaign_segments">
				<option value="none">'.$ps_mailchimp->l('Do not restrict to a segment').'</option>';
		foreach ($segments['static'] as $static)
		{
			$html .= '<option value="'.$static['id'].'">'.$static['name'].'</option>';
			$count++;
		}

		foreach ($segments['saved'] as $saved)
		{
			$html .= '<option value="'.$saved['id'].'">'.$saved['name'].'</option>';
			$count++;
		}

		$html .= '</select>';
		$result['html'] = $html;
		$result['count'] = $count;
		if (array_key_exists('status', $segments))
			$result['status'] = $segments['status'];
		else
			$result['status'] = 'ok';
		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessReplicateCampaign()
	{
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$result = array();
		$result['status'] = $ps_mailchimp->replicateCampaign($id_campaign);

		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessScheduleCampaign()
	{
		$date_time = pSQL(Tools::getValue('schedule_time_date'));
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$result = $ps_mailchimp->scheduleCampaign($id_campaign, $date_time);

		if (!array_key_exists('status', $result))
			$result['status'] = 'ok';

		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessDeleteCampaign()
	{
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$result = array();
		$result['delete'] = $ps_mailchimp->deleteCampaign($id_campaign);

		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessRefreshCampaignTable()
	{
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();

		$campaigns = $ps_mailchimp->getCampaigns();
		$campaign_data = $campaigns['data'];

		$result = array();
		$result['table'] = $ps_mailchimp->createCampaignTable($campaign_data);

		exit(Tools::jsonEncode($result));
	}

	public function ajaxProcessGetTemplateSource()
	{
		$id_template = pSQL(Tools::getValue('id_template'));
		$ps_mailchimp = new MailChimp();
		$this->module->connect();
		$template = $ps_mailchimp->getTemplate($id_template);

		if (!array_key_exists('status', $template))
			$template['status'] = 'ok';

		$template_info = array();
		$s = Tools::jsonDecode($template);

		if ($s->status == 'error')
		{
			exit(Tools::jsonEncode($s));
		}
		else
		{
			$template_info['sections'] = $template['sections'];
			$template_info['output'] = $template['source'];
			exit(Tools::jsonEncode($template_info));
		}
	}

	public function ajaxProcessGetCampaignContent()
	{
		$id_campaign = pSQL(Tools::getValue('id_campaign'));
		$ps_mailchimp = new MailChimp();
		$ps_mailchimp->connect();
		$campaign = $ps_mailchimp->getCampaignContent($id_campaign);

		exit(Tools::jsonEncode($campaign));
	}
}