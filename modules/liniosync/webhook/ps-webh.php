<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

/**
* In some cases you should not drop the tables.
* Maybe the merchant will just try to reset the module
* but does not want to loose all of the data associated to the module.
*/
define('_PS_ADMIN_DIR_', getcwd());
include(_PS_ADMIN_DIR_.'/../../../config/config.inc.php');
require_once('../classes/class.linioapi.php');
require_once('../classes/class.linioproduct.php');
require_once('../classes/class.linioorder.php');

$input = Tools::file_get_contents('php://input');

$user_id = Configuration::get('LINIO_SYNC_USER');
$last_sync = Configuration::get('LINIO_LAST_SYNC');
$customer = new Customer($user_id);
$id_lang = $context->language->id;

$obj = Tools::jsonDecode($input);
$api = new LinioApi();

switch ($obj->event) {
    case 'onProductCreated':
        foreach ($obj->payload->SellerSkus as $sku) {
			//echo 'test1';
            $id = LinioProduct::getIdBySku($sku);
			//echo 'test2';
            $prod = new LinioProduct($id, true, $id_lang);
			//echo 'test3';
            if ($prod->is_complex) {
				//echo 'test4';
                $xml = $prod->getCXmlImage($id_lang);
				//echo $xml;
            } else {
			//	echo 'test5';
                $xml = $prod->getXmlImage($id_lang);
            }
		//	echo 'test6';
            $api->postImage($xml);
        }
        break;
    case 'onOrderCreated':
	$id = (int) $obj->payload->OrderId;
	//$ord = new LinioOrder($obj->payload->OrderId);
	//echo "testttttt";
	//echo $api->handleResponse($api->getOrder($id));
	//$xl = $api->handleResponse($api->getOrder($id));
	//echo $api->getOrders($last_sync);
	//echo $api->getOrder($id);
	$items = $api->handleResponse($api->getOrderItems($obj->payload->OrderId));
	//echo $items;
	//echo 'hada huwa';
	//echo $items->Body->OrderItems->OrderItem->Sku;
	$ord = new LinioOrder();
	$item = $api->getOrderItems($obj->payload->OrderId);
	$cart = $ord->getCart($id_lang, $item);
	//echo "test cart";
    //foreach ($obj->payload->OrderId as $order) {
		//echo "el payload es $order";
		//$sql= Db::getInstance()->insert('liniosync_orders', $id);
	$xml = $api->handleResponse($api->getOrder($id));
	echo $api->getOrder($id);
	echo $api->getOrderItems($id);
	$shipp = LinioOrder::createAddress($xml->Body->Orders->Order);
	$ord = new LinioOrder($xml->Body->Orders->Order->OrderId);
	 //var_dump ($shipp);
	$ord->saveAddress($shipp);
	//echo 'test yyyyy';
	//var_dump($ord->saveAddress($shipp));
	$or = $ord->saveOrder($cart, $xml->Body->Orders->Order->OrderNumber);
	//var_dump($or);
	foreach ($items->Body->OrderItems->OrderItem as $prod) {
		
	$sql = Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."order_detail` SET
								`id_order` = (select id_order from "._DB_PREFIX_."orders where reference=".$xml->Body->Orders->Order->OrderNumber."),
								`id_shop` = 1,
								`product_id` = (select id_product from "._DB_PREFIX_."product where reference='".$items->Body->OrderItems->OrderItem->Sku."'),
								`product_name` = '".$items->Body->OrderItems->OrderItem->Name."',
								`product_quantity` = 1,
								`product_reference` = '".$items->Body->OrderItems->OrderItem->Sku."',
								`unit_price_tax_incl` = '".$items->Body->OrderItems->OrderItem->ItemPrice."';");
								 
	$qry =  Db::getInstance()->getValue("Select id_product from `"._DB_PREFIX_."product` where reference ='".$items->Body->OrderItems->OrderItem->Sku."';");
	if ($qry!='' || $qry != NULL) {
		$query = Db::getInstance()->execute('Update `'._DB_PREFIX_.'stock_available` SET quantity = quantity-1 where id_product='.$qry);
	}
	elseif(!isset($qry)){
		$qry =  Db::getInstance()->getValue("Select id_product_attribute from `"._DB_PREFIX_."product_attribute` where reference ='".$items->Body->OrderItems->OrderItem->Sku."';");
		$query = Db::getInstance()->execute('Update `'._DB_PREFIX_.'stock_available` SET quantity = quantity-1 where id_product_attribute='.$qry);
	}
	
	//$na = '0';
	//$na += $sum;
	$sum = $items->Body->OrderItems->OrderItem->ItemPrice;
	$s += $sum;
	//echo $s;
	//echo $na;
	}	
	
	
	
	$query = Db::getInstance()->execute('Update `'._DB_PREFIX_.'orders` SET
                                 total_paid_tax_incl = '.$s.',
								 total_products_wt = '.$s.'
								 where reference = '.$xml->Body->Orders->Order->OrderNumber.';');
	
	
							 /*$test = 'Select id_product from `'._DB_PREFIX_.'product` where reference ="B-ST4"';
								 echo $test;
						/*$r_count = 'INSERT INTO `'._DB_PREFIX_.'order_detail` SET
`id_order` = (select id_order from '._DB_PREFIX_.'orders where reference='.$xml->Body->Orders->Order->OrderNumber.'),
`id_shop` = 1,
`product_id` = (select id_product from '._DB_PREFIX_.'product where reference="'.$items->Body->OrderItems->OrderItem->Sku.'"),
`product_name` = "'.$items->Body->OrderItems->OrderItem->Name.'",
`product_quantity` = 1,
`product_reference` = "'.$items->Body->OrderItems->OrderItem->Sku.'";';
						echo $r_count;		*/	
    if(!isset($xml['ErrorCode'])){
		echo 'test3';
  foreach ($xml->Body->Orders->Order as $order) { 
	  echo 'test4';
	  //echo $order->OrderId;
    $r_count = Db::getInstance()->getValue('SELECT count(*) FROM '._DB_PREFIX_."liniosync_orders WHERE id_linio_order = '{$order->OrderId}'");
    if(!$r_count){
		echo 'test5';
      $ord = new LinioOrder("{$order->OrderId}");
	  var_dump($ord);
      $items = $ord->getOrderItems();
	  echo "Test5.1";
	  var_dump($ord->getOrderItems($obj->payload->OrderId));
      if($items){
		  echo 'test6';
        $cart = $ord->getCart($id_lang, $items);
		//echo "el cart $cart";
        $aux = $ord->getShippingInfo();
		var_dump($aux);
        if($aux){
			echo 'test7';
          $shipp = LinioOrder::createAddress($aux->Body->Orders->Order);
          $ord->saveAddress($shipp);
          $ord->saveOrder($cart, "{$order->OrderNumber}");
        }//*/
	}
	  }
  }
}
//}

        break;
    case 'onOrderItemsStatusChanged':
    $id = (int) $obj->payload->OrderId;
	$status = $obj->payload->NewStatus;
	$xml = $api->handleResponse($api->getOrder($id));
	$sql = Db::getInstance()->execute('Update '._DB_PREFIX_.'orders SET
                                       current_state = (select id_order_state from '._DB_PREFIX_.'order_state_lang where template= "'.$obj->payload->NewStatus.'")
                                       where reference = '.$xml->Body->Orders->Order->OrderNumber.';');
	
        break;

    default:
    # code...
        break;
}
