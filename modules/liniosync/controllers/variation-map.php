<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

define('_PS_ADMIN_DIR_', getcwd());
include(_PS_ADMIN_DIR_.'/../../../config/config.inc.php');

require_once '../classes/class.linioproduct.php';
require_once '../classes/class.linioapi.php';
require_once '../classes/class.linioorder.php';

$context = Context::getContext();

if (!$context->employee->isLoggedBack()
    || !Configuration::get('LINIO_SYNC_TOKEN')
    || Configuration::get('LINIO_SYNC_TOKEN') != Tools::getValue('token')) {
    http_response_code(401);
    echo "Please login 1: ";
    exit;
}

$id_group = Tools::getValue('group');
$api = new LinioApi();
$sync_att = Db::getInstance()->executeS('SELECT id_attribute AS ps, id_variation AS lin FROM '._DB_PREFIX_.'liniosync_variations WHERE id_attribute IN (SELECT id_attribute FROM '._DB_PREFIX_."attribute WHERE id_attribute_group = ".pSQL($id_group).")");

$attr = AttributeGroup::getAttributes(Context::getContext()->language->id, $id_group);
$rows = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'liniosync_fashion_variations ORDER BY nombre ASC');
if (!count($rows)) {
    $result = $api->getFashionVariations();
    foreach ($result as $row) {
        $arr[] = array('nombre' => "$row->Name");
    }
    Db::getInstance()->insert('liniosync_fashion_variations', $arr);
    $rows = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'liniosync_fashion_variations ORDER BY nombre ASC');
}
$opt = "<option value='0'>Select Size</option>";
foreach ($rows as $row) {
    $opt .= "<option value='{$row['id_variation']}'>{$row['nombre']}</option>";
}

foreach ($attr as $r_attr) {
    ?>

    <div class="col-md-6">
    <span><?php echo $r_attr['name']; ?></span>
    </div>
    <div class="col-md-6" style="margin-bottom: 10px;">
    <select class="form-control" name="attribute[
    <?php echo $r_attr['id_attribute']; ?>]" id="c
    <?php echo $r_attr['id_attribute']; ?>">
    <?php echo $opt; ?>
    </select>
    </div>
    <?php

}?>
<script>
var sync_att = <?php echo Tools::jsonEncode($sync_att)?>;
$(function(){
  if(sync_att)
    $.each(sync_att, function(i, val){
      $('select#c'+val.ps).val(val.lin);
      console.log(val);
    })
});

</script>
