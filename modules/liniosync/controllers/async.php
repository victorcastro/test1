<?php
 // Same as error_reporting (E_ALL); 
ini_set ('error_reporting', E_ALL); 
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

define('_PS_ADMIN_DIR_', getcwd());
include(_PS_ADMIN_DIR_.'/../../../config/config.inc.php');
require_once('../classes/class.linioapi.php');
require_once('../classes/class.linioproduct.php');

$context = Context::getContext();

if (!$context->employee->isLoggedBack()
    || !Configuration::get('LINIO_SYNC_TOKEN')
    || Configuration::get('LINIO_SYNC_TOKEN') != Tools::getValue('token')) {
    http_response_code(401);
    echo("Please login");
    exit;
}//*/


  if (Configuration::get('LINIOSYNC_LIVE_MODE') < 2) {
    $site = $context->shop->getBaseURL();
    $xml1 = "<Request><Webhook>
    <CallbackUrl>$site</CallbackUrl><Events>
    <Event>onOrderCreated</Event>
    <Event>onProductCreated</Event></Events></Webhook></Request>";
    $api->makeRequest('CreateWebhook', $xml1);
    Configuration::updateValue('LINIOSYNC_LIVE_MODE', 2);
}
/*$fulb = $_POST;
p($fulb);
$id_lang = $context->language->id;
$cat = Tools::getValue('id_category');
$lcat = Tools::getValue('cat');
$api = new LinioApi();*/
//$xml = $api->handleResponse($api->getCategoryAttributes($lcat['id']));

//***********Los if de abajo bloquean el DB:Insert, Suppongoo que son los de trynx.php 
//***********Baje $api = new LinioApi(); de la linea 31 a 76 y comentar los request response de abajo.
/*if (file_exists('controllers/cookingxml/standard-prod.xml')) {
    $xml2 = simplexml_load_file('controllers/cookingxml/standard-prod.xml');
} else {
    throw new Exception("Missing xml file", 1);
}
if (file_exists('controllers/cookingxml/standard-prod.xml')) {
    $xml3 = simplexml_load_file('controllers/cookingxml/variation-xml.xml');
} else {
    throw new Exception("Missing xml file", 1);
}*/

$fulb = $_POST;
p($fulb);
$id_lang = $context->language->id;
$cat = Tools::getValue('id_category');
$lcat = Tools::getValue('cat');
$api = new LinioApi();
//$xml = $api->handleResponse($api->getCategoryAttributes($lcat['id']));
$arr[] = array(
    'id_ps_category' => $cat,
    'id_linio_category' => $lcat['id']
  );
if (isset($arr)) {
    Db::getInstance()->insert('liniosync_sync_categories', $arr, false, true, Db::REPLACE);
}


/*$send = "<Request>\n";
$pr = new Category($cat, $id_lang);
foreach ($pr->getProducts($id_lang, 0, 100) as $rowp) {
    $prod = new LinioProduct($rowp['id_product'], true, $id_lang);
    if ($prod->is_complex) {
        $send .= $prod->getComplexXml();
    } else {
    //p($prod->getAttributeCombinations($id_lang));
        $send .= "<Product>\n".$prod->getNXml($xml2)."</Product>\n";

        foreach ($prod->attributes as $id => $val) {
            $send .= "<Product>\n". $prod->getVariationXml($id, $xml3)."</Product>\n";
        }
    }

  //echo $prod->createXml();
}
$send .="</Request>";

//$response = $api->postProduct($send);
//print_r($response);
echo "<h4 style='color: lime;'>Archivo enviado a SC</h4>";
echo "<xmp>$send</xmp>";
echo "<h4 style='color: blue;'>Respuesta de SC </h4>";
echo "<xmp>$response</xmp>";*/
