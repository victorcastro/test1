<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

define('_PS_ADMIN_DIR_', getcwd());
include(_PS_ADMIN_DIR_.'/../../../config/config.inc.php');

require_once '../classes/class.linioproduct.php';
require_once '../classes/class.linioapi.php';
require_once '../classes/class.linioorder.php';

$context = Context::getContext();

if (!$context->employee->isLoggedBack()
    || !Configuration::get('LINIO_SYNC_TOKEN')
    || Configuration::get('LINIO_SYNC_TOKEN') != Tools::getValue('token')) {
    http_response_code(401);
    var_dump($_REQUEST);
    echo("Please login");
    exit;
}


$var = Tools::getValue('attribute');

foreach ($var as $key => $value) {
    if ($value) {
        $arr[] = array(
        'id_attribute' => (int)$key,
        'id_variation' => pSQL($value),
        );
    }
}

Db::getInstance()->insert('liniosync_variations', $arr, false, true, Db::REPLACE);
