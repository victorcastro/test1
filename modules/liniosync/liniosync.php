<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once('classes/class.linioforms.php');
require_once('classes/class.linioapi.php');
require_once('classes/class.linioproduct.php');
require_once('classes/class.liniobrands.php');
require_once('classes/class.linioorder.php');

class Liniosync extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'liniosync';
        $this->tab = 'market_place';
        $this->version = '2.0.1';
        $this->author = 'Linio API Team';
        $this->need_instance = 1;
        $this->module_key = '139ad914b58a57a9bf34095cfd1fe55c';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Linio Manager Sync ®');
        $this->description = $this->l('This module is an official Prestashop Linio plugin to synchronize inventories. Linio API team makes you easy the way to manage your products in our Marketplace. The plugin respects Prestashop standards security, translations and compatibility. You could use the plugin for each regional marketplaces using your different seller accounts. Make your business easier and start using our new add-on version with the e-commerce regional leader.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (!Configuration::get('LINIO_SYNC_USER')) {
            $customer = new Customer();
            $customer->firstname = 'Linio';
            $customer->lastname = 'Service User';
            $customer->email = 'api-support@linio.com';
            $customer->passwd = 'P44s-Fu3ert3.';
            $customer->save();
            Configuration::updateValue('LINIO_SYNC_USER', $customer->id);
        }

        Configuration::updateValue('LINIOSYNC_LIVE_MODE', 1);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            Configuration::updateValue('LINIO_LAST_SYNC', date('c')) &&
            $this->registerHook('actionProductAttributeUpdate') &&
			$this->registerHook('actionProductUpdate') &&
            $this->registerHook('actionUpdateQuantity'); 
			
    }

    public function uninstall()
    {
        Configuration::deleteByName('LINIOSYNC_LIVE_MODE');
        Configuration::deleteByName('LINIO_SYNC_TAX');
        Configuration::deleteByName('LINIO_SYNC_TOKEN');
        Configuration::deleteByName('LINIO_SYNC_SESSION');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $output = null;
        $user_info = array();

        if (Tools::isSubmit('submit'.$this->name)) {
            $user_info['mail'] = (string) Tools::getValue('linio_mail');
            $user_info['api_key'] = (string) Tools::getValue('api_key');
            $user_info['country'] = (string) Tools::getValue('linio_country');
            $tax = (string) Tools::getValue('linio_tax');
            $supply = Tools::getValue('linio_time');

            if (!$user_info['api_key']
              || empty($user_info['api_key'])
              || !Validate::isGenericName($user_info['api_key'])
              || !Validate::isEmail($user_info['mail'])) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                if ($supply && Validate::isUnsignedInt($supply)) {
                    $user_info['supply'] = $supply;
                }
                Configuration::updateValue('LINIO_SYNC_SESSION', serialize($user_info));
                $warranty = Tools::getValue('linio_warranty');
                if ($warranty && Validate::isCleanHtml($warranty)) {
                    Configuration::updateValue('LINIO_SYNC_WARRANTY', $warranty);
                }
                if ($tax && Validate::isCleanHtml($tax)) {
                    $aux = unserialize(Configuration::get('LINIO_SYNC_TAX'));
                    foreach ($aux as $key => $val) {
                        $aux[$key] = false;
                    }
                    $aux[$tax] = true;
                    Configuration::updateValue('LINIO_SYNC_TAX', serialize($aux));
                }

                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        return $output.$this->displayForm();
    }

    public function displayForm()
    {
        $fields_form = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        Configuration::updateValue('LINIO_SYNC_TOKEN', md5(rand(1000, 9999)));

        $fields_form[]['form'] = LinioForms::getUserForm($this);

        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
          'save' =>
          array(
              'desc' => $this->l('Save'),
              'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
              '&token='.Tools::getAdminTokenLite('AdminModules'),
          ),
          'back' => array(
              'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
              'desc' => $this->l('Back to list')
          )
        );

        LinioForms::setUserFormValues($helper);


        return $helper->generateForm($fields_form).
        LinioForms::getFormShopProducts($this, $this->local_path);
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitLiniosyncModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return null;
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'LINIOSYNC_LIVE_MODE' => Configuration::get('LINIOSYNC_LIVE_MODE', true),
            'LINIOSYNC_ACCOUNT_EMAIL' => Configuration::get('LINIOSYNC_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'LINIOSYNC_ACCOUNT_PASSWORD' => Configuration::get('LINIOSYNC_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }
	
public function hookActionProductUpdate($params)
    {
  
   $sql = 'SELECT `reference`,
   FROM `'._DB_PREFIX_.'product`
   WHERE `id_product` = '.(int)$params['id_product'];
   $r = Db::getInstance()->getRow($sql);
   $idp = (int)$params['id_product'];
   //echo "ha houa $idp";
   $ida = 0;
   //echo $this->name;
   $api = new LinioApi();
   $pr = new LinioProduct($idp, true, Context::getContext()->language->id);
   $sku = $r['reference'];
   //echo $sku;
   $xml = simplexml_load_string($api->getProductBySku(array($sku)));
   //echo $api->getProductBySku(array($sku));
   //echo 'test18';
   $api->updateProduct($pr->getXmlDesc($ida));
   //echo $pr->getXmlDesc($ida);
   //echo 'test20';
   //echo $api->updateProduct($pr->getXmlDesc($ida));
   
  //echo "test9";
  //die;
  /*ob_start();
        p($params);
        $content = ob_get_contents();
        file_put_contents('update-pr.txt', $content, FILE_APPEND);
        ob_end_clean();*/
    }
	
    public function hookActionProductAttributeUpdate($params)
    {
		
			$sql = 'SELECT `id_product`
			FROM `'._DB_PREFIX_.'product_attribute`
			WHERE `id_product_attribute` = '.(int)$params['id_product_attribute'];
			$result = Db::getInstance()->getRow($sql);
			$idp = (int)$result['id_product'];
			//echo "este es idp $idp";
			$ida = $params['id_product_attribute'];
			//echo $this->name;
			$api = new LinioApi();
			$pr = new LinioProduct($idp, true, Context::getContext()->language->id);
			$sku = $pr->getSku($ida);
			//echo $sku;
			$xml = simplexml_load_string($api->getProductBySku(array($sku)));
			//echo $api->getProductBySku(array($sku));
			//echo 'test18';
			$api->updateProduct($pr->getXmlDesc($ida));
			//echo $pr->getXmlDesc($ida);
			//echo 'test20';
			//echo $api->updateProduct($pr->getXmlDesc($ida));
			
		//echo "test9";
		//die;
		/*ob_start();
        p($params);
        $content = ob_get_contents();
        file_put_contents('update-pr.txt', $content, FILE_APPEND);
        ob_end_clean();*/
    }

public function hookActionUpdateQuantity($params)
    {
    $sql = 'SELECT a.id_product, a.reference
    FROM `'._DB_PREFIX_.'product` a
    LEFT JOIN `'._DB_PREFIX_.'product_attribute` b on a.id_product=b.id_product
    WHERE b.id_product = '.(int)$params['id_product'];
    $result = Db::getInstance()->getRow($sql);
    //var_dump($result);
    if ($result['id_product'] !='' || $result['id_product'] != NULL) {
        $idp = $params['id_product'];
        //echo "este es idp $idp";
        $ida = $params['id_product_attribute'];
        $api = new LinioApi();
        $pr = new LinioProduct($idp, false, Context::getContext()->language->id);
        $sku = $pr->getSku($ida);
        $xml = simplexml_load_string($api->getProductBySku(array($sku)));

        $qty = (int) $xml->Body->Products->Product->Quantity;
        $res = (int) $xml->Body->Products->Product->ReservedStock;
        $avl = StockAvailable::getQuantityAvailableByProduct($idp, $ida);
        if (($qty - $res) < $avl && $avl > 3) {
            $api->updateProduct($pr->getXmlQty($ida));
            }
          } elseif(!isset($result['id_product'])) {
            $sql = 'SELECT reference
            FROM `'._DB_PREFIX_.'product`
            WHERE id_product = '.(int)$params['id_product'];
            $res = Db::getInstance()->getRow($sql);
            $idp = $params['id_product'];
            //echo "este es idp $idp";
            //$ida = $params['id_product_attribute'];
            $api = new LinioApi();
            $pr = new LinioProduct($idp, false, Context::getContext()->language->id);
            $sku = $res['reference'];
            //echo $sku;
            $xml = simplexml_load_string($api->getProductBySku(array($sku)));
            $x = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            $x .= "<Request><Product>";
            $x .= "<SellerSku>".$sku."</SellerSku>\n";
            $x .= "<Quantity>".(int)$params['quantity']."</Quantity>\n";
            $x .= "</Product></Request>";
            //echo $x;
            //$qty = (int) $xml->Body->Products->Product->Quantity;
            //$res = (int) $xml->Body->Products->Product->ReservedStock;
            //$avl = StockAvailable::getQuantityAvailableByProduct($idp, $ida);
            //if (($qty - $res) < $avl && $avl > 3) {
            //echo 'test20';
            $api->updateProduct($x);
            //echo $api->updateProduct($x);
		}
	}
}