{*
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="panel">
	<div class="row moduleconfig-header">
		<div class="col-xs-5 text-right">
			<img src="{$module_dir|escape:'html':'UTF-8'}views/img/logo.jpg" />
		</div>
		<div class="col-xs-7 text-left">
			<h2>{l s='Module' mod='liniosync'}</h2>
			<h4>{l s='Name' mod='liniosync'}</h4>
		</div>
	</div>

	<hr />

	<div class="moduleconfig-content">
		<div class="row">
			<div class="col-xs-12">
				<p>
					<h4>{l s='Done by Linio team' mod='liniosync'}</h4>
					<ul class="ul-spaced">
						<li><strong>{l s='Last version' mod='liniosync'}</strong></li>
						<li>{l s='To synchronize inventory' mod='liniosync'}</li>
						<li>{l s='To Update inventory' mod='liniosync'}</li>
						<li>{l s='Delete Inventory' mod='liniosync'}</li>
						<li>{l s='Massive upload' mod='liniosync'}</li>
					</ul>
				</p>

				<br />

				<p class="text-center">
					<strong>
						<a href="http://www.prestashop.com" target="_blank" title="Lorem ipsum dolor">
							{l s='Copyright Module' mod='liniosync' }
						</a>
					</strong>
				</p>
			</div>
		</div>
	</div>
</div>
