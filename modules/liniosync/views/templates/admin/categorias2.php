<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

//$base_url = Context::getContext()->shop->getBaseURL();
$base_url = Tools::getHttpHost(true).__PS_BASE_URI__;

$category_rows = LinioApi::getCategories();
if (!count($category_rows) && Configuration::get('LINIO_SYNC_SESSION')) {
    $api = new LinioApi();
    $api->syncCategories();
    $category_rows = LinioApi::getCategories();
}

$html = '';
$sep = '';
foreach ($category_rows as $row) {
    if (!$row['level']) {
        $html .= $sep . "<optgroup label='{$row['name']}'>";
        $sep = '</optgroup>';
    } else {
        $nbsp = str_repeat('&nbsp;', ($row['level'] - 1)* 5);
        $html .= "<option value='{$row['id_linio_category']}' title='{$row['name']}' data-fashion='{$row['is_fashion']}'>
        $nbsp {$row['name']}</option>";
    }
}

if (LinioBrands::getBrandsCount() < 1 && Configuration::get('LINIO_SYNC_SESSION')) {
    LinioBrands::saveBrands();
}

if (Configuration::get('LINIOSYNC_LIVE_MODE') < 2) {
    $api = new LinioApi();
    $site = Tools::getHttpHost(true).__PS_BASE_URI__;
    $resp = $api->createWebHook($site);
    p($resp);
    Configuration::updateValue('LINIOSYNC_LIVE_MODE', 2);
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="loadModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $obj->l('Loading');?>...</h4>
      </div>
      <div class="modal-body">
        <center>
          <img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" />
          <h4 class="modal-title"><?php echo $obj->l('Uploading products');?></h4>
        </center>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="categoryModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $obj->l('Select Category');?></h4>
      </div>
      <div class="modal-body">
        <form id="category_form">
          <!--<select id="category_select" name="category_select" class="selectpicker" data-live-search="true" data-width="80%">
            <?php //echo $html;?>
          </select>&nbsp;-->
		  <select class="form-control" id='category_select' name="category">
          <?=$html?>
          </select>
          <input type="hidden" id="r_sid" name="r_sid" value="" />
          <button type="submit" class="btn btn-info"><?php echo $obj->l('Add');?></button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="taxModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $obj->l('Select Tax');?></h4>
      </div>
      <div class="modal-body">
        <form id="tax_form">
          <div class="row">
            <div class="col-md-9" id="tax_container">

            </div>
            <div class="col-md-3">
              <button type="submit" class="btn btn-info"><?php echo $obj->l('Add');?></button>
            </div>
          </div>
          <input type="hidden" id="r_sid" name="r_sid" value="" />
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<form id="linio-category">
  <div class="panel" id="fieldset_2">
    <div class="panel-heading">
      <?php echo $obj->l('Category Mapping');?>
    </div>
    <div class="panel-body">
      <select class="selectpicker" data-live-search="true">
        <optgroup label="1">
          <option><?= $base_url?>modules/liniosync/</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
        </optgroup>
        <optgroup label="2">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
        </optgroup>
        <optgroup label="3">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
        </optgroup>
      </select>
      <table class="table" id="catalog">
        <thead>
          <tr>
            <th>ID</th>
            <th>Category name</th>
            <th>Route</th>
            <th>Linio category</th>
            <th>Tax</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Category name</th>
            <th>Route [<a href="#">Hide</a>]</th>
            <th>Linio category</th>
            <th>Tax</th>
            <th>Actions</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <div class="panel-footer">
      <input type="hidden" name="token"id="token" value="<?php echo Configuration::get('LINIO_SYNC_TOKEN');?>" />
      <button type="button" value="1" id="configuration_form_submit_btn" name="submitliniosync" class="btn btn-default pull-right"
      onclick="sendForm()">
        <i class="process-icon-save"></i><?php echo $obj->l('Save');?>
      </button>
    </div>
  </div>
</form>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css">

<script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script>

var clickCallback = function(obj){
  var button = $(obj);
  var sid = button.data('sid');
  var data = $('input#token, select#'+ sid +', input#'+ sid).serialize();
  console.log('Click: ' + sid + ' Data: ' + data);
  button.removeClass('btn-info').addClass('btn-warning').addClass('disabled').text('Cargando...');
  $('#myModal').modal('show');
};
function loadTax(obj){
    var $options = $("#linio_tax > option").clone();
    $(obj).append($options);
}
var algo = function(e){
  e.preventDefault();
  var id = $(this).data('rid');
  var data = table.row(id).data();
  console.log(data);
}


$(function(){
  var tax_options = $("#linio_tax > option").clone()

  var table = $('table#catalog').DataTable( {
    "ajax": "<?= $base_url?>modules/liniosync/categ.php",
    "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
    "order": [[ 2, 'asc' ]],
    "deferRender": true,
    'language': {
      url: 'https://cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json'
    },
    "columns": [
        { "data": "id_category" },
        { "data": "name" },
        { "data": "category_route" },
        { "render": function(data, type, row, tr){
            var txt;
            if(!row.cat)
              txt = 'Select a category';
            else {
              txt = row.cat.name;
            }
            return '<a href="#" id="category" data-rid="'+tr.row+'">'+txt+'</a>';
          }
        },
        { "render": function(data, type, row, tr){
            var txt;
            var sel = $('#tax_select option:selected');
            if(!row.tax){
              if(sel){
                row.tax = sel.val();
                txt = sel.val();
              }
              else
                txt = 'Select a Tax';
            }
            else {
              txt = row.tax;
            }
            return '<a href="#" id="tax" data-rid="'+tr.row+'">'+txt+'</a>';
          }
        },
        { "render": function(data, type, row, tr){
            var class_fo = (!row.tax || !row.cat) ? 'btn btn-info disabled' : 'btn btn-info';

            return '<button type="button" class="'+class_fo
            +'" id="sync" data-rid="'+tr.row+'">Sync</button>';
          }
        }
      ],
      "drawCallback": function(settings) {
        $("a#algo").click(function(e){
          e.preventDefault();
          var id = $(this).data('rid');
          var data = table.row(id).data();
          console.log(data);
        });

        $('a#category').click(function(e){
          e.preventDefault();
          var id = $(this).data('rid');
          $('#r_sid').val(id);
          $('#categoryModal').modal('show');
        });

        $('a#tax').click(function(e){
          e.preventDefault();
          var id = $(this).data('rid');
          $('#r_sid').val(id);
          $('#taxModal').modal('show');
        });

        $('button#sync').click(function(e){
          e.preventDefault();
          var rid = $(this).data('rid');
          var d = table.row(rid).data();
          d.token = "<?php echo Configuration::get('LINIO_SYNC_TOKEN');?>";
          console.log(d);
          $.ajax({
            url: '<?= $base_url?>modules/liniosync/trynx.php',
            //url: '<?= $base_url?>modules/liniosync/controllers/async.php',
            method: 'POST',
            data: d,
              complete: function(resp){
              console.log(resp);
              var myWindow = window.open("", "_blank");
              myWindow.document.write(resp.responseText);
            }
          });
         $.ajax({
            //url: '<?= $base_url?>modules/liniosync/trynx.php',
           url: '<?= $base_url?>modules/liniosync/controllers/async.php',
            method: 'POST',
            data: d,
            //complete: function(resp){
            //  console.log(resp);
             // var myWindow = window.open("", "_blank");
            //  myWindow.document.write(resp.responseText);
           // }
          });
        });

      }
  } );

  $('#category_form').submit(function(e){
    e.preventDefault();
    var rid = $('input#r_sid').val();
    console.log(rid);
    var sel = $('#category_select option:selected');
    var d = table.row(rid).data();
    d.cat = {
      id: sel.val(),
      name: sel.attr('title')
    };
    table.row(rid).data(d).draw();
    $('#categoryModal').modal('hide');

    if (sel.data('fashion') == 1) {
      console.log('Si es fashion');
    }
    console.log(d.cat);
  });


  if($('#linio_tax')){
    $('#tax_container').prepend($('#linio_tax').clone()
    .removeClass('fixed-width-xl').attr('id', 'tax_select'));
  }

  $('#tax_form').submit(function(e){
    e.preventDefault();
    var rid = $('input#r_sid').val();
    console.log(rid);
    var sel = $('#tax_select option:selected');
    var d = table.row(rid).data();
    d.tax = sel.val();
    table.row(rid).data(d).draw();
    $('#taxModal').modal('hide');
    console.log(d);
  });

});

</script>
