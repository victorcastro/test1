<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

$id_lang = Context::getContext()->language->id;
$cats = Category::getCategories($id_lang, true, false);

$category_rows = LinioApi::getCategories();
if (!count($category_rows) && Configuration::get('LINIO_SYNC_SESSION')) {
    $api = new LinioApi();
    $api->syncCategories();
    $category_rows = LinioApi::getCategories();
}

$opt = "<option value='0'>{$obj->l('Select category')}</option>";
foreach ($category_rows as $c_row) {
     $indent = str_repeat('&nbsp;', $c_row['level'] * 4);
     $is_fas = $c_row['is_fashion'];
     $opt .= "<option value='{$c_row['id_linio_categoria']}' data-fashion='$is_fas'>$indent{$c_row['name']}</option>";
}

$syn_opt_json = Tools::jsonEncode(LinioApi::getSyncCategories());
?>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $obj->l('Loading');?>...</h4>
      </div>
      <div class="modal-body">
        <center>
          <img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" />
          <h4 class="modal-title"><?php echo $obj->l('Uploading products');?></h4>
        </center>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<form id="linio-category">
  <div class="panel" id="fieldset_2">
    <div class="panel-heading">
      <?php echo $obj->l('Category Mapping');?>
    </div>
    <div class="panel-body">
      <?php 
      foreach ($cats as $row) {
      if ($row['level_depth'] > 1) {
          ?>
          <div class="row" style="margin-bottom: 10px;">
          <div class="col-xs-6" style="text-indent: <?php echo($row['level_depth'] - 1)?>em">
          <span><?php echo $row['name']; ?></span>
          </div>
          <div class="col-xs-6">
          <select class="form-control categories" id="
          <?php echo $row['id_category']; ?>" name="linio_cat[
          <?php echo $row['id_category']; ?>]">
            <?php echo $opt; ?>
          </select>
          </div>
          </div>
      <?php

      }
      }?>
    </div>
    <div class="panel-footer">
      <input type="hidden" name="token" value="<?php echo Configuration::get('LINIO_SYNC_TOKEN');?>" />
      <button type="button" value="1" id="configuration_form_submit_btn" name="submitliniosync" class="btn btn-default pull-right"
      onclick="sendForm()">
        <i class="process-icon-save"></i><?php echo $obj->l('Save');?>
      </button>
    </div>
  </div>
</form>
<script>
var ps_base_url = '<?php echo __PS_BASE_URI__;?>';
function sendForm(){
  var form_ser = $('#linio-category').serialize();
  console.log(form_ser);
  var is_fashion = false;
  $('select.categories').each(function(id, obj){
    if($(obj).find(':selected').data('fashion')){
      $('#fieldset_3').css('display', '');
      is_fashion = true;
    }
  });
  if(!is_fashion){
    ajaxRequest(form_ser);
  }
}

function ajaxRequest(form_ser){
  $('#myModal').modal('show');
  $.ajax({
	  //falta url de abajo?
    url: ps_base_url+'/modules/liniosync/try-ajax.php',
    type: 'POST',
    data: form_ser,
    success: function(resp){
      console.log(resp);
      var myWindow = window.open("", "_blank");
      myWindow.document.write(resp);
    },
    complete: function(resp){
      $('#myModal').modal('hide');
      console.log(resp);
    }
  });
}

$(function(){
  $('a.iframe').fancybox();
  console.log('Load.. ' + ps_base_url);
  var opt = <?php echo $syn_opt_json;?>;
  $.each(opt, function(i, val){
    $('select#'+val.ps).val(val.lin);
  })
});
</script>
