<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

$attr = AttributeGroup::getAttributesGroups(Context::getContext()->language->id);
$query = "SELECT ";
//display: none;
?>
<div class="alert alert-danger alert-dismissible" id="error" role="alert" style="display: none;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <span id="alert-txt"><strong>!Error!</strong> No se lograron guardar las configuraciónes</span>
</div>
<div class="panel" id="fieldset_3" style="">
  <div class="panel-heading">
    <?php echo $obj->l('Variations Mapping');?>
  </div>
  <div class="panel-body">
    <p>
    <?php echo $obj->l('You should match size');?>
    <?php echo $obj->l('if you want to upload products to');?>
    <b><?php echo $obj->l('Fashion');?></b>
    <?php echo $obj->l('subcategory');?>
    </p>
    <div class="row">
      <div class="col-md-4">
        <select class="form-control" id="group_var" style="margin-bottom: 10px;">
          <option>Select</option>
          <?php 
          foreach ($attr as $row) {
          if (!$row['is_color_group']) {
              ?>
              <option value="
              <?php echo $row['id_attribute_group']; ?>">
              <?php echo $row['name']; ?></option>
          <?php

          }
          }?>
        </select>
      </div>
    </div>
    <form id="var_form">
      <div class="row" id="variations">

      </div>
      <input type="hidden" name="token" value="<?php echo Configuration::get('LINIO_SYNC_TOKEN');?>" />
    </form>
  </div>
  <div class="panel-footer">
    <input type="hidden" name="token" value="<?php echo Configuration::get('LINIO_SYNC_TOKEN');?>" />
    <button type="button" value="1" id="configuration_form_submit_btn" name="submitliniosync" class="btn btn-default pull-right "
    onclick="sendForm2()">
      <i class="process-icon-save"></i><?php echo $obj->l('Save');?>
    </button>
  </div>
</div>
<script>
  var ps_base_url ='<?php echo __PS_BASE_URI__;?>';
  $('#group_var').change(function(){
    var variation_data = {
      token: $('input[name=token]').val(),
      group: $(this).val()
    };
    console.log(variation_data);
    $('div#variations').load(ps_base_url + 'modules/liniosync/controllers/variation-map.php', variation_data);
  });
  function sendForm2()
  {
    var form_ser = $('#linio-category').serialize();
    $.ajax({
      url: ps_base_url + 'modules/liniosync/controllers/savevariations.php',
      method: 'POST',
      data: $('#var_form').serialize(),
      success: function(resp){
        console.log(resp);
        $('#alert-txt').html('<strong>!Guardado!</strong> Se lograron guardar las configuraciónes correctamente');
        $('div#error').removeClass('alert-danger')
        .addClass('alert-success')
        .show();
        //ajaxRequest(form_ser);
      },
      error: function(){
        $('#alert-txt').html('<strong>!Error!</strong> No se lograron guardar las configuraciónes');
        $('div#error').removeClass('alert-success')
        .addClass('alert-danger')
        .show();

      }
    });
  }
</script>
