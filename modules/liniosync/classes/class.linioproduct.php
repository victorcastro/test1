<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class LinioProduct extends Product
{
    public $linio_category;
    public $attributes;
    public $id_lang;
    public $is_complex;
    public $l_tax;

    public function __construct($id_product = null, $full = true, $id_lang = null)
    {
        parent::__construct($id_product, $full, $id_lang);
        $query = "SELECT
            count(*)
          FROM
            "._DB_PREFIX_."liniosync_variations a,
            "._DB_PREFIX_."product_attribute_combination b,
            "._DB_PREFIX_."product_attribute c
          WHERE a.id_attribute = b.id_attribute
          AND b.id_product_attribute = c.id_product_attribute
          AND c.id_product =  $id_product ";
        $this->is_complex = Db::getInstance()->getValue($query) > 0;
        $this->id_lang = $id_lang;
        $this->linio_category = LinioProduct::getLinioCategory($this->id_category_default);
        $attr = $this->getAttributeCombinaisons($id_lang);
        $taxes = unserialize(Configuration::get('LINIO_SYNC_TAX'));
        $this->l_tax = array_search(true, $taxes);
        foreach ($attr as $row) {
            $id = $row['id_product_attribute'];
            if (isset($this->attributes[$id])) {
                $this->attributes[$id]['Variation'] .= "-{$row['attribute_name']}" ;
            } else {
                $this->attributes[$id] = array(
                  'Variation' => "{$row['attribute_name']}",
                  'ProductId' => (isset($row['ean13']) && $row['ean13'] != '' ?
                  $row['ean13'] : $row['upc']),
                  'SellerSku' => ($row['reference'] != '' ? $row['reference'] : $this->reference.'-'.$id),
                  'Quantity' => $row['quantity'],

                );
            }
        }
    }
	
    Public function prix($prix)
	{
	$query = "select 
round((a.price/100)*(100+c.rate)) 
FROM "._DB_PREFIX_."product a
inner join "._DB_PREFIX_."tax_rule b on b.id_tax_rules_group=a.id_tax_rules_group
inner join "._DB_PREFIX_."tax c on c.id_tax=b.id_tax where id_product = ". pSQL($this->id);
		//var_dump ($query);
    return Db::getInstance()->getValue($query);
		//var_dump ($prix);	
	}
	
	Public function amountprix($amountprix)
	{
	$query = "SELECT 
round((a.price/100)*(100+c.rate))-d.reduction as 'RESULTADO'
FROM "._DB_PREFIX_."product a
inner join "._DB_PREFIX_."tax_rule b on b.id_tax_rules_group=a.id_tax_rules_group
inner join "._DB_PREFIX_."tax c on c.id_tax=b.id_tax 
inner join "._DB_PREFIX_."specific_price d on a.id_product=d.id_product
where d.to>now() and d.reduction_type='amount' and a.id_product = ". pSQL($this->id); 
		//var_dump ($query);
    return Db::getInstance()->getValue($query);
		//var_dump($amountprix);	
	} 	
	
	 Public function percentageprix($percentageprix)
    {
	$query = "SELECT 
round(((a.price/100)*(100+c.rate))-(((a.price/100)*(100+c.rate))*d.reduction)) as 'RESULTADO'
FROM "._DB_PREFIX_."product a
inner join "._DB_PREFIX_."tax_rule b on b.id_tax_rules_group=a.id_tax_rules_group
inner join "._DB_PREFIX_."tax c on c.id_tax=b.id_tax 
inner join "._DB_PREFIX_."specific_price d on a.id_product=d.id_product
where d.to>now() and d.reduction_type='percentage' and a.id_product = ". pSQL($this->id); 
		//var_dump ($query);
    return Db::getInstance()->getValue($query);
  //var_dump ($percentageprix); 
     }
 
		Public function desde($from)
	{
	$query = "SELECT 
d.from
FROM "._DB_PREFIX_."product a
inner join "._DB_PREFIX_."tax_rule b on b.id_tax_rules_group=a.id_tax_rules_group
inner join "._DB_PREFIX_."tax c on c.id_tax=b.id_tax 
inner join "._DB_PREFIX_."specific_price d on a.id_product=d.id_product
where d.to>now() and d.reduction_type<>'NULL' and a.id_product = ". pSQL($this->id); 
		//var_dump ($query);
    return Db::getInstance()->getValue($query);
		//var_dump ($from);	
	}
	
		Public function hasta($to)
	{
	$query = "SELECT 
d.to
FROM "._DB_PREFIX_."product a
inner join "._DB_PREFIX_."tax_rule b on b.id_tax_rules_group=a.id_tax_rules_group
inner join "._DB_PREFIX_."tax c on c.id_tax=b.id_tax 
inner join "._DB_PREFIX_."specific_price d on a.id_product=d.id_product
where d.to>now() and d.reduction_type<>'NULL' and a.id_product = ". pSQL($this->id); 
		//var_dump ($query);
    return Db::getInstance()->getValue($query);
		//var_dump ($to);	
	}
	
    public function createXml()
    {
			
        //$this->price = round($this->price, 2);
		$this->price = round($this->prix($price), 2);
        $varr = $this->attributes;
        $xml = '<?xml version="1.0" encoding="UTF-8" ?><Request><Product>';
        $xml .= $this->getXmlProperty('SellerSku', $this->reference);
        $xml .= $this->getXmlProperty('Name', $this->name);
        //$xml .= $this->getXmlProperty('Price', $this->price);
		$xml .= $this->getXmlProperty('Price', $this->prix($price));
		if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$xml .= $this->getXmlProperty('SalePrice', $this->percentageprix($percentageprix));
		}
		elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !=''){
			$xml .= $this->getXmlProperty('SalePrice', $this->amountprix($amountprix));
		}
		else{
			$xml .= $this->getXmlProperty('SalePrice', '');
		}
		
		if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$xml .= $this->getXmlProperty('SaleStartDate', $this->desde($from));
		}
	    elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$xml .= $this->getXmlProperty('SaleStartDate', $this->desde($from));
	    }
	    else{
			$xml .= $this->getXmlProperty('SaleStartDate', '');
		}
			
        if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$xml .= $this->getXmlProperty('SaleEndDate', $this->hasta($to));
		}
		elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
	        $xml .= $this->getXmlProperty('SaleEndDate', $this->hasta($to));
	    }
		else{
			$xml .= $this->getXmlProperty('SaleEndDate', '');
		}			
		
        $xml .= $this->getXmlProperty('ProductId', $this->ean13);
        $xml .= $this->getXmlProperty('Brand', 'Pluginprestashop');
        $xml .= $this->getXmlProperty('PrimaryCategory', $this->linio_category);
        $xml .= $this->getXmlProperty('Description', $this->description);		
		
        if (count($varr)) {
            $varo = array_shift($varr);
            $xml .= $this->getXmlProperty('Variation', $varo['name']);
            $xml .= $this->getXmlProperty('Quantity', $varo['quantity']);
            $xml .= "<TaxClass>{$this->l_tax}</TaxClass>";
            $xml .= '<ProductData>
            <DeliveryTimeSupplier>2</DeliveryTimeSupplier>
            <PackageHeight>18</PackageHeight>
            <PackageLength>12</PackageLength>
            <PackageWeight>8</PackageWeight>
            <PackageWidth>10</PackageWidth>
            </ProductData>';
            foreach ($varr as $row) {
                $xml .= '</Product><Product>';
                $xml .= $this->getXmlProperty('ParentSku', $this->reference);
                $xml .= $this->getXmlProperty('Variation', $row['Variation']);
                $xml .= $this->getXmlProperty('SellerSku', $row['reference']);
                $xml .= $this->getXmlProperty('Name', $this->name);
		if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$xml .= $this->getXmlProperty('SalePrice', $this->percentageprix($percentageprix));
		    }
		    else{
			$xml .= $this->getXmlProperty('SalePrice', $this->amountprix($amountprix));
		    }
		
		if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$xml .= $this->getXmlProperty('SaleStartDate', $this->desde($from));
		    }
		    elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$xml .= $this->getXmlProperty('SaleStartDate', $this->desde($from));
	     	}
		    else{
			$xml .= $this->getXmlProperty('SaleStartDate', '');
		    }

		if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$xml .= $this->getXmlProperty('SaleEndDate', $this->hasta($to));
		    }
		    elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$xml .= $this->getXmlProperty('SaleEndDate', $this->hasta($to));
	     	}
		    else{
			$xml .= $this->getXmlProperty('SaleEndDate', '');
		    }
		
                $xml .= $this->getXmlProperty('Price', $this->prix($price));
                $xml .= $this->getXmlProperty('ProductId', $row['ean']);
                $xml .= $this->getXmlProperty('Brand', 'Pluginprestashop');
                $xml .= $this->getXmlProperty('Quantity', $row['quantity']);
                $xml .= $this->getXmlProperty('PrimaryCategory', $this->linio_category);
                $xml .= $this->getXmlProperty('Description', $this->description);
                $xml .= "<TaxClass>{$this->l_tax}</TaxClass>";
                $xml .= '<ProductData>
                <DeliveryTimeSupplier>10</DeliveryTimeSupplier>
                <PackageHeight>18</PackageHeight>
                <PackageLength>12</PackageLength>
                <PackageWeight>8</PackageWeight>
                <PackageWidth>10</PackageWidth>
                </ProductData>';
            }
        } else {
            $xml .= $this->getXmlProperty('Quantity', $this->quantity);
            $xml .= "<TaxClass>{$this->l_tax}</TaxClass>";
            $xml .= '<ProductData>
            <DeliveryTimeSupplier>2</DeliveryTimeSupplier>
            <PackageHeight>18</PackageHeight>
            <PackageLength>12</PackageLength>
            <PackageWeight>8</PackageWeight>
            <PackageWidth>10</PackageWidth>
            </ProductData>';
        }
        $xml .= '</Product></Request>';
        return $xml;
    }

    public function getComplexAttr()
    {
        $query = "SELECT a.id_product_attribute, a.id_product, b.*, c.*, d.id_attribute as var_attribute,
        d.id_product_attribute as var_product_attribute, f.name
        FROM "._DB_PREFIX_."product_attribute a, "._DB_PREFIX_."product_attribute_combination b,
        "._DB_PREFIX_."liniosync_vi_attribute c, "._DB_PREFIX_."product_attribute_combination d,
        "._DB_PREFIX_."attribute_lang f
        WHERE a.id_product_attribute = b.id_product_attribute
        AND b.id_attribute = c.id_attribute
        AND b.id_product_attribute = d.id_product_attribute
        AND b.id_attribute != d.id_attribute
        AND d.id_attribute = f.id_attribute
        AND a.id_product = ". pSQL($this->id) ."
        ORDER BY a.id_product, d.id_attribute, c.position ASC";

        $rows = Db::getInstance()->executeS($query);
        $arr = array();
        foreach ($rows as $row) {
            if (!isset($arr[$row['var_attribute']])) {
                $arr[$row['var_attribute']] = array(
                  'name' => $row['name'],
                  'attrs' =>  array($row)
                );
            } else {
                $arr[$row['var_attribute']]['attrs'][] = $row;
            }
        }

        return $arr;
    }
    //XML for fashion products
    public function getComplexXml()
    {
		$var_arr = $this->getComplexAttr();
        $xml = '';
        $xml2 = simplexml_load_file('controllers/cookingxml/product-data.xml');
        foreach ($var_arr as $row) {
            $parent = $this->attributes[$row['attrs'][0]['id_product_attribute']]['SellerSku'];
            foreach ($row['attrs'] as $pr) {
                $aux = '';
                $foo = $this->attributes[$pr['id_product_attribute']];
                $aux .= $this->getXmlProperty('SellerSku', $foo['SellerSku']);
                if ($foo['SellerSku'] != $parent) {
                    $aux .= $this->getXmlProperty('ParentSku', $parent);
                }
                $aux .= $this->getXmlProperty('Name', $this->name.' - '. $pr['name']);
                $aux .= $this->getXmlProperty('Variation', $pr['nombre']);
                $aux .= $this->getXmlProperty('Quantity', $foo['Quantity']);
		if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$aux .= $this->getXmlProperty('SalePrice', round($this->percentageprix($percentageprix),2));
		}
		elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$aux .= $this->getXmlProperty('SalePrice', round($this->amountprix($amountprix),2));
		}
		else{
			$aux .= $this->getXmlProperty('SalePrice', '');
		}
		
		if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$aux .= $this->getXmlProperty('SaleStartDate', $this->desde($from));
		}
		elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$aux .= $this->getXmlProperty('SaleStartDate', $this->desde($from));
	    }
		else{
			$aux .= $this->getXmlProperty('SaleStartDate', '');
		}
		        
	    if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$aux .= $this->getXmlProperty('SaleEndDate', $this->hasta($to));
		}
		elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$aux .= $this->getXmlProperty('SaleEndDate', $this->hasta($to));
	    }
		else{
			$aux .= $this->getXmlProperty('SaleEndDate', '');
		}
			
                $aux .= $this->getXmlProperty('Price', round($this->prix($price), 2));
				//$aux .= $this->getXmlProperty('SalePrice', round($this->percentageprix($percentageprix),2));
                $aux .= $this->getXmlProperty('ProductId', $foo['ProductId']);
                $aux .= $this->getXmlProperty('Brand', $this->getAttributeByFeedName('Brand'));
                $aux .= $this->getXmlProperty('PrimaryCategory', $this->linio_category);
                $aux .= $this->getXmlProperty('Description', $this->description);
                $aux .= "<TaxClass>{$this->l_tax}</TaxClass>";
                $aux .= "<ProductData>\n".$this->getNXml($xml2)."</ProductData>\n";

                $xml .= "<Product>$aux</Product>";
            }
        }

        return $xml;
    }

    public function getXmlProperty($name, $attribute, $check = true)
    {
        $str_attr = ($check && strpos($attribute, '<') === false) ? $attribute : "<![CDATA[$attribute]]>";
        return "<$name>$str_attr</$name>\n";
    }

    public function getXmlImage($id_lang)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?><Request><ProductImage>';
        $xml .= $this->getXmlProperty('SellerSku', $this->reference);
        $ln = new Link();
        $xml .= '<Images>';
        $arr = $this->getImages($id_lang);
        foreach ($arr as $row) {
            $xml .= $this->getXmlProperty('Image', 'http://'.$ln->getImageLink($this->link_rewrite, $row['id_image']));
        }
        $xml .= '</Images></ProductImage></Request>';
        return $xml;
    }

    public function getCXmlImage($id_lang)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?><Request>';
        $ln = new Link();
        $arr = $this->getImages($id_lang);
        $aux = '';
        $foo = $this->getComplexAttr();
        foreach ($foo as $row) {
            $parent = $this->attributes[$row['attrs'][0]['id_product_attribute']]['SellerSku'];
            $aux .= '<ProductImage>';
            $aux .= $this->getXmlProperty('SellerSku', $parent);
            $aux .= '<Images>';
            foreach ($arr as $row) {
                $aux .= $this->getXmlProperty('Image', 'http://'.$ln->getImageLink($this->link_rewrite, $row['id_image']));
            }
            $aux .= '</Images></ProductImage>';
        }

        $xml .= "$aux</Request>";

        return $xml;
    }

    public function getSku($attrib = 0)
    {
        return !$attrib ? $this->reference : $this->attributes[$attrib]['SellerSku'];
    }


    public function getXmlQty($attribute = null)
    {
        $sku = (!$attribute || !$this->attributes) ? $this->reference : $this->attributes[$attribute]['SellerSku'];
        $qty = StockAvailable::getQuantityAvailableByProduct($this->id, $attribute);
        $xml = '<?xml version="1.0" encoding="UTF-8" ?><Request><Product>';
        $xml .= $this->getXmlProperty('SellerSku', $sku);
        $xml .= $this->getXmlProperty('Quantity', $qty);
        $xml .= '</Product></Request>';
        return $xml;
    }
	
	public function getXmlDesc($attribute = null)
    {
        $sku = (!$attribute || !$this->attributes) ? $this->reference : $this->attributes[$attribute]['SellerSku'];
		//$xml2 = simplexml_load_file('controllers/cookingxml/product-data.xml');
        $xml = '<?xml version="1.0" encoding="UTF-8" ?><Request><Product>';
		$xml .= $this->getXmlProperty('SellerSku', $sku);
		//$xml .= $this->getComplexXml();
        $xml .= $this->getXmlProperty('Description', $this->description);
		$xml .= $this->getXmlProperty('Name', $this->name);
		//$xml .= $this->getXmlProperty('Price', $this->price);
		$xml .= $this->getXmlProperty('Price', $this->prix($price));
		//agregar condicion si es upc o ean
		if($this->attributes[$attribute]==0){
        $xml .= $this->getXmlProperty('ProductId', $this->upc);
        } else {
        $xml .= $this->getXmlProperty('ProductId', $this->attributes[$attribute]['ProductId']);
        }
		//$xml .= $this->getXmlProperty('ProductId', $this->upc);
		//$xml .= "<ProductData>\n".$this->getNXml($xml2)."</ProductData>\n";
		//$xml .= $this->getXmlProperty('ParentSku', $this->reference);
        //$xml .= $this->getXmlProperty('Variation', $row['Variation']);
        $xml .= "<ProductData>\n";
		$xml .= $this->getXmlProperty('ShortDescription', $this->description_short);
		//Detalles de la Condición Física en SC
		$xml .= $this->getXmlProperty('ConditionTypeNote', $this->condition);
		$xml .= $this->getXmlProperty('ConditionType', $this->getAttributeByFeedName('ConditionType'));
		$xml .= "</ProductData>\n";
		//$aux .= "<ProductData>\n".$this->getXmlProperty('ConditionTypeNote', $this->condition)."</ProductData>\n";
		//$xml .= "<ProductData>\n".$this->getXmlProperty('ConditionType', $this->getAttributeByFeedName('ConditionType'))."</ProductData>\n";
        //$xml .= $this->getXmlProperty('PrimaryCategory', $this->linio_category);
        //$xml .= $this->getXmlProperty('Description', $this->description);
        //$xml .= "<TaxClass>{$this->l_tax}</TaxClass>";
		//$xml .= $this->getNXml($xml2);
		$xml .= '</Product></Request>';
		
        return $xml;
		
    }


    public static function getLinioCategory($ps_category)
    {
        $sql = "SELECT id_linio_category FROM `"._DB_PREFIX_."liniosync_sync_categories` WHERE id_ps_category IN (
        SELECT parent.id_category FROM "._DB_PREFIX_."category AS node, "._DB_PREFIX_."category AS parent
        WHERE node.nleft BETWEEN parent.nleft AND parent.nright AND node.id_category = ".pSQL($ps_category)."
        ORDER BY parent.nleft) ORDER BY id_ps_category DESC";
        return Db::getInstance()->getValue($sql);
    }

    public function getTaxClass()
    {
        $tax = unserialize(Configuration::get('LINIO_SYNC_TAX'));
        return array_search(true, $tax);
    }

    public function getWarranty()
    {
        return Configuration::get('LINIO_SYNC_WARRANTY');
    }

    public function getNXml($xml)
    {
        $resp = '';
        foreach ($xml->children() as $key => $value) {

            if (count($value->children()) == 0) {
                $att = $this->getAttributeByFeedName("$key");
                if ($value['Mandatory'] == 1 && is_null($att)) {
                    echo "Missing mandatory value: $key for the product ".$this->name[1];
                } elseif (!is_null($att)) {
                    $resp .= "<$key>".$att."</$key>\n";
                }
            } else {
                $resp .= "<$key>\n".$this->getNXml($value)."</$key>\n";
            }

        }
        return $resp;
    }

    public function getVariationXml($id, $xml)
    {
        $resp = '';
        foreach ($xml->children() as $key => $value) {

            if (count($value->children()) == 0) {
                $att = $this->getVariationAttributeByFeedName($id, "$key");
                if ($value['Mandatory'] == 1 && is_null($att)) {
                    echo "Missing mandatory value: $key for the product ".$this->name[1];
                } elseif (!is_null($att)) {
                    $resp .= "<$key>".$att."</$key>\n";
                }
            } else {
                $resp .= "<$key>\n".$this->getVariationXml($id, $value)."</$key>\n";
            }

        }
        return $resp;
    }

    public function getVariationAttributeByFeedName($id, $attr)
    {
        $resp;
        switch ($attr) {
            case 'ParentSku':
                $resp = $this->reference;
                break;
            case 'SellerSku':
                $resp = $this->attributes[$id]['SellerSku'];
                break;
            case 'Variation':
                $resp = $this->attributes[$id]['Variation'];
                break;
            case 'ProductId':
                $resp = $this->attributes[$id]['ProductId'];
                break;
            case 'Quantity':
                $resp = $this->attributes[$id]['Quantity'];
                break;

            default:
                $resp = $this->getAttributeByFeedName($attr);
                break;
        }

        return $resp;
    }

    public function getAttributeByFeedName($attr)
    {
        $resp;
        switch ($attr) {
            case 'Name': 
            $resp = strpos($this->name, '<') == true ?
            $this->name : "<![CDATA[{$this->name}]]>";
            break;
            case 'Variation':
                $resp = $this->name;
                break;
            case 'ParentSku':
            case 'SellerSku':
                $resp = $this->reference;
                break;
            case 'PackageHeight':
                $resp = Tools::ps_round($this->height, 2);
                break;
            case 'Description':
                $resp = strpos($this->description, '<') === false ?
                $this->description : "<![CDATA[{$this->description}]]>";
                break;
            case 'ShortDescription':
                $resp = strpos($this->description_short, '<') === false ?
                $this->description_short : "<![CDATA[{$this->description_short}]]>";
                break;
            case 'Brand':
                $resp = LinioBrands::getBrandByName($this->manufacturer_name);
                if ($resp == '<![CDATA[]]>') {
                    $resp = LinioBrands::getDefaultBrand();
                }
                break;
			case 'Quantity':
                $resp = $this->quantity;
                break;
            case 'PackageLength':
                $resp = Tools::ps_round($this->depth, 2);
                break;
            case 'PackageWidth':
                $resp = Tools::ps_round($this->width, 2);
                break;
            case 'PackageWeight':
                $resp = Tools::ps_round($this->weight, 2);
                break;
            case 'DeliveryTimeSupplier':
                $resp = '10';
                break;
            case 'TaxClass':
                $resp = $this->getTaxClass();
                break;
            case 'ProductWarranty':
                $resp = $this->getWarranty();
                break;
            case 'PrimaryCategory':
                $resp = $this->linio_category;
                break;
            case 'Price':
                //$resp = Tools::ps_round($this->price, 2);
				$resp = Tools::ps_round($this->prix($price), 2);
                break;
            case 'ProductId':
                $resp = $this->ean13 && $this->ean13 != '' ? $this->ean13 : $this->upc;
                break;
            case 'ConditionType':
                $resp = $this->condition == 'new' ? 'Nuevo' : 'Reacondicionado';
                break;
            case 'ConditionTypeNote':
                $resp = $this->condition;
                break;
		    case 'SalePrice':
			if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$resp = Tools::ps_round($this->percentageprix($percentageprix),2);
		    }
		    elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$resp = Tools::ps_round($this->amountprix($amountprix),2);
	     	}
		    else{
			$resp = '';
		    }
			break;
			case 'SaleStartDate':
			    if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$resp = $this->desde($from);
		    }
		    elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$resp = $this->desde($from);
	     	}
		    else{
			$resp = '';
		    }
			break;
			case 'SaleEndDate':
			    if($this->percentageprix($percentageprix) !='' && $this->amountprix($amountprix) =='' ){
			$resp = $this->hasta($to);
		    }
		    elseif($this->percentageprix($percentageprix) =='' && $this->amountprix($amountprix) !='' ){
			$resp = $this->hasta($to);
	     	}
		    else{
			$resp = '';
		    }
			break;
			    //$resp = $this->percentageprix($percentageprix) == '' ? '' : Tools::ps_round($this->percentageprix($percentageprix), 2);
				break;
            case 'ProductMeasures':
                $resp = Tools::ps_round($this->width) .' x '.
                Tools::ps_round($this->height) .' x '. Tools::ps_round($this->depth);
                break;

            default:
                $resp = null;
                break;
        }

        return is_null($resp) || $resp === '' ? null : $resp;
    }

    public static function getIdBySku($sku)
    {
        $valid_sku = pSQL($sku);
        $query = "SELECT id_product FROM "._DB_PREFIX_."product_attribute WHERE reference = '$valid_sku'
  Union SELECT id_product FROM "._DB_PREFIX_."product WHERE reference = '$valid_sku'";
        return Db::getInstance()->getValue($query);
    }
    /*public function getNXml($xml)
    {
        $aux = array();
        foreach ($xml->xpath('Body/Attribute[ProductType!="ssr"]') as $att) {
            $feed = "{$att->FeedName}";
            $resp = $this->getAttributeByFeedName($feed);

            if (is_null($resp)) {
                if ($att->isMandatory == 1) {
                    echo "$feed is Mandatory \n";
                }
                $missing[] = $feed;
            } else {
                if($feed == 'PackageHeight'||
                $feed == 'PackageLength'||
                $feed == 'PackageWeight'||
                $feed == 'PackageWidth'||
                $feed == 'ConditionType'||
                $feed == 'ConditionTypeNote'||
                $feed == 'ShortDescription'||
                $feed == 'ProductWarranty'||
                $feed == 'DeliveryTimeSupplier'||
                $feed == 'ProductMeasures'
                )
                  $aux['ProductData'][$feed] = $resp;
                else
                  $aux[$feed] = $resp;

                //$aux['Variations'] = $this->getComplexAttr();
            }
        }
        $prod['Product'] = $aux;
        $prod[] = $this->attributes;


        echo implode($missing, ', ')."\n";
        return $prod;
    }//*/
}
