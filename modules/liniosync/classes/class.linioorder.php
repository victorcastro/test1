<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class LinioOrder
{
    private $api;
    private $id;

    private $linio_user_id;
    private $address_id;

    public function __construct($id_order)
    {
        $this->id = $id_order;
        $this->api = new LinioApi();
        $this->linio_user_id = Configuration::get('LINIO_SYNC_USER');
        $this->address_id = null;
    }

    public function getShippingInfo()
    {
        $resp = $this->api->handleResponse($this->api->getOrder($this->id));
        return isset($resp['ErrorCode']) ? false : $resp;
    }

    public function getOrderItems()
    {
        $db = Db::getInstance();
        $resp = $this->api->handleResponse($this->api->getOrderItems($this->id));

        if ($resp['ErrorCode']) {
            return false;
        }

        $arr = array();
        foreach ($resp->Body->OrderItems->OrderItem as $row) {
            $sku = (string) $row->Sku;
            $rows = $db->getRow('SELECT `id_product`, `id_product_attribute`
            FROM `' . _DB_PREFIX_ . 'product_attribute`
            WHERE `reference` = "' . pSQL($sku) . '"');

            if (!$rows and preg_match('~(\w+)-*[0-9]*~', $sku, $match)) {
                $rows = $db->getRow('SELECT `id_product`, `id_product_attribute`
                FROM `' . _DB_PREFIX_ . 'product_attribute`
                WHERE `reference` = "' . pSQL($match[1]) . '" AND id_product_attribute = '.$match[2]);
            }

            if (count($rows)) {
                if (!isset($arr[$sku])) {
                    $arr[$sku] = array_merge($rows, array('count' => 1));
                } else {
                    $arr[$sku]['count']++;
                }
            }
        }
        return isset($arr) ? $arr : false;
    }

    public function getCart($id_lang, $arr)
    {
        $cart = new Cart();
        $customer = new Customer($this->linio_user_id);

        $cart->id_currency = (int) Configuration::get('PS_CURRENCY_DEFAULT');
        $cart->id_customer = $this->linio_user_id;
        $cart->id_carrier = (int) Configuration::get('PS_CARRIER_DEFAULT');
        $cart->secure_key = $customer->secure_key;
        if ($cart->add()) {
            foreach ($arr as $pr) {
                $cart->updateQty($pr['count'], $pr['id_product'], $pr['id_product_attribute']);
            }
        }
        return $cart;
    }

    public function saveOrder($cart, $order_id)
    {
		//$xml = $api->handleResponse($this->api->getOrder($order_id));
        $order = new Order();
        $customer = new Customer($this->linio_user_id);
        $context = Context::getContext();
        $order->product_list = $cart->getProducts();
        $order->id_address_delivery = $this->address_id;
        $order->id_address_invoice = $this->address_id;
        $order->id_cart = $cart->id;
        $order->id_currency = $cart->id_currency;
        $order->id_customer = $cart->id_customer;
        $order->id_carrier = $cart->id_carrier;
        $order->payment = 'Linio Pay ';
        $order->module = 'liniosync';
        $order->conversion_rate = 1;
        $order->secure_key = $customer->secure_key;
        $order->current_state = _PS_OS_PREPARATION_;

        $order->reference = $order_id;
        $order->id_lang = $context->language->id;
        $order->id_shop = (int) $context->shop->id;
        $order->id_shop_group = (int) $context->shop->id_shop_group;

        $order->total_products = $order->total_products_wt = (float) $cart->getOrderTotal(
            false,
            Cart::ONLY_PRODUCTS,
            $order->product_list,
            $order->id_carrier
        );
        $order->total_paid_real = 0;
		//$order->total_paid_tax_incl = (int) $xml->Body->Orders->Order->OrderId->Price;
        $order->total_paid_tax_excl = $order->total_paid_tax_incl = (float) Tools::ps_round(
            (float) $cart->getOrderTotal(
                false,
                Cart::BOTH,
                $order->product_list,
                $order->id_carrier
            ),
            2
        );
        $order->total_paid = $order->total_paid_tax_incl;

        $order->invoice_date = '0000-00-00 00:00:00';
        $order->delivery_date = '0000-00-00 00:00:00';

        if ($order->add()) {
            $order_detail = new OrderDetail();
            $order_detail->createList($order, $cart, $order->current_state, $order->product_list);
            $data = array(
              'id_order' => $order->id,
              'id_linio_order' => $this->id,
            );
            Db::getInstance()->insert('liniosync_orders', $data);
            return true;
        }
        return false;
    }
	
	public static function createAddress($xml1)
    {
        $xml = $xml1->AddressShipping;
        $address = new Address();
        $address->alias = Tools::truncate('Shipp Addre Ord: '.$xml1->OrderNumber, 32);
        $address->lastname = "{$xml->LastName}";
        $address->firstname = "{$xml->FirstName}";
        $address->address1 = Tools::truncate("{$xml->Address1}", 128);
        $address->address2 = Tools::truncate("{$xml->Address2}".' '."{$xml->Address3}".' '."{$xml->Address4}", 128);
        $address->phone = "{$xml->Phone}";
        $address->phone_mobile = "{$xml->Phone2}";
        $address->city = "{$xml->City}";
        $address->postcode = "{$xml->PostCode}";
        $address->id_country = Country::getIdByName(null, "{$xml->Country}");
        return $address;
    }

    public function saveAddress($address)
    {
        $address->id_customer = $this->linio_user_id;
        $address->add();
        $this->address_id = $address->id;
    }
}
