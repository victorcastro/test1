<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class LinioForms
{

    public function __construct()
    {
    }

    public static function getUserForm($obj)
    {
        $country_opt = array(
          array(
            'id_option' => 'MX',
            'name' => $obj->l('Mexico')
          ),
          array(
            'id_option' => 'CO',
            'name' => $obj->l('Colombia')
          ),
          array(
            'id_option' => 'VE',
            'name' => $obj->l('Venezuela')
          ),
          array(
            'id_option' => 'CL',
            'name' => $obj->l('Chile')
          ),
          array(
            'id_option' => 'AR',
            'name' => $obj->l('Argentina')
          ),
          array(
            'id_option' => 'PE',
            'name' => $obj->l('Peru')
          )
        );

        $aux = array(
            'legend' => array(
              'title' => $obj->l('Configuration account'),
            ),
            'input' => array(
              array(
                'type' => 'select',
                'label' => $obj->l('Country'),
                'name' => 'linio_country',
                'required' => true,
                'options' => array(
                  'query' => $country_opt,
                  'id' => 'id_option',
                  'name' => 'name'
                )
              ),
              array(
                  'type' => 'text',
                  'label' => $obj->l('Email address'),
                  'hint' => $obj->l('Seller Center account email address'),
                  'name' => 'linio_mail',
                  'required' => true
              ),
              array(
                  'type' => 'text',
                  'label' => $obj->l('Api Key'),
                  'hint' => $obj->l('Seller Center API Key'),
                  'name' => 'api_key',
                  'required' => true
              )
          ),
          'submit' => array(
              'title' => $obj->l('Save'),
              'class' => 'btn btn-default pull-right'
          )
        );

        $taxes = LinioForms::getTaxOptions();

        $tax_opt = array();
        if ($taxes) {
            foreach ($taxes as $key => $value) {
                $tax_opt[] = array(
                  'id_option' => $key,
                  'name' => $key
                );
            }

            $aux['input'][] = array(
              'type' => 'select',
              'label' => $obj->l('Tax'),
              'name' => 'linio_tax',
              'required' => true,
              'options' => array(
                'query' => $tax_opt,
                'id' => 'id_option',
                'name' => 'name'
              )
            );

            $aux['input'][] = array(
              'type' => 'text',
              'label' => $obj->l('Supply time'),
              'name' => 'linio_time',
              'required' => true,
              'desc' => $obj->l('Number of days to supply a product')
            );

            $aux['input'][] = array(
              'type' => 'textarea',
              'label' => $obj->l('Warranty'),
              'name' => 'linio_warranty',
              'required' => true,
              'desc' => $obj->l('Describe your warranty')
            );
        }

        return $aux;
    }

    public static function getTaxOptions()
    {
        $aux = Configuration::get('LINIO_SYNC_SESSION');
        if (!$aux) {
            return false;
        }

        $user = Configuration::get('LINIO_SYNC_TAX');
        if (!$user) {
            $api = new LinioApi();
            $taxes = $api->getTaxClass();
            if (!$taxes) {
                return null;
            }
            $arr = array();
            foreach ($taxes as $tax) {
                $arr["{$tax->Name}"] = false;
            }

            Configuration::updateValue('LINIO_SYNC_TAX', serialize($arr));
            return $arr;
        }

        return unserialize($user);
    }

    public static function getFormShopProducts($obj, $path = '')
    {
        ob_start();
        include $path.'views/templates/admin/variations.php';
        include $path.'views/templates/admin/categorias2.php';
        $string = ob_get_clean();
        return $string;
    }

    public static function setUserFormValues(&$obj)
    {
        $aux = unserialize(Configuration::get('LINIO_SYNC_SESSION'));
        $tax = unserialize(Configuration::get('LINIO_SYNC_TAX'));
        $warranty = Configuration::get('LINIO_SYNC_WARRANTY');
        $obj->fields_value['linio_country'] = $aux['country'];
        $obj->fields_value['api_key'] = $aux['api_key'];
        $obj->fields_value['linio_mail'] = $aux['mail'];
        if ($tax) {
            $obj->fields_value['linio_tax'] = array_search(true, $tax);
        }
        if ($warranty) {
            $obj->fields_value['linio_warranty'] = $warranty;
        }
        if (isset($aux['supply'])) {
            $obj->fields_value['linio_time'] = $aux['supply'];
        }
    }
}
