<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class LinioBrands
{
    public static function getBrands()
    {
        $query = "SELECT * FROM "._DB_PREFIX_."liniosync_brands";
        $categ = Db::getInstance()->executeS($query);

        return $categ;
    }

    public static function getBrandByName($name)
    {
        $query = "SELECT name FROM "._DB_PREFIX_."liniosync_brands WHERE name LIKE '%".pSQL($name)."%'";
        $categ = Db::getInstance()->getValue($query);

        return ("<![CDATA[$categ]]>");
    }

    public static function getDefaultBrand()
    {
    /*$query = "SELECT name FROM "._DB_PREFIX_."liniosync_brands ORDER BY RAND()";
    $categ = Db::getInstance()->getValue($query);*/
        $categ = 'Pluginprestashop';
        return $categ;
    }

    public static function getBrandsCount()
    {
        $query = "SELECT count(*) FROM "._DB_PREFIX_."liniosync_brands";

        return Db::getInstance()->getValue($query);
    }

    public static function saveBrands()
    {
        $api = new LinioApi();
        $arr = array();

        $xml = $api->handleResponse($api->getBrands());

        foreach ($xml->Body->Brands->Brand as $row) {
            $arr[] = array('name' => pSQL("{$row->Name}"));
        }

        Db::getInstance()->insert('liniosync_brands', $arr);
    }
}
