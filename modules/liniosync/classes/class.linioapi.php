<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class LinioApi
{
    private $user;
    private $api_key;
    private $country;
    private $site;

    public function __construct()
    {
        $aux = unserialize(Configuration::get('LINIO_SYNC_SESSION'));
        $this->user = $aux['mail'];
        $this->api_key = $aux['api_key'];
        $this->country = $aux['country'];
        $sites = array(
          'MX' => 'https://sellercenter-api.linio.com.mx',
          'CO' => 'https://sellercenter-api.linio.com.co',
          'VE' => 'https://sellercenter-api.linio.com.ve',
          'CL' => 'https://sellercenter-api.linio.cl',
          'AR' => 'https://sellercenter-api.linio.com.ar',
          'PE' => 'https://sellercenter-api.linio.com.pe'
        );
        $this->site = $sites[$aux['country']];
    }

    private function makeRequest($action, $request = null, $params = array())
    {
        $usuario = $this->user;
        $api_key = $this->api_key;
        $url = $this->site;
        if ($request !== null) {
            $xml = $request;
        }

        $parameters = array(
          'Action' => $action,
          'Timestamp' => date(DateTime::ISO8601),
          'UserID' => $usuario,
          'Version' => '1.0'
        );
        $parameters = array_merge($parameters, $params);
        ksort($parameters);
        $params = array();
        foreach ($parameters as $name => $value) {
            $params[] = rawurlencode($name) . '=' . rawurlencode($value);
        }
        $strToSign = implode('&', $params);
        $parameters['Signature'] = rawurlencode(LinioApi::phpCompatHashHmac('sha256', $strToSign, $api_key, false));
        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (isset($xml)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        }
        $data = curl_exec($ch);
        $error = curl_error($ch);

        if ($error) {
            file_put_contents(getcwd().'errors.txt', $error."\n", FILE_APPEND);
        }

        curl_close($ch);

        $response = isset($parameters['Format']) && $parameters['Format'] == 'JSON' ?
        Tools::jsonDecode($data) : $data;
        return $response;
    }

    public static function getCategories()
    {
        $query = "SELECT `id_linio_category`, `name`, `level`, IF(id_liniosync_category
        BETWEEN 2242 AND 3133, true, false) as is_fashion FROM `"._DB_PREFIX_."liniosync_categories`";
        return Db::getInstance()->executeS($query);
    }

    public static function getSyncCategories()
    {
        $query = "SELECT t1.id_ps_category as ps, t1.id_linio_category as lin, t2.name as name FROM `"
        ._DB_PREFIX_."liniosync_sync_categories` as t1, "._DB_PREFIX_."liniosync_categories as t2".
        " WHERE t2.id_linio_category = t1.id_linio_category";
        return Db::getInstance()->executeS($query);
    }

    public function downloadCategories()
    {
        return $this->makeRequest('GetCategoryTree');
    }

    public function postProduct($xml)
    {
        $aux = $this->makeRequest('ProductCreate', $xml);
        $this->handleResponse($aux);
        return $aux;
    }

    public static function phpCompatHashHmac($algo, $data, $key, $raw_output = false)
    {
        $blocksize = 64;

        $ipad = str_repeat("\x36", $blocksize);
        $opad = str_repeat("\x5c", $blocksize);

        if (Tools::strlen($key) > $blocksize) {
            $key = hash($algo, $key, true);
        } else {
            $key = str_pad($key, $blocksize, "\x00");
        }

        $ipad ^= $key;
        $opad ^= $key;

        return hash($algo, $opad . hash($algo, $ipad . $data, true), $raw_output);
    }

    public function handleResponse($resp)
    {
        $xml = simplexml_load_string($resp);

        if (strpos($resp, 'ErrorResponse')) {
            $aux = array(
              'ErrorCode' => $xml->Head->ErrorCode,
              'ErrorMessage' => $xml->Head->ErrorMessage,
            );
            file_put_contents(
                getcwd().'../liniosync_error.log',
                "SC ERROR {$aux['ErrorCode']} [".date('c')."]: {$aux['ErrorMessage']} \n",
                FILE_APPEND
            );
            return null;
        }

        return $xml;
    }

    public static function createNest($name, $attrs)
    {
        $xml = "<$name>";
        foreach ($attrs as $row) {
            $xml .= $row;
        }
        $xml .= "</$name>";

        return $xml;
    }

    public static function createProductRequest($products)
    {
        return $this::createNest('Request', $products);
    }

    public static function getFeed($arr, $label = null)
    {
        $resp = '';
        foreach ($arr as $key => $value) {
            $key = is_null($label) ? $key : $label;
            if (!is_array($value)) {
                $resp .= "<$key>$value</$key>\n";
            } else {
                $resp .= "<$key>\n".LinioApi::getFeed($value)."</$key>\n";
            }
        }
        return $resp;
    }

    public function createWebHook($url)
{
  $xml1 = "<Request><Webhook>
  <CallbackUrl>$url"."modules/liniosync/webhook/ps-webh.php</CallbackUrl><Events>
  <Event>onOrderCreated</Event>
  <Event>onProductCreated</Event></Events></Webhook></Request>";
  return $this->makeRequest('CreateWebhook', $xml1);
}

    public function updateProduct($xml)
    {
        return $this->makeRequest('ProductUpdate', $xml);
    }

    public function postImage($xml)
    {
        return $this->makeRequest('Image', $xml);
    }

    public function getFeedList()
    {
        return $this->makeRequest('FeedList');
    }

    public function getFeedStatus($id_feed)
    {
        return $this->makeRequest('FeedList', null, array('FeedID' => $id_feed));
    }

    public function getProductBySku($arr)
    {
        $aux = serialize($arr);
        return $this->makeRequest('GetProducts', null, array('SkuSellerList' => $aux));
    }

    public function getCategoryAttributes($category_id)
    {
        return $this->makeRequest('GetCategoryAttributes', null, array('PrimaryCategory' => $category_id));
    }

    public function getFashionVariations()
    {
        $query = 'SELECT id_linio_category FROM `'._DB_PREFIX_.'liniosync_categories` WHERE name = "Moda"';
        $id_moda = Db::getInstance()->getValue($query);
        $aux = $this->handleResponse($this->getCategoryAttributes($id_moda));
        return $aux->xpath('Body/Attribute[Name="size"]/Options/Option');
    }

    public function getOrders($last_sync)
    {
        $sync_opt = array(
          'UpdatedAfter' => $last_sync
        );
        return $this->makeRequest('GetOrders', null, $sync_opt);
    }

    public function getBrands()
    {
        return $this->makeRequest('GetBrands', null);
    }

    public function getOrder($id_order)
    {
        $sync_opt = array(
          'OrderId' => $id_order
        );
        return $this->makeRequest('GetOrder', null, $sync_opt);
    }

    public function getOrderItems($id_order)
    {
        $sync_opt = array(
          'OrderId' => $id_order
        );
        return $this->makeRequest('GetOrderItems', null, $sync_opt);
    }

    public function getTaxClass()
    {
        $aux = $this->handleResponse($this->getCategoryAttributes('1'));
        if (!$aux) {
            return null;
        }
        return $aux->xpath('Body/Attribute[Name="TaxClass"]/Options/Option');
    }

    public function getOrdersItems($arr)
    {
        $spr = '[';
        $ser_ord = '';
        foreach ($arr as $aux) {
            $ser_ord .= "$spr $aux";
            $spr = ',';
        }
        $ser_ord .= ']';

        $sync_opt = array(
          'OrderIdList' => $ser_ord
        );
        return $this->makeRequest('GetMultipleOrderItems', null, $sync_opt);
    }

    public function syncCategories()
    {
        $aux = simplexml_load_string($this->downloadCategories());

        $arr = array();
        $this->obtenerListadoCategoriasLinio($aux->Body->Categories->Category, 0, $arr);

        Db::getInstance()->insert('liniosync_categories', $arr);
    }

    private function obtenerListadoCategoriasLinio($xmlCategorias, $nivel, &$listadoCategorias)
    {
        foreach ($xmlCategorias as $nodoCategoria) {
            $listadoCategorias[] = array(
              'id_linio_category' => (int) $nodoCategoria->CategoryId,
              'name' => (string) $nodoCategoria->Name,
              'level' => $nivel);
            foreach ($nodoCategoria->Children as $xmlCategoriaHija) {
                $this->obtenerListadoCategoriasLinio($xmlCategoriaHija, $nivel + 1, $listadoCategorias);
            }
        }
    }
}
