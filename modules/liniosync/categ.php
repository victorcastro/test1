<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

define('_PS_ADMIN_DIR_', getcwd());
include(_PS_ADMIN_DIR_.'/../../config/config.inc.php');
require_once('classes/class.linioapi.php');

$id_lang = Context::getContext()->language->id;
$raw_categories = Category::getCategories($id_lang, true, false);
$sync_cat = array();
foreach (LinioApi::getSyncCategories() as $row) {
    $sync_cat[$row['ps']] = array('id' => $row['lin'], 'name' => $row['name']);
}

$categories = array();
$categories2 = array();
//print_r($raw_categories); die;
foreach ($raw_categories as $row) {
    if ($row['level_depth'] > 1) {
        $categories[$row['id_category']] = array(
        'id_category' => $row['id_category'],
        'name' => $row['name'],
        'level_depth' => $row['level_depth'],
        'category_route' => $row['link_rewrite'],
        //'category_route' => $row['level_depth'] == 2 ? $row['link_rewrite'] : $categories[$row['id_parent']]['category_route'] ." > ". $row['link_rewrite'],
        'cat' => isset($sync_cat[$row['id_category']]) ? $sync_cat[$row['id_category']] : null,
        'tax' => null,
        'actions' => $row['id_category']
        );
    }
}

foreach ($categories as $row) {
    $categories2[] = $row;
}

$response = Tools::jsonEncode(array(
  'data' => $categories2
));

header('Content-Type: application/json');
echo "$response";
