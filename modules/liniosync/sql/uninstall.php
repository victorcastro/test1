<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

/**
* In some cases you should not drop the tables.
* Maybe the merchant will just try to reset the module
* but does not want to loose all of the data associated to the module.
*/

$sql = array();

$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_categories';
$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_fashion_variations';
$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_orders';
$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_products';
$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_product_state';
$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_sync_categories';
$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_variations';
$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_brands';
$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'liniosync_vi_attribute';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
