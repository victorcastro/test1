<?php
/**
* 2016-2024 Bazaya México S de RL de CV
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to api-support@linio.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade liniosync to newer
* versions in the future.
*
*  @author    Linio API Team <api-support@linio.com>
*  @copyright 2016-2024 Bazaya México S de RL de CV
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'liniosync_categories` (
  `id_liniosync_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_linio_category` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `level` int(10) NOT NULL DEFAULT "0",
  PRIMARY KEY (`id_liniosync_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'liniosync_fashion_variations` (
  `id_variation` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(32) NOT NULL,
  PRIMARY KEY (`id_variation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'liniosync_orders` (
  `id_order` int(11) NOT NULL,
  `id_linio_order` int(11) NOT NULL,
  `created` timestamp NOT NULL,
  `last_sync` timestamp NOT NULL,
  `is_finished` tinyint(1) NOT NULL DEFAULT "0",
  PRIMARY KEY (`id_order`),
  UNIQUE KEY `id_linio_order` (`id_linio_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'liniosync_products` (
  `id_product` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(64) NOT NULL,
  `created_time` timestamp NOT NULL,
  `update_time` timestamp NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT "0",
  `is_live` tinyint(1) NOT NULL DEFAULT "0",
  PRIMARY KEY (`id_product`),
  UNIQUE KEY `sku_2` (`sku`),
  UNIQUE KEY `sku_3` (`sku`),
  KEY `sku` (`sku`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'liniosync_product_state` (
  `id_product_state` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(64) NOT NULL,
  `feed` varchar(64) NOT NULL,
  `state` varchar(32) NOT NULL,
  `time` timestamp NOT NULL,
  PRIMARY KEY (`id_product_state`),
  KEY `sku` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'liniosync_sync_categories` (
  `id_ps_category` int(10) unsigned NOT NULL,
  `id_linio_category` int(10) unsigned NOT NULL,
  `created_date` timestamp NOT NULL,
  UNIQUE KEY `id_ps_category` (`id_ps_category`),
  KEY `id_linio_category` (`id_linio_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'liniosync_variations` (
  `id_attribute` int(11) NOT NULL,
  `id_variation` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `id_attribute` (`id_attribute`),
  KEY `id_variation` (`id_variation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'liniosync_brands` (
  `id_brand` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id_brand`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `ps_liniosync_vi_attribute` (
  `id_attribute` int(10) unsigned
  ,`id_attribute_group` int(10) unsigned
  ,`color` varchar(32)
  ,`position` int(10) unsigned
  ,`id_variation` smallint(5) unsigned
  ,`nombre` varchar(32)
)';

/*$sql[] = 'CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `'._DB_PREFIX_.'liniosync_vi_attribute` AS
  select `a`.`id_attribute` AS `id_attribute`,`a`.`id_attribute_group` AS `id_attribute_group`,`a`.`color` AS `color`,`a`.`position` AS
  `position`,`c`.`id_variation` AS `id_variation`,`c`.`nombre` AS `nombre` from ((`'._DB_PREFIX_.'attribute` `a` join `'._DB_PREFIX_.'liniosync_variations` `b`)
  join `'._DB_PREFIX_.'liniosync_fashion_variations` `c`) where ((`a`.`id_attribute` = `b`.`id_attribute`) and (`b`.`id_variation` = `c`.`id_variation`));';
*/

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
