<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Vcc_districtsperu extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'vcc_districtsperu';
        $this->tab = 'administration';
        $this->version = '1.0.4';
        $this->author = 'Victor Castro';
        $this->need_instance = 0;
        
        $this->domain = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;
        $this->getDistrictsController = $this->domain.'index.php?fc=module&module='.$this->name.'&controller=getdistricts';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Distritos del Perú');
        $this->description = $this->l('Muestra los distritos del Perú');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        include(dirname(__FILE__).'/sql/install_districts.php');
        
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayCustomerAccountForm');
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function getContent()
    {
        if (((bool)Tools::isSubmit('submitVcc_districtsperuModule')) == true) {
            $this->postProcess();
        }
        
        if (Tools::getValue('VCC_DISTRICTSPERU_CLEAN')) {
            $this->cleanData();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitVcc_districtsperuModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        
        return $helper->generateForm(array($this->getConfigForm()));
    }

    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Entiendo que todos los paises, distritos y pronvincias serán reestablecidos'),
                        'name' => 'VCC_DISTRICTSPERU_CLEAN',
                        'is_bool' => true,
                        'desc' => $this->l('Limpiar tablas e instalar data de Perú'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Reestablecer Data'),
                ),
            ),
        );
    }

    protected function getConfigFormValues()
    {
        return array(
            'VCC_DISTRICTSPERU_CLEAN' => Configuration::get('VCC_DISTRICTSPERU_CLEAN'),
        );
    }

    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, false);
        }
    }
    
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'views/js/front.js');
        $this->context->controller->addCSS($this->_path.'views/css/front.css');
    }
    
    public function hookDisplayCustomerAccountForm($params) {
        
        $this->smarty->assign(array(
			'get_distritcs' => $this->getDistrictsController,
		));
        
        if (_PS_VERSION_ <= 1.6)
            return $this->display(__FILE__, 'displayCustomerAccountForm.tpl');
        return;
    }
    
    private function cleanData(){
         include(dirname(__FILE__).'/sql/clean.php');
         
         return true;
    }
}
