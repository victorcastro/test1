# Distritos del Perú

```
INSERT INTO ps_districts (id_country, id_state, name)
SELECT
	1 as id_country, 
	24 as id_state,
	CONCAT(UCASE(MID(distrito,1,1)),LCASE(MID(distrito,2))) as name
FROM ubdistrito
WHERE idProv = 127
```