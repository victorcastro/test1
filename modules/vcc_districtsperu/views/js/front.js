/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(document).ready(function(){
    var label_country = $("select[name='id_country']").parent().parent().children('label.required')
    var label_state = $("select[name='id_state']").parent().parent().children('label')
    var label_city = $("input[name='city']").parent().parent().children('label')
    
    label_country.html('Departamento')
    label_state.html('Provincia')
    label_city.html('Distrito')
    $("input[name='city']").attr('disabled', 'disabled')
    
    $(document).on("change", "select[name='id_country']", function() {
        // No se llega a ejecutar nada por que Prestashop elimina el DOM y lo vuelve a crear
        label_country.html('Departamento')
        label_state.html('Provincia')
        label_city.html('Distrito')

        $("input[name='city']").replaceWith('<select name="city" class="form-control"><option>-</option></select>')
        $("input[name='city']").attr('disabled', 'disabled')
    })
    
    $(document).on("change", "select[name='id_state']", function() {
        $("input[name='city']").replaceWith('<select name="city" class="form-control"><option>-</option></select>')
        $('#city').empty()
        $('#city').append('<option>-</option')
        
        id_country = $("select[name='id_country'").val()
        id_state = $(this).val()
        $.ajax({
            type: "POST",
            url: '/index.php?fc=module&module=vcc_districtsperu&controller=getdistricts',
            data: {id_country : id_country, id_state: id_state},
            success: function(distritcs) {
                $("select[name='city']").find('option').remove().end()
                $.each(JSON.parse(distritcs), function (i, item) {
                    $("select[name='city']").append($('<option>', {
                        value: item,
                        text : item 
                    }));
                });
            },
        })
    })
});