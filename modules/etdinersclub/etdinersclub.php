<?php
/**
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*/

if (!defined('_PS_VERSION_'))
	exit;
class EtDinersClub extends PaymentModule
{
	private $html = '';
	private $posterrors = array();
	public $details;
	public $owner;
	public $address;
	public $extra_mail_vars;

	public function __construct()
	{
		$this->name = 'etdinersclub';
		$this->tab = 'payments_gateways';
		$this->version = '1.1.4';
		$this->author = 'victor@castrocontreras.com';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6.0', 'max' => '1.7');
		$this->module_key = 'cb95bc4966250ad493ffc5f7730b60af';

		$this->dir_img = $this->name.'/views/img/';
		$this->url_return = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'index.php?fc=module&module=etdinersclub&controller=gatewayReturn';
		$this->currencies = true;
		$this->currencies_mode = 'checkbox';
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Dinners Club');
		$this->description = $this->l('An exclusive and unique benefits card market.');
		$this->confirmUninstall = $this->l('Are you sure you want to delete all information from the module?');
	}

	public function install()
	{
		if (!parent::install()
			|| !$this->registerHook('payment')
			|| !$this->registerHook('paymentReturn')
			|| !$this->registerHook('footer')
			|| !$this->registerHook('header')
			|| !$this->registerHook('dashboardZoneOne')
			|| !$this->registerHook('dashboardZoneTwo')
			|| !$this->init())
			return false;
		return true;
	}

	public function init()
	{
		Configuration::updateValue('URL_RETURN', $this->url_return);
		Configuration::updateValue('SUMMARY', 1);
		Configuration::updateValue('MODE_DEBUG', 0);
		/*$this->createDB();
		$this->updateDB_Country();*/
		$this->genOrderState();
		return true;
	}

	public function uninstall()
	{
		if (!Configuration::deleteByName('CODIGO_MCPROCESOS')
			|| !Configuration::deleteByName('KEY_MERCHANT')
			//|| !$this->delOrderState()
			|| !parent::uninstall())
			return false;
		return true;
	}

	public function hookDisplayHeader()
	{
		$this->context->controller->addCSS($this->_path.'views/css/etdinersclub.css', 'all');
	}

	private function genOrderState()
	{
		$ps_state = new OrderState();
		$languages = Language::getLanguages(false);
		foreach ($languages as $lang)
		{
			$ps_state->name[(int)$lang['id_lang']] = 'En Espera de Pago - Diners Club';
			$ps_state->template[(int)$lang['id_lang']] = $this->name;
		}
		$ps_state->invoice = 0;
		$ps_state->send_email = 0;
		$ps_state->module_name = $this->name;
		$ps_state->color = '#4169E1';
		$ps_state->unremovable = 0;
		$ps_state->hidden = 0;
		$ps_state->logable = 0;
		$ps_state->delivery = 0;
		$ps_state->shipped = 0;
		$ps_state->paid = 0;
		$ps_state->deleted = 0;
		$ps_state->save();

		copy((dirname(__file__).'/logo.gif'), (dirname(dirname(dirname(__file__)))."/img/os/$ps_state->id.gif"));

		Configuration::updateValue('PS_OS_DINERSCLUB', $ps_state->id);

		return true;
	}

	private function delOrderState()
	{
		$sql_1 = 'DELETE FROM `'._DB_PREFIX_.'order_state` WHERE `module_name`='.$this->name;
		$sql_2 = 'DELETE FROM `'._DB_PREFIX_.'order_state_lang` WHERE `template`='.$this->name;
		Db::getInstance()->execute($sql_1);
		Db::getInstance()->execute($sql_2);

		return true;
	}

	public function checkCurrency($cart)
	{
		$currency_order = new Currency($cart->id_currency);
		$currencies_module = $this->getCurrency($cart->id_currency);

		if (is_array($currencies_module))
			foreach ($currencies_module as $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
		return false;
	}

	public function hookPayment($params)
	{
		if (!$this->active)
			return;
		if (!$this->checkCurrency($params['cart']))
			return;

		$this->smarty->assign(array(
			'mod_path' => $this->_path,
			'id_customer' =>  $this->context->cookie->id_customer,
			'sh_sum' => Configuration::get('SUMMARY'),
		));

		return $this->display(__FILE__, 'payment.tpl');
	}

	public function hookPaymentReturn($params)
	{
		if (!$this->active)
			return;

		$status = $params['objOrder']->getCurrentState();
		$url_gateway = Configuration::get('KEY_MERCHANT') > 0 ? 'https://server.punto-web.com/gateway/PagoWebDn.asp' : 'https://www.punto-web.com/gateway/PagoWebDn.asp';
		$array_datos = array(
			'I1' => Configuration::get('CODIGO_MCPROCESOS'),		// Nombre del Comercio
			'I2' => 'ORD00'.$params['objOrder']->id,				// ID de Pedido
			'I3' => number_format($params['total_to_pay'], 2),		// Monto
			'I4' => $this->context->currency->iso_code,				// Moneda
			'I5' => date('Ymd'),									// Fecha
			'I6' => date('His'),									// Hora
			'I7' => 'A'.date('YmdHis'),								// Random
			'I8' => 'REG00'.$this->context->customer->id,			// ID de cliente
			'I9' => Tools::strlen($this->context->country->iso_code) < 3 ? 'PER' : $this->context->country->iso_code,
			'keyMerchant' => Configuration::get('KEY_MERCHANT'),	// Codigo de comercio (key Merchant)
		);

		$cadenafinal = implode('', $array_datos);
		$strhash = urlencode(base64_encode($this->hmacsha1(Configuration::get('KEY_MERCHANT'), $cadenafinal)));

		$this->smarty->assign($array_datos);
		$this->smarty->assign(array(
			'strhash' => $strhash,
			'cadenafinal' => $cadenafinal,
			'mode_debug' => Configuration::get('MODE_DEBUG'),
		));

		$ps_os_outofstock = Configuration::get('PS_OS_OUTOFSTOCK');
		$ps_os_outofstock_unpaid = Configuration::get('PS_OS_OUTOFSTOCK_UNPAID');

		if (in_array($status, array(Configuration::get('PS_OS_DINERSCLUB'), $ps_os_outofstock, $ps_os_outofstock_unpaid)))
		{
			$this->smarty->assign(array(
				'total_to_pay' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
				'status' => 'ok',
				'id_order' => $params['objOrder']->id
			));
			if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
				$this->smarty->assign(array(
					'url_gateway'	=> $url_gateway,
					'path_etdinersclub_img' => Tools::getShopDomainSsl(true, true)._MODULE_DIR_.$this->dir_img,
					'purchaseOperationNumber' => $params['objOrder']->id,
				));
		}
		else
			$this->smarty->assign('status', 'failed');
		return $this->display(__FILE__, 'gatewaySend.tpl');
	}

	public function renderForm()
	{
		$fields_form = array();
		$fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Integration Parameters'),
				'icon' => 'icon-envelope'
			),

			'input' => array(
				array(
				'type' => 'text',
				'label'=>  $this->l('URL Return:'),
				'name' => 'URL_RETURN',
				'value'=> $this->url_return,
				'disabled' => true,
				'class' => 'col-sm-8',
				),
				array(
					'type' => 'text',
					'label' => $this->l('Code Integration'),
					'name' => 'CODIGO_MCPROCESOS',
					'required' => true,
					'class' => 'fixed-width-sm',
				),
				array(
					'type' => 'text',
					'label' => $this->l('Key Merchant'),
					'name' => 'KEY_MERCHANT',
					'required' => true,
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Summary Order'),
					'name' => 'SUMMARY',
					'desc' => $this->l('Show the order summary before payment.'),
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					),
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
			)
		);

		$fields_form[1]['form'] = array(
			'legend' => array(
				'title' => $this->l('Advanced Options'),
				'icon' => 'icon-cogs',
			),
			'input' => array(
				array(
					'type' => 'switch',
					'label' => $this->l('SANDBOX'),
					'name' => 'MODE_SANDBOX',
					'desc' => $this->l('On if you are in a test environment.'),
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					),
				),
				array(
					'type' => 'switch',
					'label' => $this->l('DEBUG'),
					'name' => 'MODE_DEBUG',
					'desc' => $this->l('Turns on debug mode to see Parameters Sending and Receiving Payment Gateway.'),
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					),
				),
			),
			'submit' => array(
				'title' => $this->l('Guardar'),
			)
		);

		$admincurrentindex  = '&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'btnSubmit';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).$admincurrentindex;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
	return $helper->generateForm($fields_form);
	}
	public function getContent()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			$this->postValidation();
			if (!count($this->posterrors))
				$this->postProcess();
			else
				foreach ($this->posterrors as $err)
				$this->html .= $this->displayError($err);
		}
		else
			$this->html .= '<br />';
		$this->html .= $this->display(__FILE__, 'views/templates/admin/configuration.tpl');
		$this->html .= $this->renderForm();
		return $this->html;
	}

	public function getConfigFieldsValues()
	{
		return array(
			'KEY_MERCHANT'		=> Tools::getValue('KEY_MERCHANT', Configuration::get('KEY_MERCHANT')),
			'CODIGO_MCPROCESOS'	=> Tools::getValue('CODIGO_MCPROCESOS', Configuration::get('CODIGO_MCPROCESOS')),
			'URL_RETURN'		=> Tools::getValue('URL_RETURN', Configuration::get('URL_RETURN')),
			'SUMMARY'			=> Tools::getValue('SUMMARY', Configuration::get('SUMMARY')),
			'MODE_SANDBOX'		=> Tools::getValue('MODE_SANDBOX', Configuration::get('MODE_SANDBOX')),
			'MODE_DEBUG'		=> Tools::getValue('MODE_DEBUG', Configuration::get('MODE_DEBUG')),
		);
	}

	private function postValidation()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			if (!Tools::getValue('CODIGO_MCPROCESOS'))
				$this->posterrors[] = $this->l('The code is a required field.');
			elseif (!Tools::getValue('KEY_MERCHANT'))
				$this->posterrors[] = $this->l('The Merchant Key is a required field.');
		}
	}

	private function postProcess()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			Configuration::updateValue('CODIGO_MCPROCESOS', Tools::getValue('CODIGO_MCPROCESOS'));
			Configuration::updateValue('KEY_MERCHANT', Tools::getValue('KEY_MERCHANT'));
			Configuration::updateValue('SUMMARY', Tools::getValue('SUMMARY'));
			Configuration::updateValue('MODE_SANDBOX', Tools::getValue('MODE_SANDBOX'));
			Configuration::updateValue('MODE_DEBUG', Tools::getValue('MODE_DEBUG'));
		}
		$this->html .= $this->displayConfirmation($this->l('Guardado Correctamente'));
	}

	public function hookDashboardZoneOne()
	{
		return $this->display(__FILE__, 'views/templates/admin/dashboard_zone_one.tpl');
	}

	public function hmacsha1($key, $data, $hex = false)
	{
		$blocksize = 64;
		$hashfunc = 'sha1';
		if (Tools::strlen($key) > $blocksize)
			$key = pack('H*', $hashfunc($key));
		$key = str_pad($key, $blocksize, chr(0x00));
		$ipad = str_repeat(chr(0x36), $blocksize);
		$opad = str_repeat(chr(0x5c), $blocksize);
		$hmac = pack('H*', $hashfunc(($key^$opad).pack('H*', $hashfunc(($key^$ipad).$data))));
		if ($hex == false)
			return $hmac;
		return bin2hex($hmac);
	}
}