<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{etdinersclub}prestashop>etdinersclub_fd90140d8de7f74351b125ad162352ae'] = 'DinersClub';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_79d7d7070645885f7a87c5273f93155e'] = 'Una tarjeta exclusiva y con beneficios únicos en el mercado.';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_54679080b9100c3c49226bc65695089a'] = 'Estás seguro que deseas eliminar toda la información del módulo?';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_b8d6696e3f694c3ca8f993f648dc1c2c'] = 'Parámetros de Integración';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_6c7a77b29039d7a05ec30c30ffd762e2'] = 'URL Retorno:';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_576aac54512818e9f3605b2bee9863d7'] = 'Código de Integración';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_fa1f2043eb9754be662674986b88a9e6'] = 'Código de Comercio';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_bfb3f90cd276e3a7bf1093dc53c95652'] = 'Resumen de Pedido';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_c586e107971a5b5653cdb012f7d3f0e0'] = 'Muestra el resumen del pedido antes de pagar';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Habilitar';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_b9f5c797ebbf55adccdd8539a65a0241'] = 'Deshabilitar';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_9dfc5d5738312210c3c75e68a468691d'] = 'Opciones Avanzadas';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_439d8eeff1670efd5d1eb2a4232254e8'] = 'SANDBOX';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_bb973f680bb94d092527a293bd5e8a9d'] = 'Activar si se encuentra en un ambiente de pruebas';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_dc30ec20708ef7b0f641ef78b7880a15'] = 'DEBUG';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_7cb87a5f858e90315ff7cae8bef8572c'] = 'Activa el modo debug para ver los Parámetros de Envio y Recepción de la Pasarela de Pagos.';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_d3270bdb8d2c1caaca4178dbe19be03a'] = 'Guardar';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_a22990cecd7aeb230b166f78a8c31aa8'] = 'El Código de Integración es OBLIGATORIO';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_464fb44f4e7cf758bfe5db1d82a587a8'] = 'El Código de Comercio es OBLIGATORIO';
$_MODULE['<{etdinersclub}prestashop>etdinersclub_b302ec4746da6512e874f575aaae3991'] = 'Guardado Correctamente';
$_MODULE['<{etdinersclub}prestashop>validation_e2b7dec8fa4b498156dfee6e4c84b156'] = 'El método de pago no está disponible';
$_MODULE['<{etdinersclub}prestashop>configuration_c923c12b3d654e68986b214f28fe6017'] = 'Módulo de integración con DinersClub';
$_MODULE['<{etdinersclub}prestashop>configuration_e8b7628a6c80fb3433da27e842fe867d'] = 'Este modulo ha sido creado en Abril del 2015  en caso de presentar incompatibilidad o errores con si tienda favor de comunicarse con victor@castrocontreras.com';
$_MODULE['<{etdinersclub}prestashop>configuration_c21e5fbdae16d891347a5ea3d9606373'] = 'Si necesita ayuda de como utilizar este modulo puede consultar el ';
$_MODULE['<{etdinersclub}prestashop>configuration_e4c9b306015e8795e38324a9a8e432f3'] = 'etdinersclub_es.pdf';
$_MODULE['<{etdinersclub}prestashop>configuration_a4839dac0eebe8f5329c32d13561b08a'] = 'Manual de Integración';
$_MODULE['<{etdinersclub}prestashop>dashboard_zone_one_8cf5364c4259be0f1a5010e052991c0e'] = 'DinersClub';
$_MODULE['<{etdinersclub}prestashop>dashboard_zone_one_b7d387d839e622b4c9c91462a7eaea10'] = 'Soporte Tècnico';
$_MODULE['<{etdinersclub}prestashop>dashboard_zone_one_a8b61c72e716f3310efa883d8fe2d44f'] = 'Victor Castro Contreras';
$_MODULE['<{etdinersclub}prestashop>dashboard_zone_one_874bea1574ac39f2fec556318232d904'] = 'victor@castrocontreras.com';
$_MODULE['<{etdinersclub}prestashop>dashboard_zone_one_6241d2766562742d0f9b3009675087e3'] = 'www.castrocontreras.com';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_ec08fda482112cc3e6a1ba7af482e6cd'] = 'Respuesta de la pasarela';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Descripción';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_80af25cc2463c10661addd5a0acf7225'] = 'Nro Referencia (Generado por el Comercio).';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_4586804ab38920a6f7ea434c8bd939f4'] = 'Fecha de Transacción';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_c17d0ea6843a57dde834eb4fd95c0359'] = 'Hora de Transacción';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_980ee3bcda7f62fa056fb579ec70fb93'] = 'Código de Transacción';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_39a51582a8026f7a0b0a471b45985a4d'] = 'Monto de Transacción';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_a44217022190f5734b2f72ba1e4f8a79'] = 'Número de Tarjeta';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_8c3ddbe9f3991b84588e364d856313eb'] = 'Mensaje de respuesta';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_fdabf4e63a3cc1fe987269ff4e629148'] = 'Monto de Transacción';
$_MODULE['<{etdinersclub}prestashop>gatewayreturn_7945d3635171c2fb5da14fc71b98a9a1'] = 'Parámetros recibidos de la pasarela';
$_MODULE['<{etdinersclub}prestashop>summary_8cf5364c4259be0f1a5010e052991c0e'] = 'DinersClub';
$_MODULE['<{etdinersclub}prestashop>summary_fe052c248fb8c2b8cf08c44f17341c3c'] = 'Resumen de Pedido';
$_MODULE['<{etdinersclub}prestashop>summary_572da7ef1411f2a12409e752f3eb2f7a'] = 'Su carrito está vacío.';
$_MODULE['<{etdinersclub}prestashop>summary_bb6682681fa2590c8aecb4f1d3af582e'] = 'Diners Club es una marca reconocida a nivel mundial de servir las necesidades de pago de los clientes de las empresas, desde pequeñas empresas hasta corporaciones multinacionales.';
$_MODULE['<{etdinersclub}prestashop>summary_b6acbc0f2564579b294e723f7cea91d4'] = 'Su pedido hace un total de';
$_MODULE['<{etdinersclub}prestashop>summary_f78e2fb378a006bdac55545848d7e4f8'] = '(inc IGV)';
$_MODULE['<{etdinersclub}prestashop>summary_b92b2c8256d6e479c398076ecc83cdef'] = 'que se debitará de su tarjeta Diners, si está de acuerdo dar click en el boton “Confirmar Pedido\" en caso contrario puede volver a los métodos de pago y seleccionar uno diferente.';
$_MODULE['<{etdinersclub}prestashop>summary_70d9be9b139893aa6c69b5e77e614311'] = 'Confirmar Pedido';
$_MODULE['<{etdinersclub}prestashop>gatewaysend_86dc73705e48733eb86ae1914176f31f'] = 'FAVOR DE NO CERRAR LA VENTANA HASTA CONCLUIR CON LA TRANSACCIÓN.';
$_MODULE['<{etdinersclub}prestashop>gatewaysend_8d3272b77ba79454a229dc3665118b19'] = 'En cuanto se concrete el pago serás direccionado nuevamente a nuestra tienda.';
$_MODULE['<{etdinersclub}prestashop>gatewaysend_a6b3ee4f59fd5c861bb6fefaf6bc243e'] = 'Direccionando a la pasarela en ';
$_MODULE['<{etdinersclub}prestashop>gatewaysend_10f625cabf48b200154a88f054d287ef'] = 'Si usted piensa que esto es un error, no dude en ponerse en contacto con ';
$_MODULE['<{etdinersclub}prestashop>gatewaysend_c26d1b8fd264fe8fdd587e71b4a34903'] = 'nuestro equipo de atención al cliente experto';
$_MODULE['<{etdinersclub}prestashop>payment_8ac0b8f8c91db802bb168aa934bf2b74'] = ' Diners Club Internacional';
$_MODULE['<{etdinersclub}prestashop>payment_b0a19179b1207b4fb87408799c141861'] = 'La tarjeta con el programa Miles más flexibles.';
$_MODULE['<{etdinersclub}prestashop>payment_gateway_e18d7548fca7550cfbb533a76deccc90'] = 'Estableciendo comunicación con la pasarela.';
$_MODULE['<{etdinersclub}prestashop>payment_gateway_d15feee53d81ea16269e54d4784fa123'] = 'Nos dimos cuenta de un problema con su pedido. Si usted piensa que esto es un error, no dude en ponerse en contacto con  ';
$_MODULE['<{etdinersclub}prestashop>payment_gateway_dfe239de8c0b2453a8e8f7657a191d5d'] = 'nuestro equipo de atención al cliente experto';
