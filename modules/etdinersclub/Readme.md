# Description
This module provides a new secure card payment method that uses the DinersClub platform (MCProcesos payment gateway). Accepts Mastercard and Diners Club cards.
For 1.5 and 1.6 PrestaShop versions. 

# Merchant Benefits
With this module you can accept payments DinersClub Card and Mastercard.


# Features
100% secure payment option
Accepts Mastercard and Diners Club cards
MCPROCESOS platform (MC Procesos payment gateway)
http://www.dinersclub.com/


# Customer Benefits
Customers has a new 100% secure payment option


# Installation
Upload module for him backoffice
Enter the parameters sent by DinersClub
Activate the Debug Mode to verify delivery settings
Make a test purchase
If everything is right off the DEBUG mode.


# Other
DEMO
Store: http://demos.castrocontreras.com/prestashop/
Cliente: cliente1@castrocontreras.com
Password: 123456789