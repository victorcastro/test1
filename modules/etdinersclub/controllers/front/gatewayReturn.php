<?php
/**
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*/

class EtdinersclubGatewayreturnModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function initContent()
	{
		parent::initContent();

		$o1 = Tools::getValue('O1');
		$o2 = Tools::getValue('O2');
		$o3 = Tools::getValue('O3');
		$o4 = Tools::getValue('O4');
		$o5 = Tools::getValue('O5');
		$o6 = Tools::getValue('O6');
		$o7 = Tools::getValue('O7');
		$o8 = Tools::getValue('O8');
		$o9 = Tools::getValue('O9');
		$o10 = Tools::getValue('O10');
		$o11 = Tools::getValue('O11');
		$o12 = Tools::getValue('O12');
		$o13 = Tools::getValue('O13');
		$o14 = Tools::getValue('O14');
		$o15 = Tools::getValue('O15');
		$o16 = Tools::getValue('O16');
		$o17 = Tools::getValue('O17');
		$o18 = Tools::getValue('O18');
		$o19 = Tools::getValue('O19');
		$o20 = Tools::getValue('O20');

		$history = new OrderHistory();
		$history->id_order = Tools::substr($o10, 5);

		$this->context->smarty->assign(array(
			'mode_debug' => Configuration::get('MODE_DEBUG'),
			'O1' => $o1,
			'O2' => $o2,
			'O3' => $o3,
			'O4' => $o4,
			'O5' => $o5,
			'O6' => $o6,
			'O7' => $o7,
			'O8' => $o8,
			'O9' => $o9,
			'O10' => $o10,
			'O11' => $o11,
			'O12' => $o12,
			'O13' => $o13,
			'O14' => $o14,
			'O15' => $o15,
			'O16' => $o16,
			'O17' => $o17,
			'O18' => $o18,
			'O19' => $o19,
			'O20' => $o20,
			'date_order' => date('d/m/Y'),
			'hour_order' => date('H:i'),
		));

		switch ($o1)
		{
			case 'A':
				$history->changeIdOrderState(Configuration::get('PS_OS_PAYMENT'), Tools::substr($o10, 5));
				$history->addWithemail();
				$this->context->smarty->assign(array(
					'title' 	=> 'AUTORIZACIÓN APROBADA',
					'text' 		=> 'Su Pedido ha sido registrado exitosamente en nuestra tienda, Gracias.',
				));
				break;
			case 'D':
				$history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'), Tools::substr($o10, 5));
				$history->addWithemail();
				$this->context->smarty->assign(array(
					'title' 	=> 'AUTORIZACIÓN DESAPROVADA!',
					'text' 		=> 'Su Pedido ha sido denegado por la pasarela de Pagos Online.',
				));
				break;
			case 'E':
				$history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'), Tools::substr($o10, 5));
				$history->addWithemail();
				$this->context->smarty->assign(array(
					'title' 	=> 'AUTORIZACIÓN DESAPROVADA!',
					'text' 		=> 'Su Pedido NO ha sido concetrado, favor de capturar la pantalla de error y contactarse con nosotros.',
				));
			default:
				$history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'), Tools::substr($o10, 5));
				$history->addWithemail();
				$this->context->smarty->assign(array(
					'title' 	=> 'AUTORIZACIÓN DESAPROVADA!',
					'text' 		=> 'Su Pedido NO ha sido aprovado, favor de volver a intentar luego.',
				));
				break;
		}
		$this->setTemplate('gatewayReturn.tpl');
	}
}