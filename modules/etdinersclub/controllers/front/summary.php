<?php
/**
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*/

class EtDinersClubSummaryModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	public $display_column_left = true;

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();

		$cart = $this->context->cart;
		if (!$this->module->checkCurrency($cart))
			Tools::redirect('index.php?controller=order');

		$this->context->smarty->assign(array(
			'nbProducts' => $cart->nbProducts(),
			'cust_currency' => $cart->id_currency,
			'currencies' => $this->module->getCurrency((int)$cart->id_currency),
			'total' => $cart->getOrderTotal(true, Cart::BOTH),
			'path_etdinersclub' => $this->module->getPathUri(),
			'this_path_bw' => $this->module->getPathUri(),
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/',
			'mode_debug' => Configuration::get('MODE_DEBUG'),
		));

		$this->setTemplate('summary.tpl');
	}
}
