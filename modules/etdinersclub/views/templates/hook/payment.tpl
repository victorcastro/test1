{*
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author 		Victor Castro <victor@castrocontreras.com>
*  @copyright  	2013-2015 eTechnology EIRL
*  @version  	Release: $Revision: 7040 $
*  @license    	http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*}

{if $sh_sum>0}
	{assign var="redirect" value="summary"}
{else}
	{assign var="redirect" value="validation"}
{/if}

<div class="row">
	<div class="col-xs-12 col-md-12">
        <p class="payment_module">
            <a class="dinersclub" href="{$link->getModuleLink('etdinersclub', {$redirect})|escape:'html':'UTF-8'}" 
            title="{l s='Diners Club International' mod='etdinersclub'}">
            	{l s='Diners Club International' mod='etdinersclub'} <span>{l s='The card with the program most flexible Miles.' mod='etdinersclub'}</span>
            </a>
        </p>
    </div>
</div>



