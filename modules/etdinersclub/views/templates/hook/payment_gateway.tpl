{*
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author 		Victor Castro <victor@castrocontreras.com>
*  @copyright  	2013-2015 eTechnology EIRL
*  @version  	Release: $Revision: 7040 $
*  @license    	http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*}
{if $status == 'ok'}
	{literal}
		<script language="JavaScript">

			$(document).ready(function(e) {
			   enviarvpos();
			});

			function enviarvpos(){
				var divImgLoad = document.getElementById( "preloader" );
				divImgLoad.style.visibility= "visible";
				document.params_form.target= "iframevpos";
				document.params_form.submit();
		 	}

		</script>
	{/literal}

 	<form name="params_form" id="params_form" method="post" action="{$url_gateway|escape:'url'}" >
	 	<input type="hidden" name="I1" 	id="I1" value='{$I1|escape:"htmlall"}'>
		<input type="hidden" name="I2" 	id="I2" value='{$I2|escape:"htmlall"}'>
		<input type="hidden" name="I3" 	id="I3" value='{$I3|escape:"htmlall"}'>
		<input type="hidden" name="I4" 	id="I4" value='{$I4|escape:"htmlall"}'>
		<input type="hidden" name="I5" 	id="I5" value='{$I5|escape:"htmlall"}'>
		<input type="hidden" name="I6" 	id="I6" value='{$I6|escape:"htmlall"}'>
		<input type="hidden" name="I7" 	id="I7" value='{$I7|escape:"htmlall"}'>
		<input type="hidden" name="I8" 	id="I8" value='{$I8|escape:"htmlall"}'>
		<input type="hidden" name="I9" 	id="I9" value='{$I9|escape:"htmlall"}'>
		<input type="hidden" name="I10" id="I10" value='{$strHash|escape:"htmlall"}'>
		<input type="submit" name="envio" id="envio" value="Enviar" style="display:none" />
	</form>

<div class="row">
	<div id ="preloader" class ="col-sm-4 col-sm-offset-4 text-center" >
		<img alt ="Cargando..." src ='{$path_etdinersclub_img|escape:"htmlall"}}preloader.gif'>
		<p class="text"> {l s='Establishing communication with the gateway...' mod='etdinersclub'}</p>
	</div>
	
	<div id="parameters">
		<span class="text"> $url_gateway = {$url_gateway|escape:"htmlall"} </span>
		<span class="text"> $I1= {$I1|escape:"htmlall"} </span>
		<span class="text"> $I2= {$I2|escape:"htmlall"} </span>
		<span class="text"> $I3= {$I3|escape:"htmlall"} </span>
		<span class="text"> $I4= {$I4|escape:"htmlall"} </span>
		<span class="text"> $I5= {$I5|escape:"htmlall"} </span>
		<span class="text"> $I6= {$I6|escape:"htmlall"} </span>
		<span class="text"> $I7= {$I7|escape:"htmlall"} </span>
		<span class="text"> $I8= {$I8|escape:"htmlall"} </span>
		<span class="text"> $I9= {$I9|escape:"htmlall"} </span>
		<span class="text"> $strHash= {$strHash|escape:"htmlall"} </span>
	</div>
</div>

{else}
	<p class="warning">
		{l s='We noticed a problem with your order. If you think this is an error, feel free to contact our' mod='etdinersclub'}
		<a href="{$link->getPageLink('contact', true)|escape:'url'}">{l s='expert customer support team. ' mod='etdinersclub'}</a>.
	</p>
{/if} 