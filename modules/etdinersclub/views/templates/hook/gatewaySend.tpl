{*
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author 		Victor Castro <victor@castrocontreras.com>
*  @copyright  	2013-2015 eTechnology EIRL
*  @version  	Release: $Revision: 7040 $
*  @license    	http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*}
{if $status == 'ok'}
	<script language="JavaScript">

	    window.onload = function() {
			var x = 6;
			var timer = document.getElementById("timer");
			var envio = document.getElementById("envio");
			var preloader = document.getElementById("preloader");
			var parameters = document.getElementById("parameters");
			var debug_text = document.getElementById('debug-text');
			var debug_mode = '{$mode_debug|escape:"htmlall"}';

			if(debug_mode > 0){
				debug_text.innerHTML = 'MODO DEBUG :: ACTIVADO';
				preloader.style.display = "none";
				debug_text.style.display = "block";
				envio.style.display = "block";
				parameters.style.display = "block";
			}

			else{
			{literal}
				setInterval(function() {
					if (x <= 21 && x >= 1) {
						x--;
						timer.innerHTML = '' + x + '';
					}
				}, 1000);

				var auto_refresh = setInterval(function() {
					submitform();
				}, x*1000);
				
				function submitform() {
					document.getElementById("form").submit();
				}
			{/literal}
			}
		}
	</script>

 	<span id="debug-text" class="alert alert-warning" style="display:none"></span>
 	
	<div id ="preloader" class ="col-sm-12 text-center" >
		<p class="alert alert-warning">  {l s='PLEASE DO NOT CLOSE THE WINDOW TO CONCLUDE WITH THE TRANSACTION' mod='etdinersclub'} <br><br>
		 {l s='As soon as the payment is realized will be directed back to our store.' mod='etdinersclub'}  <br>
		  {l s='Direccionando a la pasarela en' mod='etdinersclub'} <b id="timer"></b> </p>
		<img class="loading"alt ="Cargando..." src ='{$path_etdinersclub_img|escape:"htmlall"}preloader.gif'>
	</div>
	

	<dl class="dl-horizontal" id="parameters" style="display:none">
	    {for $i=1 to 9}
	    <dt style="width:100px;">I{$i|escape:"htmlall"} = </dt>
	    <dd style="margin-left:105px;">{$I{$i|escape:"htmlall"}}</dd>
	    {/for}

	    <dt style="width:100px;">url_gateway = </dt>
	    <dd style="margin-left:105px;">{$url_gateway|escape:"htmlall"}</dd>
	    <dt style="width:100px;">strhash = </dt>
	    <dd style="margin-left:105px;">{$strhash|escape:"htmlall"}</dd>
	    <dt style="width:100px;">CadenaFinal = </dt>
	    <dd style="margin-left:105px;">{$cadenafinal|escape:"htmlall"}</dd>
	</dl>

	<form name="form" id="form" method="post" action="{$url_gateway|escape:'htmlall'}" >
	 	<input type="hidden" name="I1" 	id="I1" value="{$I1|escape:'htmlall'}">
		<input type="hidden" name="I2" 	id="I2" value="{$I2|escape:'htmlall'}">
		<input type="hidden" name="I3" 	id="I3" value="{$I3|escape:'htmlall'}">
		<input type="hidden" name="I4" 	id="I4" value="{$I4|escape:'htmlall'}">
		<input type="hidden" name="I5" 	id="I5" value="{$I5|escape:'htmlall'}">
		<input type="hidden" name="I6" 	id="I6" value="{$I6|escape:'htmlall'}">
		<input type="hidden" name="I7" 	id="I7" value="{$I7|escape:'htmlall'}">
		<input type="hidden" name="I8" 	id="I8" value="{$I8|escape:'htmlall'}">
		<input type="hidden" name="I9" 	id="I9" value="{$I9|escape:'htmlall'}">
		<input type="hidden" name="I10" id="I10" value="{$strhash|escape:'htmlall'}">
		<input type="submit" name="envio" id="envio" class="btn btn-default" value="Send" style="display:none;float:right"/>
	</form>

{else}
	<p class="warning">
		{l s='We noticed a problem with your order. If you think this is an error, please contact our' mod='etdinersclub'}
		<a href="{$link->getPageLink('contact', true)|escape:'url'}">{l s='team of expert customer care.' mod='etdinersclub'}</a>.
	</p>
{/if} 