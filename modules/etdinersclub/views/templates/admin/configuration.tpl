{*
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author 		Victor Castro <victor@castrocontreras.com>
*  @copyright  	2013-2015 eTechnology EIRL
*  @version  	Release: $Revision: 7040 $
*  @license    	http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*}
<div class="alert alert-info">
	<img src="{$module_dir|htmlspecialchars}views/img/logo_1.png" style="float:right; margin-right:15px;" >
	<p><strong>{l s="Module Integration with Dinners Club" mod='etdinersclub'}</strong></p>
	<p>{l s="This module has been created in April 2015 if you have incompatibility or if errors please contact shop to victor@castrocontreras.com" mod='etdinersclub'}</p>
	<p>{l s="If you need help on how to use this module can consult " mod='etdinersclub'}<a href="{$module_dir|htmlspecialchars}manuals/{l s='etdinersclub_en.pdf' mod='etdinersclub'}" target="_blank"> {l s='Manual Integration' mod='etdinersclub'}</a></p>
</div>
