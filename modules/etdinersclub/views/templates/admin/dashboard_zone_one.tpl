{*
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author 		Victor Castro <victor@castrocontreras.com>
*  @copyright  	2013-2015 eTechnology EIRL
*  @version  	Release: $Revision: 7040 $
*  @license    	http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*}
<section id="etdinersclub" class="panel widget{if $allow_push} allow_push{/if}">
	<div class="panel-heading">
		<i class="icon-link"></i> {l s='Diners Club' mod='etdinersclub'}
	</div>
	<section id="dash_notifications" class="loading">
		<h4>{l s='Soporte Técnico' mod='etdinersclub'}</h4>
		<ul class="data_list">
			<li>
				<span class="data_label">{l s='Victor Castro Contreras' mod='etdinersclub'}</span>
			</li>
			<li>
				<span class="data_label">{l s='victor@castrocontreras.com' mod='etdinersclub'}</span>
			</li>
			<li>
				<span class="data_label">{l s='www.castrocontreras.com' mod='etdinersclub'}</span>
			</li>
		</ul>
	</section>
</section>