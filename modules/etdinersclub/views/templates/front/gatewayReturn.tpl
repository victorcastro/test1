{*
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author 		Victor Castro <victor@castrocontreras.com>
*  @copyright  	2013-2015 eTechnology EIRL
*  @version  	Release: $Revision: 7040 $
*  @license    	http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*}
{capture name=path}
	<a href="{$link->getPageLink('history', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Volver a mis pedidos' mod='etdinersclub'}">{l s='Pedidos' mod='etdinersclub'}</a>
	<span class="navigation-pipe">{$navigationPipe|escape:"htmlall"}</span>
	{l s='Gateway Response Online' mod='etdinersclub'}
{/capture}


<div class="box cheque-box">
	<h3 class="page-subheading">
	    {$title|escape:'htmlall'}
	</h3>
	<p class="text">
		{$text|escape:'htmlall'}
	</p>
	<span class="" style="padding-bottom:25px; display:table;"></span>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>{l s='Description' mod='etdinersclub'}</th>
				<th>Valor</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">O1</th>
				<td>{l s='Reference Number (Generated Commerce)' mod='etdinersclub'}</td>
				<td>{$O10|escape:"htmlall"}</td>
			</tr>
			<tr>
				<th scope="row">O2</th>
				<td>{l s='Transaction Date' mod='etdinersclub'}</td>
				<td>{$date_order|escape:"htmlall"}</td>
			</tr>
			<tr>
				<th scope="row">O3</th>
				<td>{l s='Transaction Hour' mod='etdinersclub'}</td>
				<td>{$hour_order|escape:"htmlall"}</td>
			</tr>
			{if $O1 eq 'A'}
			<tr>
				<th scope="row">O4</th>
				<td>{l s='Authorization Code' mod='etdinersclub'}</td>
				<td>{$O2|escape:"htmlall"}</td>
			</tr>
			<tr>
				<th scope="row">O5</th>
				<td>{l s='Transaction amount' mod='etdinersclub'}</td>
				<td>{$O9|escape:"htmlall"}</td>
			</tr>
			<tr>
				<th scope="row">O6</th>
				<td>{l s='Card number' mod='etdinersclub'}</td>
				<td>{$O15|escape:"htmlall"}</td>
			</tr>
			<tr>
				<th scope="row">O7</th>
				<td>{l s='Message Reply' mod='etdinersclub'}</td>
				<td>{$O17|escape:"htmlall"}</td>
			</tr>
			{else}
			<tr>
				<th scope="row">O4</th>
				<td>{l s='Transaction Amount' mod='etdinersclub'}</td>
				<td>{$O9|escape:"htmlall"}</td>
			</tr>
			<tr>
				<th scope="row">O5</th>
				<td>{l s='Card number' mod='etdinersclub'}</td>
				<td>{$O15|escape:"htmlall"}</td>
			</tr>
			<tr>
				<th scope="row">O6</th>
				<td>{l s='Message Reply' mod='etdinersclub'}</td>
				<td>{$O17|escape:"htmlall"}</td>
			</tr>
			{/if}
		</tbody>
	</table>
</div>

{if $mode_debug>0}
<div class="box">
<p id="debug-text" class="alert alert-warning">{l s='MODO DEBUG :: ACTIVADO' mod='etdinersclub'}</p>

<h4>{l s='Parameters received from the catwalk:' mod='etdinersclub'}</h4>
<dl class="dl-horizontal">
    {for $o=1 to 20}
    <dt style="width:60px;">O{$o|escape:"htmlall"} = </dt>
    <dd style="margin-left:65px;">{$O{$o|escape:"htmlall"}}</dd>
    {/for}
</dl>
</div>
{/if}