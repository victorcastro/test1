{*
* 2013-2015 eTechnology EIRL
*
* RENUNCIA DE GARANTÍA
*
* No editar o añadir a este archivo si desea conservar el Soporte Técnico para
* Las versiones mas recientes o actualizaciones en el futuro. Si desea personalizar
* esta plantilla tiene que consultar. Ingrese a http://www.castrocontreras.com para
* más información y penalidades.
*
*  @author      Victor Castro <victor@castrocontreras.com>
*  @copyright   2013-2015 eTechnology EIRL
*  @version     Release: $Revision: 7040 $
*  @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Based on PrestaShop 1.6
*}
{capture name=path}
    {l s='Diners Club' mod='etdinersclub'}
{/capture}

<h1 class="page-heading">
    {l s='Order Summary' mod='etdinersclub'}
</h1>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
<p class="alert alert-warning">
    {l s='Your cart is empty.' mod='etdinersclub'}
</p>
{else}
{if $mode_debug>0}
<p id="debug-text" class="alert alert-warning">{l s='MODO DEBUG - ACTIVADO' mod='etdinersclub'}</p>
{/if}
<form id="form_summary" action="{$link->getModuleLink('etdinersclub', 'validation', [], true)|escape:'html':'UTF-8'}" method="post">
    <div class="box cheque-box">
        <h3 class="page-subheading">
            <img src="{$path_etdinersclub|escape:'htmlall'}views/img/logo_1.png">
        </h3>
        <p class="cheque-indent">
            <strong class="dark">
                {l s='Diners Club is a globally recognized brand serving the payment needs of business customers, from small businesses to multinational corporations.' mod='etdinersclub'}
            </strong>
        </p>
        <p>
            {l s='Your order for a total of ' mod='etdinersclub'}
            <span id="amount" class="price">{displayPrice price=$total}</span>
            {if $use_taxes == 1}
                {l s='(inc. TAX)' mod='etdinersclub'}
            {/if}
        
            {l s='which will be debited from your Diners card, if you agree to click on the "Confirm" button, otherwise you can return to the methods of payment and select a different one.' mod='etdinersclub'}
		</p>
    </div><!-- .cheque-box -->

    <p class="cart_navigation clearfix" id="cart_navigation">

        <button id="submitbtn" class="button btn btn-default button-medium" type="submit">
            <span>{l s='Confirm' mod='etdinersclub'}<i class="icon-chevron-right right"></i></span>
        </button>
        <img id="preloader" class="loading" alt ="Cargando..." src='{$path_etdinersclub|escape}views/img/preloader.gif' style="display:none; padding-right:20px; float:right">
    </p>
</form>
{/if}


{literal}
<script language="JavaScript">
    window.onload = function() {
        $('#submitbtn').on('click', function () {
               preloader.style.display = "block";
                $(":submit").attr("disabled", true);
                document.getElementById("form_summary").submit();
        });
    }
</script>
{/literal}