
<?php
require_once(dirname(__FILE__).'/../../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../../config/settings.inc.php');
require_once(dirname(__FILE__).'/../etalignet.php');
require_once("vpos_plugin.php");

$etalignet = new EtAlignet();
$etalignet->defineVars();

 $arrayIn['IDACQUIRER'] = $_POST['IDACQUIRER'];
$arrayIn['IDCOMMERCE'] = $_POST['IDCOMMERCE'];
$arrayIn['XMLRES'] 	= $_POST['XMLRES'];
$arrayIn['DIGITALSIGN']= $_POST['DIGITALSIGN'];
$arrayIn['SESSIONKEY'] = $_POST['SESSIONKEY'];
$arrayOut = '';
$key_alignet_public_signature = file_get_contents(PATH_KEY_ALIGNET_PUBLIC_SIGNATURE);
$key_commerce_private_crypto = Configuration::get('KEY_COMMERCE_PRIVATE_CRYPTO');
$vector = Configuration::get('VECTOR');

if(VPOSResponse($arrayIn,$arrayOut,$key_alignet_public_signature,$key_commerce_private_crypto,$vector)){

	//echo ('OK');
	$authorizationResult = $arrayOut['authorizationResult'];
	$authorizationCode = $arrayOut['authorizationCode'];
	$errorCode = $arrayOut['errorCode'];
	$errorMessage = $arrayOut['errorMessage'];
	$purchaseOperationNumber = $arrayOut['purchaseOperationNumber'];
	$billingEMail = $arrayOut['billingEMail'];
	$billingFirstName = $arrayOut['billingFirstName'];
	$billingLastName =	$arrayOut['billingLastName'];
	$billingAddress = $arrayOut["billingAddress"];
	$billingPhone =	$arrayOut["billingPhone"];
	$billingCity = $arrayOut["billingCity"];
	$billingState = $arrayOut["billingState"];
	$billingCountry = $arrayOut["billingCountry"];
	$billingZIP = $arrayOut["billingZIP"];
	$reserved1 = $arrayOut['reserved1'];
	$reserved2 = $arrayOut['reserved2'];
	$reserved3 = $arrayOut['reserved3'];
	$reserved4 = $arrayOut['reserved4'];
}
else {echo "ERROR :: Plugin no pudo reconstrir datos <br><br>";}

echo "
	<p > <br>
		[authorizationResult] => ".$authorizationResult."<br>
		[authorizationCode] => ".$authorizationCode."<br>
		[errorCode] => ".$errorCode."<br>
		[errorMessage] => ".$errorMessage."<br>
		[purchaseOperationNumber] => ".$purchaseOperationNumber."<br>
		[billingEMail] => ".$billingEMail."<br>
		[billingFirstName] => ".$billingFirstName."<br>
		[billingLastName] => ".$billingLastName."<br>
		[billingAddress] => ".$billingAddress."<br>
		[billingPhone] => ".$billingPhone."<br>
		[billingCity] => ".$billingCity."<br>
		[billingState] => ".$billingState."<br>
		[billingCountry] => ".$billingCountry."<br>
		[billingZIP] => ".$billingZIP."<br>
		[reserved1] => ".$reserved1."<br>
		[reserved2] => ".$reserved2."<br>
		[reserved3] => ".$reserved3."<br>
		[reserved4] => ".$reserved4."<br>
	</p>
	";


switch($errorCode){

	case 2501:
		echo "Error codigo: ".$errorCode."<br><br>".$errormensaje;
	 	$history = new OrderHistory();
        $history->id_order = (int)($purchaseOperationNumber);
        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($purchaseOperationNumber));
        @$history->addWithemail();
		$etalignet->update_StatusTransactionDB($purchaseOperationNumber,'2501');
		break;

	case 2413:
		echo "Error codigo: ".$errorCode."<br><br>".$errormensaje;
	 	$history = new OrderHistory();
        $history->id_order = (int)($purchaseOperationNumber);
        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($purchaseOperationNumber));
        @$history->addWithemail();
		$etalignet->update_StatusTransactionDB($purchaseOperationNumber,'2413');
		break;

	case 2413:
		echo "Error codigo: ".$errorCode."<br><br>".$errormensaje."<br><br> Transacción se rechaza y no se autorizó debido a las normas de autorización previa";
	 	$history = new OrderHistory();
        $history->id_order = (int)($purchaseOperationNumber);
        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($purchaseOperationNumber));
        @$history->addWithemail();
		$etalignet->update_StatusTransactionDB($purchaseOperationNumber,'2413');
		break;

	case 2300:
		echo "Carrito Abandonado codigo: ".$errorCode."<br><br>".$errormensaje;
		$history = new OrderHistory();
        $history->id_order = (int)($purchaseOperationNumber);
        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($purchaseOperationNumber));
        @$history->addWithemail();
		$etalignet->update_StatusTransactionDB($purchaseOperationNumber,'2300');
		break;

	case 2202:
		echo "Numero de pedido repetido codigo: ".$errorCode."<br><br>".$errormensaje;
		$history = new OrderHistory();
        $history->id_order = (int)($purchaseOperationNumber);
        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($purchaseOperationNumber));
        @$history->addWithemail();
		$etalignet->update_StatusTransactionDB($purchaseOperationNumber,'2202');
		break;

	case 51:
		echo "Sin Fondos, código: ".$errorCode."<br><br>".$errormensaje;
		$history = new OrderHistory();
        $history->id_order = (int)($purchaseOperationNumber);
        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($purchaseOperationNumber));
        @$history->addWithemail();
		$etalignet->update_StatusTransactionDB($purchaseOperationNumber,'51');
		break;

	case 0:
		echo "OPERACION EXITOSA, código: ".$errorCode."<br><br>".$errormensaje;
        $history = new OrderHistory();
        $history->id_order = (int)($purchaseOperationNumber);
        $history->changeIdOrderState(Configuration::get('PS_OS_PAYMENT'),(int)($purchaseOperationNumber));
        @$history->addWithemail();
		$etalignet->update_StatusTransactionDB($purchaseOperationNumber,'0');
		break;

	default:
		echo "Operación denegada <br>";
		$history = new OrderHistory();
        $history->id_order = (int)($purchaseOperationNumber);
        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($purchaseOperationNumber));
        @$history->addWithemail();
		$etalignet->update_StatusTransactionDB($purchaseOperationNumber,'2501');
		break;
}

?>