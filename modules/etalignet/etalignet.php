<?php

if (!defined('_PS_VERSION_'))
	exit;

class EtAlignet extends PaymentModule
{
	private $_html = '';
	private $_postErrors = array();

	public $details;
	public $owner;
	public $address;
	public $extra_mail_vars;

	public function __construct()
	{
		$this->name = 'etalignet';
		$this->tab = 'payments_gateways';
		$this->version = '1.0.1';
		$this->author = 'eTechnology';
		$this->need_instance = 1;
		$this->ps_versions_compliancy = array('min' => '1.5.3', 'max' => '1.6');
		$this->module_key = "645801d7cb76992568bffe505c85d4f7";

		$this->dir_img = $this->name."/views/img/";
		$this->url_return = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'index.php?fc=module&module=etalignet&controller=payment_return';
		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		parent::__construct();

		$this->displayName = $this->l('Alignet');
		$this->description = $this->l('Accept payments for your products via Alignet.');
		$this->confirmUninstall = $this->l('Are you sure about removing these details?');
	}


	public function install()
	{
		if (!parent::install() || !$this->registerHook('payment') || !$this->registerHook('paymentReturn') || !$this->registerHook('footer') || !$this->registerHook('header') || !$this->init())
			return false;
		return true;
	}

	public function init()
	{
		Configuration::updateValue('URL_RETURN',$this->url_return );
		Configuration::updateValue('VECTOR','0000000000000000');
		$this->createDB();
		$this->updateDB_Country();
		$this->genOrderState();
		return true;
	}


	public function uninstall()
	{
		if (!Configuration::deleteByName('URL_RETURN')
			|| !Configuration::deleteByName('ID_ACQUIRER')
			|| !Configuration::deleteByName('ID_COMMERCE_DOLLARS')
			|| !Configuration::deleteByName('ID_COMMERCE_SOLES')
			|| !Configuration::deleteByName('VECTOR')
			|| !Configuration::deleteByName('KEY_COMMERCE_PUBLIC_CRYPTO')
			|| !Configuration::deleteByName('KEY_COMMERCE_PUBLIC_SIGNATURE')
			|| !Configuration::deleteByName('KEY_COMMERCE_PRIVATE_CRYPTO')
			|| !Configuration::deleteByName('KEY_COMMERCE_PRIVATE_SIGNATURE')
			|| !$this->deleteDB()
			|| !parent::uninstall()
			)
			return false;
		return true;
	}


	private function genOrderState()
	{
		$alignetState = new OrderState();
		$languages = Language::getLanguages(false);
		foreach ($languages as $lang) {
		    $alignetState->name[(int) $lang['id_lang']] = 'En Espera de Pago :: Alignet';
		    $alignetState->template[(int) $lang['id_lang']] = $this->name;
		}
		$alignetState->invoice = 0;
		$alignetState->send_email = 0;
		$alignetState->module_name = $this->name;
		$alignetState->color = '#4169E1';
		$alignetState->unremovable = 0;
		$alignetState->hidden = 0;
		$alignetState->logable = 0;
		$alignetState->delivery = 0;
		$alignetState->shipped = 0;
		$alignetState->paid = 0;
		$alignetState->deleted = 0;
		$alignetState->save();

		copy((dirname(__file__) ."/logo.gif"), (dirname(dirname(dirname(__file__))). "/img/os/$alignetState->id.gif"));

		Configuration::updateValue("PS_OS_ALIGNET", $alignetState->id);

		return true;
	}


	public function defineVars()
	{
		define('MODE_ALIGNET',Configuration::get('MODE'));

		if(MODE_ALIGNET == 'testing')
		{
			define('URL_VPOS','https://test2.alignetsac.com/VPOS/MM/transactionStart20.do');
			define('MODE_ALIGNET_u','TESTING');
		}

		if(MODE_ALIGNET == 'production')
		{
			define('URL_VPOS','https://vpayment.verifika.com/VPOS/MM/transactionStart20.do');
			define('MODE_ALIGNET_u','PRODUCTION');
		}

		define('PREFIX_ALIGNET','ALIGNET.');
		define('PATH_KEY_ALIGNET_PUBLIC_CRYPTO',_PS_MODULE_DIR_.$this->name."/lib/keys_alignet/".MODE_ALIGNET."/".PREFIX_ALIGNET.MODE_ALIGNET_u.".PHP.CRYPTO.PUBLIC.pem");
		define('PATH_KEY_ALIGNET_PUBLIC_SIGNATURE',_PS_MODULE_DIR_.$this->name."/lib/keys_alignet/".MODE_ALIGNET."/".PREFIX_ALIGNET.MODE_ALIGNET_u.".PHP.SIGNATURE.PUBLIC.pem");

		return true;
	}


	public function createDB()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'etech_alignet(
			`id_register` int(11) NOT NULL AUTO_INCREMENT,
			`purchaseOperationNumber` int(10) unsigned NOT NULL,
			`purchaseAmount` int(10) unsigned NOT NULL,
			`purchaseCurrencyCode` int(2) unsigned NOT NULL,
			`language` int(10) unsigned NOT NULL,
			`billingFirstName` varchar(32) NOT NULL,
			`billingLastName` varchar(32) NOT NULL,
			`billingEmail` varchar(128) NOT NULL,
			`billingAddress` varchar(128) NOT NULL,
			`billingZIP` varchar(128) NOT NULL,
			`billingCity` varchar(128) NOT NULL,
			`billingState` varchar(128) NOT NULL,
			`billingCountry` varchar(128) NOT NULL,
			`billingPhone` varchar(128) NOT NULL,
			`Status` int(4) unsigned NOT NULL COMMENT "0=>payed | 51=>unfunded | 2202=>duplicated | 2300=>abandoned | 2413=>unknown | 2501=>unknown",
			`DatePayment` datetime DEFAULT "0000-00-00 00:00:00",
			PRIMARY KEY (`id_register`)
		) ENGINE='._MYSQL_ENGINE_.' default CHARSET=utf8';

		return Db::getInstance()->execute($sql);
	}


	public function deleteDB()
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'eTech_alignet`';
		return Db::getInstance()->execute($sql);
	}


	public function updateDB_Country()
	{
		$sql = "UPDATE `"._DB_PREFIX_."country` SET `need_zip_code`='1', `zip_code_format`='NN', `contains_states` = '1' WHERE `iso_code`='PE' ";
		return Db::getInstance()->execute($sql);
	}


	private function displayForm()
	{
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$fields_form[0]['form'] = array(
			'legend' => array(
			'title' => $this->l('Configurations Commerce '),
			'image' => _MODULE_DIR_.$this->name.'/views/img/ico-store.png'
			),

			'input' => array(
				array(
					'type' => 'select',
					'label'=>  $this->l('Mode'),
					'name' => 'MODE',
					'required' => true,
					'options' => array(
						'query' => array(
							array('id' => 'testing','name' => $this->l('Testing')),
							array('id' => 'production','name' => $this->l('Production'))
						),
						'id' => 'id',
						'name' => 'name'
					)
				),
				array(
					'type' => 'text',
					'label' => $this->l('ID Acquirer'),
					'name' => 'ID_ACQUIRER',
					'size' => 3,
					'required' => true
				),
				array(
					'type' => 'text',
					'label' => $this->l('ID Commerce :: Dollars ($)'),
					'name' => 'ID_COMMERCE_DOLLARS',
					'size' => 5,
					'required' => false,
					'suffix' => $this->l("Deje en blanco si no aceptará Dolares Americanos")
				),
				array(
					'type' => 'text',
					'label' => $this->l('ID Commerce :: Soles (S/)'),
					'name' => 'ID_COMMERCE_SOLES',
					'size' => 5,
					'required' => false,
					'suffix' => $this->l("Deje en blanco si no aceptará Soles Peruanos")
				),
				array(
					'type' => 'text',
					'label' => $this->l('Vector'),
					'name' => 'VECTOR',
					'size' => 18,
					'required' => true
				)

			)
		);

		$fields_form[1]['form'] = array(
			'legend' => array(
				'title' => $this->l('advanced settings'),
				'image' => _MODULE_DIR_.$this->name.'/views/img/ico-setting.png',
			),

			'input' => array(
				array(
					'type' => 'text',
					'label'=>  $this->l('URL Return:'),
					'name' => 'URL_RETURN',
					'value'=> $this->url_return,
					'disabled' => true,
					'size' => 108,
					'desc' => $this->l("If you can't see this field, please reset module.")
				),
				array(
					'type' => 'text',
					'label' => $this->l('ID Customer'),
					'name' => 'ID_CUSTOMER_AUTHORIZED',
					'size' => 3,
					'required' => false,
					'suffix' => 'Ingrese el ID de cliente autorizado para mostrar el Metodo de Pago Alignet, esto solo es válido cuando HIDDEN_HOOKPAYMENT está activo'
				),
				array(
					'type' => 'checkbox',
					'label'=>  $this->l('Advanced'),
					'name' => 'ADVANCED',
					'values' => array(
						'query' => array(
							array(
								'id' => 'HIDDEN_HOOKPAYMENT',
								'name' => $this->l('HIDDEN_HOOKPAYMENT: Ocultar Método de Pago Alignet en la Tienda.'),
								'val' => 'HIDDEN_HOOKPAYMENT'
							),
							array(
								'id' => 'SHOW_ARRAYSEND',
								'name' => $this->l('SHOW_ARRAYSEND: Mostrar los parámetros de Envío al V-POS.'),
								'val' => 'SHOW_ARRAYSEND'
							),
							array(
								'id' => 'SHOW_ARRAYOUT',
								'name' => $this->l('SHOW_ARRAYOUT: Mostrar los parámetros de Retorno del V-POS.'),
								'val' => 'SHOW_ARRAYOUT'
							),
							array(
								'id' => 'SKIP_PRECONFIRM',
								'name' => $this->l('SKIP_PRECONFIRM: Saltar la Pre-Confirmacion del pedido antes de realizar la compra.'),
								'val' => 'SKIP_PRECONFIRM'
							)
						),
						'id' => 'id',
						'name' => 'name'
					)
				),
			)
		);


		$fields_form[2]['form'] = array(
			'legend' => array(
			'title' => $this->l('Keys Public Commerce '),
			'image' => _MODULE_DIR_.$this->name.'/views/img/ico-key_public.png'
			),

			'input' => array(
				array(
                  'type' => 'textarea',
                  'label' => $this->l('Key Public Crypto'),
                  'name' => 'KEY_COMMERCE_PUBLIC_CRYPTO',
                  'cols' => 110,
                  'rows' => 7,
                  'desc' => $this->l("Ex. MIGA.TESTING.PHP.<b>CRYPTO</b>.PUBLIC.pem"),
                  'required' => true
              	),
				array(
                  'type' => 'textarea',
                  'label' => $this->l('Key Public Signature'),
                  'name' => 'KEY_COMMERCE_PUBLIC_SIGNATURE',
                  'cols' => 110,
                  'rows' => 7,
                  'desc' => $this->l("Ex. MIGA.TESTING.PHP.<b>SIGNATURE</b>.PUBLIC.pem"),
                  'required' => true
              	)
			)
		);

		$fields_form[3]['form'] = array(
			'legend' => array(
				'title' => $this->l('Keys Private Commerce'),
				'image' => _MODULE_DIR_.$this->name.'/views/img/ico-key_private.png'
			),

			'input' => array(
              	array(
                  'type' => 'textarea',
                  'label' => $this->l('Key Private Crypto'),
                  'name' => 'KEY_COMMERCE_PRIVATE_CRYPTO',
                  'cols' => 110,
                  'rows' => 16,
                  'desc' => $this->l("Ex. MIGA.TESTING.PHP.<b>CRYPTO</b>.PRIVATE.pem"),
                  'required' => true
              	),
              	array(
                  'type' => 'textarea',
                  'label' => $this->l('Key Private Signature'),
                  'name' => 'KEY_COMMERCE_PRIVATE_SIGNATURE',
                  'cols' => 110,
                  'rows' => 16,
                  'desc' => $this->l("Ex. MIGA.TESTING.PHP.<b>SIGNATURE</b>.PRIVATE.pem"),
                  'required' => true
              	)
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);

		$helper = new HelperForm();

		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;

		$helper->title = $this->displayName;
		$helper->show_toolbar = true;
		$helper->toolbar_scroll = true;
		$helper->submit_action = 'submit'.$this->name;
		$helper->toolbar_btn = array(
		  'save' =>
		  array(
		      'desc' => $this->l('Save'),
		      'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
		      '&token='.Tools::getAdminTokenLite('AdminModules'),
		  ),
		  'back' => array(
		      'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
		      'desc' => $this->l('Back to list')
		  )
		);

		$helper->fields_value['MODE'] = Configuration::get('MODE');
		$helper->fields_value['URL_RETURN'] = Configuration::get('URL_RETURN');
		$helper->fields_value['URL_RETURN'] = Configuration::get('URL_RETURN');
		$helper->fields_value['ID_ACQUIRER'] = Configuration::get('ID_ACQUIRER');
		$helper->fields_value['ID_COMMERCE_SOLES'] = Configuration::get('ID_COMMERCE_SOLES');
		$helper->fields_value['ID_COMMERCE_DOLLARS'] = Configuration::get('ID_COMMERCE_DOLLARS');
		$helper->fields_value['ADVANCED_HIDDEN_HOOKPAYMENT'] = Configuration::get('ADVANCED_HIDDEN_HOOKPAYMENT');
		$helper->fields_value['ADVANCED_SHOW_ARRAYSEND'] = Configuration::get('ADVANCED_SHOW_ARRAYSEND');
		$helper->fields_value['ADVANCED_SHOW_ARRAYOUT'] = Configuration::get('ADVANCED_SHOW_ARRAYOUT');
		$helper->fields_value['ADVANCED_SKIP_PRECONFIRM'] = Configuration::get('ADVANCED_SKIP_PRECONFIRM');
		$helper->fields_value['ID_CUSTOMER_AUTHORIZED'] = Configuration::get('ID_CUSTOMER_AUTHORIZED');
		$helper->fields_value['VECTOR'] = Configuration::get('VECTOR');
		$helper->fields_value['KEY_COMMERCE_PUBLIC_CRYPTO'] = Configuration::get('KEY_COMMERCE_PUBLIC_CRYPTO');
		$helper->fields_value['KEY_COMMERCE_PUBLIC_SIGNATURE'] = Configuration::get('KEY_COMMERCE_PUBLIC_SIGNATURE');
		$helper->fields_value['KEY_COMMERCE_PRIVATE_CRYPTO'] = Configuration::get('KEY_COMMERCE_PRIVATE_CRYPTO');
		$helper->fields_value['KEY_COMMERCE_PRIVATE_SIGNATURE'] = Configuration::get('KEY_COMMERCE_PRIVATE_SIGNATURE');
		$helper->token_adminstores = Tools::getAdminTokenLite('AdminStores');

		return $helper->generateForm($fields_form);

	}


	public function getContent()
	{
		$output = null;
		if (Tools::isSubmit('submit'.$this->name))
		{
			$mode = strval(Tools::getValue('MODE'));
			$id_acquirer = strval(Tools::getValue('ID_ACQUIRER'));
			$id_commerce_soles = strval(Tools::getValue('ID_COMMERCE_SOLES'));
			$id_commerce_dollars = strval(Tools::getValue('ID_COMMERCE_DOLLARS'));
			$advanced_hidden_hookpayment = strval(Tools::getValue('ADVANCED_HIDDEN_HOOKPAYMENT'));
			$advanced_show_arraysend = strval(Tools::getValue('ADVANCED_SHOW_ARRAYSEND'));
			$advanced_show_arrayout = strval(Tools::getValue('ADVANCED_SHOW_ARRAYOUT'));
			$advanced_skip_preconfirm = strval(Tools::getValue('ADVANCED_SKIP_PRECONFIRM'));
			$vector = strval(Tools::getValue('VECTOR'));
			$id_customer_authorized = strval(Tools::getValue('ID_CUSTOMER_AUTHORIZED'));
			$url_return = $this->url_return;
			$key_commerce_public_crypto = strval(Tools::getValue('KEY_COMMERCE_PUBLIC_CRYPTO'));
			$key_commerce_public_signature = strval(Tools::getValue('KEY_COMMERCE_PUBLIC_SIGNATURE'));
			$key_commerce_private_crypto = strval(Tools::getValue('KEY_COMMERCE_PRIVATE_CRYPTO'));
			$key_commerce_private_signature = strval(Tools::getValue('KEY_COMMERCE_PRIVATE_SIGNATURE'));


			if (   !$id_acquirer || empty($id_acquirer)
				|| !$vector || empty($vector)
				|| !$key_commerce_public_crypto || empty($key_commerce_public_crypto)
				|| !$key_commerce_public_signature || empty($key_commerce_public_signature)
				|| !$key_commerce_private_crypto || empty($key_commerce_private_crypto)
				|| !$key_commerce_private_signature || empty($key_commerce_private_signature)
				)
			{
				Configuration::updateValue('ID_ACQUIRER',$id_acquirer);
				Configuration::updateValue('ID_COMMERCE_SOLES',$id_commerce_soles);
				Configuration::updateValue('ID_COMMERCE_DOLLARS',$id_commerce_soles);
				Configuration::updateValue('VECTOR',$vector);
				Configuration::updateValue('KEY_COMMERCE_PUBLIC_CRYPTO',$key_commerce_public_crypto);
				Configuration::updateValue('KEY_COMMERCE_PUBLIC_SIGNATURE',$key_commerce_public_signature);
				Configuration::updateValue('KEY_COMMERCE_PRIVATE_CRYPTO',$key_commerce_private_crypto);
				Configuration::updateValue('KEY_COMMERCE_PRIVATE_SIGNATURE',$key_commerce_private_signature);
				$output .= $this->displayError( $this->l('Complete all fields required'));
			}
			else
			{
				Configuration::updateValue('MODE',$mode);
				Configuration::updateValue('ID_ACQUIRER',$id_acquirer);
				Configuration::updateValue('ID_COMMERCE_SOLES',$id_commerce_soles);
				Configuration::updateValue('ID_COMMERCE_DOLLARS',$id_commerce_dollars);
				Configuration::updateValue('ADVANCED_HIDDEN_HOOKPAYMENT',$advanced_hidden_hookpayment);
				Configuration::updateValue('ADVANCED_SHOW_ARRAYSEND',$advanced_show_arraysend);
				Configuration::updateValue('ADVANCED_SHOW_ARRAYOUT',$advanced_show_arrayout);
				Configuration::updateValue('ADVANCED_SKIP_PRECONFIRM',$advanced_skip_preconfirm);
				Configuration::updateValue('ID_CUSTOMER_AUTHORIZED',$id_customer_authorized);
				Configuration::updateValue('VECTOR',$vector);
				Configuration::updateValue('URL_RETURN',$url_return);
				Configuration::updateValue('KEY_COMMERCE_PUBLIC_CRYPTO',$key_commerce_public_crypto);
				Configuration::updateValue('KEY_COMMERCE_PUBLIC_SIGNATURE',$key_commerce_public_signature);
				Configuration::updateValue('KEY_COMMERCE_PRIVATE_CRYPTO',$key_commerce_private_crypto);
				Configuration::updateValue('KEY_COMMERCE_PRIVATE_SIGNATURE',$key_commerce_private_signature);

				$output .= $this->displayConfirmation($this->l('Settings updated'));
			}
		}
		return $output.$this->displayForm();
	}

	private function clearPrice($price, $sign)
	{
		$makrs = array(',',' ','.'); // Only clear [comma] and [spaces] not [dots]
		$unmarked = str_replace($makrs,"", $price);
		$unmarked_unsign =	str_replace($sign,"",$unmarked);

		return $unmarked_unsign;
	}

	private function clearText($string)
	{
		$b = array("á","é","í","ó","ú","ä","ë","ï","ö","ü","à","è","ì","ò","ù","ñ","Ñ"," ",",",".",";",":","¡","!","¿","?",'"',"(",")","#","'","-","+","-","_");
		$c = array("a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","n",""," ","","","","","","","","",'',"","","","","","","","");
		$string = str_replace($b,$c,$string);
		return $string;
	}

	public function insertTransactionDB($purchaseOperationNumber, $purchaseAmount, $purchaseCurrencyCode, $language, $billingFirstName, $billingLastName, $billingEMail, $billingAddress, $billingZIP, $billingCity, $billingState, $billingCountry, $billingPhone,$status)
	{
		$sql ="INSERT INTO `"._DB_PREFIX_."etech_alignet` (
																`purchaseOperationNumber`,
																`purchaseAmount`,
																`purchaseCurrencyCode`,
																`language`,
																`billingFirstName`,
																`billingLastName`,
																`billingEMail`,
																`billingAddress`,
																`billingZIP`,
																`billingCity`,
																`billingState`,
																`billingCountry`,
																`billingPhone`,
																`datePayment`,
																`status`
															)
															VALUES (
																'".$purchaseOperationNumber."',
																'".$purchaseAmount."',
																'".$purchaseCurrencyCode."',
																'".$language."',
																'".$billingFirstName."',
																'".$billingLastName."',
																'".$billingEMail."',
																'".$billingAddress."',
																'".$billingZIP."',
																'".$billingCity."',
																'".$billingState."',
																'".$billingCountry."',
																'".$billingPhone."',
																'".date("Y-m-d H:i:s")."',
																'".$status."'
															)";
		$result = Db::getInstance()->execute($sql);

		if (!$result) return false;
			return $result;
	}



	public function update_StatusTransactionDB($purchaseOperationNumber,$status)
	{
		$sql = "UPDATE `"._DB_PREFIX_."etech_alignet` SET `datePayment`='".date("Y-m-d H:i:s")."', `status`='".$status."' WHERE `purchaseOperationNumber`='".$purchaseOperationNumber."'";
		return Db::getInstance()->execute($sql);
	}


	public function hookPayment($params)
	{
		if (!$this->active)
			return;
		if (!$this->checkCurrency($params['cart']))
			return;

		$this->defineVars();

		$this->smarty->assign(array(
			'this_path_img' => Tools::getShopDomainSsl(true, true)._MODULE_DIR_.$this->dir_img,
			'mode_alignet' => MODE_ALIGNET,
			'id_customer' =>  $this->context->cookie->id_customer,
			'id_authorized' => Configuration::get('ID_CUSTOMER_AUTHORIZED'),
			'advanced_skip_preconfirm' => Configuration::get('ADVANCED_SKIP_PRECONFIRM'),
			'advanced_hidden_hookpayment' => Configuration::get('ADVANCED_HIDDEN_HOOKPAYMENT'),
		));
		return $this->display(__FILE__, 'payment.tpl');
	}

	public function redondear_dos_decimal($valor) {
	   $float_redondeado=round($valor * 100) / 100;
	   return $float_redondeado;
	}


	public function hookPaymentReturn($params)
	{
		if (!$this->active || !$this->defineVars())
			return;


		require_once(dirname(__FILE__).'/lib/vpos_plugin.php');

		$order   = new Order($params['objOrder']->id);
		$address = new Address($order->id_address_delivery);
		$state	 = new State($address->id_state);


		switch ($this->context->currency->iso_code_num) {
			case '604':
				if (Configuration::get('ID_COMMERCE_SOLES') != '')
					$commerceId = Configuration::get('ID_COMMERCE_SOLES');
				else { $commerceId = 'null'; }
				break;

			case '840':
				if(Configuration::get('ID_COMMERCE_DOLLARS') != '')
					$commerceId = Configuration::get('ID_COMMERCE_DOLLARS');
				else { $commerceId = 'null';}
				break;

			default:
				$commerceId = 'null';
				break;
		}


		$total_price = $params['total_to_pay'];
		$sign_price = $params['currencyObj']->sign;


		$purchaseOperationNumber = $params['objOrder']->id;
		$purchaseAmount = $this->clearPrice($total_price, $sign_price);
		$purchaseCurrencyCode = $this->context->currency->iso_code_num;

		if($purchaseCurrencyCode == '840' ) $purchaseAmount_mastercard = $purchaseAmount;
		else {
			$mastercard_total1 = $total_price/2.65;
			$purchaseAmount_mastercard = $this->clearPrice($this->redondear_dos_decimal($mastercard_total1));
		}

		$commerceMallId = false;
		$language = ($this->context->language->iso_code == 'es') ? 'SP' : 'EN';;
		$tipAmount = 0;
		$terminalCode = false;
		$billingFirstName = $this->clearText($this->context->cookie->customer_firstname);
		$billingLastName = $this->clearText($this->context->cookie->customer_lastname);
		$billingEMail = $this->context->cookie->email;
		$billingAddress = $this->clearText($address->address1);
		$billingZIP = ($address->postcode != '') ? $address->postcode : '51';
		$billingCity =  $this->clearText($address->city);
		$billingState =  $this->clearText($state->iso_code);
		$billingCountry =  $this->clearText($this->context->country->iso_code);
		$billingPhone = $address->phone_mobile;


		$array_send['acquirerId']=Configuration::get('ID_ACQUIRER');
		$array_send['commerceId']=$commerceId;
		$array_send['purchaseOperationNumber'] = substr($purchaseOperationNumber,0,5);	// 5 characters [number]
		$array_send['purchaseAmount'] = substr($purchaseAmount,0,12);	 // 12 characters [numbers: 100.30 -> 10030] not send comma
		$array_send['purchaseCurrencyCode'] = $purchaseCurrencyCode;	 // code number ISO [840,604,...]
		//$array_send['commerceMallId'] = substr($commerceMallId,0,12);	 // 12 characters
		$array_send['language'] = substr($language,0,2);				 //  2 characters [letters: SP,EN,...]
		//$array_send['tipAmount'] = substr($tipAmount,0,12);			 // 12 characters [numbers: 100.30 -> 10030] not send comma or dot
		//$array_send['terminalCode'] = substr($terminalCode,0,12);		 // 12 characters [numbers, letters]
		$array_send['billingFirstName'] = substr($billingFirstName,0,30);// 30 characters [letters, numbers, spaces]
		$array_send['billingLastName'] 	= substr($billingLastName,0,30); // 30 characters [letters, numbers, spaces]
		$array_send['billingEMail'] 	= substr($billingEMail,0,50);	 // 50 characters
		$array_send['billingAddress'] 	= substr($billingAddress,0,50);	 // 50 characters [letters, numbers, spaces]
		$array_send['billingZIP'] 		= substr($billingZIP,0,10);		 // 10 characters [letters, numbers, spaces]
		$array_send['billingCity'] 		= substr($billingCity,0,50);	 // 50 characters [letters]
		$array_send['billingState'] 	= substr($billingState,0,2);	 //  2 Characters [letters]
		$array_send['billingCountry'] 	= substr($billingCountry,0,2);	 //  2 Characters [letters]
		$array_send['billingPhone'] 	= substr($billingPhone,0,15);	 // 15 characters [numbers]
		$array_send['reserved1'] 		= '840';	 	 				 // Siempre tiene que ser 840, es para Mastercard
		$array_send['reserved2'] 		= $purchaseAmount_mastercard;	 // Monto convertido a Dolares, es para Mastercard
		$array_send['reserved3'] 		= Configuration::get('ID_COMMERCE_DOLLARS'); // Siempre tiene que ser el ID_Commerce, es para Mastercard.
		$array_send['reserved4'] 		= $purchaseOperationNumber;
		$array_send['reserved5'] 		= $params['objOrder']->reference;

		$arrayOut['XMLREQ']="";
		$arrayOut['DIGITALSIGN']="";
		$arrayOut['SESSIONKEY']="";


		$key_alignet_public_crypto = file_get_contents(PATH_KEY_ALIGNET_PUBLIC_CRYPTO);
		$key_commerce_private_signature =Configuration::get('KEY_COMMERCE_PRIVATE_SIGNATURE');

		$vector =Configuration::get('VECTOR');


		if(VPOSSend($array_send,$arrayOut,$key_alignet_public_crypto,$key_commerce_private_signature,$vector))


		$xmlreq = $arrayOut['XMLREQ'];
		$digitalsign = $arrayOut['DIGITALSIGN'];
		$sessionkey = $arrayOut['SESSIONKEY'];


		$status = $params['objOrder']->getCurrentState();
		if ($status == Configuration::get('PS_OS_ALIGNET') || $status == Configuration::get('PS_OS_OUTOFSTOCK'))
		{
			$this->smarty->assign(array(
				'status'		=> 'ok',
				'this_path_ssl_img' => Tools::getShopDomainSsl(true, true)._MODULE_DIR_.$this->dir_img,
				'acquirerId'	=> $array_send['acquirerId'],
				'commerceId'	=> $array_send['commerceId'],
				'url_vpos'		=> URL_VPOS,
				'xmlreq' 		=> $xmlreq,
				'digitalsign' 	=> $digitalsign,
				'sessionkey' 	=> $sessionkey,
				'purchaseOperationNumber' => $array_send['purchaseOperationNumber'],
				'purchaseAmount' => $array_send['purchaseAmount'],
				'purchaseCurrencyCode' => $array_send['purchaseCurrencyCode'],
				/*'commerceMallId' => $array_send['commerceMallId'],*/
				'language' => $array_send['language'],
				/*'tipAmount' => $array_send['tipAmount'],
				'terminalCode' => $array_send['terminalCode'],*/
				'billingFirstName' => $array_send['billingFirstName'],
				'billingLastName' => $array_send['billingLastName'],
				'billingEMail' => $array_send['billingEMail'],
				'billingAddress' => $array_send['billingAddress'],
				'billingZIP' => $array_send['billingZIP'],
				'billingCity' => $array_send['billingCity'],
				'billingState' => $array_send['billingState'],
				'billingCountry' => $array_send['billingCountry'],
				'billingPhone' => $array_send['billingPhone'],
				'reserved1' => $array_send['reserved1'],
				'reserved2' => $array_send['reserved2'],
				'reserved3' => $array_send['reserved3'],
				'reserved4' => $array_send['reserved4'],
				'reserved5' => $array_send['reserved5'],
				'id_customer' =>  $this->context->cookie->id_customer,
				'mode_alignet' => MODE_ALIGNET,
				'advanced_show_arraysend' => Configuration::get('ADVANCED_SHOW_ARRAYSEND')
			));

			if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
			{
				$this->smarty->assign('reference', $params['objOrder']->reference);
				$this->insertTransactionDB($purchaseOperationNumber, $total_price, $purchaseCurrencyCode, $language, $billingFirstName, $billingLastName, $billingEMail, $billingAddress, $billingZIP, $billingCity, $billingState, $billingCountry, $billingPhone, '666');
			}

			else{ $this->smarty->assign('status', 'failed');}
		}

		return $this->display(__FILE__, 'payment_creditcard.tpl');
	}


	public function hookDisplayHeader()
	{
		$this->context->controller->addCSS($this->_path.'views/css/etalignet.css', 'all');
	}


	public function hookFooter($params)
	{
		global $smarty;
		$smarty->assign(array(
			'this_path' => $this->_path,
			'this_path_ssl' => Tools::getShopDomainSsl(true, true)._MODULE_DIR_.$this->name."/",
			'this_path_ssl_img' => Tools::getShopDomainSsl(true, true)._MODULE_DIR_.$this->dir_img
		));
		return $this->display(__FILE__, 'icons_footer.tpl');
	}

	public function checkCurrency($cart)
	{
		$currency_order = new Currency($cart->id_currency);
		$currencies_module = $this->getCurrency($cart->id_currency);

		if (is_array($currencies_module))
			foreach ($currencies_module as $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
		return false;
	}
}