<?php

/**
 * @since 1.5.0
 */
class etalignetPayment_executionModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		//$this->display_column_left = false;
		parent::initContent();

		$cart = $this->context->cart;
		if (!$this->module->checkCurrency($cart))
			Tools::redirect('index.php?controller=order');

		$iso_code_num = $this->context->currency->iso_code_num;

		switch ($iso_code_num) {
			case '604':
				if (Configuration::get('ID_COMMERCE_SOLES') != '')
				{
					$commerceId = Configuration::get('ID_COMMERCE_SOLES');
				}
				else {
					$commerceId = 'null';
				}
				break;

			case '840':
				if(Configuration::get('ID_COMMERCE_DOLLARS') != '')
				{
					$commerceId = Configuration::get('ID_COMMERCE_DOLLARS');
				}
				else {
					$commerceId = 'null';
				}
				break;

			default:
				$commerceId = 'null';
				break;
		}

		$address = new Address($cart->id_address_delivery);

		$this->context->smarty->assign(array(
			'nbProducts' 	=> $cart->nbProducts(),
			'cust_currency' => $cart->id_currency,
			'currencies' 	=> $this->module->getCurrency((int)$cart->id_currency),
			'total' 		=> $cart->getOrderTotal(true, Cart::BOTH),
			'this_path'     => $this->module->getPathUri(),
			'this_path_ssl' => Tools::getShopDomainSsl(true, true)._MODULE_DIR_.$this->module->name.'/',
			'billingFirstName'=> $this->context->cookie->customer_firstname,
			'billingLastName' => $this->context->cookie->customer_lastname,
			'billingEMail' 	=> $this->context->cookie->email,
			'billingAddress'=> $address->address1,
			'billingCity' 	=> $address->city,
			'billingCountry'=> $this->context->country->iso_code,
			'billingPhone' 	=> $address->phone_mobile,
			'commerceId' 	=> $commerceId
		));

		$this->setTemplate('payment_execution.tpl');
	}






}