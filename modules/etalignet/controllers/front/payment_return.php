<?php

/**
 * @since 1.5.0
 */
class etalignetPayment_ReturnModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function initContent()
	{
		parent::initContent();

		require_once(dirname(__FILE__).'/../../lib/vpos_plugin.php');

		$etalignet = new EtAlignet();
		$etalignet->defineVars();

		// Recibo datos del V-POS de Alignet por $_POST
		$arrayIn['IDACQUIRER'] = $_POST['IDACQUIRER'];
		$arrayIn['IDCOMMERCE'] = $_POST['IDCOMMERCE'];
		$arrayIn['XMLRES'] = $_POST['XMLRES'];
		$arrayIn['DIGITALSIGN'] = $_POST['DIGITALSIGN'];
		$arrayIn['SESSIONKEY'] = $_POST['SESSIONKEY'];

		$arrayOut = '';

		$key_alignet_public_signature = file_get_contents(PATH_KEY_ALIGNET_PUBLIC_SIGNATURE);
		$key_commerce_private_crypto = Configuration::get('KEY_COMMERCE_PRIVATE_CRYPTO');
		$vector = Configuration::get('VECTOR');


		if(VPOSResponse($arrayIn,$arrayOut,$key_alignet_public_signature,$key_commerce_private_crypto,$vector)){

			//echo ('OK');
			$authorizationResult = $arrayOut['authorizationResult'];
			$authorizationCode = $arrayOut['authorizationCode'];
			$errorCode = $arrayOut['errorCode'];
			$errorMessage = $arrayOut['errorMessage'];
			$purchaseOperationNumber = $arrayOut['purchaseOperationNumber'];
			$billingEMail = $arrayOut['billingEMail'];
			$billingFirstName = $arrayOut['billingFirstName'];
			$billingLastName =	$arrayOut['billingLastName'];
			$billingAddress = $arrayOut["billingAddress"];
			$billingPhone =	$arrayOut["billingPhone"];
			$billingCity = $arrayOut["billingCity"];
			$billingState = $arrayOut["billingState"];
			$billingCountry = $arrayOut["billingCountry"];
			$billingZIP = $arrayOut["billingZIP"];
			$reserved1 = $arrayOut['reserved1'];
			$reserved2 = $arrayOut['reserved2'];
			$reserved3 = $arrayOut['reserved3'];
			$_purchaseOperationNumber = $arrayOut['reserved4'];
			$reference = $arrayOut['reserved5'];
		}
		else {echo "ERROR :: Plugin de Alignet no pudo reconstrir datos, revizar que las llaves estén ingresadas correctamanete <br><br>";}

		//$errorCode = 0;

		switch($errorCode){

			case 2501:
				//echo "Error codigo: ".$errorCode."<br><br>".$errorMessage." <br> ";
			 	$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'2501');
				$errorMessage_es = $errorMessage;
				$errorTransaction = 'true';
				break;

			case 2413:
				//echo "Error codigo: ".$errorCode."<br><br>".$errorMessage;
			 	$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'2413');
				$errorMessage_es = $errorMessage;
				$errorTransaction = 'true';
				break;

			case 2400:
				//echo "Error codigo: ".$errorCode."<br><br>".$errorMessage."<br> Transacción se rechaza y no se autorizó debido a las normas de autorización previa";
			 	$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'2400');
				$errorMessage_es = "No se Cumplieron las normas de autorización previa.";
				$errorTransaction = 'true';
				break;

			case 2300:
				//echo "Carrito Abandonado codigo: ".$errorCode."<br><br>".$errorMessage;
				$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'2300');
				$errorMessage_es = "Abandono del portal de Alignet.";
				$errorTransaction = 'true';
				break;

			case 2202:
				//echo "Numero de pedido repetido codigo: ".$errorCode."<br><br>".$errorMessage."<br> Número de orden duplicado";
				$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'2202');
				$errorMessage_es = "El Número de pedido ya fue utilizado.";
				$errorTransaction = 'true';
				break;

			case 2200:
				//echo "Numero de pedido repetido codigo: ".$errorCode."<br><br>".$errorMessage."<br> Número de orden duplicado";
				$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'2202');
				$errorMessage_es = "V-POS necesita más datos de parte del Plug-In.";
				$errorTransaction = 'true';
				break;

			case 57:
				//echo "Sin Fondos, código: ".$errorCode."<br><br>".$errorMessage."<br> Transacción no permitido titular";
				$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'57');
				$errorMessage_es = "El Número de pedido ya fue utilizado.";
				$errorTransaction = 'true';
				break;


			case 51:
				//echo "Sin Fondos, código: ".$errorCode."<br><br>".$errorMessage;
				$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'51');
				$errorMessage_es = "Su tarjeta no tiene fondos.";
				$errorTransaction = 'true';
				break;

			case 0:
				//echo "OPERACION EXITOSA, código: ".$errorCode."<br><br>".$errorMessage;
		        $history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_PAYMENT'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'0');
				$errorMessage_es = "";
				$errorTransaction = 'false';
				break;

			default:
				//echo "Operación denegada <br>";
				$history = new OrderHistory();
		        $history->id_order = (int)($_purchaseOperationNumber);
		        $history->changeIdOrderState(Configuration::get('PS_OS_CANCELED'),(int)($_purchaseOperationNumber));
		        @$history->addWithemail();
				$etalignet->update_StatusTransactionDB($_purchaseOperationNumber,'2501');
				$errorMessage_es = $errorMessage;
				$errorTransaction = 'true';
				break;
		}


		$this->context->smarty->assign(array(
			'errorCode'=> $errorCode,
			'IDACQUIRER' => $arrayIn['IDACQUIRER'],
			'IDCOMMERCE' => $arrayIn['IDCOMMERCE'],
			'XMLRES' => $arrayIn['XMLRES'],
			'DIGITALSIGN' => $arrayIn['DIGITALSIGN'],
			'SESSIONKEY' => $arrayIn['SESSIONKEY'],
			'PATH_KEY_ALIGNET_PUBLIC_SIGNATURE' => $key_alignet_public_signature,
 			'KEY_COMMERCE_PRIVATE_CRYPTO' => $key_commerce_private_crypto,
			'vector' => $vector,
			'authorizationResult' => $authorizationResult,
			'authorizationCode' => $authorizationCode,
			'errorMessage' => $errorMessage,
			'purchaseOperationNumber' => $purchaseOperationNumber,
			'billingEMail' => $billingEMail,
			'billingFirstName' => $billingFirstName,
			'billingLastName' => $billingLastName,
			'billingAddress' => $billingAddress,
			'billingPhone' => $billingPhone,
			'billingCity' => $billingCity,
			'billingState' => $billingState,
			'billingCountry' => $billingCountry,
			'billingZIP' => $billingZIP,
			'reference' => $reference,
			'_purchaseOperationNumber' => $_purchaseOperationNumber,
			'this_path_img' => Tools::getShopDomainSsl(true, true)._MODULE_DIR_."/etalignet/views/img/",
			'url_store' => Tools::getShopDomainSsl(true, true),
			'errorMessage_es' => $errorMessage_es,
			'errorTransaction' => $errorTransaction,
			'advanced_show_arrayout' => Configuration::get('ADVANCED_SHOW_ARRAYOUT')

		));

		$this->setTemplate('payment_return.tpl');
	}


}