{if $advanced_hidden_hookpayment}

	{if $id_customer eq $id_authorized}

		<p class="payment_module">
			<a href="{if $advanced_skip_preconfirm}{$link->getModuleLink('etalignet', 'validation')}{else}{$link->getModuleLink('etalignet', 'payment_execution')}{/if}" title="{l s='Pay by Credit Card' mod='etalignet'}">
				<img src="{$this_path_img}logo-payment.jpg" alt="{l s='Pay by Credit Card' mod='etalignet'}" height="49"/>
				{l s='make payment with visa, mastercard and american express' mod='etalignet'}
			</a>
			<a id="pay_now" style="float: right;" class="button" href="{if $advanced_skip_preconfirm}{$link->getModuleLink('etalignet', 'validation')}{else}{$link->getModuleLink('etalignet', 'payment_execution')}{/if}" >{l s='Pagar Ahora' mod='etalignet'}</a>
		</p>
	{else}

	{/if}

{else}

	<p class="payment_module">
		<a href="{if $advanced_skip_preconfirm}{$link->getModuleLink('etalignet', 'validation')}{else}{$link->getModuleLink('etalignet', 'payment_execution')}{/if}" title="{l s='Pay by Credit Card' mod='etalignet'}">
			<img src="{$this_path_img}logo-payment.jpg" alt="{l s='Pay by Credit Card' mod='etalignet'}" height="49"/>
			{l s='make payment with visa, mastercard and american express' mod='etalignet'}
		</a>
		<a id="pay_now" style="float: right;" class="button" href="{if $advanced_skip_preconfirm}{$link->getModuleLink('etalignet', 'validation')}{else}{$link->getModuleLink('etalignet', 'payment_execution')}{/if}" >{l s='Pagar Ahora' mod='etalignet'}</a>
	</p>


{/if}