{if $status == 'ok'}
	{literal}
		<script language="JavaScript">

			$(document).ready(function(e) {
				{/literal}
					{if $advanced_show_arraysend}
					{else}
			   			enviarvpos();
			   		{/if}
			   	{literal}
			});

			function enviarvpos(){
				var divOverlay = document.getElementById( "overlayvpos" );
				var divImgLoad = document.getElementById( "imgloadvpos" );
				var divModal = document.getElementById( "modalvpos" );

				divOverlay.style.visibility= "visible";
				divImgLoad.style.visibility= "visible";
				divModal.style.visibility= "visible";
				document.params_form.target= "iframevpos";
				document.params_form.submit();
		 	}



		</script>
	{/literal}

<p style="display:{if $advanced_show_arraysend} block {else} none {/if}">

acquirerId = {$acquirerId} <br>
commerceId = {$commerceId} <br>
purchaseOperationNumber = {$purchaseOperationNumber} <br>
purchaseAmount = {$purchaseAmount} <br>
purchaseCurrencyCode = {$purchaseCurrencyCode} <br>
<!-- commerceMallId = {$commerceMallId} <br> -->
language = {$language} <br>
<!-- tipAmount = {$tipAmount} <br>
terminalCode = {$terminalCode} <br> -->
billingFirstName = {$billingFirstName} <br>
billingLastName = {$billingLastName} <br>
billingEMail = {$billingEMail} <br>
billingAddress = {$billingAddress} <br>
billingZIP = {$billingZIP} <br>
billingCity = {$billingCity} <br>
billingState = {$billingState} <br>
billingCountry = {$billingCountry} <br>
billingPhone = {$billingPhone} <br>
reserved1 = {$reserved1} <br>
reserved2 = {$reserved2} <br>
reserved3 = {$reserved3} <br>
reserved4 = {$reserved4} <br>
reserved5 = {$reserved5} <br>
id_customer = {$id_customer}<br>
mode_alignet = {$mode_alignet}<br>

<br><br>
<b>xmlreq</b> = {$xmlreq} <br><br>
<b>digitalsign</b> = {$digitalsign} <br><br>
<b>sessionkey</b> = {$sessionkey} <br><br>
</p>




 	<form name="params_form" id="params_form" method="post" action="{$url_vpos}" >
	 	<input type="hidden" name="IDACQUIRER" 	id="IDACQUIRER" value="{$acquirerId}">
		<input type="hidden" name="IDCOMMERCE" 	id="IDCOMMERCE" value="{$commerceId}">
		<input type="hidden" name="XMLREQ" 		id="XMLREQ" 	value="{$xmlreq}">
		<input type="hidden" name="DIGITALSIGN" id="SIGNATURE" 	value="{$digitalsign}">
		<input type="hidden" name="SESSIONKEY" 	id="SESSIONKEY" value="{$sessionkey}">
		<input type="submit" name="envio" 		id="envio" 		value="Enviar" style="display:{if $advanced_show_arraysend} block {else} none {/if}" />
	</form>

<div id ="overlayvpos" class ="overlayvpos" ></div >
<div id ="imgloadvpos" class ="imgloadvpos" >
	<img alt ="Cargando VPOS" src ="{$this_path_ssl_img}loading.gif" class ="imgloadingvpos" >
</div>
<div id ="modalvpos" class ="modalvpos" >
	<iframe name ="iframevpos" class ="iframevpos" scrolling ="no" frameborder ="0" style="width: 100%; height: 500px;" ></iframe>
</div>


{else}
	<p class="warning">
		{l s='We noticed a problem with your order. If you think this is an error, feel free to contact our' mod='etalignet'}
		<a href="{$link->getPageLink('contact', true)}">{l s='expert customer support team. ' mod='etalignet'}</a>.
	</p>
{/if}
 