
{capture name=path}{l s='Alignet payment.' mod='etalignet'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}


{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
	<p class="warning">{l s='Your shopping cart is empty.' mod='etalignet'}</p>
{else}

<h3>{l s='Alignet payment.' mod='etalignet'}</h3>

{if $commerceId eq 'null'}
	<p class="error"> {l s="This Currency it's no available with this payment, please select other curreny" mod='etalignet'} 

	{if $currencies|@count > 1}
		<select id="currency_payement" name="currency_payement" onchange="setCurrency($('#currency_payement').val());">
			{foreach from=$currencies item=currency}
				<option value="{$currency.id_currency}" {if $currency.id_currency == $cust_currency}selected="selected"{/if}>{$currency.name}</option>
			{/foreach}
		</select>
	{/if}
	</p>

{/if}


<form action="{$link->getModuleLink('etalignet', 'validation', [], true)}" method="post">
<p>
	<img src="{$this_path_ssl_img}alignet.png" alt="{l s='Bank wire' mod='etalignet'}" />
</p>

<p>
	<b> {$billingFirstName} {$billingLastName} </b>
</p>

<p>
{l s='You choose to pay by credit card, then presented is a summary of your order and your billing information:' mod='etalignet'}<br>
<br>
<span class="text">{l s='Firstname' mod='etalignet'}</span> : {$billingFirstName}<br>
<span class="text">{l s='Lastname' mod='etalignet'}</span>  : {$billingLastName}<br>
<span class="text">{l s='Cellphone' mod='etalignet'}</span> : {$billingPhone}<br>
<span class="text">{l s='Email' mod='etalignet'}</span>     : {$billingEMail}<br>
<span class="text">{l s='Address' mod='etalignet'}</span>  	: {$billingAddress}<br>
<span class="text">{l s='City' mod='etalignet'}</span>    	: {$billingCity}<br>
<span class="text">{l s='Country' mod='etalignet'}</span>   : {$billingCountry}<br>
<br>
{l s='Total Payment' mod='etalignet'}: <span id="amount" class="price">{displayPrice price=$total} {if $use_taxes == 1}	{l s='(tax incl.)' mod='etalignet'} {/if}</span><br>
<br>
<br>
<b>{l s='Please confirm your order by clicking "Place my order."' mod='etalignet'}.</b>
</p>
<p class="cart_navigation">
		<input type="submit" name="submit" value="{l s='I confirm my order' mod='etalignet'}" class="exclusive_large" />
		<a href="{$link->getPageLink('order', true, NULL, "step=3")}" class="button_large">{l s='Other payment methods' mod='etalignet'}</a>
	</p>
</form>
{/if}
