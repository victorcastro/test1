<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{etalignet}prestashop>etalignet_d002e23baee5d0d206b7c9b4d8e606c1'] = 'Alignet';
$_MODULE['<{etalignet}prestashop>etalignet_cbe0a99684b145e77f3e14174ac212e3'] = 'Estas Seguro que deseas eliminar todos los datos y transacciones?';
$_MODULE['<{etalignet}prestashop>etalignet_0431ae8fd5e01afde83ef613ef1550c5'] = 'Configuración del Comercio';
$_MODULE['<{etalignet}prestashop>etalignet_809c481ee9a1da6c07fea51e41122c24'] = 'ID Acquirer';
$_MODULE['<{etalignet}prestashop>etalignet_99c086c7e35c20dba2b5d67af752cd9c'] = 'ID Commerce :: Soles (S/.)';
$_MODULE['<{etalignet}prestashop>etalignet_d799977ca8cf5a10f0703ae154b2351f'] = 'ID Commerce :: Dolares ($)';
$_MODULE['<{etalignet}prestashop>etalignet_4994a8ffeba4ac3140beb89e8d41f174'] = 'Idioma';
$_MODULE['<{etalignet}prestashop>etalignet_cb5480c32e71778852b08ae1e8712775'] = 'Español';
$_MODULE['<{etalignet}prestashop>etalignet_78463a384a5aa4fad5fa73e2f506ecfc'] = 'Ingles';
$_MODULE['<{etalignet}prestashop>etalignet_a17c64cde0166859e5188bdd084c4aec'] = 'Modalidad de Alignet';
$_MODULE['<{etalignet}prestashop>etalignet_650be61892bf690026089544abbd9d26'] = 'Moldalidad';
$_MODULE['<{etalignet}prestashop>etalignet_fa6a5a3224d7da66d9e0bdec25f62cf0'] = 'Pruebas';
$_MODULE['<{etalignet}prestashop>etalignet_756d97bb256b8580d4d71ee0c547804e'] = 'Producción';
$_MODULE['<{etalignet}prestashop>etalignet_57dea6f5039281b7fee517fc43bf3110'] = 'Vector';
$_MODULE['<{etalignet}prestashop>etalignet_b27f3163b2717eb5b19f0330de0435e0'] = 'URL de Retorno Prestashop 1.5';
$_MODULE['<{etalignet}prestashop>etalignet_d698afd78b3365f8732f201041b25b5d'] = 'Llaves Públicas del Comercio';
$_MODULE['<{etalignet}prestashop>etalignet_c560f3f243515393e44d726a7a387b78'] = 'Llave Pública Encriptada';
$_MODULE['<{etalignet}prestashop>etalignet_eb0bf71541216bca97bb678b5bb35db2'] = 'Llave Pública Firma';
$_MODULE['<{etalignet}prestashop>etalignet_9ad95e0264260339f8d8cb8b9d0034f3'] = 'Llaves Privadas del Comercio';
$_MODULE['<{etalignet}prestashop>etalignet_11ee8dac1c923ab634678e2220bb9ee3'] = 'Llave Privada Encriptada';
$_MODULE['<{etalignet}prestashop>etalignet_a420f75e32ec60d6d5e2a96c8929e0f4'] = 'Llave Privada Firma';
$_MODULE['<{etalignet}prestashop>etalignet_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{etalignet}prestashop>etalignet_630f6dc397fe74e52d5189e2c80f282b'] = 'Volver al listado';
$_MODULE['<{etalignet}prestashop>etalignet_92c07f0f0d613e3a33d640b11a6c2d8f'] = 'Complete todos los campos';
$_MODULE['<{etalignet}prestashop>etalignet_c888438d14855d7d96a2724ee9c306bd'] = 'Configuración Actualizada';
$_MODULE['<{etalignet}prestashop>validation_e2b7dec8fa4b498156dfee6e4c84b156'] = 'Este método de pago no está disponible';
$_MODULE['<{etalignet}prestashop>payment_execution_128bf017df30271d2e4fe7e53830985a'] = 'Pago con Alignet';
$_MODULE['<{etalignet}prestashop>payment_execution_879f6b8877752685a966564d072f498f'] = 'Su Carrito de compras está vacío.';
$_MODULE['<{etalignet}prestashop>payment_execution_9bc382c7ff8c2187b9cba4535b4a3c05'] = 'Esta divisa no está disponible para este método de pago, por favor seleccione otra moneda';
$_MODULE['<{etalignet}prestashop>payment_execution_127469a6b4253ebb77adccc0dd48461e'] = 'Sr.';
$_MODULE['<{etalignet}prestashop>payment_execution_f89b9f476ba6394d28161f4a6663496f'] = 'Usted a elegido pagar con tarjeta de crédito, a continuación se presenta el resumen de su pedido y los datos de facturación:';
$_MODULE['<{etalignet}prestashop>payment_execution_04176f095283bc729f1e3926967e7034'] = 'Nombres';
$_MODULE['<{etalignet}prestashop>payment_execution_dff4bf10409100d989495c6d5486035e'] = 'Apellido';
$_MODULE['<{etalignet}prestashop>payment_execution_4e186c431f7c016c761c722debb9768e'] = 'Teléfono';
$_MODULE['<{etalignet}prestashop>payment_execution_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'Email';
$_MODULE['<{etalignet}prestashop>payment_execution_dd7bf230fde8d4836917806aff6a6b27'] = 'Dirección';
$_MODULE['<{etalignet}prestashop>payment_execution_57d056ed0984166336b7879c2af3657f'] = 'Ciudad';
$_MODULE['<{etalignet}prestashop>payment_execution_59716c97497eb9694541f7c3d37b1a4d'] = 'Pais';
$_MODULE['<{etalignet}prestashop>payment_execution_4935ede30a688b922153c6abc8d48e14'] = 'Pago Total';
$_MODULE['<{etalignet}prestashop>payment_execution_1f87346a16cf80c372065de3c54c86d9'] = '(Inc. Impuestos)';
$_MODULE['<{etalignet}prestashop>payment_execution_93c1f9dffc8c38b2c108d449a9181d92'] = 'Por favor para confirmar su pedido haga click en \"Confirmar Pedido\"';
$_MODULE['<{etalignet}prestashop>payment_execution_46b9e3665f187c739c55983f757ccda0'] = 'Yo confirmo mi pedido';
$_MODULE['<{etalignet}prestashop>payment_execution_569fd05bdafa1712c4f6be5b153b8418'] = 'Otros Métodos de Pago';
$_MODULE['<{etalignet}prestashop>payment_return_128bf017df30271d2e4fe7e53830985a'] = 'Pago con Alignet';
$_MODULE['<{etalignet}prestashop>icons_footer_f81c66b5f0d6a39e15b4048e171c8401'] = 'Tienda 100% asegurada con:';
$_MODULE['<{etalignet}prestashop>payment_e77bb171c7ae55bba8b2ed869fadfb19'] = 'Page con Tarjeta de crédito';
$_MODULE['<{etalignet}prestashop>payment_f4c2943241b359d9b31e2d32120e161c'] = 'Realice su pago con Visa, Mastercad y Amercican Express';
$_MODULE['<{etalignet}prestashop>payment_6dd8d0f132754af29b06918be6f4a736'] = 'Pagar Ahora';
$_MODULE['<{etalignet}prestashop>payment_creditcard_d15feee53d81ea16269e54d4784fa123'] = 'Nos dimos cuenta de un problema con su pedido. Si crees que esto es un error, por favor no dude en contactar con nuestro';
$_MODULE['<{etalignet}prestashop>payment_creditcard_dfe239de8c0b2453a8e8f7657a191d5d'] = 'equipo de atención al cliente.';
