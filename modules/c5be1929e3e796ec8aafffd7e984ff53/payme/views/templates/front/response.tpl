<!--
	@author    ALIGNET
	@copyright 2014 ALIGNET
-->
<link rel="stylesheet" href="{$css_dir}global.css" type="text/css" media="all">
<div class="box">
	<h1>{$resultadoOperacion|escape:'htmlall':'UTF-8'}</h1>
	<p class="cheque-indent">
		<strong class="dark">{l s='Datos de la compra' mod='payme'}</strong>
	</p>
{if $valid}
	<table style='border: 0;'>
		<tr align="left">
			<td style="width: 200px;">{l s='Resultado de la Operación' mod='payme'}</td>
			<td>{$resultadoOperacion|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Número de Operación' mod='payme'}</td>
			<td>{$numeroOperacion|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Fecha y Hora de la Operación' mod='payme'}</td>
			<td>{$fechaOperacion|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Monto' mod='payme'}</td>
			<td>{$monto|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Moneda' mod='payme'}</td>
			<td>{$moneda|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Marca de la Tarjeta' mod='payme'}</td>
			<td>{$marcaTarjeta|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Número de Tarjeta' mod='payme'}</td>
			<td>{$numeroTarjeta|escape:'htmlall':'UTF-8'}</td>
		</tr>
	</table>
{else}
	<table style='border: 0;'>
		<tr align="left">
			<td style="width: 200px;">{l s='Resultado de la Operación' mod='payme'}</td>
			<td>{$resultadoOperacion|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Número de Operación' mod='payme'}</td>
			<td>{$numeroOperacion|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Fecha y Hora de la Operación' mod='payme'}</td>
			<td>{$fechaOperacion|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Monto' mod='payme'}</td>
			<td>{$monto|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Moneda' mod='payme'}</td>
			<td>{$moneda|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Marca de la Tarjeta' mod='payme'}</td>
			<td>{$marcaTarjeta|escape:'htmlall':'UTF-8'}</td>
		</tr>
		<tr align="left">
			<td>{l s='Número de Tarjeta' mod='payme'}</td>
			<td>{$numeroTarjeta|escape:'htmlall':'UTF-8'}</td>
		</tr>
	</table>
{/if}
</div>