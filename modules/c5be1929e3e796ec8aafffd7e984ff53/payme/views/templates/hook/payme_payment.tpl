<!--
*
* 2007-2015 PrestaShop
* Módulos de Pagos Alignet para Prestashop
* @author Alignet S.A.C.
* @copyright  2015 Alignet
*
-->
<script type="text/javascript">
{literal}
$(document).ready(function() {
	$('.ajax_cart_block_remove_link').click(function() {
		updateCarrierSelectionAndGift(); 
	});
	$('.cart_quantity_button').click(function() {  
		updateCarrierSelectionAndGift();  
	});     
	$('.cart_quantity_delete').click(function() {    
		updateCarrierSelectionAndGift();     
	});     
	$('#cgv').click(function() {      
		updateCarrierSelectionAndGift();   
	});
	$('#submitAccount').click(function() {
		updateCarrierSelectionAndGift();
	});
	$('#SubmitLogin').click(function() {
		updateCarrierSelectionAndGift();
	});
	$('#submitAddDiscount').click(function() {
		updateCarrierSelectionAndGift();
	});
});
{/literal}
</script>
<div class="row">
	<div class="col-xs-12 col-md-6">
        <p class="payment_module">
            <a href="{$module_dir}validation.php" title="{l s='Pagar con Pay-me' mod='payme'}">
                <img src="{$module_dir}{$payme_img}" alt="{l s='Pagar con Pay-me' mod='payme'}" />
            </a>
        </p>
    </div>
</div>