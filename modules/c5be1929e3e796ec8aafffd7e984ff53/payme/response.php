<?php

/**
*
*  @author    ALIGNET
*  @copyright 2014 ALIGNET
*/

include('../../config/config.inc.php');
include('../../init.php');
include('payme.php');
include('../../header.php');

if(empty($_POST))
	header("Location: ../../index.php");
// die('Hay un problema con la respuesta VPOS. La comunicación no se hizo en el contexto adecuado.');

$payme = new Payme();

$idEntCommerce = Configuration::get('ALIGNET_IDENTCOMMERCE');
$keywallet = Configuration::get('ALIGNET_KEYWALLET');
$acquirerId = Configuration::get('ALIGNET_IDACQUIRER');
$idCommerce = Configuration::get('ALIGNET_IDCOMMERCE');
$key = Configuration::get('ALIGNET_KEY');
$mcc = Configuration::get('ALIGNET_MCC');

$authorizationResult = trim($_POST['authorizationResult']) == "" ? "-" : $_POST['authorizationResult'];
$authorizationCode = trim($_POST['authorizationCode']) == "" ? "-" : $_POST['authorizationCode'];
$errorCode = trim($_POST['errorCode']) == "" ? "-" : $_POST['errorCode'];
$errorMessage = trim($_POST['errorMessage']) == "" ? "-" : $_POST['errorMessage'];
$bin = trim($_POST['bin']) == "" ? "-" : $_POST['bin'];
$brand = trim($_POST['brand']) == "" ? "-" : $_POST['brand'];
$paymentReferenceCode = trim($_POST['paymentReferenceCode']) == "" ? "-" : $_POST['paymentReferenceCode'];
$reserved1 = trim($_POST['reserved1']) == "" ? "-" : $_POST['reserved1'];
$reserved3 = trim($_POST['reserved3']) == "" ? "-" : $_POST['reserved3'];
$reserved4 = trim($_POST['reserved4']) == "" ? "-" : $_POST['reserved4'];
$reserved10 = trim($_POST['reserved10']) == "" ? "-" : $_POST['reserved10'];

$fechaHora = date("d/m/Y H:i:s");

$purchaseOperationNumber = $_POST['purchaseOperationNumber'];
$purchaseOperationNumber = str_pad($purchaseOperationNumber, 5, "0", STR_PAD_LEFT);
$purchaseAmount = $_POST['purchaseAmount'];
$purchaseCurrencyCode = $_POST['purchaseCurrencyCode'];

if ($authorizationResult == '00') {
	$resultadoOperacion = $payme->l('Operación Autorizada.');
} elseif ($authorizationResult == '01'){
	$resultadoOperacion = $payme->l('Operación Denegada.');
} elseif ($authorizationResult == '05') {
	$resultadoOperacion = $payme->l('Operación Rechazada.');
} else {
	$resultadoOperacion = $payme->l('Operación Incompleta.');
}

$cart = new Cart((int)$reserved1);

if ($authorizationResult == '00') {
	if (!($cart->orderExists())) {
		$message = $payme->l('Número de Operación de Compra') . ": " . $purchaseOperationNumber . " | ";
		$message .= $payme->l('Resultado de la Operación') . ": " . $resultadoOperacion . " | ";
		$message .= $payme->l('Fecha y Hora de la Operación') . ": " . $fechaHora . " | ";
		$message .= $payme->l('Monto') . ": " . $reserved3 . " | ";
		$message .= $payme->l('Moneda') . ": " . $reserved4 . " | ";
		$message .= $payme->l('Marca de la Tarjeta') . ": " . $brand . " | ";
		$message .= $payme->l('Número de Tarjeta') . ": " . $paymentReferenceCode . "";

		$customer = new Customer((int)$cart->id_customer);
		Context::getContext()->customer = $customer;
		$payme->validateOrder((int)$cart->id, Configuration::get('PAYME_OS_PAID'), (float)$cart->getordertotal(true), $payme->displayName, $message, array('transaction_id' => $purchaseOperationNumber), (int)$cart->id_currency, false, $customer->secure_key);
	}

	Context::getContext()->smarty->assign(
		array(
			'resultadoOperacion' => $resultadoOperacion,
			'numeroOperacion' => $purchaseOperationNumber,
			'fechaOperacion' => $fechaHora,
			'monto' => $reserved3,
			'moneda' => $reserved4,
			'marcaTarjeta' => $brand,
			'numeroTarjeta' => $paymentReferenceCode,
			'valid' => true
		)
	);
} else {
	if (!($cart->orderExists())) {
		$message = $payme->l('Número de Operación de Compra') . ": " . $purchaseOperationNumber . " | ";
		$message .= $payme->l('Resultado de la Operación') . ": " . $resultadoOperacion . " | ";
		if ($authorizationResult != '00' && $authorizationResult != '01') {
			$message .= $payme->l('Detalle de la transacción') . ": " . $errorMessage . " | ";
		}
		$message .= $payme->l('Fecha y Hora de la Operación') . ": " . $fechaHora . " | ";
		$message .= $payme->l('Monto') . ": " . $reserved3 . " | ";
		$message .= $payme->l('Moneda') . ": " . $reserved4 . " | ";
		$message .= $payme->l('Marca de la Tarjeta') . ": " . $brand . " | ";
		$message .= $payme->l('Número de Tarjeta') . ": " . $paymentReferenceCode . "";
		$customer = new Customer((int)$cart->id_customer);
		Context::getContext()->customer = $customer;
		$payme->validateOrder((int)$cart->id, Configuration::get('PAYME_OS_FAILED'), (float)$cart->getordertotal(true), $payme->displayName, $message, array('transaction_id' => $purchaseOperationNumber), (int)$cart->id_currency, false, $customer->secure_key);
	}

	Context::getContext()->cart = new Cart();
	Context::getContext()->cart->id_currency = Context::getContext()->currency->id;
	Context::getContext()->cart->id_lang = Context::getContext()->language->id;;
	Context::getContext()->cart->save();

	Context::getContext()->cookie->id_cart = (int)Context::getContext()->cart->id;
	Context::getContext()->cookie->write();

	$newCart = Context::getContext()->cart;

	$_idProducts = explode(',', $reserved10);
	for ($i = 0; $i < count($_idProducts); $i++) {
		$newCart->updateQty(1, $_idProducts[$i]);
	}

	Context::getContext()->cookie->write();

	Context::getContext()->smarty->assign(
		array(
			'resultadoOperacion' => $resultadoOperacion,
			'numeroOperacion' => $purchaseOperationNumber,
			'fechaOperacion' => $fechaHora,
			'monto' => $reserved3,
			'moneda' => $reserved4,
			'marcaTarjeta' => $brand,
			'numeroTarjeta' => $paymentReferenceCode,
			'valid' => false
		)
	);
}

Context::getContext()->smarty->display(_PS_MODULE_DIR_ . 'payme/views/templates/front/response.tpl');

include('../../footer.php');

?>