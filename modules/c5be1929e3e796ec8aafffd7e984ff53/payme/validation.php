<?php

/**
*
*  @author    ALIGNET
*  @copyright 2014 ALIGNET
*/

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/payme.php');

$payme = new Payme();

$cart = Context::getContext()->cart;
$customer = Context::getContext()->customer;
$billing_address = new Address(Context::getContext()->cart->id_address_invoice);
$billing_address->country = new Country($billing_address->id_country);
$delivery_address = new Address(Context::getContext()->cart->id_address_delivery);
$delivery_address->country = new Country($delivery_address->id_country);
$currency = new Currency((int)$cart->id_currency);
$products = $cart->getProducts();

switch(Configuration::get('ALIGNET_URLTPV')){
	case 0:
		$urltpv = "https://integracion.alignetsac.com/VPOS2/faces/pages/startPayme.xhtml";
		break;
	case 1:
		$urltpv = "https://vpayment.verifika.com/VPOS2/faces/pages/startPayme.xhtml";
		break;
}

$idEntCommerce = Configuration::get('ALIGNET_IDENTCOMMERCE');
$keywallet = Configuration::get('ALIGNET_KEYWALLET');
$acquirerId = Configuration::get('ALIGNET_IDACQUIRER');
$idCommerce = Configuration::get('ALIGNET_IDCOMMERCE');
$key = Configuration::get('ALIGNET_KEY');
$mcc = Configuration::get('ALIGNET_MCC');

$countProducts = 0;
$descriptionProducts = '';
$Products = '';
$idProducts = '';
foreach ($products as $product) {
	$descriptionProducts .= $product['name'].',';
	$Products .= $product['name'].',';
	$idProducts .= $product['id_product'].',';
	$countProducts++;
}

$codCardHolderCommerce = $customer->id;
$purchaseCurrencyCode = $currency->iso_code_num;
$long = 0; 

$num_ope = $cart->id;
if ($acquirerId == 144 || $acquirerId == 29) {
	$long = 5;
} elseif ($acquirerId == 84 || $acquirerId == 10 || $acquirerId == 123 || $acquirerId == 23 || $acquirerId == 205 || $acquirerId == 35) {
	$long = 9;
}
$purchaseOperationNumber = str_pad($num_ope, $long, "0", STR_PAD_LEFT);
$purchaseAmount = floatval(str_replace('.','',number_format($cart->getOrderTotal(true, 3), 2, '.', '')));
$billingFirstName = $billing_address->firstname;
$billingLastName = $billing_address->lastname;
$billingEmail = $cookie->email;
$billingAddress = $billing_address->address1;
$billingZip = $billing_address->postcode;
$billingCity = $billing_address->city;
$billingState = $billing_address->city;
$billingCountry = $billing_address->country->iso_code;
$billingPhone = $billing_address->phone;
$shippingFirstName = $delivery_address->firstname;
$shippingLastName = $delivery_address->lastname;
$shippingEmail = $cookie->email;
$shippingAddress = $delivery_address->address1;
$shippingZIP = $delivery_address->postcode;
$shippingCity = $delivery_address->city;
$shippingState = $billing_address->city;
$shippingCountry = $delivery_address->country->iso_code;
$shippingPhone = $delivery_address->phone;
if ($countProducts == 1) {
	$descriptionProducts = substr(trim($descriptionProducts, ','), 0, 30);
} else {
	$descriptionProducts = "Varios ($countProducts Productos)";
}

// Datos reservados de Aligent
$reserved1 = $num_ope; // Número de pedido de operación
$reserved2 = "-"; // Número de orden genarado por Prestashop
$reserved3 = number_format($cart->getOrderTotal(true, 3), 2, '.', ''); // Monto en formato 0000.00
$reserved4 = $currency->name; // Moneda en formato texto
$reserved5 = _PS_VERSION_; // Versión del Prestashop del Comercio
$reserved6 = '2.0'; // Versión del módulo Payme
$reserved7 = phpversion(); // Versión del Lenguaje de Programación de Prestashop
$reserved8 = php_uname(); // Versión del Sistema Operativo del Servidor
$reserved9 = 'Comercio Prestashop';

$ps_language = new Language(intval($cookie->id_lang));
$shop_lang = $ps_language->iso_code;
switch ($shop_lang) {
	case 'es':
		$language = 'ES';
		break;
	case 'en':
		$language = 'EN';
		break;
	default:
		$language = 'ES';
}
$commerceAssociated = '';
if ($acquirerId == 144 || $acquirerId == 29) {
	switch ($purchaseCurrencyCode) {
		case 604:
			$commerceAssociated = 'MALL ALIGNET-PSP SOLES';
			break;
		case 840:
			$commerceAssociated = 'MALL ALIGNET-PSP DOLARES';
			break;
		default:
			$commerceAssociated = 'MALL ALIGNET-PSP';
			break;
	}
}

if (phpversion() >= 5.3) {
	$registerVerification = openssl_digest($idEntCommerce . $codCardHolderCommerce . $billingEmail . $keywallet, 'sha512');
	$purchaseVerification = openssl_digest($acquirerId . $idCommerce . $purchaseOperationNumber . $purchaseAmount . $purchaseCurrencyCode . $key, 'sha512');
} else {
	$registerVerification = hash('sha512', $idEntCommerce . $codCardHolderCommerce . $billingEmail . $keywallet);
	$purchaseVerification = hash('sha512', $acquirerId . $idCommerce . $purchaseOperationNumber . $purchaseAmount . $purchaseCurrencyCode . $key);
}

$_reserved1 = '';
$_reserved2 = '';
$_reserved3 = '';

$codAsoCardHolderWallet = '';

switch(Configuration::get('ALIGNET_URLTPV')){
	case 0:
		$wsdl = "https://integracion.alignetsac.com/WALLETWS/services/WalletCommerce?wsdl";
		break;
	case 1:
		$wsdl = "https://www.pay-me.pe/WALLETWS/services/WalletCommerce?wsdl";
		break;
}
$client = new SoapClient($wsdl);
$params = array(
    'idEntCommerce'=>(string)$idEntCommerce,
    'codCardHolderCommerce'=>(string)$codCardHolderCommerce,
    'names'=>$billingFirstName,
    'lastNames'=>$billingLastName,
    'mail'=>$billingEmail,
    'reserved1'=>$_reserved1,
    'reserved2'=>$_reserved2,
    'reserved3'=>$_reserved3,
    'registerVerification'=>$registerVerification
);
$result = $client->RegisterCardHolder($params);
$codAsoCardHolderWallet = $result->codAsoCardHolderWallet;

if(Configuration::get('ALIGNET_DEBUG') == '1') {
	echo "WALLET<br><br>";
	echo "idEntCommerce: " . (string)$idEntCommerce . "<br>";
    echo "codAsoCardHolderWallet: " . (string)$codCardHolderCommerce . "<br>";
    echo "names: " . $billingFirstName . "<br>";
    echo "lastNames: " . $billingLastName . "<br>";
    echo "mail: " . $billingEmail . "<br>";
	echo "<br>VPOS2<br>";
	echo "acquirerId: " . Tools::safeOutput($acquirerId) . "<br>";
	echo "idCommerce: " . Tools::safeOutput($idCommerce) . "<br>";
	echo "purchaseOperationNumber: " . Tools::safeOutput($purchaseOperationNumber) . "<br>";
	echo "purchaseAmount: " . Tools::safeOutput($purchaseAmount) . "<br>";
	echo "purchaseCurrencyCode: " . Tools::safeOutput($purchaseCurrencyCode) . "<br>";
	echo "language: " . Tools::safeOutput($language) . "<br>";
	echo "billingFirstName: " . Tools::safeOutput($billingFirstName) . "<br>";
	echo "billingLastName: " . Tools::safeOutput($billingLastName) . "<br>";
	echo "billingEmail: " . Tools::safeOutput($billingEmail) . "<br>";
	echo "billingAddress: " . Tools::safeOutput($billingAddress) . "<br>";
	echo "billingZIP: " . Tools::safeOutput($billingZip) . "<br>";
	echo "billingCity: " . Tools::safeOutput($billingCity) . "<br>";
	echo "billingState: " . Tools::safeOutput($billingState) . "<br>";
	echo "billingCountry: " . Tools::safeOutput($billingCountry) . "<br>";
	echo "billingPhone: " . Tools::safeOutput($billingPhone) . "<br>";
	echo "shippingFirstName: " . Tools::safeOutput($shippingFirstName) . "<br>";
	echo "shippingLastName: " . Tools::safeOutput($shippingLastName) . "<br>";
	echo "shippingEmail: " . Tools::safeOutput($shippingEmail) . "<br>";
	echo "shippingAddress: " . Tools::safeOutput($shippingAddress) . "<br>";
	echo "shippingZIP: " . Tools::safeOutput($shippingZIP) . "<br>";
	echo "shippingCity: " . Tools::safeOutput($shippingCity) . "<br>";
	echo "shippingState: " . Tools::safeOutput($shippingState) . "<br>";
	echo "shippingCountry: " . Tools::safeOutput($shippingCountry) . "<br>";
	echo "shippingPhone: " . Tools::safeOutput($shippingPhone) . "<br>";
	echo "descriptionProducts: " . Tools::safeOutput($descriptionProducts) . "<br>";
	if ($acquirerId == 144 || $acquirerId == 29){
		echo "MCC: " . Tools::safeOutput($mcc) . "<br>";
		echo "commerceAssociated: " . Tools::safeOutput($commerceAssociated) . "<br>";
	}
	echo "programmingLanguage: PHP<br>";
	echo "reserved1: " . Tools::safeOutput($reserved1) . "<br>";
	echo "reserved2: " . Tools::safeOutput($reserved2) . "<br>";
	echo "reserved3: " . Tools::safeOutput($reserved3) . "<br>";
	echo "reserved4: " . Tools::safeOutput($reserved4) . "<br>";
	echo "reserved5: " . Tools::safeOutput($reserved5) . "<br>";
	echo "reserved6: " . Tools::safeOutput($reserved6) . "<br>";
	echo "reserved7: " . Tools::safeOutput($reserved7) . "<br>";
	echo "reserved8: " . Tools::safeOutput($reserved8) . "<br>";
	echo "reserved9: " . Tools::safeOutput($reserved9) . "<br>";
	die();
}

?>
<center>
	<img src="<?php echo Tools::getShopDomain(true).__PS_BASE_URI__.'modules/payme/' ?>img/payme.png"/>
	</br>
	<?php echo $payme->l('Se le redireccionará a la pasarela VPOS de Alignet'); ?>
</center>
<form action="<?php echo $urltpv ?>" method="post" name="alignet_form" id="alignet_form" class="hidden"> <!--class="hidden"-->
	<input type="hidden" name="acquirerId" id="acquirerId" value="<?php echo Tools::safeOutput($acquirerId); ?>" />
	<input type="hidden" name="idCommerce" id="idCommerce" value="<?php echo Tools::safeOutput($idCommerce); ?>" />
	<input type="hidden" name="purchaseOperationNumber" id="purchaseOperationNumber" value="<?php echo Tools::safeOutput($purchaseOperationNumber); ?>" />
	<input type="hidden" name="purchaseAmount" id="purchaseAmount" value="<?php echo Tools::safeOutput($purchaseAmount); ?>" />
	<input type="hidden" name="purchaseCurrencyCode" id="purchaseCurrencyCode" value="<?php echo Tools::safeOutput($purchaseCurrencyCode); ?>" />
	<input type="hidden" name="language" id="language" value="<?php echo Tools::safeOutput($language); ?>" />
	<input type="hidden" name="billingFirstName" id="billingFirstName" value="<?php echo Tools::safeOutput($billingFirstName); ?>" />
	<input type="hidden" name="billingLastName" id="billingLastName" value="<?php echo Tools::safeOutput($billingLastName); ?>" />
	<input type="hidden" name="billingEmail" id="billingEmail" value="<?php echo Tools::safeOutput($billingEmail); ?>" />
	<input type="hidden" name="billingAddress" id="billingAddress" value="<?php echo Tools::safeOutput($billingAddress); ?>" />
	<input type="hidden" name="billingZip" id="billingZip" value="<?php echo Tools::safeOutput($billingZip); ?>" />
	<input type="hidden" name="billingCity" id="billingCity" value="<?php echo Tools::safeOutput($billingCity); ?>" />
	<input type="hidden" name="billingState" id="billingState" value="<?php echo Tools::safeOutput($billingState); ?>" />
	<input type="hidden" name="billingCountry" id="billingCountry" value="<?php echo Tools::safeOutput($billingCountry); ?>" />
	<input type="hidden" name="billingPhone" id="billingPhone" value="<?php echo Tools::safeOutput($billingPhone); ?>" />
	<input type="hidden" name="shippingFirstName" id="shippingFirstName" value="<?php echo Tools::safeOutput($shippingFirstName); ?>" />
	<input type="hidden" name="shippingLastName" id="shippingLastName" value="<?php echo Tools::safeOutput($shippingLastName); ?>" />
	<input type="hidden" name="shippingEmail" id="shippingEmail" value="<?php echo Tools::safeOutput($shippingEmail); ?>" />
	<input type="hidden" name="shippingAddress" id="shippingAddress" value="<?php echo Tools::safeOutput($shippingAddress); ?>" />
	<input type="hidden" name="shippingZIP" id="shippingZIP" value="<?php echo Tools::safeOutput($shippingZIP); ?>" />
	<input type="hidden" name="shippingCity" id="shippingCity" value="<?php echo Tools::safeOutput($shippingCity); ?>" />
	<input type="hidden" name="shippingState" id="shippingState" value="<?php echo Tools::safeOutput($shippingState); ?>" />
	<input type="hidden" name="shippingCountry" id="shippingCountry" value="<?php echo Tools::safeOutput($shippingCountry); ?>" />
	<input type="hidden" name="shippingPhone" id="shippingPhone" value="<?php echo Tools::safeOutput($shippingPhone); ?>" />
	<input type="hidden" name="userCommerce" id="userCommerce" value="<?php echo Tools::safeOutput($codCardHolderCommerce); ?>" />
	<input type="hidden" name="userCodePayme" id="userCodePayme" value="<?php echo Tools::safeOutput($codAsoCardHolderWallet); ?>" />
	<?php if ($acquirerId == 144 || $acquirerId == 29) { ?>
		<input type="hidden" name="mcc" id="mcc" value="<?php echo Tools::safeOutput($mcc); ?>" />
		<input type="hidden" name="commerceAssociated" id="commerceAssociated" value="<?php echo Tools::safeOutput($commerceAssociated); ?>" />
	<?php } ?>
	<input type="hidden" name="descriptionProducts" id="descriptionProducts" value="<?php echo Tools::safeOutput($descriptionProducts); ?>" />
	<input type="hidden" name="programmingLanguage" id="programmingLanguage" value="<?php echo Tools::safeOutput('PHP'); ?>" />
	<input type="hidden" name="reserved1" id="reserved1" value="<?php echo Tools::safeOutput($reserved1); ?>" />
	<input type="hidden" name="reserved2" id="reserved2" value="<?php echo Tools::safeOutput($reserved2); ?>" />
	<input type="hidden" name="reserved3" id="reserved3" value="<?php echo Tools::safeOutput($reserved3); ?>" />
	<input type="hidden" name="reserved4" id="reserved4" value="<?php echo Tools::safeOutput($reserved4); ?>" />
	<input type="hidden" name="reserved5" id="reserved5" value="<?php echo Tools::safeOutput($reserved5); ?>" />
	<input type="hidden" name="reserved6" id="reserved6" value="<?php echo Tools::safeOutput($reserved6); ?>" />
	<input type="hidden" name="reserved7" id="reserved7" value="<?php echo Tools::safeOutput($reserved7); ?>" />
	<input type="hidden" name="reserved8" id="reserved8" value="<?php echo Tools::safeOutput($reserved8); ?>" />
	<input type="hidden" name="reserved9" id="reserved9" value="<?php echo Tools::safeOutput($reserved9); ?>" />
	<input type="hidden" name="reserved10" id="reserved10" value="<?php echo Tools::safeOutput(trim($idProducts, ',')); ?>" />
	<input type="hidden" name="purchaseVerification" id="purchaseVerification" value="<?php echo Tools::safeOutput($purchaseVerification); ?>" />
</form>
<script type="text/javascript">
	window.onload = function() {
		document.alignet_form.submit();
	};
</script>