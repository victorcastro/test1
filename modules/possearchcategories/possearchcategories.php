<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
	//print_r(_PS_VERSION_);die;
class Possearchcategories extends Module
{
	private $spacer_size = '1';
	private $html = '';
	private $_html = '';
	public static $level = array( 
		1 => array('id' =>1 , 'name' => '2'),
        2 => array('id' =>2 , 'name' => '3'),
        3 => array('id' =>3 , 'name' => '4'),   
    );

	public function __construct()
	{
		$this->name = 'possearchcategories';
		$this->tab = 'Search and filter';
		$this->version = 1.6;
		$this->author = 'Posthemes';
		$this->need_instance = 0;
		$this->bootstrap =true ;
		parent::__construct();
		$this->displayName = $this->l('Quick search categories ');
		$this->description = $this->l('Adds a quick search field categories to your website.');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	}
	public function install()
	{	
		Configuration::updateValue('POSSEARCH_CATE', 1);
        Configuration::updateValue('POSSEARCH_LEVEL', 3);
        Configuration::updateValue('POSSEARCH_ITEMS', 6);

		return parent :: install()
			&& $this->registerHook('displayTop')
			&& $this->registerHook('header');
	}

	public function uninstall(){
		Configuration::deleteByName('POSSEARCH_CATE');
		Configuration::deleteByName('POSSEARCH_LEVEL');
		Configuration::deleteByName('POSSEARCH_ITEMS');
		return parent::uninstall();
	}

	public function getContent(){
		if(Tools::isSubmit('submitUpdate')){
			Configuration::UpdateValue('POSSEARCH_CATE', Tools::getValue('POSSEARCH_CATE'));
			Configuration::UpdateValue('POSSEARCH_LEVEL', Tools::getValue('POSSEARCH_LEVEL'));
			Configuration::UpdateValue('POSSEARCH_ITEMS', Tools::getValue('POSSEARCH_ITEMS'));
			$this->html = $this->displayConfirmation($this->l('Settings updated successfully.'));
		}
		$this->html .= $this->renderForm();
		return $this->html;
	}

	public function renderForm(){
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type'      => 'switch',
						'label'     => $this->l('Enable list categories'),
						'desc'      => $this->l('Would you like show  categories ?'),
						'name'      => 'POSSEARCH_CATE',
						'values'    => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),

							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),	
					array(
	                    'type' => 'select',
	                    'label' => $this->l('Category level'),
	                    'name' => 'POSSEARCH_LEVEL',
	                    'options' => array(
	                        'query' => self::$level,
	                        'id' => 'id',
	                        'name' => 'name',
	                    ),
	                    'validation' => 'isUnsignedInt',
	                    'desc' => $this->l('Level depth of category which you want to show. This field is valuable when "Enable list categories"')
	                ),					
					array(
							'type' => 'text',
							'label' => $this->l('Limited products'),
							'name' => 'POSSEARCH_ITEMS',
							'class' => 'fixed-width-sm',
							'desc' => $this->l('Number of products is shown when searching. Default is 6.')
					),

				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitUpdate';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'POSSEARCH_CATE' => Tools::getValue('POSSEARCH_CATE', Configuration::get('POSSEARCH_CATE')),
			'POSSEARCH_LEVEL' => Tools::getValue('POSSEARCH_LEVEL', Configuration::get('POSSEARCH_LEVEL')),
			'POSSEARCH_ITEMS' => Tools::getValue('POSSEARCH_ITEMS', Configuration::get('POSSEARCH_ITEMS')),
		);
	}
	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'possearch.css', 'all');
		$this->context->controller->addCSS(($this->_path).'bootstrap-select.css', 'all');
		$this->context->controller->addJS(($this->_path).'bootstrap-select.js', 'all');
		$this->context->controller->addJqueryPlugin('autocomplete');

		if (Configuration::get('PS_SEARCH_AJAX'))
			$this->context->controller->addJqueryPlugin('autocomplete');

		if (Configuration::get('PS_INSTANT_SEARCH'))
			$this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
		
		if (Configuration::get('PS_SEARCH_AJAX'))
		{
			Media::addJsDef(array('search_url' => $this->context->link->getPageLink('search', Tools::usingSecureMode())));
			$this->context->controller->addJS(($this->_path).'possearch.js');
		}
	}
	public function hookLeftColumn($params)
	{
		return $this->hookRightColumn($params);
	}

	public function hookRightColumn($params)
	{

		if (Tools::getValue('search_query') || !$this->isCached('possearch.tpl', $this->getCacheId()))
		{
			$this->calculHookCommon($params);
			$this->smarty->assign(array(
				'possearch_type' => 'block',
				'search_query' => (string)Tools::getValue('search_query')
				)
			);
		}
		Media::addJsDef(array('possearch_type' => 'block'));
		return $this->display(__FILE__, 'possearch.tpl', Tools::getValue('search_query') ? null : $this->getCacheId());
	}

	public function hookdisplayTop($params)
	{
		global $cookie ;
		$key = $this->getCacheId('blocksearch-top');
		$cate_on = (int)Configuration::get('POSSEARCH_CATE');
		$items = (int)Configuration::get('POSSEARCH_ITEMS');
		$categories_option = $this->getCategoryOption(1, (int)$cookie->id_lang, (int) Shop::getContextShopID());
		if (Tools::getValue('search_query') || !$this->isCached('possearch-top.tpl', $key))
		{
			$this->calculHookCommon($params);
			$this->smarty->assign(array(
				'possearch_type' => 'top',
				'cate_on' =>$cate_on,
				'categories_option'=>$categories_option,
				'search_query' => (string)Tools::getValue('search_query')
				)
			);
		}
		Media::addJsDef(array(
			'possearch_type' => 'top',
			'limited_items' => $items));
		return $this->display(__FILE__, 'possearch-top.tpl', Tools::getValue('search_query') ? null : $key);
	}
		

	private function getCategoryOption($id_category = 1, $id_lang = false, $id_shop = false, $recursive = true) {
		$server = $_SERVER['REQUEST_URI'];
		$level_depth = (int)Configuration::get('POSSEARCH_LEVEL') +1 ;
		$id_cate_current = 1;
		if( Dispatcher::getInstance()->getController() == 'search' ) {
			if(strpos($server,"poscats")) {
				$getstring= substr($server,strpos($server,"poscats"));
				preg_match('(\d+)', $getstring, $getnumber);
				if($getnumber) $id_cate_current = $getnumber[0];
			}
		} 

		$id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;
		$category = new Category((int)$id_category, (int)$id_lang, (int)$id_shop);
		if (is_null($category->id))
			return;
		if ($recursive)
		{
			$children = Category::getChildren((int)$id_category, (int)$id_lang, true, (int)$id_shop);
			if($category->level_depth <= '2'){
				$spacer='';
			}else{
				$spacer = str_repeat('-', $this->spacer_size * ((int)$category->level_depth - 1));
			}

		}
		$selected = 1;
		$shop = (object) Shop::getShop((int)$category->getShopID());
		if($category->name!='Root' && $category->name!='Home' && $category->level_depth <= $level_depth){
			$this->_html .= '<option value="'.(int)$category->id.'" '.(($id_cate_current - $category->id) ? '' : 'selected').'>'.(isset($spacer) ? $spacer : '').$category->name.' </option>';
		}
			if (isset($children) && count($children))
			foreach ($children as $child)
				$this->getCategoryOption((int)$child['id_category'], (int)$id_lang, (int)$child['id_shop']);
		return $this->_html;
	}


	private function calculHookCommon($params)
	{
		$this->smarty->assign(array(
			'ENT_QUOTES' =>		ENT_QUOTES,
			'search_ssl' =>		Tools::usingSecureMode(),
			'ajaxsearch' =>		Configuration::get('PS_SEARCH_AJAX'),
			'instantsearch' =>	Configuration::get('PS_INSTANT_SEARCH'),
			'self' =>			dirname(__FILE__),
		));
		return true;
	}
}

