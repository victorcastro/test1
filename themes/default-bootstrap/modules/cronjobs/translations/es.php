<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Descripción';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_0eff773cf33456a033e913f6ed18045c'] = 'Objetivo del enlace';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_f4f70727dc34561dfde1a3c529b6205c'] = 'Ajustes';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_9b6545e4cea9b4ad4979d41bb9170e2b'] = 'Avanzado';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_b55e509c697e4cca0e1d160a7806698f'] = 'Hora';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_03727ac48595a24daed975559c944a44'] = 'Día';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_7cbb885aa1164b390a0bc050a64e1812'] = 'Mes';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Activar';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_6e7b34fa59e1bd229b207892956dc41c'] = 'Nunca';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_c512b685438f41daa7386329a3b8f8d3'] = 'Diariamente';
$_MODULE['<{cronjobs}default-bootstrap>cronjobsforms_9030e39f00132d583da4122532e509e9'] = 'Mensualmente';
