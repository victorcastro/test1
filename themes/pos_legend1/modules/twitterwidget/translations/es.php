<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_73b72cf7bea3a427ed076fadfeee8ff2'] = 'Twitter Widget Free';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_71857962e58f15a2a1a031b68faa5ea6'] = 'Este módulo agrega un widget de Twitter para tu tienda.';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_f4d1ea475eaa85102e2b4e6d95da84bd'] = 'Configuración actualizado';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_c888438d14855d7d96a2724ee9c306bd'] = 'Configuración actualizado';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_f67bc99cb81f84cf5f22f323d0051583'] = 'Twitter Widget configuración';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_f9e4884c7654daa6581b42ed90aeaba4'] = 'columna de la izquierda';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_a6105c0a611b41b08f1209506350279e'] = 'sí';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_feb6cc332459769fe15570bf332a6b50'] = 'columna derecha';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_8cf04a9734132302f96da8e113e80ce5'] = 'Home';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_a676a52506a4b5016fdd0c844e60d8d4'] = 'Twitter Widget ancho';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_b9cfba5082d9cf8bcdb47427cdc68b6f'] = 'El valor mínimo del parámetro: 220';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_198fae7418302c5eb07213a463b4ce11'] = 'Twitter nombre';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_6aae0f6960b2cdbe3442626175fc2d71'] = 'Su nombre de cuenta en Twitter';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_4d2900f87d53445bad2db338bf31a6da'] = 'Twitter Widget de Identificación';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_20c92ed5f444f140c58879ba68488e30'] = 'El Twitter Widget Identificación';
$_MODULE['<{twitterwidget}pos_legend1>twitterwidget_9daf1fb753b42c3cdc8f1d01669cd6d8'] = 'Guardar configuración';
