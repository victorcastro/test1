Hola {firstname} {lastname},
 
En referencia a tu pedido #{id_order}:
 
El pago de tu pedido fue recibido CORRECTAMENTE .
 
Datos de {shop_name}:
Número de pedido: {id_order} 
Referencia del pedido:{order_name} 
Forma de Pago: {nombre_mediopago}
 
Puedes revisar tu pedido y descargar la factura aquí: "Historial de Pedidos" dentro de tu cuenta "Entrar en Mi Cuenta" de nuestro sitio web.
If you have guest account, you can follow your order at "Guest Tracking" section on our Website.
 
{shop_name}®
